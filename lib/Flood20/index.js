import {
  template,
  addEventListener,
  insert,
  memo,
  effect,
  setAttribute,
  style,
  delegateEvents,
} from "solid-js/web";

const _tmpl$ = template(
    `<svg data-carbon-icon="Flood20" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" preserveAspectRatio="xMidYMid meet"><path d="M29 28a2.8828 2.8828 0 01-1-.1816v-.0059a3.7662 3.7662 0 01-2.0532-2.1338A.971.971 0 0025 25a1.007 1.007 0 00-.9487.6836A3.4376 3.4376 0 0121 28a3.44 3.44 0 01-3.0532-2.3213A.9894.9894 0 0017 25a1.007 1.007 0 00-.9487.6836A3.4376 3.4376 0 0113 28a3.44 3.44 0 01-3.0532-2.3213 1 1 0 00-1.8955.0049A3.4376 3.4376 0 015 28H2v2H5a4.9316 4.9316 0 004-1.9873 5.0192 5.0192 0 008 0 5.0192 5.0192 0 008 0 5.1433 5.1433 0 002.1379 1.62A4.8374 4.8374 0 0029 30h1V28zM28 13.63L29.7573 15 31 13.4282 16.6123 2.2139a1.0094 1.0094 0 00-1.2427 0L1 13.4194l1.2427 1.5718L4 13.6211v5.1875a3.6907 3.6907 0 01-2 2.0039V22.896a4.9958 4.9958 0 003-1.8833 5.0192 5.0192 0 008 0 5.0192 5.0192 0 008 0A4.9316 4.9316 0 0025 23h5V21H28zm-6.0513 5.0532a1 1 0 00-1.8955-.0049A3.44 3.44 0 0117 21a3.4376 3.4376 0 01-3.0513-2.3164A1.007 1.007 0 0013 18a.9894.9894 0 00-.9468.6787A3.44 3.44 0 019 21a3.37 3.37 0 01-3.0021-2.19L6 12.0615l9.991-7.79L26 12.0718 26.0017 21H25A3.4376 3.4376 0 0121.9487 18.6836z"></path></svg>`,
    4
  ),
  _tmpl$2 = template(`<title></title>`, 2);

const Flood20 = (props) =>
  (() => {
    const _el$ = _tmpl$.cloneNode(true);
    _el$.firstChild;

    addEventListener(_el$, "keydown", props.onKeyDown, true);

    addEventListener(_el$, "keyup", props.onKeyUp, true);

    addEventListener(_el$, "mouseleave", props.onMouseLeave);

    addEventListener(_el$, "mouseenter", props.onMouseEnter);

    addEventListener(_el$, "mouseover", props.onMouseOver, true);

    addEventListener(_el$, "click", props.onClick, true);

    insert(
      _el$,
      (() => {
        const _c$ = memo(() => props.children, true);

        return () =>
          _c$() ||
          (props.title &&
            (() => {
              const _el$3 = _tmpl$2.cloneNode(true);

              insert(_el$3, () => props.title);

              return _el$3;
            })());
      })(),
      null
    );

    effect(
      (_p$) => {
        const _v$ = props.fill || "currentColor",
          _v$2 = props.width || "20",
          _v$3 = props.height || "20",
          _v$4 = props["aria-label"],
          _v$5 = props["aria-labelledby"],
          _v$6 = props["aria-label"] || props["aria-labelledby"] || props.title,
          _v$7 =
            props["aria-label"] || props["aria-labelledby"] || props.title
              ? "img"
              : undefined,
          _v$8 = props.tabindex === "0" ? true : props.focusable,
          _v$9 = props.tabindex,
          _v$10 = props.id,
          _v$11 = props.class,
          _v$12 = props.title,
          _v$13 = props.style,
          _v$14 = props.stroke;

        _v$ !== _p$._v$ && setAttribute(_el$, "fill", (_p$._v$ = _v$));
        _v$2 !== _p$._v$2 && setAttribute(_el$, "width", (_p$._v$2 = _v$2));
        _v$3 !== _p$._v$3 && setAttribute(_el$, "height", (_p$._v$3 = _v$3));
        _v$4 !== _p$._v$4 &&
          setAttribute(_el$, "aria-label", (_p$._v$4 = _v$4));
        _v$5 !== _p$._v$5 &&
          setAttribute(_el$, "aria-labelledby", (_p$._v$5 = _v$5));
        _v$6 !== _p$._v$6 &&
          setAttribute(_el$, "aria-hidden", (_p$._v$6 = _v$6));
        _v$7 !== _p$._v$7 && setAttribute(_el$, "role", (_p$._v$7 = _v$7));
        _v$8 !== _p$._v$8 && setAttribute(_el$, "focusable", (_p$._v$8 = _v$8));
        _v$9 !== _p$._v$9 && setAttribute(_el$, "tabindex", (_p$._v$9 = _v$9));
        _v$10 !== _p$._v$10 && setAttribute(_el$, "id", (_p$._v$10 = _v$10));
        _v$11 !== _p$._v$11 && setAttribute(_el$, "class", (_p$._v$11 = _v$11));
        _v$12 !== _p$._v$12 && setAttribute(_el$, "title", (_p$._v$12 = _v$12));
        _p$._v$13 = style(_el$, _v$13, _p$._v$13);
        _v$14 !== _p$._v$14 &&
          setAttribute(_el$, "stroke", (_p$._v$14 = _v$14));
        return _p$;
      },
      {
        _v$: undefined,
        _v$2: undefined,
        _v$3: undefined,
        _v$4: undefined,
        _v$5: undefined,
        _v$6: undefined,
        _v$7: undefined,
        _v$8: undefined,
        _v$9: undefined,
        _v$10: undefined,
        _v$11: undefined,
        _v$12: undefined,
        _v$13: undefined,
        _v$14: undefined,
      }
    );

    return _el$;
  })();

delegateEvents(["click", "mouseover", "keyup", "keydown"]);

export default Flood20;
export { Flood20 };
