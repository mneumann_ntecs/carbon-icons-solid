import { CarbonIconComponent } from "../types";
export const ChartErrorBar16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="ChartErrorBar16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M22,28V12H18V8h2V6H14V8h2v4H12V28H4V2H2V28a2,2,0,0,0,2,2H30V28ZM14,14h2v4H14Zm6,14H14V20h6Zm0-10H18V14h2Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default ChartErrorBar16;
