import { CarbonIconComponent } from "../types";
export const Cda32: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="Cda32"
    fill={props.fill || "currentColor"}
    width={props.width || "32"}
    height={props.height || "32"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M28 9H24a2.002 2.002 0 00-2 2V23h2V18h4v5h2V11A2.0023 2.0023 0 0028 9zm-4 7V11h4v5zM16 23H12V9h4a4.0042 4.0042 0 014 4v6A4.0039 4.0039 0 0116 23zm-2-2h2a2.0027 2.0027 0 002-2V13a2.0023 2.0023 0 00-2-2H14zM10 23H4a2.0023 2.0023 0 01-2-2V11A2.002 2.002 0 014 9h6v2H4V21h6z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default Cda32;
