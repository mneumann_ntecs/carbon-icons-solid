import { CarbonIconComponent } from "../types";
export const Network_432: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="Network_432"
    fill={props.fill || "currentColor"}
    width={props.width || "32"}
    height={props.height || "32"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <circle cx="21" cy="26" r="2"></circle>
    <circle cx="21" cy="6" r="2"></circle>
    <circle cx="4" cy="16" r="2"></circle>
    <path d="M28,12a3.9962,3.9962,0,0,0-3.8579,3H19.8579a3.9655,3.9655,0,0,0-5.4914-2.6426L11.19,8.3872A3.9626,3.9626,0,0,0,12,6a4,4,0,1,0-4,4,3.96,3.96,0,0,0,1.6338-.3574l3.176,3.97a3.9239,3.9239,0,0,0,0,4.7744l-3.1758,3.97A3.96,3.96,0,0,0,8,22a4,4,0,1,0,4,4,3.9624,3.9624,0,0,0-.81-2.3872l3.1758-3.97A3.9658,3.9658,0,0,0,19.8579,17h4.2842A3.9934,3.9934,0,1,0,28,12ZM6,6A2,2,0,1,1,8,8,2.0023,2.0023,0,0,1,6,6ZM8,28a2,2,0,1,1,2-2A2.0023,2.0023,0,0,1,8,28Zm8-10a2,2,0,1,1,2-2A2.0023,2.0023,0,0,1,16,18Zm12,0a2,2,0,1,1,2-2A2.0023,2.0023,0,0,1,28,18Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default Network_432;
