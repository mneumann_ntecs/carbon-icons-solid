import { CarbonIconComponent } from "../types";
export const Sunset16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="Sunset16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M2 27.005H29.998V29.005H2z"></path>
    <path
      d="M16,20a4.0045,4.0045,0,0,1,4,4h2a6,6,0,0,0-12,0h2A4.0045,4.0045,0,0,1,16,20Z"
      transform="translate(0 .005)"
    ></path>
    <path d="M25 22.005H30V24.005H25z"></path>
    <path
      d="M21.668 14.854H26.625999999999998V16.854H21.668z"
      transform="rotate(-45 24.152 15.856)"
    ></path>
    <path d="M19.59 9.595L17 12.175 17 4.005 15 4.005 15 12.175 12.41 9.595 11 11.005 16 16.005 21 11.005 19.59 9.595z"></path>
    <path
      d="M6.854 13.374H8.854V18.332H6.854z"
      transform="rotate(-45 7.86 15.856)"
    ></path>
    <path d="M2 22.005H7V24.005H2z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default Sunset16;
