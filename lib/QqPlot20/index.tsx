import { CarbonIconComponent } from "../types";
export const QqPlot20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="QqPlot20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <circle cx="20" cy="4" r="2"></circle>
    <circle cx="8" cy="16" r="2"></circle>
    <circle cx="28" cy="12" r="2"></circle>
    <circle cx="11" cy="7" r="2"></circle>
    <circle cx="16" cy="24" r="2"></circle>
    <path d="M30,3.4131,28.5859,2,4,26.585V2H2V28a2,2,0,0,0,2,2H30V28H5.4131Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default QqPlot20;
