import { CarbonIconComponent } from "../types";
export const ThumbsUp20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="ThumbsUp20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M26,11H20V6a3.64,3.64,0,0,0-3.24-4A3.22,3.22,0,0,0,16,2H14.43A1.51,1.51,0,0,0,13,3.17v.12L12,9.63,8.46,15H2V29H21c7.44,0,9-4.35,9-8V15a3.64,3.64,0,0,0-3.24-4A3.22,3.22,0,0,0,26,11ZM8,27H4V17H8Zm20-6c0,4.09-2.22,6-7,6H10V16.3l4-5.93L14.73,5l.14-1H16c1.44,0,2,.56,2,2v7h8c1.44,0,2,.56,2,2Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default ThumbsUp20;
