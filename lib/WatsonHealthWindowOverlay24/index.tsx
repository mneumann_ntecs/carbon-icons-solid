import { CarbonIconComponent } from "../types";
export const WatsonHealthWindowOverlay24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="WatsonHealthWindowOverlay24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M15 6H17V9H15zM25 17H28V19H25zM15 27H17V30H15zM4 17H7V19H4z"></path>
    <path
      d="M7.55 9.03H9.55V12.03H7.55z"
      transform="rotate(-45 8.558 10.545)"
    ></path>
    <path
      d="M21.96 9.54H24.96V11.54H21.96z"
      transform="rotate(-45 23.466 10.54)"
    ></path>
    <path
      d="M22.46 23.94H24.46V26.94H22.46z"
      transform="rotate(-45 23.459 25.443)"
    ></path>
    <path
      d="M7.04 24.45H10.04V26.45H7.04z"
      transform="rotate(-45 8.55 25.448)"
    ></path>
    <path d="M4 2H28V4H4zM16 24a6 6 0 10-6-6A6 6 0 0016 24zm0-10v8a4 4 0 010-8z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default WatsonHealthWindowOverlay24;
