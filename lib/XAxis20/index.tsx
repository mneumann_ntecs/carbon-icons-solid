import { CarbonIconComponent } from "../types";
export const XAxis20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="XAxis20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M23,20l-1.4141,1.4141L24.1719,24H6V4H4V24a2.0023,2.0023,0,0,0,2,2H24.1719l-2.586,2.5859L23,30l5-5Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default XAxis20;
