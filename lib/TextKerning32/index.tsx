import { CarbonIconComponent } from "../types";
export const TextKerning32: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="TextKerning32"
    fill={props.fill || "currentColor"}
    width={props.width || "32"}
    height={props.height || "32"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M30 24L18.83 24 21.12 21.71 19.71 20.29 15 25 19.71 29.71 21.12 28.29 18.83 26 30 26 30 24zM14 21L20 4 18 4 12 21 14 21zM13 4L9 16 5 4 3 4 8 18 10 18 15 4 13 4zM28 18h2L25 4H23L18 18h2l1-3h6zm-6.33-5L24 6l2.33 7z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default TextKerning32;
