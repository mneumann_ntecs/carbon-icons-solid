import { CarbonIconComponent } from "../types";
export const LogoJupyter24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="LogoJupyter24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M26.0766 3.5878a1.69 1.69 0 11-1.76-1.5853 1.67 1.67 0 011.76 1.5853zM16.2191 23.1113c-4.4866 0-8.43-1.61-10.4688-3.9882a11.1618 11.1618 0 0020.9377 0C24.6541 21.5011 20.7257 23.1113 16.2191 23.1113zM16.2191 7.6483c4.4867 0 8.43 1.61 10.4689 3.9881a11.1618 11.1618 0 00-20.9377 0C7.7892 9.2535 11.7126 7.6483 16.2191 7.6483zM10.0427 27.7388a2.1085 2.1085 0 11-.2031-.7976 2.1278 2.1278 0 01.2031.7976zM6.26 7.1064A1.2263 1.2263 0 117.4517 5.83 1.2415 1.2415 0 016.26 7.1064z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default LogoJupyter24;
