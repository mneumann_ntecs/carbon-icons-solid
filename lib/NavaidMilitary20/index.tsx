import { CarbonIconComponent } from "../types";
export const NavaidMilitary20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="NavaidMilitary20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M16,30A14,14,0,1,1,30,16,14.0158,14.0158,0,0,1,16,30ZM16,4A12,12,0,1,0,28,16,12.0137,12.0137,0,0,0,16,4Z"></path>
    <path d="M16,24a8,8,0,1,1,8-8A8.0092,8.0092,0,0,1,16,24Zm0-14a6,6,0,1,0,6,6A6.0067,6.0067,0,0,0,16,10Z"></path>
    <circle cx="16" cy="16" r="2"></circle>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default NavaidMilitary20;
