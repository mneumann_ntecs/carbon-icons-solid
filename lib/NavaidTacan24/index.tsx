import { CarbonIconComponent } from "../types";
export const NavaidTacan24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="NavaidTacan24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <circle cx="16" cy="14" r="2"></circle>
    <path d="M20,28H12a1,1,0,0,1-1-1V21.2656L7.2612,14.7231,2.4854,11.8574a1,1,0,0,1-.3536-1.3535l4-7a1,1,0,0,1,1.3828-.3613L12.2769,6h7.4462l4.7623-2.8574a1,1,0,0,1,1.3828.3613l4,7a1,1,0,0,1-.3536,1.3535l-4.7758,2.8657L21,21.2656V27A1,1,0,0,1,20,28Zm-7-2h6V20.7346l4.2616-7.4578,4.3844-2.6306L24.6387,5.3831,20.277,8H11.723L7.3613,5.3831,4.354,10.6462l4.3844,2.6306L13,20.7346Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default NavaidTacan24;
