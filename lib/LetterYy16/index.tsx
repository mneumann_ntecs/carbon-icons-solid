import { CarbonIconComponent } from "../types";
export const LetterYy16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="LetterYy16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M15 9L13 9 11 16 9 9 7 9 10 18 10 23 12 23 12 18 12 18 15 9zM23 13l-2 7.52L19.08 13H17l3.15 9.87L19.53 25H17v2h2.26a2 2 0 001.91-1.42L25 13z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default LetterYy16;
