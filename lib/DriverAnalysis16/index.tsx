import { CarbonIconComponent } from "../types";
export const DriverAnalysis16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="DriverAnalysis16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <circle cx="22" cy="16" r="2"></circle>
    <path d="M30,15H27.91A6.0048,6.0048,0,0,0,22,10l-.022.001A9.9833,9.9833,0,0,0,4.0508,15H2v2H4.0508A9.9833,9.9833,0,0,0,21.978,21.999L22,22a6.0048,6.0048,0,0,0,5.91-5H30ZM14,8a7.9769,7.9769,0,0,1,5.7383,2.4463A6.0152,6.0152,0,0,0,16.0891,15H6.0693A8.0073,8.0073,0,0,1,14,8Zm0,16a8.0073,8.0073,0,0,1-7.9307-7h10.02a6.0152,6.0152,0,0,0,3.6492,4.5537A7.9769,7.9769,0,0,1,14,24Zm8-4a4,4,0,1,1,4-4A4.0045,4.0045,0,0,1,22,20Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default DriverAnalysis16;
