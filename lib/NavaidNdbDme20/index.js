import {
  template,
  addEventListener,
  insert,
  memo,
  effect,
  setAttribute,
  style,
  delegateEvents,
} from "solid-js/web";

const _tmpl$ = template(
    `<svg data-carbon-icon="NavaidNdbDme20" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" preserveAspectRatio="xMidYMid meet"><path d="M4 12H2V4A2.0021 2.0021 0 014 2h8V4H4zM12 30H4a2.0021 2.0021 0 01-2-2V20H4v8h8zM28 30H20V28h8V20h2v8A2.0021 2.0021 0 0128 30zM30 12H28V4H20V2h8a2.0021 2.0021 0 012 2zM16 22a6 6 0 116-6A6.0067 6.0067 0 0116 22zm0-10a4 4 0 104 4A4.0045 4.0045 0 0016 12z"></path><circle cx="16" cy="16" r="2"></circle><circle cx="16" cy="7" r="1"></circle><circle cx="16" cy="3" r="1"></circle><circle cx="16" cy="25" r="1"></circle><circle cx="16" cy="29" r="1"></circle><circle cx="25" cy="16" r="1"></circle><circle cx="29" cy="16" r="1"></circle><circle cx="7" cy="16" r="1"></circle><circle cx="3" cy="16" r="1"></circle><circle cx="9.636" cy="9.636" r="1"></circle><circle cx="7" cy="7" r="1"></circle><circle cx="22.364" cy="22.364" r="1"></circle><circle cx="25" cy="25" r="1"></circle><circle cx="22.364" cy="9.636" r="1"></circle><circle cx="25" cy="7" r="1"></circle><circle cx="9.636" cy="22.364" r="1"></circle><circle cx="7" cy="25" r="1"></circle></svg>`,
    38
  ),
  _tmpl$2 = template(`<title></title>`, 2);

const NavaidNdbDme20 = (props) =>
  (() => {
    const _el$ = _tmpl$.cloneNode(true),
      _el$2 = _el$.firstChild,
      _el$3 = _el$2.nextSibling,
      _el$4 = _el$3.nextSibling,
      _el$5 = _el$4.nextSibling,
      _el$6 = _el$5.nextSibling,
      _el$7 = _el$6.nextSibling,
      _el$8 = _el$7.nextSibling,
      _el$9 = _el$8.nextSibling,
      _el$10 = _el$9.nextSibling,
      _el$11 = _el$10.nextSibling,
      _el$12 = _el$11.nextSibling,
      _el$13 = _el$12.nextSibling,
      _el$14 = _el$13.nextSibling,
      _el$15 = _el$14.nextSibling,
      _el$16 = _el$15.nextSibling,
      _el$17 = _el$16.nextSibling,
      _el$18 = _el$17.nextSibling;
    _el$18.nextSibling;

    addEventListener(_el$, "keydown", props.onKeyDown, true);

    addEventListener(_el$, "keyup", props.onKeyUp, true);

    addEventListener(_el$, "mouseleave", props.onMouseLeave);

    addEventListener(_el$, "mouseenter", props.onMouseEnter);

    addEventListener(_el$, "mouseover", props.onMouseOver, true);

    addEventListener(_el$, "click", props.onClick, true);

    insert(
      _el$,
      (() => {
        const _c$ = memo(() => props.children, true);

        return () =>
          _c$() ||
          (props.title &&
            (() => {
              const _el$20 = _tmpl$2.cloneNode(true);

              insert(_el$20, () => props.title);

              return _el$20;
            })());
      })(),
      null
    );

    effect(
      (_p$) => {
        const _v$ = props.fill || "currentColor",
          _v$2 = props.width || "20",
          _v$3 = props.height || "20",
          _v$4 = props["aria-label"],
          _v$5 = props["aria-labelledby"],
          _v$6 = props["aria-label"] || props["aria-labelledby"] || props.title,
          _v$7 =
            props["aria-label"] || props["aria-labelledby"] || props.title
              ? "img"
              : undefined,
          _v$8 = props.tabindex === "0" ? true : props.focusable,
          _v$9 = props.tabindex,
          _v$10 = props.id,
          _v$11 = props.class,
          _v$12 = props.title,
          _v$13 = props.style,
          _v$14 = props.stroke;

        _v$ !== _p$._v$ && setAttribute(_el$, "fill", (_p$._v$ = _v$));
        _v$2 !== _p$._v$2 && setAttribute(_el$, "width", (_p$._v$2 = _v$2));
        _v$3 !== _p$._v$3 && setAttribute(_el$, "height", (_p$._v$3 = _v$3));
        _v$4 !== _p$._v$4 &&
          setAttribute(_el$, "aria-label", (_p$._v$4 = _v$4));
        _v$5 !== _p$._v$5 &&
          setAttribute(_el$, "aria-labelledby", (_p$._v$5 = _v$5));
        _v$6 !== _p$._v$6 &&
          setAttribute(_el$, "aria-hidden", (_p$._v$6 = _v$6));
        _v$7 !== _p$._v$7 && setAttribute(_el$, "role", (_p$._v$7 = _v$7));
        _v$8 !== _p$._v$8 && setAttribute(_el$, "focusable", (_p$._v$8 = _v$8));
        _v$9 !== _p$._v$9 && setAttribute(_el$, "tabindex", (_p$._v$9 = _v$9));
        _v$10 !== _p$._v$10 && setAttribute(_el$, "id", (_p$._v$10 = _v$10));
        _v$11 !== _p$._v$11 && setAttribute(_el$, "class", (_p$._v$11 = _v$11));
        _v$12 !== _p$._v$12 && setAttribute(_el$, "title", (_p$._v$12 = _v$12));
        _p$._v$13 = style(_el$, _v$13, _p$._v$13);
        _v$14 !== _p$._v$14 &&
          setAttribute(_el$, "stroke", (_p$._v$14 = _v$14));
        return _p$;
      },
      {
        _v$: undefined,
        _v$2: undefined,
        _v$3: undefined,
        _v$4: undefined,
        _v$5: undefined,
        _v$6: undefined,
        _v$7: undefined,
        _v$8: undefined,
        _v$9: undefined,
        _v$10: undefined,
        _v$11: undefined,
        _v$12: undefined,
        _v$13: undefined,
        _v$14: undefined,
      }
    );

    return _el$;
  })();

delegateEvents(["click", "mouseover", "keyup", "keydown"]);

export default NavaidNdbDme20;
export { NavaidNdbDme20 };
