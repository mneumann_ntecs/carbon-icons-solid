import { CarbonIconComponent } from "../types";
export const Identification20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="Identification20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M28,6V26H4V6H28m0-2H4A2,2,0,0,0,2,6V26a2,2,0,0,0,2,2H28a2,2,0,0,0,2-2V6a2,2,0,0,0-2-2Z"></path>
    <path d="M6 10H13V12H6zM6 14H10V16H6zM23 18H17a3 3 0 00-3 3v2h2V21a1 1 0 011-1h6a1 1 0 011 1v2h2V21A3 3 0 0023 18zM20 17a4 4 0 10-4-4A4 4 0 0020 17zm0-6a2 2 0 11-2 2A2 2 0 0120 11z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default Identification20;
