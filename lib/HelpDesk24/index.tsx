import { CarbonIconComponent } from "../types";
export const HelpDesk24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="HelpDesk24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M27 10H21a3.0033 3.0033 0 00-3 3v6a2.0023 2.0023 0 002 2v7a2.0023 2.0023 0 002 2h4a2.0023 2.0023 0 002-2V21a2.0023 2.0023 0 002-2V13A3.0033 3.0033 0 0027 10zm1 9H26v9H22V19H20V13a1.0009 1.0009 0 011-1h6a1.0009 1.0009 0 011 1zM20 5a4 4 0 114 4A4.0042 4.0042 0 0120 5zm2 0a2 2 0 102-2A2.0023 2.0023 0 0022 5zM14 16V13a3.0033 3.0033 0 00-3-3H5a3.0033 3.0033 0 00-3 3v3H0v2H16V16zM4 13a1.0009 1.0009 0 011-1h6a1.0009 1.0009 0 011 1v3H4zM4 5A4 4 0 118 9 4.0042 4.0042 0 014 5zM6 5A2 2 0 108 3 2.0023 2.0023 0 006 5z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default HelpDesk24;
