import { CarbonIconComponent } from "../types";
export const DirectionCurveFilled16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="DirectionCurveFilled16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M28,2H4A2,2,0,0,0,2,4V28a2,2,0,0,0,2,2H28a2,2,0,0,0,2-2V4A2,2,0,0,0,28,2ZM22.5859,13.4141,18,8.8281v5.7686a5.0206,5.0206,0,0,1-1.0957,3.124l-2.2471,2.8086A3.01,3.01,0,0,0,14,22.4033V27H12V22.4033a5.0206,5.0206,0,0,1,1.0957-3.124l2.2471-2.8086A3.01,3.01,0,0,0,16,14.5967V8.8281l-4.5859,4.586L10,12l7-7,7,7Z"></path>
    <path
      fill="none"
      d="M22.5859,13.4141,18,8.8281v5.7686a5.0206,5.0206,0,0,1-1.0957,3.124l-2.2471,2.8086A3.01,3.01,0,0,0,14,22.4033V27H12V22.4033a5.0206,5.0206,0,0,1,1.0957-3.124l2.2471-2.8086A3.01,3.01,0,0,0,16,14.5967V8.8281l-4.5859,4.586L10,12l7-7,7,7Z"
      data-icon-path="inner-path"
    ></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default DirectionCurveFilled16;
