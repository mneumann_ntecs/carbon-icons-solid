import { CarbonIconComponent } from "../types";
export const ChartRing24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="ChartRing24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M16,2A14,14,0,1,0,30,16,14.0158,14.0158,0,0,0,16,2ZM27.9493,15.001H21.91a5.9825,5.9825,0,0,0-9.3806-3.8853l-4.27-4.27a11.9783,11.9783,0,0,1,19.69,8.1548ZM16,20a4,4,0,1,1,4-4A4.0045,4.0045,0,0,1,16,20ZM4,16A11.937,11.937,0,0,1,6.8455,8.26l4.27,4.2695A5.9836,5.9836,0,0,0,15,21.9106v6.04A12.0105,12.0105,0,0,1,4,16ZM17,27.95v-6.04A6.0069,6.0069,0,0,0,21.91,17h6.04A12.0082,12.0082,0,0,1,17,27.95Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default ChartRing24;
