import { CarbonIconComponent } from "../types";
export const ZAxis24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="ZAxis24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M26 22L26 25.586 17 16.586 17 10 17 6 17 5.828 19.586 8.414 21 7 16 2 11 7 12.414 8.414 15 5.828 15 6 15 10 15 16.586 6 25.586 6 22 4 22 4 29 11 29 11 27 7.414 27 16 18.414 24.586 27 21 27 21 29 28 29 28 22 26 22z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default ZAxis24;
