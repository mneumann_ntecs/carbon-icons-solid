import { CarbonIconComponent } from "../types";
export const Migrate20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="Migrate20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M26 2H6A2 2 0 004 4V8a2 2 0 002 2h9v6.17l-2.59-2.58L11 15l5 5 5-5-1.41-1.41L17 16.17V10h9a2 2 0 002-2V4A2 2 0 0026 2zM6 4h4V8H6zM26 8H12V4H26zM26 22H6a2 2 0 00-2 2v4a2 2 0 002 2H26a2 2 0 002-2V24A2 2 0 0026 22zM6 24H20v4H6zm20 4H22V24h4z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default Migrate20;
