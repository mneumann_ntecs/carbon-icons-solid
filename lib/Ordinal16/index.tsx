import { CarbonIconComponent } from "../types";
export const Ordinal16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="Ordinal16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 16 16"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M1,13 L1,12 L3,12 L3,9 L5.999,9 L6,6 L9,6 L9,3 L13,3 L13,12 L15,12 L15,13 L1,13 Z M5.999,10 L4,10 L4,12 L5.999,12 L5.999,10 Z M8.999,7 L6.999,7 L6.999,9 L7,9 L7,12 L8.999,12 L8.999,7 Z M12,4 L10,4 L10,12 L12,12 L12,4 Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default Ordinal16;
