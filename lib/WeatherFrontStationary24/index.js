import {
  template,
  addEventListener,
  insert,
  memo,
  effect,
  setAttribute,
  style,
  delegateEvents,
} from "solid-js/web";

const _tmpl$ = template(
    `<svg data-carbon-icon="WeatherFrontStationary24" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" preserveAspectRatio="xMidYMid meet"><path d="M28.1655 2a12.9848 12.9848 0 00-8.4074 3.1065A3.9947 3.9947 0 1015.583 11.728l-1.1362 4.1665c-.0725.2657-.1579.5254-.2489.7818a3.9864 3.9864 0 00-5.1547 6.0054A10.9541 10.9541 0 013.8345 24H2v2H3.8345c.2975 0 .5918-.0171.8853-.0371l7.9291 2.9736A1 1 0 0014 28V21.0923a12.9311 12.9311 0 00.9121-1.313l6.3628-1.8179a1 1 0 00.5059-1.5864L17.7843 11.38a10.9268 10.9268 0 011.1516-2.3472l5.7477 1.916A1 1 0 0026 10V4.2251A10.8956 10.8956 0 0128.1655 4H30V2zM17 6a1.98 1.98 0 011.3237.53A12.9413 12.9413 0 0016.24 9.8482 1.9988 1.9988 0 0117 6zM10 20a1.9879 1.9879 0 013.3374-1.4717 11.0157 11.0157 0 01-2.5959 3.0147A1.9975 1.9975 0 0110 20zM8.3354 25.1826A13.047 13.047 0 0012 23.0943v3.4624zm7.74-7.8154c.1107-.3105.2136-.625.3013-.9463l.732-2.6846L19.28 16.4512zM24 8.6123l-3.7411-1.247A11.0712 11.0712 0 0124 4.8306zM22 22L22 24 26.586 24 22 28.586 23.414 30 28 25.414 28 30 30 30 30 22 22 22zM2 2L2 4 6.586 4 2 8.586 3.414 10 8 5.414 8 10 10 10 10 2 2 2z"></path></svg>`,
    4
  ),
  _tmpl$2 = template(`<title></title>`, 2);

const WeatherFrontStationary24 = (props) =>
  (() => {
    const _el$ = _tmpl$.cloneNode(true);
    _el$.firstChild;

    addEventListener(_el$, "keydown", props.onKeyDown, true);

    addEventListener(_el$, "keyup", props.onKeyUp, true);

    addEventListener(_el$, "mouseleave", props.onMouseLeave);

    addEventListener(_el$, "mouseenter", props.onMouseEnter);

    addEventListener(_el$, "mouseover", props.onMouseOver, true);

    addEventListener(_el$, "click", props.onClick, true);

    insert(
      _el$,
      (() => {
        const _c$ = memo(() => props.children, true);

        return () =>
          _c$() ||
          (props.title &&
            (() => {
              const _el$3 = _tmpl$2.cloneNode(true);

              insert(_el$3, () => props.title);

              return _el$3;
            })());
      })(),
      null
    );

    effect(
      (_p$) => {
        const _v$ = props.fill || "currentColor",
          _v$2 = props.width || "24",
          _v$3 = props.height || "24",
          _v$4 = props["aria-label"],
          _v$5 = props["aria-labelledby"],
          _v$6 = props["aria-label"] || props["aria-labelledby"] || props.title,
          _v$7 =
            props["aria-label"] || props["aria-labelledby"] || props.title
              ? "img"
              : undefined,
          _v$8 = props.tabindex === "0" ? true : props.focusable,
          _v$9 = props.tabindex,
          _v$10 = props.id,
          _v$11 = props.class,
          _v$12 = props.title,
          _v$13 = props.style,
          _v$14 = props.stroke;

        _v$ !== _p$._v$ && setAttribute(_el$, "fill", (_p$._v$ = _v$));
        _v$2 !== _p$._v$2 && setAttribute(_el$, "width", (_p$._v$2 = _v$2));
        _v$3 !== _p$._v$3 && setAttribute(_el$, "height", (_p$._v$3 = _v$3));
        _v$4 !== _p$._v$4 &&
          setAttribute(_el$, "aria-label", (_p$._v$4 = _v$4));
        _v$5 !== _p$._v$5 &&
          setAttribute(_el$, "aria-labelledby", (_p$._v$5 = _v$5));
        _v$6 !== _p$._v$6 &&
          setAttribute(_el$, "aria-hidden", (_p$._v$6 = _v$6));
        _v$7 !== _p$._v$7 && setAttribute(_el$, "role", (_p$._v$7 = _v$7));
        _v$8 !== _p$._v$8 && setAttribute(_el$, "focusable", (_p$._v$8 = _v$8));
        _v$9 !== _p$._v$9 && setAttribute(_el$, "tabindex", (_p$._v$9 = _v$9));
        _v$10 !== _p$._v$10 && setAttribute(_el$, "id", (_p$._v$10 = _v$10));
        _v$11 !== _p$._v$11 && setAttribute(_el$, "class", (_p$._v$11 = _v$11));
        _v$12 !== _p$._v$12 && setAttribute(_el$, "title", (_p$._v$12 = _v$12));
        _p$._v$13 = style(_el$, _v$13, _p$._v$13);
        _v$14 !== _p$._v$14 &&
          setAttribute(_el$, "stroke", (_p$._v$14 = _v$14));
        return _p$;
      },
      {
        _v$: undefined,
        _v$2: undefined,
        _v$3: undefined,
        _v$4: undefined,
        _v$5: undefined,
        _v$6: undefined,
        _v$7: undefined,
        _v$8: undefined,
        _v$9: undefined,
        _v$10: undefined,
        _v$11: undefined,
        _v$12: undefined,
        _v$13: undefined,
        _v$14: undefined,
      }
    );

    return _el$;
  })();

delegateEvents(["click", "mouseover", "keyup", "keydown"]);

export default WeatherFrontStationary24;
export { WeatherFrontStationary24 };
