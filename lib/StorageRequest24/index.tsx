import { CarbonIconComponent } from "../types";
export const StorageRequest24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="StorageRequest24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M4 21H2v3a2.0059 2.0059 0 002 2H7V24H4zM4 8H7V6H4A2.0059 2.0059 0 002 8v3H4zM17 6H23V8H17zM9 6H15V8H9zM17 24H23V26H17zM28 15L4 15 4 13 2 13 2 19 4 19 4 17 28 17 28 19 30 19 30 13 28 13 28 15zM28 24H25v2h3a2.0059 2.0059 0 002-2V21H28zM28 6H25V8h3v3h2V8A2.0059 2.0059 0 0028 6zM9 24H15V26H9z"></path>
    <circle cx="7" cy="12" r="1"></circle>
    <circle cx="7" cy="20" r="1"></circle>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default StorageRequest24;
