import { CarbonIconComponent } from "../types";
export const IncompleteCancel32: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="IncompleteCancel32"
    fill={props.fill || "currentColor"}
    width={props.width || "32"}
    height={props.height || "32"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M30 21.41L28.59 20 25 23.59 21.41 20 20 21.41 23.59 25 20 28.59 21.41 30 25 26.41 28.59 30 30 28.59 26.41 25 30 21.41zM14 26A12 12 0 0114 2zM17.8257 4.7642a10.0288 10.0288 0 013.2422 2.1679l1.4133-1.4135a12.0359 12.0359 0 00-3.89-2.6016zM26 14a11.93 11.93 0 00-.9167-4.5908l-1.8472.7651A9.9428 9.9428 0 0124 14z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default IncompleteCancel32;
