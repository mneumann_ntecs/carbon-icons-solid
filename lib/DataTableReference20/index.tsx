import { CarbonIconComponent } from "../types";
export const DataTableReference20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="DataTableReference20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M4 20L4 22 8.586 22 2 28.586 3.414 30 10 23.414 10 28 12 28 12 20 4 20zM20 22H24V24H20zM16 18H20V20H16zM20 14H24V16H20zM8 14H12V16H8z"></path>
    <path d="M27,3H5A2.0027,2.0027,0,0,0,3,5V16H5V11H27V27H16v2H27a2.0027,2.0027,0,0,0,2-2V5A2.0027,2.0027,0,0,0,27,3Zm0,6H5V5H27Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default DataTableReference20;
