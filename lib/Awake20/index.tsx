import { CarbonIconComponent } from "../types";
export const Awake20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="Awake20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M15 2H17V6.96H15z"></path>
    <path
      d="M21.67 6.85H26.630000000000003V8.85H21.67z"
      transform="rotate(-45 24.142 7.85)"
    ></path>
    <path d="M25.04 15H30V17H25.04z"></path>
    <path
      d="M23.15 21.67H25.15V26.630000000000003H23.15z"
      transform="rotate(-45 24.152 24.146)"
    ></path>
    <path d="M15 25.04H17V30H15z"></path>
    <path
      d="M5.37 23.15H10.33V25.15H5.37z"
      transform="rotate(-45 7.86 24.144)"
    ></path>
    <path d="M2 15H6.96V17H2z"></path>
    <path
      d="M6.85 5.37H8.85V10.33H6.85z"
      transform="rotate(-45 7.85 7.848)"
    ></path>
    <path d="M16,12a4,4,0,1,1-4,4,4,4,0,0,1,4-4m0-2a6,6,0,1,0,6,6,6,6,0,0,0-6-6Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default Awake20;
