import { CarbonIconComponent } from "../types";
export const ChemistryReference24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="ChemistryReference24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M4 20L4 22 8.586 22 2 28.586 3.414 30 10 23.414 10 28 12 28 12 20 4 20z"></path>
    <path d="M20,13.67V4h2V2H10V4h2v9.67L9.58,17h2.4767L14,14.33V4h4V14.33l7.61,10.46a2.0133,2.0133,0,0,1-.44,2.82,2.0406,2.0406,0,0,1-1.19.39H15v2h8.98a4.0154,4.0154,0,0,0,3.25-6.38Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default ChemistryReference24;
