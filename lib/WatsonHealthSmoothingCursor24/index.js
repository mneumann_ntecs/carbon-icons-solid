import {
  template,
  addEventListener,
  insert,
  memo,
  effect,
  setAttribute,
  style,
  delegateEvents,
} from "solid-js/web";

const _tmpl$ = template(
    `<svg data-carbon-icon="WatsonHealthSmoothingCursor24" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" preserveAspectRatio="xMidYMid meet"><circle cx="18" cy="18" r="4"></circle><path d="M18 30a11.8932 11.8932 0 01-4.1035-.72L14.58 27.4A9.9725 9.9725 0 0018 28zM22.1484 29.2637l-.6914-1.877a9.9556 9.9556 0 002.9973-1.748l1.2916 1.5268A11.9525 11.9525 0 0122.1484 29.2637zM10.2893 27.1951A12.0141 12.0141 0 017.61 24.0078l1.73-1.0029a10.0236 10.0236 0 002.2347 2.6584zM28.4133 23.967l-1.7343-.9956a9.9159 9.9159 0 001.176-3.2641l1.9712.3388A11.8956 11.8956 0 0128.4133 23.967zM6.1821 20.0925A12.1282 12.1282 0 016.1777 15.93l1.9707.3423a10.1214 10.1214 0 00.0035 3.4738zM27.8447 16.2339a9.9019 9.9019 0 00-1.1953-3.2564l1.7285-1.0063a11.9029 11.9029 0 011.4361 3.9121zM9.3313 13.0107l-1.7324-.999a12.01 12.01 0 012.6738-3.1931l1.2886 1.53A10.01 10.01 0 009.3313 13.0107zM24.4094 10.3237a9.96 9.96 0 00-3.0088-1.7309l.68-1.8809a11.9491 11.9491 0 013.6118 2.0772zM14.562 8.6064l-.6875-1.8779A12.1031 12.1031 0 0118 6V8A10.0875 10.0875 0 0014.562 8.6064zM11 2L2 2 2 11 4 11 4 4 11 4 11 2z"></path></svg>`,
    6
  ),
  _tmpl$2 = template(`<title></title>`, 2);

const WatsonHealthSmoothingCursor24 = (props) =>
  (() => {
    const _el$ = _tmpl$.cloneNode(true),
      _el$2 = _el$.firstChild;
    _el$2.nextSibling;

    addEventListener(_el$, "keydown", props.onKeyDown, true);

    addEventListener(_el$, "keyup", props.onKeyUp, true);

    addEventListener(_el$, "mouseleave", props.onMouseLeave);

    addEventListener(_el$, "mouseenter", props.onMouseEnter);

    addEventListener(_el$, "mouseover", props.onMouseOver, true);

    addEventListener(_el$, "click", props.onClick, true);

    insert(
      _el$,
      (() => {
        const _c$ = memo(() => props.children, true);

        return () =>
          _c$() ||
          (props.title &&
            (() => {
              const _el$4 = _tmpl$2.cloneNode(true);

              insert(_el$4, () => props.title);

              return _el$4;
            })());
      })(),
      null
    );

    effect(
      (_p$) => {
        const _v$ = props.fill || "currentColor",
          _v$2 = props.width || "24",
          _v$3 = props.height || "24",
          _v$4 = props["aria-label"],
          _v$5 = props["aria-labelledby"],
          _v$6 = props["aria-label"] || props["aria-labelledby"] || props.title,
          _v$7 =
            props["aria-label"] || props["aria-labelledby"] || props.title
              ? "img"
              : undefined,
          _v$8 = props.tabindex === "0" ? true : props.focusable,
          _v$9 = props.tabindex,
          _v$10 = props.id,
          _v$11 = props.class,
          _v$12 = props.title,
          _v$13 = props.style,
          _v$14 = props.stroke;

        _v$ !== _p$._v$ && setAttribute(_el$, "fill", (_p$._v$ = _v$));
        _v$2 !== _p$._v$2 && setAttribute(_el$, "width", (_p$._v$2 = _v$2));
        _v$3 !== _p$._v$3 && setAttribute(_el$, "height", (_p$._v$3 = _v$3));
        _v$4 !== _p$._v$4 &&
          setAttribute(_el$, "aria-label", (_p$._v$4 = _v$4));
        _v$5 !== _p$._v$5 &&
          setAttribute(_el$, "aria-labelledby", (_p$._v$5 = _v$5));
        _v$6 !== _p$._v$6 &&
          setAttribute(_el$, "aria-hidden", (_p$._v$6 = _v$6));
        _v$7 !== _p$._v$7 && setAttribute(_el$, "role", (_p$._v$7 = _v$7));
        _v$8 !== _p$._v$8 && setAttribute(_el$, "focusable", (_p$._v$8 = _v$8));
        _v$9 !== _p$._v$9 && setAttribute(_el$, "tabindex", (_p$._v$9 = _v$9));
        _v$10 !== _p$._v$10 && setAttribute(_el$, "id", (_p$._v$10 = _v$10));
        _v$11 !== _p$._v$11 && setAttribute(_el$, "class", (_p$._v$11 = _v$11));
        _v$12 !== _p$._v$12 && setAttribute(_el$, "title", (_p$._v$12 = _v$12));
        _p$._v$13 = style(_el$, _v$13, _p$._v$13);
        _v$14 !== _p$._v$14 &&
          setAttribute(_el$, "stroke", (_p$._v$14 = _v$14));
        return _p$;
      },
      {
        _v$: undefined,
        _v$2: undefined,
        _v$3: undefined,
        _v$4: undefined,
        _v$5: undefined,
        _v$6: undefined,
        _v$7: undefined,
        _v$8: undefined,
        _v$9: undefined,
        _v$10: undefined,
        _v$11: undefined,
        _v$12: undefined,
        _v$13: undefined,
        _v$14: undefined,
      }
    );

    return _el$;
  })();

delegateEvents(["click", "mouseover", "keyup", "keydown"]);

export default WatsonHealthSmoothingCursor24;
export { WatsonHealthSmoothingCursor24 };
