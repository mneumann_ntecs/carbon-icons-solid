import { CarbonIconComponent } from "../types";
export const LetterSs32: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="LetterSs32"
    fill={props.fill || "currentColor"}
    width={props.width || "32"}
    height={props.height || "32"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M22 23H17V21h5V19H19a2 2 0 01-2-2V15a2 2 0 012-2h5v2H19v2h3a2 2 0 012 2v2A2 2 0 0122 23zM13 23H7V21h6V17H9a2 2 0 01-2-2V11A2 2 0 019 9h6v2H9v4h4a2 2 0 012 2v4A2 2 0 0113 23z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default LetterSs32;
