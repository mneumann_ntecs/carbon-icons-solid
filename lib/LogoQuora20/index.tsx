import { CarbonIconComponent } from "../types";
export const LogoQuora20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="LogoQuora20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M29,23.86H27a1.72,1.72,0,0,1-1.73,1.72c-1.15,0-1.82-.86-2.58-2a11,11,0,0,0,4.79-9.1c.09-7.19-5.85-11.89-12.17-11.89S3,7.37,3,14.37,9,26.16,15.17,26.16a10.71,10.71,0,0,0,3.07-.48c1.15,2.11,2.59,3.74,5.46,3.74A5,5,0,0,0,29,23.86Zm-8.14-3.45a6.31,6.31,0,0,0-5.56-3.16,6,6,0,0,0-3.74,1.15L12.2,20a2.56,2.56,0,0,1,.86-.1c1.82,0,3,1.53,4.12,3.64a7.58,7.58,0,0,1-1.91.19C10.86,23.76,9,20.6,9,14.47s1.91-9.3,6.23-9.3,6.32,3.16,6.32,9.3A20.68,20.68,0,0,1,20.83,20.41Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default LogoQuora20;
