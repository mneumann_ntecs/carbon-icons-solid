import { CarbonIconComponent } from "../types";
export const WatsonHealthRegistration20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="WatsonHealthRegistration20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M28 25H20a2.0027 2.0027 0 01-2-2V20h2v3h8V9H20v3H18V9a2.0023 2.0023 0 012-2h8a2.0023 2.0023 0 012 2V23A2.0027 2.0027 0 0128 25zM8 15H12V17H8z"></path>
    <path d="M20 15H24V17H20zM14 15H18V17H14zM12 25H4a2.0023 2.0023 0 01-2-2V9A2.002 2.002 0 014 7h8a2.002 2.002 0 012 2v3H12V9H4V23h8V20h2v3A2.0023 2.0023 0 0112 25z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default WatsonHealthRegistration20;
