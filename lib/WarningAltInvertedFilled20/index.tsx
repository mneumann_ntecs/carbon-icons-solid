import { CarbonIconComponent } from "../types";
export const WarningAltInvertedFilled20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="WarningAltInvertedFilled20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path
      fill="none"
      d="M14.875,5h2.25V15h-2.25ZM16,21a1.5,1.5,0,1,1,1.5-1.5A1.5,1.5,0,0,1,16,21Z"
    ></path>
    <path d="M29.855,2.481A1.0011,1.0011,0,0,0,29,2H3a1,1,0,0,0-.8872,1.4614l13,25a1,1,0,0,0,1.7744,0l13-25A1.001,1.001,0,0,0,29.855,2.481ZM14.875,5h2.25V15h-2.25ZM16,21a1.5,1.5,0,1,1,1.5-1.5A1.5,1.5,0,0,1,16,21Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default WarningAltInvertedFilled20;
