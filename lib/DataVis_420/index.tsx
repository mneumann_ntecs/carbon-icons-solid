import { CarbonIconComponent } from "../types";
export const DataVis_420: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="DataVis_420"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M27,3H5A2.002,2.002,0,0,0,3,5V27a2.0023,2.0023,0,0,0,2,2H27a2.0027,2.0027,0,0,0,2-2V5A2.0023,2.0023,0,0,0,27,3Zm0,6H17V5H27ZM15,27H11V23h4Zm0-6H11V17h4ZM9,21H5V17H9Zm2-6V11H21v4Zm0-6V5h4V9Zm12,2h4l0,4H23ZM9,5V15H5V5ZM5,23H9v4H5Zm12,4V17H27.001l.001,10Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default DataVis_420;
