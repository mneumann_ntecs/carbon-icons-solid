import { CarbonIconComponent } from "../types";
export const CurrencyRupee16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="CurrencyRupee16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M24,7V5H8V7h5.5a4.4906,4.4906,0,0,1,4.45,4H8v2h9.95A4.4906,4.4906,0,0,1,13.5,17H8v2.3452L17.6169,28l1.3379-1.4863L10.606,19H13.5a6.5041,6.5041,0,0,0,6.4746-6H24V11H19.9746A6.4719,6.4719,0,0,0,18.18,7Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default CurrencyRupee16;
