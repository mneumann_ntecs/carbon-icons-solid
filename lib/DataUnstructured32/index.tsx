import { CarbonIconComponent } from "../types";
export const DataUnstructured32: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="DataUnstructured32"
    fill={props.fill || "currentColor"}
    width={props.width || "32"}
    height={props.height || "32"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M6 24a2 2 0 11-2 2 2 2 0 012-2m0-2a4 4 0 104 4A4 4 0 006 22zM16 4a2 2 0 11-2 2 2 2 0 012-2m0-2a4 4 0 104 4A4 4 0 0016 2zM26 4a2 2 0 11-2 2 2 2 0 012-2m0-2a4 4 0 104 4A4 4 0 0026 2zM18 24v4H14V24h4m2-2H12v8h8z"></path>
    <path d="M27,22.14V17a2,2,0,0,0-2-2H7V10h3V2H2v8H5v5a2,2,0,0,0,2,2H25v5.14a4,4,0,1,0,2,0ZM4,4H8V8H4ZM26,28a2,2,0,1,1,2-2A2,2,0,0,1,26,28Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default DataUnstructured32;
