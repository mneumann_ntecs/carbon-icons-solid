import {
  template,
  addEventListener,
  insert,
  memo,
  effect,
  setAttribute,
  style,
  delegateEvents,
} from "solid-js/web";

const _tmpl$ = template(
    `<svg data-carbon-icon="WatsonHealthDna24" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" preserveAspectRatio="xMidYMid meet"><path d="M22,2V1H20V2a7.04,7.04,0,0,1-.08,1H12.08A7.04,7.04,0,0,1,12,2V1H10V2c0,3.7549,1.9707,5.6035,4.1008,7C11.9707,10.3965,10,12.2451,10,16s1.9707,5.6035,4.1008,7C11.9707,24.3965,10,26.2451,10,30v1h2V30a7.04,7.04,0,0,1,.08-1H19.92A7.04,7.04,0,0,1,20,30v1h2V30c0-3.7549-1.9707-5.6035-4.1008-7C20.0293,21.6035,22,19.7549,22,16s-1.9707-5.6035-4.1008-7C20.0293,7.6035,22,5.7549,22,2ZM20,16a7.04,7.04,0,0,1-.08,1H12.08a6.3212,6.3212,0,0,1,0-2H19.92A7.04,7.04,0,0,1,20,16Zm-.7559,11H12.7559A8.9078,8.9078,0,0,1,16,24.17,8.9078,8.9078,0,0,1,19.2441,27ZM16,21.83A8.9078,8.9078,0,0,1,12.7559,19h6.4882A8.9078,8.9078,0,0,1,16,21.83ZM19.2441,13H12.7559A8.9078,8.9078,0,0,1,16,10.17,8.9078,8.9078,0,0,1,19.2441,13ZM16,7.83A8.9078,8.9078,0,0,1,12.7559,5h6.4882A8.9078,8.9078,0,0,1,16,7.83Z"></path></svg>`,
    4
  ),
  _tmpl$2 = template(`<title></title>`, 2);

const WatsonHealthDna24 = (props) =>
  (() => {
    const _el$ = _tmpl$.cloneNode(true);
    _el$.firstChild;

    addEventListener(_el$, "keydown", props.onKeyDown, true);

    addEventListener(_el$, "keyup", props.onKeyUp, true);

    addEventListener(_el$, "mouseleave", props.onMouseLeave);

    addEventListener(_el$, "mouseenter", props.onMouseEnter);

    addEventListener(_el$, "mouseover", props.onMouseOver, true);

    addEventListener(_el$, "click", props.onClick, true);

    insert(
      _el$,
      (() => {
        const _c$ = memo(() => props.children, true);

        return () =>
          _c$() ||
          (props.title &&
            (() => {
              const _el$3 = _tmpl$2.cloneNode(true);

              insert(_el$3, () => props.title);

              return _el$3;
            })());
      })(),
      null
    );

    effect(
      (_p$) => {
        const _v$ = props.fill || "currentColor",
          _v$2 = props.width || "24",
          _v$3 = props.height || "24",
          _v$4 = props["aria-label"],
          _v$5 = props["aria-labelledby"],
          _v$6 = props["aria-label"] || props["aria-labelledby"] || props.title,
          _v$7 =
            props["aria-label"] || props["aria-labelledby"] || props.title
              ? "img"
              : undefined,
          _v$8 = props.tabindex === "0" ? true : props.focusable,
          _v$9 = props.tabindex,
          _v$10 = props.id,
          _v$11 = props.class,
          _v$12 = props.title,
          _v$13 = props.style,
          _v$14 = props.stroke;

        _v$ !== _p$._v$ && setAttribute(_el$, "fill", (_p$._v$ = _v$));
        _v$2 !== _p$._v$2 && setAttribute(_el$, "width", (_p$._v$2 = _v$2));
        _v$3 !== _p$._v$3 && setAttribute(_el$, "height", (_p$._v$3 = _v$3));
        _v$4 !== _p$._v$4 &&
          setAttribute(_el$, "aria-label", (_p$._v$4 = _v$4));
        _v$5 !== _p$._v$5 &&
          setAttribute(_el$, "aria-labelledby", (_p$._v$5 = _v$5));
        _v$6 !== _p$._v$6 &&
          setAttribute(_el$, "aria-hidden", (_p$._v$6 = _v$6));
        _v$7 !== _p$._v$7 && setAttribute(_el$, "role", (_p$._v$7 = _v$7));
        _v$8 !== _p$._v$8 && setAttribute(_el$, "focusable", (_p$._v$8 = _v$8));
        _v$9 !== _p$._v$9 && setAttribute(_el$, "tabindex", (_p$._v$9 = _v$9));
        _v$10 !== _p$._v$10 && setAttribute(_el$, "id", (_p$._v$10 = _v$10));
        _v$11 !== _p$._v$11 && setAttribute(_el$, "class", (_p$._v$11 = _v$11));
        _v$12 !== _p$._v$12 && setAttribute(_el$, "title", (_p$._v$12 = _v$12));
        _p$._v$13 = style(_el$, _v$13, _p$._v$13);
        _v$14 !== _p$._v$14 &&
          setAttribute(_el$, "stroke", (_p$._v$14 = _v$14));
        return _p$;
      },
      {
        _v$: undefined,
        _v$2: undefined,
        _v$3: undefined,
        _v$4: undefined,
        _v$5: undefined,
        _v$6: undefined,
        _v$7: undefined,
        _v$8: undefined,
        _v$9: undefined,
        _v$10: undefined,
        _v$11: undefined,
        _v$12: undefined,
        _v$13: undefined,
        _v$14: undefined,
      }
    );

    return _el$;
  })();

delegateEvents(["click", "mouseover", "keyup", "keydown"]);

export default WatsonHealthDna24;
export { WatsonHealthDna24 };
