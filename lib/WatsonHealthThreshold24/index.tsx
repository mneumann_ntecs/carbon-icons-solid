import { CarbonIconComponent } from "../types";
export const WatsonHealthThreshold24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="WatsonHealthThreshold24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M26,4H6A2.0025,2.0025,0,0,0,4,6V26a2.0025,2.0025,0,0,0,2,2H26a2.0023,2.0023,0,0,0,2-2V6A2.0023,2.0023,0,0,0,26,4ZM6,6H26V16H24v2h2v2H24v2h2v2H24v2H22V24H20v2H18V24H16v2H14V24H12v2H10V24H8v2H6Z"></path>
    <path d="M8 20H10V22H8zM12 20H14V22H12zM16 20H18V22H16zM20 20H22V22H20zM8 16H10V18H8zM16 16H18V18H16zM20 16H22V18H20zM20 12H22V14H20zM20 8H22V10H20z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default WatsonHealthThreshold24;
