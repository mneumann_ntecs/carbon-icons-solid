import { CarbonIconComponent } from "../types";
export const Function32: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="Function32"
    fill={props.fill || "currentColor"}
    width={props.width || "32"}
    height={props.height || "32"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M19.23 29.6252l-.46-1.9462A12.0032 12.0032 0 0025.2312 8.3323l1.5376-1.2788A14.0029 14.0029 0 0119.23 29.6252zM23 6V4H17.9133a1.9906 1.9906 0 00-1.9919 1.8188L15.2686 13H11v2h4.0867l-1 11H9v2h5.0867a1.9906 1.9906 0 001.9919-1.8189L17.0952 15H22V13H17.2769l.6364-7zM5.2312 24.9465A14.0029 14.0029 0 0112.77 2.3748l.46 1.9462A12.0032 12.0032 0 006.7688 23.6677z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default Function32;
