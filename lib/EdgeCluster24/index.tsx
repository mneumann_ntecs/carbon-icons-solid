import { CarbonIconComponent } from "../types";
export const EdgeCluster24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="EdgeCluster24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M16 7a3 3 0 113-3A3.0033 3.0033 0 0116 7zm0-4a1 1 0 101 1A1.001 1.001 0 0016 3zM11 30a3 3 0 113-3A3.0033 3.0033 0 0111 30zm0-4a1 1 0 101 1A1.001 1.001 0 0011 26zM7 11a3 3 0 113-3A3.0033 3.0033 0 017 11zM7 7A1 1 0 108 8 1.001 1.001 0 007 7zM21 30a3 3 0 113-3A3.0033 3.0033 0 0121 30zm0-4a1 1 0 101 1A1.001 1.001 0 0021 26zM25 11a3 3 0 113-3A3.0033 3.0033 0 0125 11zm0-4a1 1 0 101 1A1.001 1.001 0 0025 7zM4 21a3 3 0 113-3A3.0033 3.0033 0 014 21zm0-4a1 1 0 101 1A1.001 1.001 0 004 17zM28 21a3 3 0 113-3A3.0033 3.0033 0 0128 21zm0-4a1 1 0 101 1A1.001 1.001 0 0028 17zM16 22a6 6 0 116-6A6.0069 6.0069 0 0116 22zm0-10a4 4 0 104 4A4.0045 4.0045 0 0016 12z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default EdgeCluster24;
