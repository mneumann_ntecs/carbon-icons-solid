import { CarbonIconComponent } from "../types";
export const Nominal24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="Nominal24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M24 28a6 6 0 116-6A6.0069 6.0069 0 0124 28zm0-10a4 4 0 104 4A4.0045 4.0045 0 0024 18zM8 28a6 6 0 116-6A6.0069 6.0069 0 018 28zM8 18a4 4 0 104 4A4.0045 4.0045 0 008 18zM16 14a6 6 0 116-6A6.0069 6.0069 0 0116 14zM16 4a4 4 0 104 4A4.0045 4.0045 0 0016 4z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default Nominal24;
