import { CarbonIconComponent } from "../types";
export const Policy16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="Policy16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M30 18A6 6 0 1020 22.46v7.54l4-1.8926 4 1.8926V22.46A5.98 5.98 0 0030 18zm-4 8.84l-2-.9467L22 26.84V23.65a5.8877 5.8877 0 004 0zM24 22a4 4 0 114-4A4.0045 4.0045 0 0124 22zM9 14H16V16H9zM9 8H19V10H9z"></path>
    <path d="M6,30a2.0021,2.0021,0,0,1-2-2V4A2.0021,2.0021,0,0,1,6,2H22a2.0021,2.0021,0,0,1,2,2V8H22V4H6V28H16v2Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default Policy16;
