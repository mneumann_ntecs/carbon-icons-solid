import { CarbonIconComponent } from "../types";
export const Sdk20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="Sdk20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M30 9L27.9 9 24 15.6 24 9 22 9 22 23 24 23 24 18.7 24.9 17.2 27.9 23 30 23 26.1 15.4 30 9zM16 23H12V9h4a4.0118 4.0118 0 014 4v6A4.0118 4.0118 0 0116 23zm-2-2h2a2.0059 2.0059 0 002-2V13a2.0059 2.0059 0 00-2-2H14zM8 23H2V21H8V17H4a2.0059 2.0059 0 01-2-2V11A2.0059 2.0059 0 014 9h6v2H4v4H8a2.0059 2.0059 0 012 2v4A2.0059 2.0059 0 018 23z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default Sdk20;
