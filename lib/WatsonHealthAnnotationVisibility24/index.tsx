import { CarbonIconComponent } from "../types";
export const WatsonHealthAnnotationVisibility24: CarbonIconComponent = (
  props
) => (
  <svg
    data-carbon-icon="WatsonHealthAnnotationVisibility24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M28,4H4A2,2,0,0,0,2,6V26a2,2,0,0,0,2,2H28a2,2,0,0,0,2-2V6A2,2,0,0,0,28,4Zm0,22H4V6H28Z"></path>
    <path d="M21 20H13a3.51 3.51 0 00-.88-1.86l3.8-6.64-1.74-1-3.78 6.62A3.35 3.35 0 009.5 17a3.5 3.5 0 103.15 5H21zM9.5 22A1.5 1.5 0 1111 20.5 1.5 1.5 0 019.5 22zM19 8H26V10H19zM19 12H23V14H19z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default WatsonHealthAnnotationVisibility24;
