import {
  template,
  addEventListener,
  insert,
  memo,
  effect,
  setAttribute,
  style,
  delegateEvents,
} from "solid-js/web";

const _tmpl$ = template(
    `<svg data-carbon-icon="LogoPython20" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" preserveAspectRatio="xMidYMid meet"><path d="M23.4882,9.14v2.9663a4.2843,4.2843,0,0,1-4.173,4.2352H12.6431a3.4077,3.4077,0,0,0-3.34,3.3945v6.3607c0,1.81,1.5742,2.8751,3.34,3.3945a11.1762,11.1762,0,0,0,6.6721,0c1.6817-.4869,3.34-1.4668,3.34-3.3945V23.55H15.983v-.8486H25.9951c1.9414,0,2.6648-1.3542,3.34-3.3867a11.4642,11.4642,0,0,0,0-6.7889c-.48-1.9327-1.3961-3.3867-3.34-3.3867ZM19.7356,25.2476a1.2731,1.2731,0,1,1-1.2534,1.269A1.2607,1.2607,0,0,1,19.7356,25.2476Z"></path><path fill="none" d="M19.7356,25.2476a1.2731,1.2731,0,1,1-1.2534,1.269A1.2607,1.2607,0,0,1,19.7356,25.2476Z"></path><path d="M15.8351,2a19.0717,19.0717,0,0,0-3.192.2725c-2.8268.4994-3.34,1.5447-3.34,3.4723V8.2909h6.68V9.14H6.7962a4.1708,4.1708,0,0,0-4.173,3.3867,12.4855,12.4855,0,0,0,0,6.7889c.4748,1.9776,1.6088,3.3867,3.55,3.3867H8.47V19.65a4.2449,4.2449,0,0,1,4.173-4.15h6.6721a3.3654,3.3654,0,0,0,3.34-3.3944V5.745a3.729,3.729,0,0,0-3.34-3.4723A20.8377,20.8377,0,0,0,15.8351,2ZM12.2226,4.0478a1.2731,1.2731,0,1,1-1.2534,1.2768A1.2675,1.2675,0,0,1,12.2226,4.0478Z"></path><path fill="none" d="M12.2226,4.0478a1.2731,1.2731,0,1,1-1.2534,1.2768A1.2675,1.2675,0,0,1,12.2226,4.0478Z"></path></svg>`,
    10
  ),
  _tmpl$2 = template(`<title></title>`, 2);

const LogoPython20 = (props) =>
  (() => {
    const _el$ = _tmpl$.cloneNode(true),
      _el$2 = _el$.firstChild,
      _el$3 = _el$2.nextSibling,
      _el$4 = _el$3.nextSibling;
    _el$4.nextSibling;

    addEventListener(_el$, "keydown", props.onKeyDown, true);

    addEventListener(_el$, "keyup", props.onKeyUp, true);

    addEventListener(_el$, "mouseleave", props.onMouseLeave);

    addEventListener(_el$, "mouseenter", props.onMouseEnter);

    addEventListener(_el$, "mouseover", props.onMouseOver, true);

    addEventListener(_el$, "click", props.onClick, true);

    insert(
      _el$,
      (() => {
        const _c$ = memo(() => props.children, true);

        return () =>
          _c$() ||
          (props.title &&
            (() => {
              const _el$6 = _tmpl$2.cloneNode(true);

              insert(_el$6, () => props.title);

              return _el$6;
            })());
      })(),
      null
    );

    effect(
      (_p$) => {
        const _v$ = props.fill || "currentColor",
          _v$2 = props.width || "20",
          _v$3 = props.height || "20",
          _v$4 = props["aria-label"],
          _v$5 = props["aria-labelledby"],
          _v$6 = props["aria-label"] || props["aria-labelledby"] || props.title,
          _v$7 =
            props["aria-label"] || props["aria-labelledby"] || props.title
              ? "img"
              : undefined,
          _v$8 = props.tabindex === "0" ? true : props.focusable,
          _v$9 = props.tabindex,
          _v$10 = props.id,
          _v$11 = props.class,
          _v$12 = props.title,
          _v$13 = props.style,
          _v$14 = props.stroke;

        _v$ !== _p$._v$ && setAttribute(_el$, "fill", (_p$._v$ = _v$));
        _v$2 !== _p$._v$2 && setAttribute(_el$, "width", (_p$._v$2 = _v$2));
        _v$3 !== _p$._v$3 && setAttribute(_el$, "height", (_p$._v$3 = _v$3));
        _v$4 !== _p$._v$4 &&
          setAttribute(_el$, "aria-label", (_p$._v$4 = _v$4));
        _v$5 !== _p$._v$5 &&
          setAttribute(_el$, "aria-labelledby", (_p$._v$5 = _v$5));
        _v$6 !== _p$._v$6 &&
          setAttribute(_el$, "aria-hidden", (_p$._v$6 = _v$6));
        _v$7 !== _p$._v$7 && setAttribute(_el$, "role", (_p$._v$7 = _v$7));
        _v$8 !== _p$._v$8 && setAttribute(_el$, "focusable", (_p$._v$8 = _v$8));
        _v$9 !== _p$._v$9 && setAttribute(_el$, "tabindex", (_p$._v$9 = _v$9));
        _v$10 !== _p$._v$10 && setAttribute(_el$, "id", (_p$._v$10 = _v$10));
        _v$11 !== _p$._v$11 && setAttribute(_el$, "class", (_p$._v$11 = _v$11));
        _v$12 !== _p$._v$12 && setAttribute(_el$, "title", (_p$._v$12 = _v$12));
        _p$._v$13 = style(_el$, _v$13, _p$._v$13);
        _v$14 !== _p$._v$14 &&
          setAttribute(_el$, "stroke", (_p$._v$14 = _v$14));
        return _p$;
      },
      {
        _v$: undefined,
        _v$2: undefined,
        _v$3: undefined,
        _v$4: undefined,
        _v$5: undefined,
        _v$6: undefined,
        _v$7: undefined,
        _v$8: undefined,
        _v$9: undefined,
        _v$10: undefined,
        _v$11: undefined,
        _v$12: undefined,
        _v$13: undefined,
        _v$14: undefined,
      }
    );

    return _el$;
  })();

delegateEvents(["click", "mouseover", "keyup", "keydown"]);

export default LogoPython20;
export { LogoPython20 };
