import { CarbonIconComponent } from "../types";
export const ChartBullet24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="ChartBullet24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M30 22H16V21H14v1H2v6H14v1h2V28H30zM4 26V24H14v2zm24 0H16V24H28zM30 13H24V12H22v1H2v6H22v1h2V19h6zM4 17V15H22v2zm24 0H24V15h4zM30 4H10V3H8V4H2v6H8v1h2V10H30zM4 8V6H8V8zM28 8H10V6H28z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default ChartBullet24;
