import { CarbonIconComponent } from "../types";
export const CharacterPatterns32: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="CharacterPatterns32"
    fill={props.fill || "currentColor"}
    width={props.width || "32"}
    height={props.height || "32"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M6 26V17.8281l-3.5859 3.586L1 20l6-6 6 6-1.4141 1.4141L8 17.8281V26H18v2H8A2.0024 2.0024 0 016 26zM30 22L30 20 22 20 22 22 27.5 22 22 28 22 30 30 30 30 28 24.507 28 30 22zM26 6v8.1719l3.5859-3.586L31 12l-6 6-6-6 1.4141-1.4141L24 14.1719V6H14V4H24A2.0024 2.0024 0 0126 6zM8 2H3V4H8V6H4A2 2 0 002 8v2a2 2 0 002 2h6V4A2.0023 2.0023 0 008 2zm0 8H4V8H8z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default CharacterPatterns32;
