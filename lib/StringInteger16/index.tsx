import { CarbonIconComponent } from "../types";
export const StringInteger16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="StringInteger16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M26 12H22v2h4v2H23v2h3v2H22v2h4a2.0027 2.0027 0 002-2V14A2.0023 2.0023 0 0026 12zM19 22H13V18a2.002 2.002 0 012-2h2V14H13V12h4a2.0023 2.0023 0 012 2v2a2.0023 2.0023 0 01-2 2H15v2h4zM8 20L8 12 6 12 6 13 4 13 4 15 6 15 6 20 4 20 4 22 10 22 10 20 8 20z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default StringInteger16;
