import { CarbonIconComponent } from "../types";
export const SpellCheck32: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="SpellCheck32"
    fill={props.fill || "currentColor"}
    width={props.width || "32"}
    height={props.height || "32"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M20,22h2L17,10H15L10,22h2l1.24-3h5.53Zm-5.93-5,1.82-4.42h.25L18,17Z"></path>
    <path d="M12 28H6a2 2 0 01-2-2V6A2 2 0 016 4H26a2 2 0 012 2V17H26V6H6V26h6zM23 27.18L20.41 24.59 19 26 23 30 30 23 28.59 21.59 23 27.18z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default SpellCheck32;
