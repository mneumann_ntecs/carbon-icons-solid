import { CarbonIconComponent } from "../types";
export const TextSelection24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="TextSelection24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M28 27H21a3 3 0 01-3-3V22a3 3 0 013-3h5V17a1 1 0 00-1-1H19V14h6a3 3 0 013 3zm-7-6a1 1 0 00-1 1v2a1 1 0 001 1h5V21zM13 7h3V5H13a4 4 0 00-3 1.38A4 4 0 007 5H4V7H7A2 2 0 019 9v5H5v2H9v7a2 2 0 01-2 2H4v2H7a4 4 0 003-1.38A4 4 0 0013 27h3V25H13a2 2 0 01-2-2V16h4V14H11V9A2 2 0 0113 7z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default TextSelection24;
