import { CarbonIconComponent } from "../types";
export const ChartColumnTarget16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="ChartColumnTarget16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M27 28V6H19V28H15V14H7V28H4V2H2V28a2 2 0 002 2H30V28zM13 28H9V16h4zm12 0H21V8h4zM19 2H27V4H19z"></path>
    <path d="M7 10H15V12H7z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default ChartColumnTarget16;
