import { CarbonIconComponent } from "../types";
export const InProgressWarning16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="InProgressWarning16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M27.38,28H20.6178L24,21.2358ZM24,18a1,1,0,0,0-.8947.5527l-5,10A1.0005,1.0005,0,0,0,19,30H29a1,1,0,0,0,.9214-1.3892L24.8946,18.5527A1,1,0,0,0,24,18Z"></path>
    <path d="M18.746,22.7993A9.999,9.999,0,1,1,14,4V14l6.0971,6.0972,1.22-2.44A2.9849,2.9849,0,0,1,24,16h1.8193A11.993,11.993,0,1,0,14,26a11.9337,11.9337,0,0,0,3.3939-.4966Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default InProgressWarning16;
