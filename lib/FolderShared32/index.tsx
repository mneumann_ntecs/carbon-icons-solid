import { CarbonIconComponent } from "../types";
export const FolderShared32: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="FolderShared32"
    fill={props.fill || "currentColor"}
    width={props.width || "32"}
    height={props.height || "32"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M28,8H16L12.59,4.59A2,2,0,0,0,11.17,4H4A2,2,0,0,0,2,6V26a2,2,0,0,0,2,2H28a2,2,0,0,0,2-2V10A2,2,0,0,0,28,8ZM22,26H14V25a1,1,0,0,1,1-1h6a1,1,0,0,1,1,1Zm6,0H24V25a3,3,0,0,0-3-3H15a3,3,0,0,0-3,3v1H4V6h7.17l3.42,3.41.58.59H28Z"></path>
    <path d="M14,17a4,4,0,1,0,4-4A4,4,0,0,0,14,17Zm4-2a2,2,0,1,1-2,2A2,2,0,0,1,18,15Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default FolderShared32;
