import {
  template,
  addEventListener,
  insert,
  memo,
  effect,
  setAttribute,
  style,
  delegateEvents,
} from "solid-js/web";

const _tmpl$ = template(
    `<svg data-carbon-icon="RadarEnhanced20" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" preserveAspectRatio="xMidYMid meet"><path d="M30,3.4146,28.5854,2,17.2932,13.2925a1,1,0,1,0,1.4141,1.414l3.4543-3.4541A4.9817,4.9817,0,0,1,18,19a5.0385,5.0385,0,0,1-.6074-.05,8.0432,8.0432,0,0,0-4.3421-4.3423A4.9,4.9,0,0,1,18,9V7a7.0078,7.0078,0,0,0-7,7c0,.0234.0056.0469.0059.07A8.0469,8.0469,0,0,0,10,14a7.95,7.95,0,0,0-1.9883.26C8.0095,14.1729,8,14.0879,8,14A10.0114,10.0114,0,0,1,18,4V2A12.0137,12.0137,0,0,0,6,14c0,.355.0171.7061.0471,1.0537A7.9943,7.9943,0,1,0,16.9465,25.9521c.35.03.7019.0479,1.0535.0479A11.9853,11.9853,0,0,0,27.1538,6.2607ZM15.91,21H13.9563a12.029,12.029,0,0,0-1.218-4.332A6.01,6.01,0,0,1,15.91,21ZM4.09,23H6.0435a12.0318,12.0318,0,0,0,1.2182,4.3325A6.01,6.01,0,0,1,4.09,23Zm1.9532-2H4.09a6.01,6.01,0,0,1,3.1714-4.332A12.0321,12.0321,0,0,0,6.0435,21ZM10,27.9858A10.0149,10.0149,0,0,1,8.0552,23h3.89A10.0149,10.0149,0,0,1,10,27.9858ZM8.0552,21A10.0118,10.0118,0,0,1,10,16.0151,10.01,10.01,0,0,1,11.9448,21Zm4.6831,6.3325A12.0318,12.0318,0,0,0,13.9565,23H15.91A6.01,6.01,0,0,1,12.7383,27.3325ZM28,14A10.0114,10.0114,0,0,1,18,24c-.0869,0-.1733-.01-.26-.0117a7.5527,7.5527,0,0,0,.19-2.9942c.0237,0,.0466.0059.07.0059A6.9991,6.9991,0,0,0,23.6055,9.8086l2.1313-2.1314A9.95,9.95,0,0,1,28,14Z"></path></svg>`,
    4
  ),
  _tmpl$2 = template(`<title></title>`, 2);

const RadarEnhanced20 = (props) =>
  (() => {
    const _el$ = _tmpl$.cloneNode(true);
    _el$.firstChild;

    addEventListener(_el$, "keydown", props.onKeyDown, true);

    addEventListener(_el$, "keyup", props.onKeyUp, true);

    addEventListener(_el$, "mouseleave", props.onMouseLeave);

    addEventListener(_el$, "mouseenter", props.onMouseEnter);

    addEventListener(_el$, "mouseover", props.onMouseOver, true);

    addEventListener(_el$, "click", props.onClick, true);

    insert(
      _el$,
      (() => {
        const _c$ = memo(() => props.children, true);

        return () =>
          _c$() ||
          (props.title &&
            (() => {
              const _el$3 = _tmpl$2.cloneNode(true);

              insert(_el$3, () => props.title);

              return _el$3;
            })());
      })(),
      null
    );

    effect(
      (_p$) => {
        const _v$ = props.fill || "currentColor",
          _v$2 = props.width || "20",
          _v$3 = props.height || "20",
          _v$4 = props["aria-label"],
          _v$5 = props["aria-labelledby"],
          _v$6 = props["aria-label"] || props["aria-labelledby"] || props.title,
          _v$7 =
            props["aria-label"] || props["aria-labelledby"] || props.title
              ? "img"
              : undefined,
          _v$8 = props.tabindex === "0" ? true : props.focusable,
          _v$9 = props.tabindex,
          _v$10 = props.id,
          _v$11 = props.class,
          _v$12 = props.title,
          _v$13 = props.style,
          _v$14 = props.stroke;

        _v$ !== _p$._v$ && setAttribute(_el$, "fill", (_p$._v$ = _v$));
        _v$2 !== _p$._v$2 && setAttribute(_el$, "width", (_p$._v$2 = _v$2));
        _v$3 !== _p$._v$3 && setAttribute(_el$, "height", (_p$._v$3 = _v$3));
        _v$4 !== _p$._v$4 &&
          setAttribute(_el$, "aria-label", (_p$._v$4 = _v$4));
        _v$5 !== _p$._v$5 &&
          setAttribute(_el$, "aria-labelledby", (_p$._v$5 = _v$5));
        _v$6 !== _p$._v$6 &&
          setAttribute(_el$, "aria-hidden", (_p$._v$6 = _v$6));
        _v$7 !== _p$._v$7 && setAttribute(_el$, "role", (_p$._v$7 = _v$7));
        _v$8 !== _p$._v$8 && setAttribute(_el$, "focusable", (_p$._v$8 = _v$8));
        _v$9 !== _p$._v$9 && setAttribute(_el$, "tabindex", (_p$._v$9 = _v$9));
        _v$10 !== _p$._v$10 && setAttribute(_el$, "id", (_p$._v$10 = _v$10));
        _v$11 !== _p$._v$11 && setAttribute(_el$, "class", (_p$._v$11 = _v$11));
        _v$12 !== _p$._v$12 && setAttribute(_el$, "title", (_p$._v$12 = _v$12));
        _p$._v$13 = style(_el$, _v$13, _p$._v$13);
        _v$14 !== _p$._v$14 &&
          setAttribute(_el$, "stroke", (_p$._v$14 = _v$14));
        return _p$;
      },
      {
        _v$: undefined,
        _v$2: undefined,
        _v$3: undefined,
        _v$4: undefined,
        _v$5: undefined,
        _v$6: undefined,
        _v$7: undefined,
        _v$8: undefined,
        _v$9: undefined,
        _v$10: undefined,
        _v$11: undefined,
        _v$12: undefined,
        _v$13: undefined,
        _v$14: undefined,
      }
    );

    return _el$;
  })();

delegateEvents(["click", "mouseover", "keyup", "keydown"]);

export default RadarEnhanced20;
export { RadarEnhanced20 };
