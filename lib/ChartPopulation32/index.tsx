import { CarbonIconComponent } from "../types";
export const ChartPopulation32: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="ChartPopulation32"
    fill={props.fill || "currentColor"}
    width={props.width || "32"}
    height={props.height || "32"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M30,22H17V20h9V14H17V12h5V6H17V2H15V6H10v6h5v2H6v6h9v2H2v6H15v2h2V28H30ZM20,8v2H17V8Zm-8,2V8h3v2Zm12,6v2H17V16ZM8,18V16h7v2ZM4,26V24H15v2Zm24,0H17V24H28Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default ChartPopulation32;
