import { CarbonIconComponent } from "../types";
export const Temperature20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="Temperature20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M13 17.26V6A4 4 0 005 6V17.26a7 7 0 108 0zM9 4a2 2 0 012 2v7H7V6A2 2 0 019 4zM9 28a5 5 0 01-2.5-9.33l.5-.28V15h4v3.39l.5.28A5 5 0 019 28zM20 4H30V6H20zM20 10H27V12H20zM20 16H30V18H20zM20 22H27V24H20z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default Temperature20;
