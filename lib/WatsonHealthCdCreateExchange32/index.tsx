import { CarbonIconComponent } from "../types";
export const WatsonHealthCdCreateExchange32: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="WatsonHealthCdCreateExchange32"
    fill={props.fill || "currentColor"}
    width={props.width || "32"}
    height={props.height || "32"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <circle cx="14" cy="14" r="2"></circle>
    <path d="M14,2a12,12,0,0,0,0,24V24A10,10,0,1,1,24,14,8.27,8.27,0,0,1,24,15h2c0-.33.05-.66.05-1A12,12,0,0,0,14,2Z"></path>
    <path d="M17 24L26.17 24 23.58 26.59 25 28 30 23 25 18 23.58 19.42 26.17 22 17 22 17 24zM14 20a6 6 0 116-6A6 6 0 0114 20zm0-10a4 4 0 104 4A4 4 0 0014 10z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default WatsonHealthCdCreateExchange32;
