import { CarbonIconComponent } from "../types";
export const PicnicArea16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="PicnicArea16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M24 12a4 4 0 114-4A4.0045 4.0045 0 0124 12zm0-6a2 2 0 102 2A2.0021 2.0021 0 0024 6zM26 22H21.8472L21.18 18H24V16H8v2h2.82l-.6668 4H6v2H9.82l-.6668 4H11.18l.6668-4h8.3056l.6668 4h2.0276L22.18 24H26zM12.18 22l.6665-4h6.3062l.6665 4z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default PicnicArea16;
