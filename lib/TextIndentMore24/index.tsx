import { CarbonIconComponent } from "../types";
export const TextIndentMore24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="TextIndentMore24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M20 6H30V8H20zM20 12H27V14H20zM20 18H30V20H20zM20 24H27V26H20zM14 4H16V28H14zM3.85 22.5L2.44 21.12 8.73 15 2.44 8.88 3.85 7.5 11.56 15 3.85 22.5z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default TextIndentMore24;
