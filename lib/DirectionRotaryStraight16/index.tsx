import { CarbonIconComponent } from "../types";
export const DirectionRotaryStraight16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="DirectionRotaryStraight16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M17,12.09V5.8281l4.5859,4.586L23,9,16,2,9,9l1.4141,1.4141L15,5.8281V12.09A5.9925,5.9925,0,0,0,15,23.91V28h2V23.91A5.9925,5.9925,0,0,0,17,12.09ZM16,22a4,4,0,1,1,4-4A4.0045,4.0045,0,0,1,16,22Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default DirectionRotaryStraight16;
