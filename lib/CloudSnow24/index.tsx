import { CarbonIconComponent } from "../types";
export const CloudSnow24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="CloudSnow24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M23.5 22H8.5A6.5 6.5 0 017.2 9.14a9 9 0 0117.6 0A6.5 6.5 0 0123.5 22zM16 4a7 7 0 00-6.94 6.14L9 11 8.14 11a4.5 4.5 0 00.36 9h15a4.5 4.5 0 00.36-9L23 11l-.1-.82A7 7 0 0016 4zM12 25.05L10.95 24 9.5 25.45 8.05 24 7 25.05 8.45 26.5 7 27.95 8.05 29 9.5 27.55 10.95 29 12 27.95 10.55 26.5 12 25.05zM26 25.05L24.95 24 23.5 25.45 22.05 24 21 25.05 22.45 26.5 21 27.95 22.05 29 23.5 27.55 24.95 29 26 27.95 24.55 26.5 26 25.05zM19 27.05L17.95 26 16.5 27.45 15.05 26 14 27.05 15.45 28.5 14 29.95 15.05 31 16.5 29.55 17.95 31 19 29.95 17.55 28.5 19 27.05z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default CloudSnow24;
