import { CarbonIconComponent } from "../types";
export const WorshipChristian16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="WorshipChristian16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M17,30H15a2.0021,2.0021,0,0,1-2-2V14H8a2.0021,2.0021,0,0,1-2-2V10A2.0021,2.0021,0,0,1,8,8h5V4a2.0021,2.0021,0,0,1,2-2h2a2.0021,2.0021,0,0,1,2,2V8h5a2.0021,2.0021,0,0,1,2,2v2a2.0021,2.0021,0,0,1-2,2H19V28A2.0021,2.0021,0,0,1,17,30ZM8,10v2h7V28h2V12h7V10H17V4H15v6Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default WorshipChristian16;
