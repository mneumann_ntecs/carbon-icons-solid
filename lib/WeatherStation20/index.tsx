import { CarbonIconComponent } from "../types";
export const WeatherStation20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="WeatherStation20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M17,28V17h1a2.0023,2.0023,0,0,0,2-2V11a2.0023,2.0023,0,0,0-2-2H14a2.0023,2.0023,0,0,0-2,2v4a2.0023,2.0023,0,0,0,2,2h1V28H2v2H30V28ZM14,11h4l.0015,4H14Z"></path>
    <path d="M9.3325 18.2168a7.0007 7.0007 0 010-10.4341l1.334 1.49a5 5 0 000 7.4537zM22.667 18.2168l-1.334-1.49a4.9995 4.9995 0 000-7.4537l1.334-1.49a7 7 0 010 10.4341z"></path>
    <path d="M6.3994 21.8008a11.0019 11.0019 0 010-17.6006L7.6 5.8a9.0009 9.0009 0 000 14.4014zM25.6006 21.8008l-1.2012-1.6a9.001 9.001 0 000-14.4019l1.2012-1.6a11.002 11.002 0 010 17.6011z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default WeatherStation20;
