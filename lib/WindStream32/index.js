import {
  template,
  addEventListener,
  insert,
  memo,
  effect,
  setAttribute,
  style,
  delegateEvents,
} from "solid-js/web";

const _tmpl$ = template(
    `<svg data-carbon-icon="WindStream32" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" preserveAspectRatio="xMidYMid meet"><path d="M25,2,23.5859,3.4141,26.1719,6h-5.09a16.93,16.93,0,0,0-6.3139,1.2158L10.4893,8.9272A14.93,14.93,0,0,1,4.9185,10H2v2H4.9185a16.93,16.93,0,0,0,6.3139-1.2158l4.2783-1.7114A14.93,14.93,0,0,1,21.0815,8h5.09l-2.586,2.5859L25,12l5-5Z"></path><path d="M21,11l-1.4141,1.4141L22.1719,15H18.9014a16.9422,16.9422,0,0,0-5.9693,1.0825l-2.5664.9624A14.9456,14.9456,0,0,1,5.0986,18H2v2H5.0986a16.9422,16.9422,0,0,0,5.9693-1.0825l2.5664-.9624A14.9456,14.9456,0,0,1,18.9014,17h3.2705l-2.586,2.5859L21,21l5-5Z"></path><path d="M17,20l-1.4141,1.4141L18.1719,24H16.5967a16.9879,16.9879,0,0,0-5.3765.8721l-1.0727.3584A14.9852,14.9852,0,0,1,5.4033,26H2v2H5.4033a16.9879,16.9879,0,0,0,5.3765-.8721l1.0727-.3584A14.9852,14.9852,0,0,1,16.5967,26h1.5752l-2.586,2.5859L17,30l5-5Z"></path></svg>`,
    8
  ),
  _tmpl$2 = template(`<title></title>`, 2);

const WindStream32 = (props) =>
  (() => {
    const _el$ = _tmpl$.cloneNode(true),
      _el$2 = _el$.firstChild,
      _el$3 = _el$2.nextSibling;
    _el$3.nextSibling;

    addEventListener(_el$, "keydown", props.onKeyDown, true);

    addEventListener(_el$, "keyup", props.onKeyUp, true);

    addEventListener(_el$, "mouseleave", props.onMouseLeave);

    addEventListener(_el$, "mouseenter", props.onMouseEnter);

    addEventListener(_el$, "mouseover", props.onMouseOver, true);

    addEventListener(_el$, "click", props.onClick, true);

    insert(
      _el$,
      (() => {
        const _c$ = memo(() => props.children, true);

        return () =>
          _c$() ||
          (props.title &&
            (() => {
              const _el$5 = _tmpl$2.cloneNode(true);

              insert(_el$5, () => props.title);

              return _el$5;
            })());
      })(),
      null
    );

    effect(
      (_p$) => {
        const _v$ = props.fill || "currentColor",
          _v$2 = props.width || "32",
          _v$3 = props.height || "32",
          _v$4 = props["aria-label"],
          _v$5 = props["aria-labelledby"],
          _v$6 = props["aria-label"] || props["aria-labelledby"] || props.title,
          _v$7 =
            props["aria-label"] || props["aria-labelledby"] || props.title
              ? "img"
              : undefined,
          _v$8 = props.tabindex === "0" ? true : props.focusable,
          _v$9 = props.tabindex,
          _v$10 = props.id,
          _v$11 = props.class,
          _v$12 = props.title,
          _v$13 = props.style,
          _v$14 = props.stroke;

        _v$ !== _p$._v$ && setAttribute(_el$, "fill", (_p$._v$ = _v$));
        _v$2 !== _p$._v$2 && setAttribute(_el$, "width", (_p$._v$2 = _v$2));
        _v$3 !== _p$._v$3 && setAttribute(_el$, "height", (_p$._v$3 = _v$3));
        _v$4 !== _p$._v$4 &&
          setAttribute(_el$, "aria-label", (_p$._v$4 = _v$4));
        _v$5 !== _p$._v$5 &&
          setAttribute(_el$, "aria-labelledby", (_p$._v$5 = _v$5));
        _v$6 !== _p$._v$6 &&
          setAttribute(_el$, "aria-hidden", (_p$._v$6 = _v$6));
        _v$7 !== _p$._v$7 && setAttribute(_el$, "role", (_p$._v$7 = _v$7));
        _v$8 !== _p$._v$8 && setAttribute(_el$, "focusable", (_p$._v$8 = _v$8));
        _v$9 !== _p$._v$9 && setAttribute(_el$, "tabindex", (_p$._v$9 = _v$9));
        _v$10 !== _p$._v$10 && setAttribute(_el$, "id", (_p$._v$10 = _v$10));
        _v$11 !== _p$._v$11 && setAttribute(_el$, "class", (_p$._v$11 = _v$11));
        _v$12 !== _p$._v$12 && setAttribute(_el$, "title", (_p$._v$12 = _v$12));
        _p$._v$13 = style(_el$, _v$13, _p$._v$13);
        _v$14 !== _p$._v$14 &&
          setAttribute(_el$, "stroke", (_p$._v$14 = _v$14));
        return _p$;
      },
      {
        _v$: undefined,
        _v$2: undefined,
        _v$3: undefined,
        _v$4: undefined,
        _v$5: undefined,
        _v$6: undefined,
        _v$7: undefined,
        _v$8: undefined,
        _v$9: undefined,
        _v$10: undefined,
        _v$11: undefined,
        _v$12: undefined,
        _v$13: undefined,
        _v$14: undefined,
      }
    );

    return _el$;
  })();

delegateEvents(["click", "mouseover", "keyup", "keydown"]);

export default WindStream32;
export { WindStream32 };
