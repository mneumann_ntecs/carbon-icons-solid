import {
  template,
  addEventListener,
  insert,
  memo,
  effect,
  setAttribute,
  style,
  delegateEvents,
} from "solid-js/web";

const _tmpl$ = template(
    `<svg data-carbon-icon="LinuxAlt20" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" preserveAspectRatio="xMidYMid meet"><path d="M22.6121,20.5215A6.1582,6.1582,0,0,0,24,16.5254C24,13.4785,21.9812,11,19.5,11A4.2435,4.2435,0,0,0,16,13.06,4.2435,4.2435,0,0,0,12.5,11C10.0188,11,8,13.4785,8,16.5254a6.1593,6.1593,0,0,0,1.3879,3.9961c-.5688.3686-.9389.6416-.988.6787a1,1,0,0,0-.1807,1.4248C8.6592,23.1748,12.6169,28,16,28s7.3408-4.8252,7.7808-5.375A1,1,0,0,0,23.6,21.2C23.551,21.1631,23.1812,20.89,22.6121,20.5215ZM12.5,13c1.3552,0,2.5,1.6143,2.5,3.5254v1.5664a9.1005,9.1005,0,0,0-1.0244.2314A2.6411,2.6411,0,0,0,14,18c0-1.1045-.6716-2-1.5-2s-1.5.8955-1.5,2a2.38,2.38,0,0,0,.4072,1.3623c-.0813.0415-.1687.0806-.248.1221A4.0291,4.0291,0,0,1,10,16.5254C10,14.6143,11.1448,13,12.5,13ZM16,26c-1.5691,0-3.9648-2.084-5.52-3.8057C11.9,21.2788,14.2656,20,16,20s4.1,1.2788,5.52,2.1943C19.9648,23.916,17.5691,26,16,26Zm4.8408-6.5156c-.0793-.0415-.1667-.0806-.248-.1221A2.38,2.38,0,0,0,21,18c0-1.1045-.6716-2-1.5-2s-1.5.8955-1.5,2a2.6411,2.6411,0,0,0,.0244.3232A9.1005,9.1005,0,0,0,17,18.0918V16.5254C17,14.6143,18.1448,13,19.5,13S22,14.6143,22,16.5254A4.0291,4.0291,0,0,1,20.8408,19.4844Z"></path><path d="M30,30a3.8876,3.8876,0,0,1-4-4V14A10,10,0,0,0,6,14V26a3.8876,3.8876,0,0,1-4,4V28a1.8793,1.8793,0,0,0,2-2V14a12,12,0,0,1,24,0V26a1.8825,1.8825,0,0,0,2,2Z"></path></svg>`,
    6
  ),
  _tmpl$2 = template(`<title></title>`, 2);

const LinuxAlt20 = (props) =>
  (() => {
    const _el$ = _tmpl$.cloneNode(true),
      _el$2 = _el$.firstChild;
    _el$2.nextSibling;

    addEventListener(_el$, "keydown", props.onKeyDown, true);

    addEventListener(_el$, "keyup", props.onKeyUp, true);

    addEventListener(_el$, "mouseleave", props.onMouseLeave);

    addEventListener(_el$, "mouseenter", props.onMouseEnter);

    addEventListener(_el$, "mouseover", props.onMouseOver, true);

    addEventListener(_el$, "click", props.onClick, true);

    insert(
      _el$,
      (() => {
        const _c$ = memo(() => props.children, true);

        return () =>
          _c$() ||
          (props.title &&
            (() => {
              const _el$4 = _tmpl$2.cloneNode(true);

              insert(_el$4, () => props.title);

              return _el$4;
            })());
      })(),
      null
    );

    effect(
      (_p$) => {
        const _v$ = props.fill || "currentColor",
          _v$2 = props.width || "20",
          _v$3 = props.height || "20",
          _v$4 = props["aria-label"],
          _v$5 = props["aria-labelledby"],
          _v$6 = props["aria-label"] || props["aria-labelledby"] || props.title,
          _v$7 =
            props["aria-label"] || props["aria-labelledby"] || props.title
              ? "img"
              : undefined,
          _v$8 = props.tabindex === "0" ? true : props.focusable,
          _v$9 = props.tabindex,
          _v$10 = props.id,
          _v$11 = props.class,
          _v$12 = props.title,
          _v$13 = props.style,
          _v$14 = props.stroke;

        _v$ !== _p$._v$ && setAttribute(_el$, "fill", (_p$._v$ = _v$));
        _v$2 !== _p$._v$2 && setAttribute(_el$, "width", (_p$._v$2 = _v$2));
        _v$3 !== _p$._v$3 && setAttribute(_el$, "height", (_p$._v$3 = _v$3));
        _v$4 !== _p$._v$4 &&
          setAttribute(_el$, "aria-label", (_p$._v$4 = _v$4));
        _v$5 !== _p$._v$5 &&
          setAttribute(_el$, "aria-labelledby", (_p$._v$5 = _v$5));
        _v$6 !== _p$._v$6 &&
          setAttribute(_el$, "aria-hidden", (_p$._v$6 = _v$6));
        _v$7 !== _p$._v$7 && setAttribute(_el$, "role", (_p$._v$7 = _v$7));
        _v$8 !== _p$._v$8 && setAttribute(_el$, "focusable", (_p$._v$8 = _v$8));
        _v$9 !== _p$._v$9 && setAttribute(_el$, "tabindex", (_p$._v$9 = _v$9));
        _v$10 !== _p$._v$10 && setAttribute(_el$, "id", (_p$._v$10 = _v$10));
        _v$11 !== _p$._v$11 && setAttribute(_el$, "class", (_p$._v$11 = _v$11));
        _v$12 !== _p$._v$12 && setAttribute(_el$, "title", (_p$._v$12 = _v$12));
        _p$._v$13 = style(_el$, _v$13, _p$._v$13);
        _v$14 !== _p$._v$14 &&
          setAttribute(_el$, "stroke", (_p$._v$14 = _v$14));
        return _p$;
      },
      {
        _v$: undefined,
        _v$2: undefined,
        _v$3: undefined,
        _v$4: undefined,
        _v$5: undefined,
        _v$6: undefined,
        _v$7: undefined,
        _v$8: undefined,
        _v$9: undefined,
        _v$10: undefined,
        _v$11: undefined,
        _v$12: undefined,
        _v$13: undefined,
        _v$14: undefined,
      }
    );

    return _el$;
  })();

delegateEvents(["click", "mouseover", "keyup", "keydown"]);

export default LinuxAlt20;
export { LinuxAlt20 };
