import { CarbonIconComponent } from "../types";
export const VideoOff20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="VideoOff20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M29.46 8.11a1 1 0 00-1 .08L23 12.06V10.44l7-7L28.56 2 2 28.56 3.44 30l4-4H21a2 2 0 002-2V19.94l5.42 3.87A1 1 0 0030 23V9A1 1 0 0029.46 8.11zM28 21.06l-5.42-3.87A1 1 0 0021 18v6H9.44L21 12.44V14a1 1 0 001.58.81L28 10.94zM4 24V8H20V6H4A2 2 0 002 8V24z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default VideoOff20;
