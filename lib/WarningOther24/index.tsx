import { CarbonIconComponent } from "../types";
export const WarningOther24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="WarningOther24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M18 20H30V22H18zM18 24H30V26H18zM18 28H30V30H18zM14 18a1.5 1.5 0 101.5 1.5A1.5 1.5 0 0014 18zM13 7H15V16H13z"></path>
    <path d="M14,4A10.0111,10.0111,0,0,1,24,14h2A12,12,0,1,0,14,26V24A10,10,0,0,1,14,4Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default WarningOther24;
