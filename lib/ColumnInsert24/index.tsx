import { CarbonIconComponent } from "../types";
export const ColumnInsert24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="ColumnInsert24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M28 30H22a2.0021 2.0021 0 01-2-2V10a2.0021 2.0021 0 012-2h6a2.0021 2.0021 0 012 2V28A2.0021 2.0021 0 0128 30zM22 10V28h6V10zM16 9L21.586 3.414 20.172 2 16 6.172 11.828 2 10.414 3.414 16 9zM10 30H4a2.0021 2.0021 0 01-2-2V10A2.0021 2.0021 0 014 8h6a2.0021 2.0021 0 012 2V28A2.0021 2.0021 0 0110 30zM4 10V28h6V10z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default ColumnInsert24;
