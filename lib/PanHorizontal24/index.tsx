import { CarbonIconComponent } from "../types";
export const PanHorizontal24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="PanHorizontal24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M24 10L22.586 11.414 26.172 15 5.828 15 9.414 11.414 8 10 2 16 8 22 9.414 20.586 5.828 17 26.172 17 22.586 20.586 24 22 30 16 24 10z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default PanHorizontal24;
