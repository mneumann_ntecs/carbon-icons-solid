import { CarbonIconComponent } from "../types";
export const Interactions24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="Interactions24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M26 18H22V14H20V28h6a2.0027 2.0027 0 002-2V20A2.0023 2.0023 0 0026 18zm-4 8V20h4v6zM20 6.076L19.256 4.219 16 5.522 16 2 14 2 14 5.523 10.744 4.22 10 6.077 13.417 7.444 10.9 10.8 12.5 12 15 8.667 17.5 12 19.1 10.8 16.583 7.443 20 6.076zM10 18H5v2h5v2H6a2 2 0 00-2 2v2a2 2 0 002 2h6V20A2.0023 2.0023 0 0010 18zm0 8H6V24h4z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default Interactions24;
