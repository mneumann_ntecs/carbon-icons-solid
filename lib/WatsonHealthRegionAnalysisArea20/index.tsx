import { CarbonIconComponent } from "../types";
export const WatsonHealthRegionAnalysisArea20: CarbonIconComponent = (
  props
) => (
  <svg
    data-carbon-icon="WatsonHealthRegionAnalysisArea20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M30,23V21H28V19H26v2H23V19H21v2H19v2h2v3H19v2h2v2h2V28h3v2h2V28h2V26H28V23Zm-4,3H23V23h3Z"></path>
    <path d="M16,30A14,14,0,1,1,30,16H28A12,12,0,1,0,16,28Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default WatsonHealthRegionAnalysisArea20;
