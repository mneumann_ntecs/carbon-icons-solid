import { CarbonIconComponent } from "../types";
export const WatsonHealthAutoScroll32: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="WatsonHealthAutoScroll32"
    fill={props.fill || "currentColor"}
    width={props.width || "32"}
    height={props.height || "32"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M12 16a4 4 0 104-4A4 4 0 0012 16zm6 0a2 2 0 11-2-2A2 2 0 0118 16zM16 27.17L10.4 21.58 9 23 16 30 23 23 21.59 21.59 16 27.17zM16 4.83L21.58 10.4 23 9 16 2 9 9 10.41 10.41 16 4.83z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default WatsonHealthAutoScroll32;
