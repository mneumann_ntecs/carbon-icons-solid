import { CarbonIconComponent } from "../types";
export const TextHighlight32: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="TextHighlight32"
    fill={props.fill || "currentColor"}
    width={props.width || "32"}
    height={props.height || "32"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M12 15H5a3 3 0 01-3-3V10A3 3 0 015 7h5V5A1 1 0 009 4H3V2H9a3 3 0 013 3zM5 9a1 1 0 00-1 1v2a1 1 0 001 1h5V9zM20 23v2a1 1 0 001 1h5V22H21A1 1 0 0020 23z"></path>
    <path d="M2,30H30V2Zm26-2H21a3,3,0,0,1-3-3V23a3,3,0,0,1,3-3h5V18a1,1,0,0,0-1-1H19V15h6a3,3,0,0,1,3,3Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default TextHighlight32;
