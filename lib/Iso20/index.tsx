import { CarbonIconComponent } from "../types";
export const Iso20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="Iso20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M28 23H24a2 2 0 01-2-2V11a2 2 0 012-2h4a2 2 0 012 2V21A2 2 0 0128 23zM24 11V21h4V11zM18 23H12V21h6V17H14a2 2 0 01-2-2V11a2 2 0 012-2h6v2H14v4h4a2 2 0 012 2v4A2 2 0 0118 23zM2 11L5 11 5 21 2 21 2 23 10 23 10 21 7 21 7 11 10 11 10 9 2 9 2 11z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default Iso20;
