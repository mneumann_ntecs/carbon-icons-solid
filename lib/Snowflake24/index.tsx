import { CarbonIconComponent } from "../types";
export const Snowflake24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="Snowflake24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M21.4148,12H28V10H23.4148l4.5857-4.5854L26.5864,4,22,8.5869V4H20v6.5869L18.5867,12H17V8H15v4H13.4133L12,10.5869V4H10V8.5869L5.4136,4,4,5.4146,8.5852,10H4v2h6.5852L12,13.415V15H8v2h4v1.5869L10.5867,20H4v2H8.5867L4,26.5864l1.4141,1.4141L10,23.415V28h2V21.415L13.4148,20H15v4h2V20h1.5852L20,21.415V28h2V23.415l4.5854,4.5855L28,26.5864,23.4133,22H28V20H21.4133L20,18.5869V17h4V15H20V13.415ZM18,18H14V14h4Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default Snowflake24;
