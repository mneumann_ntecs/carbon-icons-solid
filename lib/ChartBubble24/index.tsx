import { CarbonIconComponent } from "../types";
export const ChartBubble24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="ChartBubble24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M30,30H4a2,2,0,0,1-2-2V2H4V28H30Z"></path>
    <path d="M10 16a2 2 0 11-2 2 2 2 0 012-2m0-2a4 4 0 104 4 4 4 0 00-4-4zM21 6a4 4 0 11-4 4 4 4 0 014-4m0-2a6 6 0 106 6 6 6 0 00-6-6zM24.5 21A1.5 1.5 0 1123 22.5 1.5 1.5 0 0124.5 21m0-2A3.5 3.5 0 1028 22.5 3.5 3.5 0 0024.5 19z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default ChartBubble24;
