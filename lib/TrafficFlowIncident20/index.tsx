import { CarbonIconComponent } from "../types";
export const TrafficFlowIncident20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="TrafficFlowIncident20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M26 9.42L24.59 8 22.005 10.585 22 10.58 21.995 10.585 19.42 8.01 18 9.42 20.58 12 18 14.58 19.41 16 21.995 13.415 22 13.42 22.005 13.415 24.58 15.99 26 14.58 23.42 12 26 9.42zM14 17.42L12.59 16 10.005 18.585 10 18.58 9.995 18.585 7.42 16.01 6 17.42 8.58 20 6 22.58 7.41 24 9.995 21.415 10 21.42 10.005 21.415 12.58 23.99 14 22.58 11.42 20 14 17.42zM2 2H4V30H2zM28 2H30V30H28zM15 2H17V6H15zM15 10H17V14H15zM15 18H17V22H15zM15 26H17V30H15z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default TrafficFlowIncident20;
