import { CarbonIconComponent } from "../types";
export const ChartBarFloating32: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="ChartBarFloating32"
    fill={props.fill || "currentColor"}
    width={props.width || "32"}
    height={props.height || "32"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M28 24H14V16H28zM16 22H26V18H16zM26 12H8V4H26zM10 10H24V6H10z"></path>
    <path d="M30,30H4a2.0023,2.0023,0,0,1-2-2V2H4V28H30Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default ChartBarFloating32;
