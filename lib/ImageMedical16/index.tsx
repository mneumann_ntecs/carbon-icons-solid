import { CarbonIconComponent } from "../types";
export const ImageMedical16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="ImageMedical16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M26,6V26H6V6H26m0-2H6A2,2,0,0,0,4,6V26a2,2,0,0,0,2,2H26a2,2,0,0,0,2-2V6a2,2,0,0,0-2-2Z"></path>
    <path d="M22 16L22 14 17 14 17 12 20 12 20 10 17 10 17 8 15 8 15 10 12 10 12 12 15 12 15 14 10 14 10 16 15 16 15 18 10 18 10 20 15 20 15 22 12 22 12 24 20 24 20 22 17 22 17 20 22 20 22 18 17 18 17 16 22 16z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default ImageMedical16;
