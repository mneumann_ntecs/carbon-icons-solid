import { CarbonIconComponent } from "../types";
export const WatsonHealthCrossReference32: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="WatsonHealthCrossReference32"
    fill={props.fill || "currentColor"}
    width={props.width || "32"}
    height={props.height || "32"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M11 24H15V26H11zM5 24H9V26H5zM23 24H27V26H23zM17 24H21V26H17zM9 22a4.92 4.92 0 014-2h6a5.22 5.22 0 014 2h2.3A6.87 6.87 0 0019 18H13a6.87 6.87 0 00-6.3 4zM24 28H26V30H24zM6 28H8V30H6zM16 16a7 7 0 117-7A7 7 0 0116 16zM16 4a5 5 0 00-5 5A5 5 0 0021 9 5 5 0 0016 4z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default WatsonHealthCrossReference32;
