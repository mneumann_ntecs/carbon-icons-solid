import {
  template,
  addEventListener,
  insert,
  memo,
  effect,
  setAttribute,
  style,
  delegateEvents,
} from "solid-js/web";

const _tmpl$ = template(
    `<svg data-carbon-icon="BeeBat32" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" preserveAspectRatio="xMidYMid meet"><circle cx="13" cy="8" r="2"></circle><circle cx="19" cy="8" r="2"></circle><path d="M31.8083,8.665A.8745.8745,0,0,0,31,8.125H29c-4.21,0-7.721,2.002-9.1648,4.9014a4.8186,4.8186,0,0,0-7.67,0C10.721,10.127,7.21,8.125,3,8.125H1A.8749.8749,0,0,0,.3816,9.6191L.4834,9.72A5.3885,5.3885,0,0,1,2.1612,15.751.8751.8751,0,0,0,3,16.875a2.8966,2.8966,0,0,1,2.1587.7266c.9783.9814.9714,2.8872.9668,4.1479l0,.25A.875.875,0,0,0,7,22.875h4.125V23a4.875,4.875,0,0,0,9.75,0v-.125H25A.875.875,0,0,0,25.875,22l0-.25c-.0046-1.2607-.0115-3.1665.9668-4.1479A2.8966,2.8966,0,0,1,29,16.875a.8751.8751,0,0,0,.8388-1.124,5.3883,5.3883,0,0,1,1.6765-6.0293l.1033-.103A.875.875,0,0,0,31.8083,8.665ZM16,12.875A3.1286,3.1286,0,0,1,19.125,16v.125h-6.25V16A3.1286,3.1286,0,0,1,16,12.875Zm-3.125,5h6.25v3.25h-6.25Zm-5.0007,3.25c-.0178-1.459-.1839-3.4619-1.4761-4.7588a4.1876,4.1876,0,0,0-2.31-1.1518A6.657,6.657,0,0,0,2.9524,9.875H3c4.5562,0,8.125,2.69,8.125,6.125v5.125ZM19.125,23a3.125,3.125,0,0,1-6.25,0v-.125h6.25Zm8.7868-7.7856a4.1876,4.1876,0,0,0-2.31,1.1518c-1.2922,1.2969-1.4583,3.3-1.4761,4.7588H20.875V16c0-3.4346,3.5688-6.125,8.125-6.125h.0481A6.6561,6.6561,0,0,0,27.9118,15.2144Z"></path></svg>`,
    8
  ),
  _tmpl$2 = template(`<title></title>`, 2);

const BeeBat32 = (props) =>
  (() => {
    const _el$ = _tmpl$.cloneNode(true),
      _el$2 = _el$.firstChild,
      _el$3 = _el$2.nextSibling;
    _el$3.nextSibling;

    addEventListener(_el$, "keydown", props.onKeyDown, true);

    addEventListener(_el$, "keyup", props.onKeyUp, true);

    addEventListener(_el$, "mouseleave", props.onMouseLeave);

    addEventListener(_el$, "mouseenter", props.onMouseEnter);

    addEventListener(_el$, "mouseover", props.onMouseOver, true);

    addEventListener(_el$, "click", props.onClick, true);

    insert(
      _el$,
      (() => {
        const _c$ = memo(() => props.children, true);

        return () =>
          _c$() ||
          (props.title &&
            (() => {
              const _el$5 = _tmpl$2.cloneNode(true);

              insert(_el$5, () => props.title);

              return _el$5;
            })());
      })(),
      null
    );

    effect(
      (_p$) => {
        const _v$ = props.fill || "currentColor",
          _v$2 = props.width || "32",
          _v$3 = props.height || "32",
          _v$4 = props["aria-label"],
          _v$5 = props["aria-labelledby"],
          _v$6 = props["aria-label"] || props["aria-labelledby"] || props.title,
          _v$7 =
            props["aria-label"] || props["aria-labelledby"] || props.title
              ? "img"
              : undefined,
          _v$8 = props.tabindex === "0" ? true : props.focusable,
          _v$9 = props.tabindex,
          _v$10 = props.id,
          _v$11 = props.class,
          _v$12 = props.title,
          _v$13 = props.style,
          _v$14 = props.stroke;

        _v$ !== _p$._v$ && setAttribute(_el$, "fill", (_p$._v$ = _v$));
        _v$2 !== _p$._v$2 && setAttribute(_el$, "width", (_p$._v$2 = _v$2));
        _v$3 !== _p$._v$3 && setAttribute(_el$, "height", (_p$._v$3 = _v$3));
        _v$4 !== _p$._v$4 &&
          setAttribute(_el$, "aria-label", (_p$._v$4 = _v$4));
        _v$5 !== _p$._v$5 &&
          setAttribute(_el$, "aria-labelledby", (_p$._v$5 = _v$5));
        _v$6 !== _p$._v$6 &&
          setAttribute(_el$, "aria-hidden", (_p$._v$6 = _v$6));
        _v$7 !== _p$._v$7 && setAttribute(_el$, "role", (_p$._v$7 = _v$7));
        _v$8 !== _p$._v$8 && setAttribute(_el$, "focusable", (_p$._v$8 = _v$8));
        _v$9 !== _p$._v$9 && setAttribute(_el$, "tabindex", (_p$._v$9 = _v$9));
        _v$10 !== _p$._v$10 && setAttribute(_el$, "id", (_p$._v$10 = _v$10));
        _v$11 !== _p$._v$11 && setAttribute(_el$, "class", (_p$._v$11 = _v$11));
        _v$12 !== _p$._v$12 && setAttribute(_el$, "title", (_p$._v$12 = _v$12));
        _p$._v$13 = style(_el$, _v$13, _p$._v$13);
        _v$14 !== _p$._v$14 &&
          setAttribute(_el$, "stroke", (_p$._v$14 = _v$14));
        return _p$;
      },
      {
        _v$: undefined,
        _v$2: undefined,
        _v$3: undefined,
        _v$4: undefined,
        _v$5: undefined,
        _v$6: undefined,
        _v$7: undefined,
        _v$8: undefined,
        _v$9: undefined,
        _v$10: undefined,
        _v$11: undefined,
        _v$12: undefined,
        _v$13: undefined,
        _v$14: undefined,
      }
    );

    return _el$;
  })();

delegateEvents(["click", "mouseover", "keyup", "keydown"]);

export default BeeBat32;
export { BeeBat32 };
