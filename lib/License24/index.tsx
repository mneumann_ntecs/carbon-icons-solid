import { CarbonIconComponent } from "../types";
export const License24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="License24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M10 6H22V8H10zM10 10H22V12H10zM10 24H16V26H10zM10 14H16V16H10z"></path>
    <path d="M24,30H8a2.0023,2.0023,0,0,1-2-2V4A2.0023,2.0023,0,0,1,8,2H24a2.0023,2.0023,0,0,1,2,2V28A2.0023,2.0023,0,0,1,24,30ZM8,4V28H24V4Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default License24;
