import { CarbonIconComponent } from "../types";
export const ChartEvaluation16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="ChartEvaluation16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path
      d="M25.672 6.207H29.915V8.206H25.672z"
      transform="rotate(-45.02 27.793 7.207)"
    ></path>
    <path d="M16 20.5L11 15.539 12.59 13.969 16 17.349 23.41 10 25 11.579 16 20.5z"></path>
    <path d="M4,28V25.4131L10.4141,19,9,17.585l-5,5V2H2V28a2,2,0,0,0,2,2H30V28Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default ChartEvaluation16;
