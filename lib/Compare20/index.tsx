import { CarbonIconComponent } from "../types";
export const Compare20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="Compare20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M28,6H18V4a2,2,0,0,0-2-2H4A2,2,0,0,0,2,4V24a2,2,0,0,0,2,2H14v2a2,2,0,0,0,2,2H28a2,2,0,0,0,2-2V8A2,2,0,0,0,28,6ZM4,15h6.17L7.59,17.59,9,19l5-5L9,9,7.59,10.41,10.17,13H4V4H16V24H4ZM16,28V26a2,2,0,0,0,2-2V8H28v9H21.83l2.58-2.59L23,13l-5,5,5,5,1.41-1.41L21.83,19H28v9Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default Compare20;
