import { CarbonIconComponent } from "../types";
export const Forward_1016: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="Forward_1016"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M26,18A10,10,0,1,1,16,8h4v5l6-6L20,1V6H16A12,12,0,1,0,28,18Z"></path>
    <path d="M19.63 22.13a2.84 2.84 0 01-1.28-.27 2.44 2.44 0 01-.89-.77 3.57 3.57 0 01-.52-1.25 7.69 7.69 0 01-.17-1.68 7.83 7.83 0 01.17-1.68 3.65 3.65 0 01.52-1.25 2.44 2.44 0 01.89-.77 2.84 2.84 0 011.28-.27 2.44 2.44 0 012.16 1 5.23 5.23 0 01.7 2.93 5.23 5.23 0 01-.7 2.93A2.44 2.44 0 0119.63 22.13zm0-1.22a1.07 1.07 0 001-.55A3.38 3.38 0 0021 18.85V17.47a3.31 3.31 0 00-.29-1.5 1.23 1.23 0 00-2.06 0 3.31 3.31 0 00-.29 1.5v1.38a3.38 3.38 0 00.29 1.51A1.06 1.06 0 0019.63 20.91zM10.63 22V20.82h2V15.63l-1.86 1-.55-1.06 2.32-1.3H14v6.5h1.78V22z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default Forward_1016;
