import { CarbonIconComponent } from "../types";
export const DirectionUTurn16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="DirectionUTurn16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M26.5859,19.5859,22,24.1719V8a2.0023,2.0023,0,0,0-2-2H8A2.0023,2.0023,0,0,0,6,8V28H8V8H20V24.1719l-4.5859-4.586L14,21l7,7,7-7Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default DirectionUTurn16;
