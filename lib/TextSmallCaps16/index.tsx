import { CarbonIconComponent } from "../types";
export const TextSmallCaps16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="TextSmallCaps16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M23 27L23 15 18 15 18 13 30 13 30 15 25 15 25 27 23 27z"></path>
    <path d="M11 27L11 8 2 8 2 6 22 6 22 8 13 8 13 27 11 27z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default TextSmallCaps16;
