import { CarbonIconComponent } from "../types";
export const LogoDigg16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="LogoDigg16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M6.48 7.6v3.92H2V21H9.28V7.6zm0 11.2H4.8v-5H6.48zM10.4 11.52H13.2V21.04H10.4zM10.4 7.6H13.2V10.399999999999999H10.4zM14.32 11.52V21H18.8v1.12H14.32V24.4H21.6V11.52zM18.8 18.8H17.12v-5H18.8zM22.72 11.52V21H27.2v1.12H22.72V24.4H30V11.52zM27.2 18.8H25.52v-5H27.2z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default LogoDigg16;
