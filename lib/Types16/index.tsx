import { CarbonIconComponent } from "../types";
export const Types16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="Types16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M30 16L22 24 20.586 22.586 27.172 16 20.586 9.414 22 8 30 16z"></path>
    <path d="M16,22a.9967.9967,0,0,1-.707-.293l-5-5a.9994.9994,0,0,1,0-1.414l5-5a.9994.9994,0,0,1,1.414,0l5,5a.9994.9994,0,0,1,0,1.414l-5,5A.9967.9967,0,0,1,16,22Zm-3.5859-6L16,19.5859,19.5859,16,16,12.4141Z"></path>
    <path d="M2 16L10 8 11.414 9.414 4.828 16 11.414 22.586 10 24 2 16z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default Types16;
