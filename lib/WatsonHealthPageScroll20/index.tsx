import { CarbonIconComponent } from "../types";
export const WatsonHealthPageScroll20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="WatsonHealthPageScroll20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M16,28H4a1.89,1.89,0,0,1-2-2V14a1.89,1.89,0,0,1,2-2H16a1.89,1.89,0,0,1,2,2V26A1.89,1.89,0,0,1,16,28ZM4,14V26H16V14Z"></path>
    <path d="M22,19H20V10H10V8H20a1.89,1.89,0,0,1,2,2Z"></path>
    <path d="M26 14H24V6H16V4h8a1.89 1.89 0 012 2zM24 17L24 19 26.8 19 22 24.4 22 22 20 22 20 28 26 28 26 26 23.2 26 28 20.6 28 23 30 23 30 17 24 17z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default WatsonHealthPageScroll20;
