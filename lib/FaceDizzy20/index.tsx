import { CarbonIconComponent } from "../types";
export const FaceDizzy20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="FaceDizzy20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M16,2A14,14,0,1,0,30,16,14,14,0,0,0,16,2Zm0,26A12,12,0,1,1,28,16,12,12,0,0,1,16,28Z"></path>
    <path d="M24.41 11L23 9.59 21 11.59 19 9.59 17.59 11 19.59 13 17.59 15 19 16.41 21 14.41 23 16.41 24.41 15 22.41 13 24.41 11zM14.41 11L13 9.59 11 11.59 9 9.59 7.59 11 9.59 13 7.59 15 9 16.41 11 14.41 13 16.41 14.41 15 12.41 13 14.41 11zM16 19a3 3 0 103 3 3 3 0 00-3-3z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default FaceDizzy20;
