import {
  template,
  addEventListener,
  insert,
  memo,
  effect,
  setAttribute,
  style,
  delegateEvents,
} from "solid-js/web";

const _tmpl$ = template(
    `<svg data-carbon-icon="Smoke20" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" preserveAspectRatio="xMidYMid meet"><path d="M27.001 28a.998.998 0 01-.8008-.4l-.8145-1.086a8.9976 8.9976 0 01-1.6005-7.2856l1.4736-6.8762a6.9956 6.9956 0 00-1.2446-5.6668L23.2 5.6a1 1 0 111.6-1.2l.8145 1.0857a8.9976 8.9976 0 011.6005 7.2856l-1.4736 6.8762a6.9956 6.9956 0 001.2446 5.6668L27.8 26.4A1 1 0 0127.001 28zM22.001 28a.998.998 0 01-.8008-.4l-.8145-1.086a8.9976 8.9976 0 01-1.6005-7.2856l1.4736-6.8762a6.9956 6.9956 0 00-1.2446-5.6668L18.2 5.6a1 1 0 111.6-1.2l.8145 1.0857a8.9976 8.9976 0 011.6005 7.2856l-1.4736 6.8762a6.9956 6.9956 0 001.2446 5.6668L22.8 26.4A1 1 0 0122.001 28zM17.001 28a.998.998 0 01-.8008-.4l-.8145-1.086a8.9976 8.9976 0 01-1.6005-7.2856l1.4736-6.8762a6.9956 6.9956 0 00-1.2446-5.6668L13.2 5.6a1 1 0 111.6-1.2l.8145 1.0857a8.9976 8.9976 0 011.6005 7.2856l-1.4736 6.8762a6.9956 6.9956 0 001.2446 5.6668L17.8 26.4A1 1 0 0117.001 28zM12.001 28a.998.998 0 01-.8008-.4l-.8145-1.086a8.9976 8.9976 0 01-1.6005-7.2856l1.4736-6.8762A6.9956 6.9956 0 009.0142 6.6855L8.2 5.6A1 1 0 119.8 4.4l.8145 1.0857a8.9976 8.9976 0 011.6005 7.2856l-1.4736 6.8762a6.9956 6.9956 0 001.2446 5.6668L12.8 26.4A1 1 0 0112.001 28zM7.001 28A.998.998 0 016.2 27.6l-.8145-1.086a8.9976 8.9976 0 01-1.6-7.2856l1.4736-6.8762A6.9956 6.9956 0 004.0142 6.6855L3.2 5.6A1 1 0 114.8 4.4l.8145 1.0857a8.9976 8.9976 0 011.6005 7.2856L5.7412 19.6474a6.9956 6.9956 0 001.2446 5.6668L7.8 26.4A1 1 0 017.001 28z"></path></svg>`,
    4
  ),
  _tmpl$2 = template(`<title></title>`, 2);

const Smoke20 = (props) =>
  (() => {
    const _el$ = _tmpl$.cloneNode(true);
    _el$.firstChild;

    addEventListener(_el$, "keydown", props.onKeyDown, true);

    addEventListener(_el$, "keyup", props.onKeyUp, true);

    addEventListener(_el$, "mouseleave", props.onMouseLeave);

    addEventListener(_el$, "mouseenter", props.onMouseEnter);

    addEventListener(_el$, "mouseover", props.onMouseOver, true);

    addEventListener(_el$, "click", props.onClick, true);

    insert(
      _el$,
      (() => {
        const _c$ = memo(() => props.children, true);

        return () =>
          _c$() ||
          (props.title &&
            (() => {
              const _el$3 = _tmpl$2.cloneNode(true);

              insert(_el$3, () => props.title);

              return _el$3;
            })());
      })(),
      null
    );

    effect(
      (_p$) => {
        const _v$ = props.fill || "currentColor",
          _v$2 = props.width || "20",
          _v$3 = props.height || "20",
          _v$4 = props["aria-label"],
          _v$5 = props["aria-labelledby"],
          _v$6 = props["aria-label"] || props["aria-labelledby"] || props.title,
          _v$7 =
            props["aria-label"] || props["aria-labelledby"] || props.title
              ? "img"
              : undefined,
          _v$8 = props.tabindex === "0" ? true : props.focusable,
          _v$9 = props.tabindex,
          _v$10 = props.id,
          _v$11 = props.class,
          _v$12 = props.title,
          _v$13 = props.style,
          _v$14 = props.stroke;

        _v$ !== _p$._v$ && setAttribute(_el$, "fill", (_p$._v$ = _v$));
        _v$2 !== _p$._v$2 && setAttribute(_el$, "width", (_p$._v$2 = _v$2));
        _v$3 !== _p$._v$3 && setAttribute(_el$, "height", (_p$._v$3 = _v$3));
        _v$4 !== _p$._v$4 &&
          setAttribute(_el$, "aria-label", (_p$._v$4 = _v$4));
        _v$5 !== _p$._v$5 &&
          setAttribute(_el$, "aria-labelledby", (_p$._v$5 = _v$5));
        _v$6 !== _p$._v$6 &&
          setAttribute(_el$, "aria-hidden", (_p$._v$6 = _v$6));
        _v$7 !== _p$._v$7 && setAttribute(_el$, "role", (_p$._v$7 = _v$7));
        _v$8 !== _p$._v$8 && setAttribute(_el$, "focusable", (_p$._v$8 = _v$8));
        _v$9 !== _p$._v$9 && setAttribute(_el$, "tabindex", (_p$._v$9 = _v$9));
        _v$10 !== _p$._v$10 && setAttribute(_el$, "id", (_p$._v$10 = _v$10));
        _v$11 !== _p$._v$11 && setAttribute(_el$, "class", (_p$._v$11 = _v$11));
        _v$12 !== _p$._v$12 && setAttribute(_el$, "title", (_p$._v$12 = _v$12));
        _p$._v$13 = style(_el$, _v$13, _p$._v$13);
        _v$14 !== _p$._v$14 &&
          setAttribute(_el$, "stroke", (_p$._v$14 = _v$14));
        return _p$;
      },
      {
        _v$: undefined,
        _v$2: undefined,
        _v$3: undefined,
        _v$4: undefined,
        _v$5: undefined,
        _v$6: undefined,
        _v$7: undefined,
        _v$8: undefined,
        _v$9: undefined,
        _v$10: undefined,
        _v$11: undefined,
        _v$12: undefined,
        _v$13: undefined,
        _v$14: undefined,
      }
    );

    return _el$;
  })();

delegateEvents(["click", "mouseover", "keyup", "keydown"]);

export default Smoke20;
export { Smoke20 };
