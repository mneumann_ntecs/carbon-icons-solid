import { CarbonIconComponent } from "../types";
export const JsonReference24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="JsonReference24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M4 20L4 22 8.586 22 2 28.586 3.414 30 10 23.414 10 28 12 28 12 20 4 20zM29 12L27 6 25 6 25 16 27 16 27 10 29 16 31 16 31 6 29 6 29 12zM21.3335 6h-2.667A1.6683 1.6683 0 0017 7.6665v6.667A1.6684 1.6684 0 0018.6665 16h2.667A1.6683 1.6683 0 0023 14.3335V7.6665A1.6683 1.6683 0 0021.3335 6zM21 14H19V8h2zM9 7.6665V10a2.002 2.002 0 002 2h2v2H9v2h4.3335A1.6683 1.6683 0 0015 14.3335V12a2.002 2.002 0 00-2-2H11V8h4V6H10.6665A1.6683 1.6683 0 009 7.6665zM5 14H3V12H1v2.3335A1.6684 1.6684 0 002.6665 16h2.667A1.6683 1.6683 0 007 14.3335V6H5z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default JsonReference24;
