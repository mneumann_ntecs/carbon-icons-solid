import { CarbonIconComponent } from "../types";
export const Roadmap24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="Roadmap24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M12 30H4a2.0023 2.0023 0 01-2-2V24a2.0023 2.0023 0 012-2h8a2.0023 2.0023 0 012 2v4A2.0023 2.0023 0 0112 30zM4 24v4h8V24zM28 20H12a2.0023 2.0023 0 01-2-2V14a2.0023 2.0023 0 012-2H28a2.0023 2.0023 0 012 2v4A2.0023 2.0023 0 0128 20zM12 14v4H28V14zM16 10H4A2.0023 2.0023 0 012 8V4A2.0023 2.0023 0 014 2H16a2.0023 2.0023 0 012 2V8A2.0023 2.0023 0 0116 10zM4 4V8H16V4z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default Roadmap24;
