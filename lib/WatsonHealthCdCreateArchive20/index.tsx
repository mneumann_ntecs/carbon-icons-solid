import { CarbonIconComponent } from "../types";
export const WatsonHealthCdCreateArchive20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="WatsonHealthCdCreateArchive20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M14,20a6,6,0,1,1,6-6A6,6,0,0,1,14,20Zm0-10a4,4,0,1,0,4,4A4,4,0,0,0,14,10Z"></path>
    <circle cx="14" cy="14" r="2"></circle>
    <path d="M17,23.54A10,10,0,1,1,24,14c0,.34,0,.67-.05,1h2c0-.33.05-.66.05-1A12,12,0,1,0,14,26a12.33,12.33,0,0,0,3-.39Z"></path>
    <path d="M25,30l-2.14-1A5,5,0,0,1,20,24.47V18H30v6.47A5,5,0,0,1,27.14,29ZM22,20v4.47a3,3,0,0,0,1.72,2.71l1.28.61,1.28-.61A3,3,0,0,0,28,24.47V20Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default WatsonHealthCdCreateArchive20;
