import { CarbonIconComponent } from "../types";
export const Fish20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="Fish20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <circle cx="24" cy="7" r="1"></circle>
    <path d="M26,2A16.0183,16.0183,0,0,0,10,18v2H3a1,1,0,0,0-.707,1.707l8,8A1,1,0,0,0,12,29V22h2A16.0183,16.0183,0,0,0,30,6V2ZM10,26.5857,5.4141,22H10ZM12,20V18a13.9394,13.9394,0,0,1,3.908-9.6777l7.77,7.7695A13.94,13.94,0,0,1,14,20ZM28,6a13.9163,13.9163,0,0,1-2.98,8.6055L17.3945,6.98A13.9163,13.9163,0,0,1,26,4h2Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default Fish20;
