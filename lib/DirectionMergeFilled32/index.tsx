import { CarbonIconComponent } from "../types";
export const DirectionMergeFilled32: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="DirectionMergeFilled32"
    fill={props.fill || "currentColor"}
    width={props.width || "32"}
    height={props.height || "32"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M28,2H4A2,2,0,0,0,2,4V28a2,2,0,0,0,2,2H28a2,2,0,0,0,2-2V4A2,2,0,0,0,28,2ZM17.8784,15.4648l2.6572,2.6563A4.9682,4.9682,0,0,1,22,21.6567V26H20V21.6567a2.9821,2.9821,0,0,0-.8784-2.1215l-2.6572-2.6563A5.0021,5.0021,0,0,1,16,16.3135a5.0021,5.0021,0,0,1-.4644.5654l-2.6572,2.6567A2.9805,2.9805,0,0,0,12,21.6567V26H10V21.6567a4.9682,4.9682,0,0,1,1.4644-3.5356l2.6572-2.6567A2.9805,2.9805,0,0,0,15,13.3433V8.8281l-4.5859,4.586L9,12l7-7,7,7-1.4141,1.4141L17,8.8281v4.5152A2.9821,2.9821,0,0,0,17.8784,15.4648Z"></path>
    <path
      fill="none"
      d="M17.8784,15.4648l2.6572,2.6563A4.9682,4.9682,0,0,1,22,21.6567V26H20V21.6567a2.9821,2.9821,0,0,0-.8784-2.1215l-2.6572-2.6563A5.0021,5.0021,0,0,1,16,16.3135a5.0021,5.0021,0,0,1-.4644.5654l-2.6572,2.6567A2.9805,2.9805,0,0,0,12,21.6567V26H10V21.6567a4.9682,4.9682,0,0,1,1.4644-3.5356l2.6572-2.6567A2.9805,2.9805,0,0,0,15,13.3433V8.8281l-4.5859,4.586L9,12l7-7,7,7-1.4141,1.4141L17,8.8281v4.5152A2.9821,2.9821,0,0,0,17.8784,15.4648Z"
      data-icon-path="inner-path"
    ></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default DirectionMergeFilled32;
