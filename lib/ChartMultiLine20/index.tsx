import { CarbonIconComponent } from "../types";
export const ChartMultiLine20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="ChartMultiLine20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M4.5859,28l7.1783-5.998L19.7578,23.94a2.021,2.021,0,0,0,1.314-.1206L28,20.5811l-.8479-1.8121-6.9163,3.229L12.2422,20.06a2.0033,2.0033,0,0,0-1.74.3838L4,25.8818V20.49l7.7642-6.4877L19.7578,15.94a2.021,2.021,0,0,0,1.314-.1206l6.9287-3.2383-.8467-1.812-6.918,3.2285L12.2422,12.06a2.0049,2.0049,0,0,0-1.74.3838L4,17.8818V12.49L11.7642,6.002,19.7578,7.94a2.021,2.021,0,0,0,1.314-.1206L28,4.5859l-.8457-1.8115L20.2358,5.998,12.2422,4.06a2.0033,2.0033,0,0,0-1.74.3838L4,9.8818V2H2V28a2,2,0,0,0,2,2H30V28Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default ChartMultiLine20;
