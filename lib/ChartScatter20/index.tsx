import { CarbonIconComponent } from "../types";
export const ChartScatter20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="ChartScatter20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M30,30H4a2,2,0,0,1-2-2V2H4V28H30Z"></path>
    <circle cx="10" cy="22" r="2"></circle>
    <circle cx="14" cy="15" r="2"></circle>
    <circle cx="22" cy="15" r="2"></circle>
    <circle cx="26" cy="6" r="2"></circle>
    <circle cx="14" cy="8" r="2"></circle>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default ChartScatter20;
