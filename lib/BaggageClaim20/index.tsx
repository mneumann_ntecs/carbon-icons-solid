import { CarbonIconComponent } from "../types";
export const BaggageClaim20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="BaggageClaim20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <circle cx="6" cy="26" r="2"></circle>
    <path d="M28,18H25V16a2.0023,2.0023,0,0,0-2-2H19a2.0023,2.0023,0,0,0-2,2v2H14a2.0023,2.0023,0,0,0-2,2v8a2.0023,2.0023,0,0,0,2,2H28a2.0023,2.0023,0,0,0,2-2V20A2.0023,2.0023,0,0,0,28,18Zm-9-2h4v2H19Zm9,12H14V20H28Z"></path>
    <path d="M10,6h4v6h2V6h4v6h2V6h4v6h2V6.0049A2.0047,2.0047,0,0,0,25.9951,4H4.0049A2.0047,2.0047,0,0,0,2,6.0049v13.99A2.0047,2.0047,0,0,0,4.0049,22H10ZM8,20H4V6H8Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default BaggageClaim20;
