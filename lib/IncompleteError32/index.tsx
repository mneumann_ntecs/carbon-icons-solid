import { CarbonIconComponent } from "../types";
export const IncompleteError32: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="IncompleteError32"
    fill={props.fill || "currentColor"}
    width={props.width || "32"}
    height={props.height || "32"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M14 26A12 12 0 0114 2zM17.8257 4.7642a10.0288 10.0288 0 013.2422 2.1679l1.4133-1.4135a12.0359 12.0359 0 00-3.89-2.6016zM26 14a11.93 11.93 0 00-.9167-4.5908l-1.8472.7651A9.9428 9.9428 0 0124 14zM30 24a6 6 0 10-6 6A6.0066 6.0066 0 0030 24zm-2 0a3.9521 3.9521 0 01-.5669 2.019L21.981 20.5669A3.9529 3.9529 0 0124 20 4.0045 4.0045 0 0128 24zm-8 0a3.9521 3.9521 0 01.5669-2.019l5.4521 5.4521A3.9529 3.9529 0 0124 28 4.0045 4.0045 0 0120 24z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default IncompleteError32;
