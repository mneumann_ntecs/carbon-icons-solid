import { CarbonIconComponent } from "../types";
export const IceAccretion24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="IceAccretion24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M4 2A2 2 0 002 4V20l1 2 1-2V4H8V14l1 2 1-2V2zM28 2H14v8l1 2 1-2V4h2V17l1 2 1-2V4h4V14l1 2 1-2V4h2V24l1 2 1-2V4A2 2 0 0028 2zM14 28L13 30 12 28 12 16 14 16 14 28z"></path>
    <path d="M24 26L23 28 22 26 22 20 24 20 24 26zM8 24L7 26 6 24 6 18 8 18 8 24z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default IceAccretion24;
