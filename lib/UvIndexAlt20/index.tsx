import { CarbonIconComponent } from "../types";
export const UvIndexAlt20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="UvIndexAlt20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M13 30H9a2.0027 2.0027 0 01-2-2V20H9v8h4V20h2v8A2.0027 2.0027 0 0113 30zM25 20L23.25 20 21 29.031 18.792 20 17 20 19.5 30 22.5 30 25 20zM15 2H17V7H15z"></path>
    <path
      d="M21.668 6.854H26.625999999999998V8.854H21.668z"
      transform="rotate(-45 24.147 7.853)"
    ></path>
    <path d="M25 15H30V17H25zM2 15H7V17H2z"></path>
    <path
      d="M6.854 5.375H8.854V10.333H6.854z"
      transform="rotate(-45 7.854 7.853)"
    ></path>
    <path d="M22,17H20V16a4,4,0,0,0-8,0v1H10V16a6,6,0,0,1,12,0Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default UvIndexAlt20;
