import { CarbonIconComponent } from "../types";
export const DirectionBearRight_01Filled16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="DirectionBearRight_01Filled16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M28,2H4A2,2,0,0,0,2,4V28a2,2,0,0,0,2,2H28a2,2,0,0,0,2-2V4A2,2,0,0,0,28,2ZM22,16H20V9.4141L13.4644,15.95A4.9683,4.9683,0,0,0,12,19.4854V26H10V19.4854a6.954,6.954,0,0,1,2.05-4.95L18.5859,8H12V6H22Z"></path>
    <path
      fill="none"
      d="M22,16H20V9.4141L13.4644,15.95A4.9683,4.9683,0,0,0,12,19.4854V26H10V19.4854a6.954,6.954,0,0,1,2.05-4.95L18.5859,8H12V6H22Z"
      data-icon-path="inner-path"
    ></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default DirectionBearRight_01Filled16;
