import { CarbonIconComponent } from "../types";
export const GenderFemale20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="GenderFemale20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M17,19.9307a8,8,0,1,0-2,0V22H10v2h5v4h2V24h5V22H17ZM10,12a6,6,0,1,1,6,6A6.0066,6.0066,0,0,1,10,12Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default GenderFemale20;
