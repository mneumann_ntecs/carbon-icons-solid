import { CarbonIconComponent } from "../types";
export const UserFavoriteAltFilled20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="UserFavoriteAltFilled20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M26.4938 3a3.4735 3.4735 0 00-2.48 1.0393l-.5111.5228-.5161-.5228a3.4792 3.4792 0 00-4.96 0 3.59 3.59 0 000 5.0251l5.4766 5.5427 5.4716-5.5427a3.59 3.59 0 000-5.0251A3.4738 3.4738 0 0026.4938 3zM16 30H14V25a3.0033 3.0033 0 00-3-3H7a3.0033 3.0033 0 00-3 3v5H2V25a5.0059 5.0059 0 015-5h4a5.0059 5.0059 0 015 5zM9 10a3 3 0 11-3 3 3 3 0 013-3M9 8a5 5 0 105 5A5 5 0 009 8z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default UserFavoriteAltFilled20;
