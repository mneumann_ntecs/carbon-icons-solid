import { CarbonIconComponent } from "../types";
export const DistributeVerticalTop20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="DistributeVerticalTop20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M24 30H8a2.0021 2.0021 0 01-2-2V24a2.0021 2.0021 0 012-2H24a2.0021 2.0021 0 012 2v4A2.0021 2.0021 0 0124 30zM8 24v4H24V24zM2 18H30V20H2zM20 14H12a2.0021 2.0021 0 01-2-2V8a2.0021 2.0021 0 012-2h8a2.0021 2.0021 0 012 2v4A2.0021 2.0021 0 0120 14zM12 8v4h8V8zM2 2H30V4H2z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default DistributeVerticalTop20;
