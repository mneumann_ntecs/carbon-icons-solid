import { CarbonIconComponent } from "../types";
export const Password24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="Password24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M21,2a9,9,0,0,0-9,9,8.87,8.87,0,0,0,.39,2.61L2,24v6H8L18.39,19.61A9,9,0,0,0,30,11.74a8.77,8.77,0,0,0-1.65-6A9,9,0,0,0,21,2Zm0,16a7,7,0,0,1-2-.3l-1.15-.35L17,18.2l-3.18,3.18L12.41,20,11,21.41l1.38,1.38-1.59,1.59L9.41,23,8,24.41l1.38,1.38L7.17,28H4V24.83L13.8,15l.85-.85-.29-.95a7.14,7.14,0,0,1,3.4-8.44,7,7,0,0,1,10.24,6,6.69,6.69,0,0,1-1.09,4A7,7,0,0,1,21,18Z"></path>
    <circle cx="22" cy="10" r="2"></circle>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default Password24;
