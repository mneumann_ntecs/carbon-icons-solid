import { CarbonIconComponent } from "../types";
export const WatsonHealthSaveAnnotation32: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="WatsonHealthSaveAnnotation32"
    fill={props.fill || "currentColor"}
    width={props.width || "32"}
    height={props.height || "32"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M21.56,15.1l-3.48-4.35A2,2,0,0,0,16.52,10H4a2,2,0,0,0-2,2V28a2,2,0,0,0,2,2H20a2,2,0,0,0,2-2V16.35A2,2,0,0,0,21.56,15.1ZM9,12h6v3H9Zm6,16H9V22h6Zm2,0V22a2,2,0,0,0-2-2H9a2,2,0,0,0-2,2v6H4V12H7v3a2,2,0,0,0,2,2h6a2,2,0,0,0,2-2V12.6l3,3.75V28Z"></path>
    <path d="M28,20H25V18h3V4H8V7H6V4A2,2,0,0,1,8,2H28a2,2,0,0,1,2,2V18A2,2,0,0,1,28,20Z"></path>
    <path d="M20 6H26V8H20zM22 10H26V12H22z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default WatsonHealthSaveAnnotation32;
