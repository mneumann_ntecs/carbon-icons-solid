import {
  template,
  addEventListener,
  insert,
  memo,
  effect,
  setAttribute,
  style,
  delegateEvents,
} from "solid-js/web";

const _tmpl$ = template(
    `<svg data-carbon-icon="ThunderstormSevere24" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" preserveAspectRatio="xMidYMid meet"><path d="M21 30a1 1 0 01-.8944-1.4474l2-4.0005a1 1 0 111.7887.8947l-2 4A.9981.9981 0 0121 30zM9 32a1 1 0 01-.8944-1.4474l2-4.0005a1 1 0 111.7887.8947l-2 4A.9981.9981 0 019 32zM15.901 30.496L14.165 29.504 17.309 24 11.31 24 16.165 15.504 17.901 16.496 14.756 22 20.757 22 15.901 30.496zM27.9516 15A4.399 4.399 0 0128 15.5 4.5049 4.5049 0 0123.5 20H23v2h.5A6.5075 6.5075 0 0030 15.5c0-.17-.0312-.333-.0444-.5z"></path><path fill="none" d="M25.75,8h-1.5V4h1.5ZM25,9a1,1,0,1,0,1,1A1,1,0,0,0,25,9Z"></path><path d="M31.9115 11.9355L25.6283.3706a.7181.7181 0 00-1.2568 0L18.0883 11.9355A.72.72 0 0018.72 13H31.28A.72.72 0 0031.9115 11.9355zM24.25 4h1.5V8h-1.5zM25 11a1 1 0 111-1A1 1 0 0125 11zM8.144 11.019l.8154-.0639.0991-.812a6.9863 6.9863 0 019.97-5.45L20 2.9468A8.9777 8.9777 0 007.2 9.1362 6.4929 6.4929 0 007.43 21.9043L6.1059 24.5527a1 1 0 101.7885.8946l2-4a1 1 0 00-.4471-1.3418A.9786.9786 0 009 20.01V20H8.5a4.4975 4.4975 0 01-.3559-8.981z"></path></svg>`,
    8
  ),
  _tmpl$2 = template(`<title></title>`, 2);

const ThunderstormSevere24 = (props) =>
  (() => {
    const _el$ = _tmpl$.cloneNode(true),
      _el$2 = _el$.firstChild,
      _el$3 = _el$2.nextSibling;
    _el$3.nextSibling;

    addEventListener(_el$, "keydown", props.onKeyDown, true);

    addEventListener(_el$, "keyup", props.onKeyUp, true);

    addEventListener(_el$, "mouseleave", props.onMouseLeave);

    addEventListener(_el$, "mouseenter", props.onMouseEnter);

    addEventListener(_el$, "mouseover", props.onMouseOver, true);

    addEventListener(_el$, "click", props.onClick, true);

    insert(
      _el$,
      (() => {
        const _c$ = memo(() => props.children, true);

        return () =>
          _c$() ||
          (props.title &&
            (() => {
              const _el$5 = _tmpl$2.cloneNode(true);

              insert(_el$5, () => props.title);

              return _el$5;
            })());
      })(),
      null
    );

    effect(
      (_p$) => {
        const _v$ = props.fill || "currentColor",
          _v$2 = props.width || "24",
          _v$3 = props.height || "24",
          _v$4 = props["aria-label"],
          _v$5 = props["aria-labelledby"],
          _v$6 = props["aria-label"] || props["aria-labelledby"] || props.title,
          _v$7 =
            props["aria-label"] || props["aria-labelledby"] || props.title
              ? "img"
              : undefined,
          _v$8 = props.tabindex === "0" ? true : props.focusable,
          _v$9 = props.tabindex,
          _v$10 = props.id,
          _v$11 = props.class,
          _v$12 = props.title,
          _v$13 = props.style,
          _v$14 = props.stroke;

        _v$ !== _p$._v$ && setAttribute(_el$, "fill", (_p$._v$ = _v$));
        _v$2 !== _p$._v$2 && setAttribute(_el$, "width", (_p$._v$2 = _v$2));
        _v$3 !== _p$._v$3 && setAttribute(_el$, "height", (_p$._v$3 = _v$3));
        _v$4 !== _p$._v$4 &&
          setAttribute(_el$, "aria-label", (_p$._v$4 = _v$4));
        _v$5 !== _p$._v$5 &&
          setAttribute(_el$, "aria-labelledby", (_p$._v$5 = _v$5));
        _v$6 !== _p$._v$6 &&
          setAttribute(_el$, "aria-hidden", (_p$._v$6 = _v$6));
        _v$7 !== _p$._v$7 && setAttribute(_el$, "role", (_p$._v$7 = _v$7));
        _v$8 !== _p$._v$8 && setAttribute(_el$, "focusable", (_p$._v$8 = _v$8));
        _v$9 !== _p$._v$9 && setAttribute(_el$, "tabindex", (_p$._v$9 = _v$9));
        _v$10 !== _p$._v$10 && setAttribute(_el$, "id", (_p$._v$10 = _v$10));
        _v$11 !== _p$._v$11 && setAttribute(_el$, "class", (_p$._v$11 = _v$11));
        _v$12 !== _p$._v$12 && setAttribute(_el$, "title", (_p$._v$12 = _v$12));
        _p$._v$13 = style(_el$, _v$13, _p$._v$13);
        _v$14 !== _p$._v$14 &&
          setAttribute(_el$, "stroke", (_p$._v$14 = _v$14));
        return _p$;
      },
      {
        _v$: undefined,
        _v$2: undefined,
        _v$3: undefined,
        _v$4: undefined,
        _v$5: undefined,
        _v$6: undefined,
        _v$7: undefined,
        _v$8: undefined,
        _v$9: undefined,
        _v$10: undefined,
        _v$11: undefined,
        _v$12: undefined,
        _v$13: undefined,
        _v$14: undefined,
      }
    );

    return _el$;
  })();

delegateEvents(["click", "mouseover", "keyup", "keydown"]);

export default ThunderstormSevere24;
export { ThunderstormSevere24 };
