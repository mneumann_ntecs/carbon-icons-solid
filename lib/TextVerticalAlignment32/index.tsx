import { CarbonIconComponent } from "../types";
export const TextVerticalAlignment32: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="TextVerticalAlignment32"
    fill={props.fill || "currentColor"}
    width={props.width || "32"}
    height={props.height || "32"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M16 28H30V30H16zM16 23H30V25H16zM10.8458 30H13L8.64 20H6.36L2 30H4.1542l.8-2h5.0916zM5.7541 26L7.5 21.6347 9.2459 26zM2 15H30V17H2zM16 7H30V9H16zM16 2H30V4H16zM10.8458 12H13L8.64 2H6.36L2 12H4.1542l.8-2h5.0916zM5.7541 8L7.5 3.6347 9.2459 8z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default TextVerticalAlignment32;
