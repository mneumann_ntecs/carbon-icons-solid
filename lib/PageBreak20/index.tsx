import { CarbonIconComponent } from "../types";
export const PageBreak20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="PageBreak20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M2 18H6V20H2zM26 18H30V20H26zM24 22v6H8V22H6v6a2.0058 2.0058 0 002 2H24a2.0058 2.0058 0 002-2V22zM8 16V4h8v6a2.0058 2.0058 0 002 2h6v4h2V10a.9092.9092 0 00-.3-.7l-7-7A.9087.9087 0 0018 2H8A2.0058 2.0058 0 006 4V16zM18 4.4L23.6 10H18zM10 18H14V20H10zM18 18H22V20H18z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default PageBreak20;
