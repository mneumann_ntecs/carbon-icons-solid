import { CarbonIconComponent } from "../types";
export const Sunrise20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="Sunrise20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M2 27H29.998V29H2zM16 20a4.0045 4.0045 0 014 4h2a6 6 0 00-12 0h2A4.0045 4.0045 0 0116 20zM25 22H30V24H25z"></path>
    <path
      d="M21.668 14.854H26.625999999999998V16.854H21.668z"
      transform="rotate(-45 24.146 15.854)"
    ></path>
    <path d="M16 4L11 9 12.41 10.41 15 7.83 15 8 15 15 17 15 17 8 17 7.83 19.59 10.41 21 9 16 4z"></path>
    <path
      d="M6.854 13.374H8.854V18.332H6.854z"
      transform="rotate(-45 7.854 15.854)"
    ></path>
    <path d="M2 22H7V24H2z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default Sunrise20;
