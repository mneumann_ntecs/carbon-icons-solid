import { CarbonIconComponent } from "../types";
export const Percentage32: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="Percentage32"
    fill={props.fill || "currentColor"}
    width={props.width || "32"}
    height={props.height || "32"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M9,14a5,5,0,1,1,5-5A5.0055,5.0055,0,0,1,9,14ZM9,6a3,3,0,1,0,3,3A3.0033,3.0033,0,0,0,9,6Z"></path>
    <path d="M0.029 15H31.97V17H0.029z" transform="rotate(-45 16 16)"></path>
    <path d="M23,28a5,5,0,1,1,5-5A5.0055,5.0055,0,0,1,23,28Zm0-8a3,3,0,1,0,3,3A3.0033,3.0033,0,0,0,23,20Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default Percentage32;
