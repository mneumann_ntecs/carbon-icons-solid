import { CarbonIconComponent } from "../types";
export const AgricultureAnalytics32: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="AgricultureAnalytics32"
    fill={props.fill || "currentColor"}
    width={props.width || "32"}
    height={props.height || "32"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M24.251 21.3691l2.1943 1.4629A1 1 0 0027.8 22.6l3-4-1.6-1.2-2.4326 3.2437L24.5547 19.168a1 1 0 00-1.3687.2509L20 23.8789V16H18V26a2.0023 2.0023 0 002 2H30V26H20.9434zM2 21H16V23H2zM2 26H16V28H2zM11 16V11h1a4.0046 4.0046 0 004-4V4H13a3.9782 3.9782 0 00-2.7468 1.1066A6.0033 6.0033 0 005 2H2V5a6.0066 6.0066 0 006 6H9v5H2v2H16V16zM13 6h1V7a2.002 2.002 0 01-2 2H11V8A2.0019 2.0019 0 0113 6zM8 9A4.0046 4.0046 0 014 5V4H5A4.0045 4.0045 0 019 8V9z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default AgricultureAnalytics32;
