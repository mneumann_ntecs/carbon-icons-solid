import { CarbonIconComponent } from "../types";
export const IsoOutline20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="IsoOutline20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M24 21H21a2 2 0 01-2-2V13a2 2 0 012-2h3a2 2 0 012 2v6A2 2 0 0124 21zm-3-8v6h3V13zM15 21H10V19h5V17H12a2 2 0 01-2-2V13a2 2 0 012-2h5v2H12v2h3a2 2 0 012 2v2A2 2 0 0115 21zM6 11H8V21H6z"></path>
    <path d="M28,6H4A2,2,0,0,0,2,8V24a2,2,0,0,0,2,2H28a2,2,0,0,0,2-2V8A2,2,0,0,0,28,6ZM4,24V8H28V24Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default IsoOutline20;
