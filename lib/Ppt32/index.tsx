import { CarbonIconComponent } from "../types";
export const Ppt32: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="Ppt32"
    fill={props.fill || "currentColor"}
    width={props.width || "32"}
    height={props.height || "32"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M22 11L25 11 25 23 27 23 27 11 30 11 30 9 22 9 22 11zM14 23H12V9h6a2.002 2.002 0 012 2v5a2.002 2.002 0 01-2 2H14zm0-7h4V10.9985H14zM4 23H2V9H8a2.002 2.002 0 012 2v5a2.002 2.002 0 01-2 2H4zm0-7H8V10.9985H4z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default Ppt32;
