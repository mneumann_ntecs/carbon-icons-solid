import { CarbonIconComponent } from "../types";
export const Windy32: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="Windy32"
    fill={props.fill || "currentColor"}
    width={props.width || "32"}
    height={props.height || "32"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M21 15H8V13H21a3 3 0 10-3-3H16a5 5 0 115 5zM23 28a5.0057 5.0057 0 01-5-5h2a3 3 0 103-3H4V18H23a5 5 0 010 10z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default Windy32;
