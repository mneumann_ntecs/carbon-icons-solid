import { CarbonIconComponent } from "../types";
export const Radar16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="Radar16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M30,3.4141,28.5859,2,15.293,15.293a1,1,0,0,0,1.414,1.414l4.18-4.1792A5.9956,5.9956,0,1,1,16,10V8a8.011,8.011,0,1,0,6.3164,3.0977L25.1631,8.251A11.881,11.881,0,0,1,28,16,12,12,0,1,1,16,4V2A14,14,0,1,0,30,16a13.8572,13.8572,0,0,0-3.4224-9.1636Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default Radar16;
