import { CarbonIconComponent } from "../types";
export const Corn16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="Corn16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M23,16c-.3374,0-.67.02-1,.0508V16C22,8.115,19.0435,2,16.5,2S11,8.115,11,16v5H6v3a6.0067,6.0067,0,0,0,6,6h3A11.0125,11.0125,0,0,0,26,19V16ZM13,16h2V14H13.0728a25.8942,25.8942,0,0,1,.5654-4H16V8H14.2c.81-2.4058,1.8149-3.7715,2.3-3.9863.66.2919,2.2842,2.7036,3.0654,6.9863H18v2h1.8442A29.1625,29.1625,0,0,1,20,16v.4263A11.0554,11.0554,0,0,0,13.79,21H13ZM12,28a4.0045,4.0045,0,0,1-4-4V23h4.7639A10.9033,10.9033,0,0,0,12,27Zm12-9a9.01,9.01,0,0,1-9,9H14V27a9.01,9.01,0,0,1,9-9h1Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default Corn16;
