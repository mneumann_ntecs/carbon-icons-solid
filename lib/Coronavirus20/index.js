import {
  template,
  addEventListener,
  insert,
  memo,
  effect,
  setAttribute,
  style,
  delegateEvents,
} from "solid-js/web";

const _tmpl$ = template(
    `<svg data-carbon-icon="Coronavirus20" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" preserveAspectRatio="xMidYMid meet"><circle cx="15.5" cy="11.5" r="1.5"></circle><circle cx="20.5" cy="15.5" r="1.5"></circle><circle cx="16" cy="16" r="1"></circle><circle cx="16.5" cy="20.5" r="1.5"></circle><circle cx="11.5" cy="16.5" r="1.5"></circle><path d="M28,13v2H25.9492a9.9476,9.9476,0,0,0-2.2143-5.3213l1.4367-1.4365,1.4143,1.4145L28,8.2427,23.7578,4l-1.414,1.4141,1.414,1.414-1.437,1.4365A9.9464,9.9464,0,0,0,17,6.0508V4h2V2H13V4h2V6.0508A9.9464,9.9464,0,0,0,9.6792,8.2646L8.2422,6.8281l1.414-1.414L8.2422,4,4,8.2427l1.4141,1.414L6.8284,8.2422,8.2651,9.6787A9.9476,9.9476,0,0,0,6.0508,15H4V13H2v6H4V17H6.0508a9.9476,9.9476,0,0,0,2.2143,5.3213L6.8284,23.7578,5.4141,22.3433,4,23.7573,8.2422,28l1.414-1.4141-1.414-1.414,1.437-1.4365A9.9464,9.9464,0,0,0,15,25.9492V28H13v2h6V28H17V25.9492a9.9464,9.9464,0,0,0,5.3208-2.2138l1.437,1.4365-1.414,1.414L23.7578,28,28,23.7573l-1.4141-1.414-1.4143,1.4145-1.4367-1.4365A9.9476,9.9476,0,0,0,25.9492,17H28v2h2V13ZM16,24a8,8,0,1,1,8-8A8.0092,8.0092,0,0,1,16,24Z"></path></svg>`,
    14
  ),
  _tmpl$2 = template(`<title></title>`, 2);

const Coronavirus20 = (props) =>
  (() => {
    const _el$ = _tmpl$.cloneNode(true),
      _el$2 = _el$.firstChild,
      _el$3 = _el$2.nextSibling,
      _el$4 = _el$3.nextSibling,
      _el$5 = _el$4.nextSibling,
      _el$6 = _el$5.nextSibling;
    _el$6.nextSibling;

    addEventListener(_el$, "keydown", props.onKeyDown, true);

    addEventListener(_el$, "keyup", props.onKeyUp, true);

    addEventListener(_el$, "mouseleave", props.onMouseLeave);

    addEventListener(_el$, "mouseenter", props.onMouseEnter);

    addEventListener(_el$, "mouseover", props.onMouseOver, true);

    addEventListener(_el$, "click", props.onClick, true);

    insert(
      _el$,
      (() => {
        const _c$ = memo(() => props.children, true);

        return () =>
          _c$() ||
          (props.title &&
            (() => {
              const _el$8 = _tmpl$2.cloneNode(true);

              insert(_el$8, () => props.title);

              return _el$8;
            })());
      })(),
      null
    );

    effect(
      (_p$) => {
        const _v$ = props.fill || "currentColor",
          _v$2 = props.width || "20",
          _v$3 = props.height || "20",
          _v$4 = props["aria-label"],
          _v$5 = props["aria-labelledby"],
          _v$6 = props["aria-label"] || props["aria-labelledby"] || props.title,
          _v$7 =
            props["aria-label"] || props["aria-labelledby"] || props.title
              ? "img"
              : undefined,
          _v$8 = props.tabindex === "0" ? true : props.focusable,
          _v$9 = props.tabindex,
          _v$10 = props.id,
          _v$11 = props.class,
          _v$12 = props.title,
          _v$13 = props.style,
          _v$14 = props.stroke;

        _v$ !== _p$._v$ && setAttribute(_el$, "fill", (_p$._v$ = _v$));
        _v$2 !== _p$._v$2 && setAttribute(_el$, "width", (_p$._v$2 = _v$2));
        _v$3 !== _p$._v$3 && setAttribute(_el$, "height", (_p$._v$3 = _v$3));
        _v$4 !== _p$._v$4 &&
          setAttribute(_el$, "aria-label", (_p$._v$4 = _v$4));
        _v$5 !== _p$._v$5 &&
          setAttribute(_el$, "aria-labelledby", (_p$._v$5 = _v$5));
        _v$6 !== _p$._v$6 &&
          setAttribute(_el$, "aria-hidden", (_p$._v$6 = _v$6));
        _v$7 !== _p$._v$7 && setAttribute(_el$, "role", (_p$._v$7 = _v$7));
        _v$8 !== _p$._v$8 && setAttribute(_el$, "focusable", (_p$._v$8 = _v$8));
        _v$9 !== _p$._v$9 && setAttribute(_el$, "tabindex", (_p$._v$9 = _v$9));
        _v$10 !== _p$._v$10 && setAttribute(_el$, "id", (_p$._v$10 = _v$10));
        _v$11 !== _p$._v$11 && setAttribute(_el$, "class", (_p$._v$11 = _v$11));
        _v$12 !== _p$._v$12 && setAttribute(_el$, "title", (_p$._v$12 = _v$12));
        _p$._v$13 = style(_el$, _v$13, _p$._v$13);
        _v$14 !== _p$._v$14 &&
          setAttribute(_el$, "stroke", (_p$._v$14 = _v$14));
        return _p$;
      },
      {
        _v$: undefined,
        _v$2: undefined,
        _v$3: undefined,
        _v$4: undefined,
        _v$5: undefined,
        _v$6: undefined,
        _v$7: undefined,
        _v$8: undefined,
        _v$9: undefined,
        _v$10: undefined,
        _v$11: undefined,
        _v$12: undefined,
        _v$13: undefined,
        _v$14: undefined,
      }
    );

    return _el$;
  })();

delegateEvents(["click", "mouseover", "keyup", "keydown"]);

export default Coronavirus20;
export { Coronavirus20 };
