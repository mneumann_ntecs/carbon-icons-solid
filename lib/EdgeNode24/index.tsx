import { CarbonIconComponent } from "../types";
export const EdgeNode24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="EdgeNode24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M22 6H26V10H22z"></path>
    <circle cx="7" cy="7" r="1"></circle>
    <circle cx="25" cy="25" r="1"></circle>
    <circle cx="25" cy="21" r="1"></circle>
    <circle cx="25" cy="17" r="1"></circle>
    <path d="M22,17V15H20V14a2.0023,2.0023,0,0,0-2-2H17V10H15v2H13V10H11v2H10a2.0023,2.0023,0,0,0-2,2v1H6v2H8v2H6v2H8v1a2.0023,2.0023,0,0,0,2,2h1v2h2V24h2v2h2V24h1a2.0023,2.0023,0,0,0,2-2V21h2V19H20V17Zm-4,5H10V14h8Z"></path>
    <path d="M28,30H4a2.0021,2.0021,0,0,1-2-2V4A2.0021,2.0021,0,0,1,4,2H28a2.0021,2.0021,0,0,1,2,2V28A2.0021,2.0021,0,0,1,28,30ZM4,4V28H28V4Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default EdgeNode24;
