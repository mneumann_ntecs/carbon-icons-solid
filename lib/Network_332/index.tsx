import { CarbonIconComponent } from "../types";
export const Network_332: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="Network_332"
    fill={props.fill || "currentColor"}
    width={props.width || "32"}
    height={props.height || "32"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M30 30H22V22h8zm-6-2h4V24H24zM20 27H8A6 6 0 018 15h2v2H8a4 4 0 000 8H20z"></path>
    <path d="M20,20H12V12h8Zm-6-2h4V14H14Z"></path>
    <path d="M24 17H22V15h2a4 4 0 000-8H12V5H24a6 6 0 010 12zM10 10H2V2h8zM4 8H8V4H4z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default Network_332;
