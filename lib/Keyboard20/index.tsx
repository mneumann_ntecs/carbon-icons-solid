import { CarbonIconComponent } from "../types";
export const Keyboard20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="Keyboard20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M28,26H4a2,2,0,0,1-2-2V10A2,2,0,0,1,4,8H28a2,2,0,0,1,2,2V24A2,2,0,0,1,28,26ZM4,10V24H28V10Z"></path>
    <path d="M10 20H21V22H10zM6 12H8V14H6zM10 12H12V14H10zM14 12H16V14H14zM18 12H20V14H18zM6 20H8V22H6zM6 16H8V18H6zM10 16H12V18H10zM14 16H16V18H14zM22 12H26V14H22zM22 16H26V18H22zM18 16H20V18H18zM23 20H26V22H23z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default Keyboard20;
