import {
  template,
  addEventListener,
  insert,
  memo,
  effect,
  setAttribute,
  style,
  delegateEvents,
} from "solid-js/web";

const _tmpl$ = template(
    `<svg data-carbon-icon="TropicalStormModelTracks24" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" preserveAspectRatio="xMidYMid meet"><path d="M10,17a4,4,0,1,1,4-4A4.0045,4.0045,0,0,1,10,17Zm0-6a2,2,0,1,0,2,2A2.0021,2.0021,0,0,0,10,11Z"></path><path d="M14.8188 6.65c.1163-.1211 1.8643-1.919 2.8882-2.9434A1 1 0 0017 2C11.812 2 2 4.2988 2 13a7.8938 7.8938 0 003.1812 6.35c-.1612.1675-1.9507 2.0054-2.8882 2.9429A1 1 0 003 24c5.188 0 15-2.2988 15-11A7.8927 7.8927 0 0014.8188 6.65zM9.7065 20.793a20.94 20.94 0 01-4.0932.9853c.1621-.166 2.7685-2.9053 2.7685-2.9053l-1.1743-.6074A5.9064 5.9064 0 014 13c0-3.667 2.1177-6.2886 6.2935-7.793a20.94 20.94 0 014.0932-.9853c-.227.2324-2.769 2.9057-2.769 2.9057l1.1748.607A5.9075 5.9075 0 0116 13C16 16.667 13.8823 19.2886 9.7065 20.793zM30 17l-5-5-5 5 1.4126 1.4155L24 15.8325V19a9.01 9.01 0 01-9 9H12v2h3A11.0125 11.0125 0 0026 19V15.8325l2.5859 2.5816z"></path></svg>`,
    6
  ),
  _tmpl$2 = template(`<title></title>`, 2);

const TropicalStormModelTracks24 = (props) =>
  (() => {
    const _el$ = _tmpl$.cloneNode(true),
      _el$2 = _el$.firstChild;
    _el$2.nextSibling;

    addEventListener(_el$, "keydown", props.onKeyDown, true);

    addEventListener(_el$, "keyup", props.onKeyUp, true);

    addEventListener(_el$, "mouseleave", props.onMouseLeave);

    addEventListener(_el$, "mouseenter", props.onMouseEnter);

    addEventListener(_el$, "mouseover", props.onMouseOver, true);

    addEventListener(_el$, "click", props.onClick, true);

    insert(
      _el$,
      (() => {
        const _c$ = memo(() => props.children, true);

        return () =>
          _c$() ||
          (props.title &&
            (() => {
              const _el$4 = _tmpl$2.cloneNode(true);

              insert(_el$4, () => props.title);

              return _el$4;
            })());
      })(),
      null
    );

    effect(
      (_p$) => {
        const _v$ = props.fill || "currentColor",
          _v$2 = props.width || "24",
          _v$3 = props.height || "24",
          _v$4 = props["aria-label"],
          _v$5 = props["aria-labelledby"],
          _v$6 = props["aria-label"] || props["aria-labelledby"] || props.title,
          _v$7 =
            props["aria-label"] || props["aria-labelledby"] || props.title
              ? "img"
              : undefined,
          _v$8 = props.tabindex === "0" ? true : props.focusable,
          _v$9 = props.tabindex,
          _v$10 = props.id,
          _v$11 = props.class,
          _v$12 = props.title,
          _v$13 = props.style,
          _v$14 = props.stroke;

        _v$ !== _p$._v$ && setAttribute(_el$, "fill", (_p$._v$ = _v$));
        _v$2 !== _p$._v$2 && setAttribute(_el$, "width", (_p$._v$2 = _v$2));
        _v$3 !== _p$._v$3 && setAttribute(_el$, "height", (_p$._v$3 = _v$3));
        _v$4 !== _p$._v$4 &&
          setAttribute(_el$, "aria-label", (_p$._v$4 = _v$4));
        _v$5 !== _p$._v$5 &&
          setAttribute(_el$, "aria-labelledby", (_p$._v$5 = _v$5));
        _v$6 !== _p$._v$6 &&
          setAttribute(_el$, "aria-hidden", (_p$._v$6 = _v$6));
        _v$7 !== _p$._v$7 && setAttribute(_el$, "role", (_p$._v$7 = _v$7));
        _v$8 !== _p$._v$8 && setAttribute(_el$, "focusable", (_p$._v$8 = _v$8));
        _v$9 !== _p$._v$9 && setAttribute(_el$, "tabindex", (_p$._v$9 = _v$9));
        _v$10 !== _p$._v$10 && setAttribute(_el$, "id", (_p$._v$10 = _v$10));
        _v$11 !== _p$._v$11 && setAttribute(_el$, "class", (_p$._v$11 = _v$11));
        _v$12 !== _p$._v$12 && setAttribute(_el$, "title", (_p$._v$12 = _v$12));
        _p$._v$13 = style(_el$, _v$13, _p$._v$13);
        _v$14 !== _p$._v$14 &&
          setAttribute(_el$, "stroke", (_p$._v$14 = _v$14));
        return _p$;
      },
      {
        _v$: undefined,
        _v$2: undefined,
        _v$3: undefined,
        _v$4: undefined,
        _v$5: undefined,
        _v$6: undefined,
        _v$7: undefined,
        _v$8: undefined,
        _v$9: undefined,
        _v$10: undefined,
        _v$11: undefined,
        _v$12: undefined,
        _v$13: undefined,
        _v$14: undefined,
      }
    );

    return _el$;
  })();

delegateEvents(["click", "mouseover", "keyup", "keydown"]);

export default TropicalStormModelTracks24;
export { TropicalStormModelTracks24 };
