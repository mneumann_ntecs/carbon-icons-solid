import { CarbonIconComponent } from "../types";
export const WatsonHealthStudySkip20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="WatsonHealthStudySkip20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M22,9h5.2A11,11,0,0,0,7,15H5A13,13,0,0,1,28,6.7V3h2v8H22Z"></path>
    <path d="M16 12a3 3 0 11-3 3 2.95 2.95 0 013-3m0-2a5 5 0 105 5A5 5 0 0016 10zM23 30H21V27a3 3 0 00-3-3H14a3 3 0 00-3 3v3H9V27a5 5 0 015-5h4a5 5 0 015 5z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default WatsonHealthStudySkip20;
