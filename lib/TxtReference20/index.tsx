import { CarbonIconComponent } from "../types";
export const TxtReference20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="TxtReference20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M4 20L4 22 8.586 22 2 28.586 3.414 30 10 23.414 10 28 12 28 12 20 4 20zM21 4L24 4 24 16 26 16 26 4 29 4 29 2 21 2 21 4zM20 2L18 2 16 8 14 2 12 2 14.752 9 12 16 14 16 16 10 18 16 20 16 17.245 9 20 2zM3 4L6 4 6 16 8 16 8 4 11 4 11 2 3 2 3 4z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default TxtReference20;
