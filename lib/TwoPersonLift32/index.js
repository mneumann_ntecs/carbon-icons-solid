import {
  template,
  addEventListener,
  insert,
  memo,
  effect,
  setAttribute,
  style,
  delegateEvents,
} from "solid-js/web";

const _tmpl$ = template(
    `<svg data-carbon-icon="TwoPersonLift32" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" preserveAspectRatio="xMidYMid meet"><path d="M26.96 30l-1.9215-6.7253a1.0008 1.0008 0 01.3369-1.0554L29.874 18.62 28.52 13.2014l-2.7382 3.4234A1.0026 1.0026 0 0125 17H20V15h4.52l3.6993-4.6248a1 1 0 011.7509.3824l2 8a.9989.9989 0 01-.3447 1.0232l-4.48 3.5845 1.7389 6.0854zM23 5.5A3.5 3.5 0 1126.5 9 3.5042 3.5042 0 0123 5.5zm2 0A1.5 1.5 0 1026.5 4 1.5017 1.5017 0 0025 5.5z"></path><path d="M20.0039,19A2.0039,2.0039,0,0,1,18,16.9961V15.0039A2.0039,2.0039,0,0,1,20.0039,13H22V10H10v3h1.9961A2.0039,2.0039,0,0,1,14,15.0039v1.9922A2.0039,2.0039,0,0,1,11.9961,19H10v3H22V19Z"></path><path d="M5.04 30l1.9215-6.7253a1.0013 1.0013 0 00-.3369-1.0555L2.126 18.62l1.3545-5.4185 2.7382 3.4234A1.0026 1.0026 0 007 17h5V15H7.4805L3.7812 10.3752a1 1 0 00-1.7509.3824l-2 8A.9989.9989 0 00.375 19.7808l4.4805 3.5844-1.739 6.0855zM5.5 9A3.5 3.5 0 119 5.5 3.5042 3.5042 0 015.5 9zm0-5A1.5 1.5 0 107 5.5 1.5017 1.5017 0 005.5 4z"></path></svg>`,
    8
  ),
  _tmpl$2 = template(`<title></title>`, 2);

const TwoPersonLift32 = (props) =>
  (() => {
    const _el$ = _tmpl$.cloneNode(true),
      _el$2 = _el$.firstChild,
      _el$3 = _el$2.nextSibling;
    _el$3.nextSibling;

    addEventListener(_el$, "keydown", props.onKeyDown, true);

    addEventListener(_el$, "keyup", props.onKeyUp, true);

    addEventListener(_el$, "mouseleave", props.onMouseLeave);

    addEventListener(_el$, "mouseenter", props.onMouseEnter);

    addEventListener(_el$, "mouseover", props.onMouseOver, true);

    addEventListener(_el$, "click", props.onClick, true);

    insert(
      _el$,
      (() => {
        const _c$ = memo(() => props.children, true);

        return () =>
          _c$() ||
          (props.title &&
            (() => {
              const _el$5 = _tmpl$2.cloneNode(true);

              insert(_el$5, () => props.title);

              return _el$5;
            })());
      })(),
      null
    );

    effect(
      (_p$) => {
        const _v$ = props.fill || "currentColor",
          _v$2 = props.width || "32",
          _v$3 = props.height || "32",
          _v$4 = props["aria-label"],
          _v$5 = props["aria-labelledby"],
          _v$6 = props["aria-label"] || props["aria-labelledby"] || props.title,
          _v$7 =
            props["aria-label"] || props["aria-labelledby"] || props.title
              ? "img"
              : undefined,
          _v$8 = props.tabindex === "0" ? true : props.focusable,
          _v$9 = props.tabindex,
          _v$10 = props.id,
          _v$11 = props.class,
          _v$12 = props.title,
          _v$13 = props.style,
          _v$14 = props.stroke;

        _v$ !== _p$._v$ && setAttribute(_el$, "fill", (_p$._v$ = _v$));
        _v$2 !== _p$._v$2 && setAttribute(_el$, "width", (_p$._v$2 = _v$2));
        _v$3 !== _p$._v$3 && setAttribute(_el$, "height", (_p$._v$3 = _v$3));
        _v$4 !== _p$._v$4 &&
          setAttribute(_el$, "aria-label", (_p$._v$4 = _v$4));
        _v$5 !== _p$._v$5 &&
          setAttribute(_el$, "aria-labelledby", (_p$._v$5 = _v$5));
        _v$6 !== _p$._v$6 &&
          setAttribute(_el$, "aria-hidden", (_p$._v$6 = _v$6));
        _v$7 !== _p$._v$7 && setAttribute(_el$, "role", (_p$._v$7 = _v$7));
        _v$8 !== _p$._v$8 && setAttribute(_el$, "focusable", (_p$._v$8 = _v$8));
        _v$9 !== _p$._v$9 && setAttribute(_el$, "tabindex", (_p$._v$9 = _v$9));
        _v$10 !== _p$._v$10 && setAttribute(_el$, "id", (_p$._v$10 = _v$10));
        _v$11 !== _p$._v$11 && setAttribute(_el$, "class", (_p$._v$11 = _v$11));
        _v$12 !== _p$._v$12 && setAttribute(_el$, "title", (_p$._v$12 = _v$12));
        _p$._v$13 = style(_el$, _v$13, _p$._v$13);
        _v$14 !== _p$._v$14 &&
          setAttribute(_el$, "stroke", (_p$._v$14 = _v$14));
        return _p$;
      },
      {
        _v$: undefined,
        _v$2: undefined,
        _v$3: undefined,
        _v$4: undefined,
        _v$5: undefined,
        _v$6: undefined,
        _v$7: undefined,
        _v$8: undefined,
        _v$9: undefined,
        _v$10: undefined,
        _v$11: undefined,
        _v$12: undefined,
        _v$13: undefined,
        _v$14: undefined,
      }
    );

    return _el$;
  })();

delegateEvents(["click", "mouseover", "keyup", "keydown"]);

export default TwoPersonLift32;
export { TwoPersonLift32 };
