import { CarbonIconComponent } from "../types";
export const SkipBackOutlineSolid32: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="SkipBackOutlineSolid32"
    fill={props.fill || "currentColor"}
    width={props.width || "32"}
    height={props.height || "32"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M23,22a1.0037,1.0037,0,0,1-.4858-.1257l-9-5a1,1,0,0,1,0-1.7485l9-5A1,1,0,0,1,24,11V21a1,1,0,0,1-1,1Z"></path>
    <path d="M9 10H11V22H9z" transform="rotate(-180 10 16)"></path>
    <path d="M16,30A14,14,0,1,1,30,16,14.0158,14.0158,0,0,1,16,30ZM16,4A12,12,0,1,0,28,16,12.0137,12.0137,0,0,0,16,4Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default SkipBackOutlineSolid32;
