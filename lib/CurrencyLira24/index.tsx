import { CarbonIconComponent } from "../types";
export const CurrencyLira24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="CurrencyLira24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M23,19a6.0066,6.0066,0,0,1-6,6H13V18.1079L21.2139,15V13L13,16.1079v-3L21.2139,10V8L13,11.1079V5H11v6.8647L8,13v2l3-1.1353v3L8,18v2l3-1.1353V27h6a8.0092,8.0092,0,0,0,8-8Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default CurrencyLira24;
