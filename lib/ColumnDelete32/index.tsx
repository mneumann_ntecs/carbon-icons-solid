import { CarbonIconComponent } from "../types";
export const ColumnDelete32: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="ColumnDelete32"
    fill={props.fill || "currentColor"}
    width={props.width || "32"}
    height={props.height || "32"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M30 3.41L28.59 2 25 5.59 21.41 2 20 3.41 23.59 7 20 10.59 21.41 12 25 8.41 28.59 12 30 10.59 26.41 7 30 3.41z"></path>
    <path d="M24 14V28H18V6H16V28a2.0023 2.0023 0 002 2h6a2.0023 2.0023 0 002-2V14zM10 30H4a2.0021 2.0021 0 01-2-2V8A2.0021 2.0021 0 014 6h6a2.0021 2.0021 0 012 2V28A2.0021 2.0021 0 0110 30zM4 8V28h6V8z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default ColumnDelete32;
