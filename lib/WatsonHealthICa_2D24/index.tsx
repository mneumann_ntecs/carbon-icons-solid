import { CarbonIconComponent } from "../types";
export const WatsonHealthICa_2D24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="WatsonHealthICa_2D24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M2.001 21L2.001 30 11 30 11 28 4 28 4 21 2.001 21zM21.001 30L30 30 30 21 28.001 21 28.001 28 21.001 28 21.001 30zM22 24H10a2.0023 2.0023 0 01-2-2V10a2.002 2.002 0 012-2H22a2.0023 2.0023 0 012 2V22A2.0027 2.0027 0 0122 24zM10 10V22H22.002L22 10zM30 11L30 2 21.001 2 21.001 4 28.001 4 28.001 11 30 11zM11 2L2.001 2 2.001 11 4 11 4 4 11 4 11 2z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default WatsonHealthICa_2D24;
