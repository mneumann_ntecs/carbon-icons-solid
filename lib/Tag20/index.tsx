import { CarbonIconComponent } from "../types";
export const Tag20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="Tag20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M18.52,30a3,3,0,0,1-2.12-.88L2.88,15.61A3,3,0,0,1,2,13.49V5A3,3,0,0,1,5,2h8.49a3,3,0,0,1,2.12.88L29.12,16.39a3,3,0,0,1,0,4.25l-8.48,8.48A3,3,0,0,1,18.52,30ZM5,4A1,1,0,0,0,4,5v8.49a1,1,0,0,0,.3.71L17.81,27.71a1,1,0,0,0,1.41,0l8.49-8.49a1,1,0,0,0,0-1.41L14.2,4.3a1,1,0,0,0-.71-.3H5Z"></path>
    <path d="M10,14a4,4,0,1,1,4-4A4,4,0,0,1,10,14Zm0-6a2,2,0,1,0,2,2A2,2,0,0,0,10,8Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default Tag20;
