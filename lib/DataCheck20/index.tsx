import { CarbonIconComponent } from "../types";
export const DataCheck20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="DataCheck20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M23 27.18L20.41 24.59 19 26 23 30 30 23 28.59 21.59 23 27.18z"></path>
    <circle cx="11" cy="8" r="1"></circle>
    <circle cx="11" cy="16" r="1"></circle>
    <circle cx="11" cy="24" r="1"></circle>
    <path d="M24,3H8A2,2,0,0,0,6,5V27a2,2,0,0,0,2,2h8V27H8V21H26V5A2,2,0,0,0,24,3Zm0,16H8V13H24Zm0-8H8V5H24Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default DataCheck20;
