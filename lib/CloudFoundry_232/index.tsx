import { CarbonIconComponent } from "../types";
export const CloudFoundry_232: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="CloudFoundry_232"
    fill={props.fill || "currentColor"}
    width={props.width || "32"}
    height={props.height || "32"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M25 11L25 9 17 9 17 23 19 23 19 17 24 17 24 15 19 15 19 11 25 11zM15 23H9a2 2 0 01-2-2V11A2 2 0 019 9h6v2H9V21h6z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default CloudFoundry_232;
