import { CarbonIconComponent } from "../types";
export const Data_124: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="Data_124"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M15 6H28V8H15zM15 24H28V26H15zM4 15H17V17H4zM7 11a4 4 0 114-4A4 4 0 017 11zM7 5A2 2 0 109 7 2 2 0 007 5zM7 29a4 4 0 114-4A4 4 0 017 29zm0-6a2 2 0 102 2A2 2 0 007 23zM25 20a4 4 0 114-4A4 4 0 0125 20zm0-6a2 2 0 102 2A2 2 0 0025 14z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default Data_124;
