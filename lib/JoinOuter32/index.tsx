import { CarbonIconComponent } from "../types";
export const JoinOuter32: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="JoinOuter32"
    fill={props.fill || "currentColor"}
    width={props.width || "32"}
    height={props.height || "32"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M20 6a9.9539 9.9539 0 00-4 .8379 9.9953 9.9953 0 010 18.3242A9.9988 9.9988 0 1020 6zM10 16a9.9976 9.9976 0 016-9.1621 10 10 0 100 18.3242A9.9976 9.9976 0 0110 16z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default JoinOuter32;
