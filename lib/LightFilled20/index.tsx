import { CarbonIconComponent } from "../types";
export const LightFilled20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="LightFilled20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M15 2H17V7H15z"></path>
    <path
      d="M21.668 6.854H26.625999999999998V8.854H21.668z"
      transform="rotate(-45 24.147 7.853)"
    ></path>
    <path d="M25 15H30V17H25z"></path>
    <path
      d="M23.147 21.668H25.147V26.625999999999998H23.147z"
      transform="rotate(-45 24.147 24.146)"
    ></path>
    <path d="M15 25H17V30H15z"></path>
    <path
      d="M5.375 23.147H10.333V25.147H5.375z"
      transform="rotate(-45 7.853 24.146)"
    ></path>
    <path d="M2 15H7V17H2z"></path>
    <path
      d="M6.854 5.375H8.854V10.333H6.854z"
      transform="rotate(-45 7.854 7.853)"
    ></path>
    <path d="M16,10a6,6,0,1,0,6,6,6,6,0,0,0-6-6Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default LightFilled20;
