import { CarbonIconComponent } from "../types";
export const WatsonHealthSpineLabel24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="WatsonHealthSpineLabel24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M3 11L3 13 8.59 13 2.29 19.29 3.71 20.71 10 14.41 10 20 12 20 12 11 3 11zM26 13H23V12H21v1H18a2 2 0 00-2 2v2a2 2 0 002 2h3v1h2V19h3a2 2 0 002-2V15A2 2 0 0026 13zm-8 4V15h8v2zM26 23H23V22H21v1H18a2 2 0 00-2 2v2a2 2 0 002 2h3v1h2V29h3a2 2 0 002-2V25A2 2 0 0026 23zm-8 4V25h8v2zM26 3H23V2H21V3H18a2 2 0 00-2 2V7a2 2 0 002 2h3v1h2V9h3a2 2 0 002-2V5A2 2 0 0026 3zM18 7V5h8V7z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default WatsonHealthSpineLabel24;
