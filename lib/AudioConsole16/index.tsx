import { CarbonIconComponent } from "../types";
export const AudioConsole16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="AudioConsole16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path
      d="M28 14H27V2H25V14H24a2.0019 2.0019 0 00-2 2v2a2.0023 2.0023 0 002 2h1V30h2V20h1a2.0027 2.0027 0 002-2V16A2.0023 2.0023 0 0028 14zm0 4H24V16h4zM18 6H17V2H15V6H14a2.0019 2.0019 0 00-2 2v2a2.0019 2.0019 0 002 2h1V30h2V12h1a2.002 2.002 0 002-2V8A2.002 2.002 0 0018 6zm0 4H14V8h4zM8 20H7V2H5V20H4a2.0023 2.0023 0 00-2 2v2a2.0023 2.0023 0 002 2H5v4H7V26H8a2.0023 2.0023 0 002-2V22A2.0023 2.0023 0 008 20zm0 4H4V22H8z"
      transform="translate(0 .005)"
    ></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default AudioConsole16;
