import {
  template,
  addEventListener,
  insert,
  memo,
  effect,
  setAttribute,
  style,
  delegateEvents,
} from "solid-js/web";

const _tmpl$ = template(
    `<svg data-carbon-icon="RainScatteredNight32" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" preserveAspectRatio="xMidYMid meet"><path d="M8.5084 32a1.0115 1.0115 0 01-.4485-.1055.9986.9986 0 01-.4486-1.3418l1.4934-3a1.0025 1.0025 0 011.7943.8945l-1.4934 3A1.0015 1.0015 0 018.5084 32zM29.8442 13.0347a1.5184 1.5184 0 00-1.2309-.8658 5.3587 5.3587 0 01-3.4094-1.7163 6.4648 6.4648 0 01-1.285-6.393 1.6031 1.6031 0 00-.3-1.5459 1.4535 1.4535 0 00-1.3594-.4922l-.0191.0029a7.8549 7.8549 0 00-6.1054 6.48A7.3725 7.3725 0 0013.5 8a7.5511 7.5511 0 00-7.1494 5.2441A5.9926 5.9926 0 008 25h7.3818L14.106 27.5527a1 1 0 101.7885.8946L17.6177 25H19a5.9549 5.9549 0 005.88-7.1455 7.5 7.5 0 004.8672-3.3A1.5381 1.5381 0 0029.8442 13.0347zM19 23H8a3.9926 3.9926 0 01-.6733-7.9292l.663-.1128.1456-.6562a5.496 5.496 0 0110.7294 0l.1456.6562.6626.1128A3.9925 3.9925 0 0119 23zm5.1509-7.0479a5.9639 5.9639 0 00-3.5015-2.708A7.5076 7.5076 0 0018.0286 9.55a6.01 6.01 0 013.77-5.334 8.4581 8.4581 0 001.9395 7.5972A7.4007 7.4007 0 0027.64 14.041 5.4392 5.4392 0 0124.1509 15.9521z"></path></svg>`,
    4
  ),
  _tmpl$2 = template(`<title></title>`, 2);

const RainScatteredNight32 = (props) =>
  (() => {
    const _el$ = _tmpl$.cloneNode(true);
    _el$.firstChild;

    addEventListener(_el$, "keydown", props.onKeyDown, true);

    addEventListener(_el$, "keyup", props.onKeyUp, true);

    addEventListener(_el$, "mouseleave", props.onMouseLeave);

    addEventListener(_el$, "mouseenter", props.onMouseEnter);

    addEventListener(_el$, "mouseover", props.onMouseOver, true);

    addEventListener(_el$, "click", props.onClick, true);

    insert(
      _el$,
      (() => {
        const _c$ = memo(() => props.children, true);

        return () =>
          _c$() ||
          (props.title &&
            (() => {
              const _el$3 = _tmpl$2.cloneNode(true);

              insert(_el$3, () => props.title);

              return _el$3;
            })());
      })(),
      null
    );

    effect(
      (_p$) => {
        const _v$ = props.fill || "currentColor",
          _v$2 = props.width || "32",
          _v$3 = props.height || "32",
          _v$4 = props["aria-label"],
          _v$5 = props["aria-labelledby"],
          _v$6 = props["aria-label"] || props["aria-labelledby"] || props.title,
          _v$7 =
            props["aria-label"] || props["aria-labelledby"] || props.title
              ? "img"
              : undefined,
          _v$8 = props.tabindex === "0" ? true : props.focusable,
          _v$9 = props.tabindex,
          _v$10 = props.id,
          _v$11 = props.class,
          _v$12 = props.title,
          _v$13 = props.style,
          _v$14 = props.stroke;

        _v$ !== _p$._v$ && setAttribute(_el$, "fill", (_p$._v$ = _v$));
        _v$2 !== _p$._v$2 && setAttribute(_el$, "width", (_p$._v$2 = _v$2));
        _v$3 !== _p$._v$3 && setAttribute(_el$, "height", (_p$._v$3 = _v$3));
        _v$4 !== _p$._v$4 &&
          setAttribute(_el$, "aria-label", (_p$._v$4 = _v$4));
        _v$5 !== _p$._v$5 &&
          setAttribute(_el$, "aria-labelledby", (_p$._v$5 = _v$5));
        _v$6 !== _p$._v$6 &&
          setAttribute(_el$, "aria-hidden", (_p$._v$6 = _v$6));
        _v$7 !== _p$._v$7 && setAttribute(_el$, "role", (_p$._v$7 = _v$7));
        _v$8 !== _p$._v$8 && setAttribute(_el$, "focusable", (_p$._v$8 = _v$8));
        _v$9 !== _p$._v$9 && setAttribute(_el$, "tabindex", (_p$._v$9 = _v$9));
        _v$10 !== _p$._v$10 && setAttribute(_el$, "id", (_p$._v$10 = _v$10));
        _v$11 !== _p$._v$11 && setAttribute(_el$, "class", (_p$._v$11 = _v$11));
        _v$12 !== _p$._v$12 && setAttribute(_el$, "title", (_p$._v$12 = _v$12));
        _p$._v$13 = style(_el$, _v$13, _p$._v$13);
        _v$14 !== _p$._v$14 &&
          setAttribute(_el$, "stroke", (_p$._v$14 = _v$14));
        return _p$;
      },
      {
        _v$: undefined,
        _v$2: undefined,
        _v$3: undefined,
        _v$4: undefined,
        _v$5: undefined,
        _v$6: undefined,
        _v$7: undefined,
        _v$8: undefined,
        _v$9: undefined,
        _v$10: undefined,
        _v$11: undefined,
        _v$12: undefined,
        _v$13: undefined,
        _v$14: undefined,
      }
    );

    return _el$;
  })();

delegateEvents(["click", "mouseover", "keyup", "keydown"]);

export default RainScatteredNight32;
export { RainScatteredNight32 };
