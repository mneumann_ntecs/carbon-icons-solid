import { CarbonIconComponent } from "../types";
export const PhoneOffFilled32: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="PhoneOffFilled32"
    fill={props.fill || "currentColor"}
    width={props.width || "32"}
    height={props.height || "32"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M26.74 19.56l-2.52-1a2 2 0 00-2.15.44L20 21.06a9.93 9.93 0 01-5.35-2.29L30 3.41 28.59 2 2 28.59 3.41 30l7.93-7.92c3.24 3.12 7.89 5.5 14.55 5.92A2 2 0 0028 26V21.41A2 2 0 0026.74 19.56zM8.15 18.19l3.52-3.52A11.68 11.68 0 0110.85 12l2.07-2.07a2 2 0 00.44-2.15l-1-2.52A2 2 0 0010.5 4H6A2 2 0 004 6.22 29 29 0 008.15 18.19z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default PhoneOffFilled32;
