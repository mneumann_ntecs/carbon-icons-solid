import { CarbonIconComponent } from "../types";
export const DirectionRight_01Filled20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="DirectionRight_01Filled20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M28,2H4A2,2,0,0,0,2,4V28a2,2,0,0,0,2,2H28a2,2,0,0,0,2-2V4A2,2,0,0,0,28,2ZM19,18l-1.4141-1.4141L22.1719,12H10V26H8V12a2,2,0,0,1,2-2H22.1719l-4.586-4.5859L19,4l7,7Z"></path>
    <path
      fill="none"
      d="M19,18l-1.4141-1.4141L22.1719,12H10V26H8V12a2,2,0,0,1,2-2H22.1719l-4.586-4.5859L19,4l7,7Z"
      data-icon-path="inner-path"
    ></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default DirectionRight_01Filled20;
