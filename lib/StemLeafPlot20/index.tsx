import { CarbonIconComponent } from "../types";
export const StemLeafPlot20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="StemLeafPlot20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M19 10a3 3 0 113-3A3.0033 3.0033 0 0119 10zm0-4a1 1 0 101 1A1.0011 1.0011 0 0019 6zM19 19a3 3 0 113-3A3.0033 3.0033 0 0119 19zm0-4a1 1 0 101 1A1.0011 1.0011 0 0019 15zM27 19a3 3 0 113-3A3.0033 3.0033 0 0127 19zm0-4a1 1 0 101 1A1.0011 1.0011 0 0027 15zM19 28a3 3 0 113-3A3.0033 3.0033 0 0119 28zm0-4a1 1 0 101 1A1.0011 1.0011 0 0019 24zM12 2H14V30H12zM7 28a3 3 0 113-3A3.0033 3.0033 0 017 28zm0-4a1 1 0 101 1A1.0011 1.0011 0 007 24zM7 19a3 3 0 113-3A3.0033 3.0033 0 017 19zm0-4a1 1 0 101 1A1.0011 1.0011 0 007 15zM7 10a3 3 0 113-3A3.0033 3.0033 0 017 10zM7 6A1 1 0 108 7 1.0011 1.0011 0 007 6z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default StemLeafPlot20;
