import { CarbonIconComponent } from "../types";
export const ScooterFront16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="ScooterFront16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M15 19H17V28H15z"></path>
    <path d="M23,10V8H20a2.0025,2.0025,0,0,0-2-2H14a2.0025,2.0025,0,0,0-2,2H9v2h3v4.1841A2.9957,2.9957,0,0,0,10,17v7h2V17a1.0008,1.0008,0,0,1,1-1h6a1.0008,1.0008,0,0,1,1,1v7h2V17a2.9957,2.9957,0,0,0-2-2.8159V10ZM18,8v6H14V8Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default ScooterFront16;
