import { CarbonIconComponent } from "../types";
export const Factor16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="Factor16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M27,28V9H24V4h2V2H20V4h2V9H19V28H15V16H12V11h2V9H8v2h2v5H7V28H4V2H2V28a2,2,0,0,0,2,2H30V28ZM13,28H9V18h4Zm8-17h4V28H21Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default Factor16;
