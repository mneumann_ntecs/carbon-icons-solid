import { CarbonIconComponent } from "../types";
export const TrainTime24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="TrainTime24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path
      fill="none"
      d="M25.496 10.088L22.874 7.466 22.874 3 25.124 3 25.124 6.534 27.088 8.498 25.496 10.088z"
    ></path>
    <path d="M24,1a6,6,0,1,0,6,6A6.0066,6.0066,0,0,0,24,1Zm1.4971,9.0884L22.875,7.4658V3h2.25V6.5342l1.9639,1.9634Z"></path>
    <path d="M6,16V10h9V8H6.1843A2.9948,2.9948,0,0,1,9,6h6V4H9A5.0057,5.0057,0,0,0,4,9V21a4.99,4.99,0,0,0,3.582,4.77L5.7693,30H7.9451l1.7143-4h8.6812l1.7143,4h2.1758L20.418,25.77A4.99,4.99,0,0,0,24,21V16Zm16,4H19v2h2.8157A2.9948,2.9948,0,0,1,19,24H9a2.9948,2.9948,0,0,1-2.8157-2H9V20H6V18H22Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default TrainTime24;
