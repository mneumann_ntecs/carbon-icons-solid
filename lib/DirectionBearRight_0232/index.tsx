import { CarbonIconComponent } from "../types";
export const DirectionBearRight_0232: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="DirectionBearRight_0232"
    fill={props.fill || "currentColor"}
    width={props.width || "32"}
    height={props.height || "32"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M16,4V6h6.5859L16.05,12.5356a6.954,6.954,0,0,0-2.05,4.95V28h2V17.4854A4.9683,4.9683,0,0,1,17.4644,13.95L24,7.4141V14h2V4Z"></path>
    <path d="M8.5 5.55H10.5V13.45H8.5z" transform="rotate(-45 9.5 9.5)"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default DirectionBearRight_0232;
