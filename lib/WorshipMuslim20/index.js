import {
  template,
  addEventListener,
  insert,
  memo,
  effect,
  setAttribute,
  style,
  delegateEvents,
} from "solid-js/web";

const _tmpl$ = template(
    `<svg data-carbon-icon="WorshipMuslim20" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" preserveAspectRatio="xMidYMid meet"><path d="M27,18a.9986.9986,0,0,1-.5415-.1594L22,14.9688l-4.4585,2.8718a1,1,0,0,1-1.5088-1.095l1.35-5.1318L14.3105,8.69a1,1,0,0,1,.5508-1.7146l4.3745-.6128,1.8663-3.8032A1.0008,1.0008,0,0,1,21.9878,2a1.0171,1.0171,0,0,1,.8989.5378L24.88,6.3625l4.2622.6133A1,1,0,0,1,29.69,8.69l-3.0723,2.9239,1.35,5.1318A1,1,0,0,1,27,18Zm-5-5.2207a.999.999,0,0,1,.5415.1594l2.8369,1.8272-.8457-3.2149a1.0006,1.0006,0,0,1,.2778-.9787l2.0074-1.91-2.73-.3929a.9994.9994,0,0,1-.7441-.5275L22.0269,5.2151,20.7979,7.72a1.0008,1.0008,0,0,1-.7593.55L17.19,8.6687l2,1.9036a1.0006,1.0006,0,0,1,.2778.9787l-.8457,3.2149,2.8369-1.8272A.999.999,0,0,1,22,12.7793Z"></path><path d="M15.8169,30A13.8137,13.8137,0,0,1,9.0752,4.1277a1,1,0,0,1,1.3613,1.3611A11.8091,11.8091,0,0,0,26.5107,21.5635a1,1,0,0,1,1.3619,1.3611A13.8411,13.8411,0,0,1,15.8169,30ZM7.3271,7.9727a11.81,11.81,0,0,0,16.7,16.7,13.8086,13.8086,0,0,1-16.7-16.7Z"></path></svg>`,
    6
  ),
  _tmpl$2 = template(`<title></title>`, 2);

const WorshipMuslim20 = (props) =>
  (() => {
    const _el$ = _tmpl$.cloneNode(true),
      _el$2 = _el$.firstChild;
    _el$2.nextSibling;

    addEventListener(_el$, "keydown", props.onKeyDown, true);

    addEventListener(_el$, "keyup", props.onKeyUp, true);

    addEventListener(_el$, "mouseleave", props.onMouseLeave);

    addEventListener(_el$, "mouseenter", props.onMouseEnter);

    addEventListener(_el$, "mouseover", props.onMouseOver, true);

    addEventListener(_el$, "click", props.onClick, true);

    insert(
      _el$,
      (() => {
        const _c$ = memo(() => props.children, true);

        return () =>
          _c$() ||
          (props.title &&
            (() => {
              const _el$4 = _tmpl$2.cloneNode(true);

              insert(_el$4, () => props.title);

              return _el$4;
            })());
      })(),
      null
    );

    effect(
      (_p$) => {
        const _v$ = props.fill || "currentColor",
          _v$2 = props.width || "20",
          _v$3 = props.height || "20",
          _v$4 = props["aria-label"],
          _v$5 = props["aria-labelledby"],
          _v$6 = props["aria-label"] || props["aria-labelledby"] || props.title,
          _v$7 =
            props["aria-label"] || props["aria-labelledby"] || props.title
              ? "img"
              : undefined,
          _v$8 = props.tabindex === "0" ? true : props.focusable,
          _v$9 = props.tabindex,
          _v$10 = props.id,
          _v$11 = props.class,
          _v$12 = props.title,
          _v$13 = props.style,
          _v$14 = props.stroke;

        _v$ !== _p$._v$ && setAttribute(_el$, "fill", (_p$._v$ = _v$));
        _v$2 !== _p$._v$2 && setAttribute(_el$, "width", (_p$._v$2 = _v$2));
        _v$3 !== _p$._v$3 && setAttribute(_el$, "height", (_p$._v$3 = _v$3));
        _v$4 !== _p$._v$4 &&
          setAttribute(_el$, "aria-label", (_p$._v$4 = _v$4));
        _v$5 !== _p$._v$5 &&
          setAttribute(_el$, "aria-labelledby", (_p$._v$5 = _v$5));
        _v$6 !== _p$._v$6 &&
          setAttribute(_el$, "aria-hidden", (_p$._v$6 = _v$6));
        _v$7 !== _p$._v$7 && setAttribute(_el$, "role", (_p$._v$7 = _v$7));
        _v$8 !== _p$._v$8 && setAttribute(_el$, "focusable", (_p$._v$8 = _v$8));
        _v$9 !== _p$._v$9 && setAttribute(_el$, "tabindex", (_p$._v$9 = _v$9));
        _v$10 !== _p$._v$10 && setAttribute(_el$, "id", (_p$._v$10 = _v$10));
        _v$11 !== _p$._v$11 && setAttribute(_el$, "class", (_p$._v$11 = _v$11));
        _v$12 !== _p$._v$12 && setAttribute(_el$, "title", (_p$._v$12 = _v$12));
        _p$._v$13 = style(_el$, _v$13, _p$._v$13);
        _v$14 !== _p$._v$14 &&
          setAttribute(_el$, "stroke", (_p$._v$14 = _v$14));
        return _p$;
      },
      {
        _v$: undefined,
        _v$2: undefined,
        _v$3: undefined,
        _v$4: undefined,
        _v$5: undefined,
        _v$6: undefined,
        _v$7: undefined,
        _v$8: undefined,
        _v$9: undefined,
        _v$10: undefined,
        _v$11: undefined,
        _v$12: undefined,
        _v$13: undefined,
        _v$14: undefined,
      }
    );

    return _el$;
  })();

delegateEvents(["click", "mouseover", "keyup", "keydown"]);

export default WorshipMuslim20;
export { WorshipMuslim20 };
