import { CarbonIconComponent } from "../types";
export const TrafficCone24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="TrafficCone24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M24.7957,28,18.9746,2.7754A1,1,0,0,0,18,2H14a1,1,0,0,0-.9746.7754L7.2043,28H4v2H28V28ZM19.9736,16H12.0264l.9229-4h6.1014Zm.4616,2,.923,4H10.6418l.923-4Zm-5.64-14h2.4092l1.3845,6H13.4109ZM10.18,24H21.82l.923,4H9.2573Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default TrafficCone24;
