import { CarbonIconComponent } from "../types";
export const ChangeCatalog20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="ChangeCatalog20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M24 24v2h2.4592A5.94 5.94 0 0122 28a6.0066 6.0066 0 01-6-6H14a7.9841 7.9841 0 0014 5.2651V30h2V24zM22 14a8.04 8.04 0 00-6 2.7349V14H14v6h6V18H17.5408A5.94 5.94 0 0122 16a6.0066 6.0066 0 016 6h2A8.0092 8.0092 0 0022 14z"></path>
    <path d="M12,28H6V24H8V22H6V17H8V15H6V10H8V8H6V4H24v8h2V4a2,2,0,0,0-2-2H6A2,2,0,0,0,4,4V8H2v2H4v5H2v2H4v5H2v2H4v4a2,2,0,0,0,2,2h6Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default ChangeCatalog20;
