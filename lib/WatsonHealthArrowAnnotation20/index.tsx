import { CarbonIconComponent } from "../types";
export const WatsonHealthArrowAnnotation20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="WatsonHealthArrowAnnotation20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M13.71 12.29L7.42 6 14 6 14 4 4 4 4 13.99 6 14.01 6 7.41 12.29 13.7 13.71 12.29zM28 10H18v2H28V28H12V18H10V28a2 2 0 002 2H28a2 2 0 002-2V12A2 2 0 0028 10z"></path>
    <path d="M19 25L21 25 21 18 24 18 24 16 16 16 16 18 19 18 19 25z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default WatsonHealthArrowAnnotation20;
