import { CarbonIconComponent } from "../types";
export const NavaidMilitaryCivil32: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="NavaidMilitaryCivil32"
    fill={props.fill || "currentColor"}
    width={props.width || "32"}
    height={props.height || "32"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M16,24a8,8,0,1,1,8-8A8.0092,8.0092,0,0,1,16,24Zm0-14a6,6,0,1,0,6,6A6.0067,6.0067,0,0,0,16,10Z"></path>
    <path d="M30,14H27.8193A12.0112,12.0112,0,0,0,18,4.1807V2H14V4.1807A12.0112,12.0112,0,0,0,4.1807,14H2v4H4.1807A12.0112,12.0112,0,0,0,14,27.8193V30h4V27.8193A12.0112,12.0112,0,0,0,27.8193,18H30ZM16,26A10,10,0,1,1,26,16,10.0114,10.0114,0,0,1,16,26Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default NavaidMilitaryCivil32;
