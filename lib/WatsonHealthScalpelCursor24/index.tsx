import { CarbonIconComponent } from "../types";
export const WatsonHealthScalpelCursor24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="WatsonHealthScalpelCursor24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M28.8281 7.1338a4.0941 4.0941 0 00-5.6562 0L.3433 29.9619H9.5889a4.9682 4.9682 0 003.5356-1.4648L28.8281 12.79a3.9984 3.9984 0 000-5.6562zM12.2928 20.8406l2.086-2.0858 2.8293 2.8293L15.1224 23.67zM11.71 27.083a2.9824 2.9824 0 01-2.1215.8789H5.1714l5.7073-5.7072 2.83 2.83zM27.4141 11.376L18.6221 20.17l-2.8293-2.8292 8.7931-8.7928a2.0471 2.0471 0 012.8282 0 1.9993 1.9993 0 010 2.8281zM13 4L4 4 4 13 6 13 6 6 13 6 13 4z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default WatsonHealthScalpelCursor24;
