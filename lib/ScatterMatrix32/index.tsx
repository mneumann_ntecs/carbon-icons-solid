import { CarbonIconComponent } from "../types";
export const ScatterMatrix32: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="ScatterMatrix32"
    fill={props.fill || "currentColor"}
    width={props.width || "32"}
    height={props.height || "32"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <circle cx="9.5" cy="9.5" r="2.5"></circle>
    <circle cx="9.5" cy="22.5" r="2.5"></circle>
    <circle cx="22.5" cy="22.5" r="2.5"></circle>
    <path d="M28,2H4A2.0023,2.0023,0,0,0,2,4V28a2.0023,2.0023,0,0,0,2,2H28a2.0023,2.0023,0,0,0,2-2V4A2.0023,2.0023,0,0,0,28,2Zm0,13H17V4H28ZM15,4V15H4V4ZM4,17H15V28H4ZM17,28V17H28V28Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default ScatterMatrix32;
