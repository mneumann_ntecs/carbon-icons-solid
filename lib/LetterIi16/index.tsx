import { CarbonIconComponent } from "../types";
export const LetterIi16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="LetterIi16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M10 11L13 11 13 21 10 21 10 23 18 23 18 21 15 21 15 11 18 11 18 9 10 9 10 11zM20 13H22V23H20zM20 9H22V11H20z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default LetterIi16;
