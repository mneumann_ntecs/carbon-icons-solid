import { CarbonIconComponent } from "../types";
export const WatsonHealthTextAnnotationToggle32: CarbonIconComponent = (
  props
) => (
  <svg
    data-carbon-icon="WatsonHealthTextAnnotationToggle32"
    fill={props.fill || "currentColor"}
    width={props.width || "32"}
    height={props.height || "32"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M29.537 13.76L26.24 10.463a1.5862 1.5862 0 00-2.24 0L10 24.4674V30h5.5326L29.537 16a1.5862 1.5862 0 000-2.24zM14.7042 28H12V25.2958l9.4409-9.4409 2.7042 2.7042zM25.5591 17.145L22.855 14.4409l2.2672-2.2672 2.7042 2.7042zM11 17L13 17 13 10 16 10 16 8 8 8 8 10 11 10 11 17z"></path>
    <path d="M8,20H4V4H20V8h2V4a2,2,0,0,0-2-2H4A2,2,0,0,0,2,4V20a2,2,0,0,0,2,2H8Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default WatsonHealthTextAnnotationToggle32;
