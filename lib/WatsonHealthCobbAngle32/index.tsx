import { CarbonIconComponent } from "../types";
export const WatsonHealthCobbAngle32: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="WatsonHealthCobbAngle32"
    fill={props.fill || "currentColor"}
    width={props.width || "32"}
    height={props.height || "32"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M20 17V15a2 2 0 00-2-2H10a2 2 0 00-2 2v2a2 2 0 002 2h8A2 2 0 0020 17zM10 15h8v2H10zM23.42 24.48A2 2 0 0021 23.06l-7.73 2.07a2 2 0 00-1.42 2.44h0l.52 1.93A2 2 0 0014.27 31a3 3 0 00.52-.07l7.73-2.07h0a2 2 0 001.41-2.45zM14.27 29l-.51-1.94L21.48 25 22 26.92zM13.24 6.86L21 8.93h0a2.24 2.24 0 00.51.07 2 2 0 001.94-1.48L24 5.58a2 2 0 00-1.41-2.45L14.79 1.06a2 2 0 00-2.45 1.41h0l-.52 1.93a2 2 0 001.42 2.44zm1-3.86L22 5.07 21.48 7 13.76 4.93zM14 21H16V23H14zM14 9H16V11H14z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default WatsonHealthCobbAngle32;
