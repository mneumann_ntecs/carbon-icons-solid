import { CarbonIconComponent } from "../types";
export const WatsonHealthSubVolume32: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="WatsonHealthSubVolume32"
    fill={props.fill || "currentColor"}
    width={props.width || "32"}
    height={props.height || "32"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M29.4741,19.12,23.6807,16l5.7934-3.12a1,1,0,0,0,0-1.7607l-13-7a.9982.9982,0,0,0-.9482,0l-13,7a1,1,0,0,0,0,1.7607L8.3193,16,2.5259,19.12a1,1,0,0,0,0,1.7607l13,7a.9995.9995,0,0,0,.9482,0l13-7a1,1,0,0,0,0-1.7607ZM16,6.1359,26.8906,12,16,17.8643,5.1094,12Zm0,19.7284L5.1094,20l5.3193-2.8642L15.5259,19.88a.9995.9995,0,0,0,.9482,0l5.0972-2.7446L26.8906,20Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default WatsonHealthSubVolume32;
