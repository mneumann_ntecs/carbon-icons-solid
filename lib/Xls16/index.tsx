import { CarbonIconComponent } from "../types";
export const Xls16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="Xls16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M28 23H22V21h6V17H24a2.002 2.002 0 01-2-2V11a2.002 2.002 0 012-2h6v2H24v4h4a2.002 2.002 0 012 2v4A2.0023 2.0023 0 0128 23zM14 21L14 9 12 9 12 23 20 23 20 21 14 21zM10 9L8 9 6 15 4 9 2 9 4.752 16 2 23 4 23 6 17 8 23 10 23 7.245 16 10 9z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default Xls16;
