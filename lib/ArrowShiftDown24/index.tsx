import { CarbonIconComponent } from "../types";
export const ArrowShiftDown24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="ArrowShiftDown24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M23,6V16h5a1,1,0,0,1,.707,1.707l-12,12a.9994.9994,0,0,1-1.414,0l-12-12A1,1,0,0,1,4,16H9V6a2.0023,2.0023,0,0,1,2-2H21A2.0027,2.0027,0,0,1,23,6ZM16,27.5859,25.5859,18H21V6H11V18H6.4141Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default ArrowShiftDown24;
