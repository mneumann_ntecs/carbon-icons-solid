import { CarbonIconComponent } from "../types";
export const RestaurantFine20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="RestaurantFine20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M30 11.0005v-8a1 1 0 00-1-1H19a1 1 0 00-1 1v8A6.0039 6.0039 0 0023 16.91V28H19v2H29V28H25V16.91A6.0039 6.0039 0 0030 11.0005zm-10 0V4h8v7.0005a4 4 0 11-8 0zM12 2v9.02a3.9644 3.9644 0 01-3.96 3.96A4.0052 4.0052 0 014 11.02V2H2v9.02a5.9888 5.9888 0 005 5.8652V30H9V16.8953A5.9646 5.9646 0 0014 11.02V2z"></path>
    <path d="M7 2H9V11.98H7z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default RestaurantFine20;
