import { CarbonIconComponent } from "../types";
export const WatsonHealthPointerText16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="WatsonHealthPointerText16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M13.71 12.29L7.41 6 13 6 13 4 4 4 4 13 6 13 6 7.41 12.29 13.71 13.71 12.29zM28 30H12a2 2 0 01-2-2V18h2V28H28V12H18V10H28a2 2 0 012 2V28A2 2 0 0128 30z"></path>
    <path d="M22,15H17v2h5v2H18a2,2,0,0,0-2,2v2a2,2,0,0,0,2,2h6V17A2,2,0,0,0,22,15Zm0,8H18V21h4Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default WatsonHealthPointerText16;
