import { CarbonIconComponent } from "../types";
export const FlightRoster16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="FlightRoster16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path
      fill-rule="evenodd"
      d="M26,6a2,2,0,0,0-2-2H8A2,2,0,0,0,6,6V26a2,2,0,0,0,2,2h8V26H8V6H24v6h2Z"
    ></path>
    <path d="M10 18H16V20H10zM10 14H22V16H10z"></path>
    <path
      fill-rule="evenodd"
      d="M22 10v2H10V10zM25 23l5 2V23l-5-2.5V18a1 1 0 00-2 0v2.5L18 23v2l5-2v3.5L21 28v1l3-1 3 1V28l-2-1.5z"
    ></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default FlightRoster16;
