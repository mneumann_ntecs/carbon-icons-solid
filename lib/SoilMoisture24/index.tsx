import { CarbonIconComponent } from "../types";
export const SoilMoisture24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="SoilMoisture24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M24.5,28A5.385,5.385,0,0,1,19,22.751a5.3837,5.3837,0,0,1,.874-2.8308L23.49,14.5383a1.217,1.217,0,0,1,2.02,0L29.06,19.8154A5.4923,5.4923,0,0,1,30,22.751,5.385,5.385,0,0,1,24.5,28Zm0-11.38-2.9356,4.3672A3.3586,3.3586,0,0,0,21,22.751a3.51,3.51,0,0,0,7,0,3.4356,3.4356,0,0,0-.63-1.867Z"></path>
    <circle cx="5" cy="13" r="1"></circle>
    <circle cx="11" cy="19" r="1"></circle>
    <circle cx="15" cy="25" r="1"></circle>
    <circle cx="17" cy="15" r="1"></circle>
    <circle cx="13" cy="11" r="1"></circle>
    <circle cx="27" cy="11" r="1"></circle>
    <circle cx="9" cy="27" r="1"></circle>
    <circle cx="3" cy="21" r="1"></circle>
    <path d="M2 6H30V8H2z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default SoilMoisture24;
