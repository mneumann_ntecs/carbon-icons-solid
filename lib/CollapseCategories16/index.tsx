import { CarbonIconComponent } from "../types";
export const CollapseCategories16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="CollapseCategories16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M14 25H28V27H14zM7.17 26L4.59 28.58 6 30 10 26 6 22 4.58 23.41 7.17 26zM14 15H28V17H14zM7.17 16L4.59 18.58 6 20 10 16 6 12 4.58 13.41 7.17 16zM14 5H28V7H14zM7.17 6L4.59 8.58 6 10 10 6 6 2 4.58 3.41 7.17 6z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default CollapseCategories16;
