import { CarbonIconComponent } from "../types";
export const Loop24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="Loop24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M23.59 19.41L26 22 21 22 21 24 26 24 23.59 26.59 25 28 30 23 25 18 23.59 19.41zM13 6A9 9 0 005.52 20l1.66-1.11A7 7 0 1113 22H2v2H13A9 9 0 0013 6z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default Loop24;
