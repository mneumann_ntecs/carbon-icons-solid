import {
  template,
  addEventListener,
  insert,
  memo,
  effect,
  setAttribute,
  style,
  delegateEvents,
} from "solid-js/web";

const _tmpl$ = template(
    `<svg data-carbon-icon="Apple24" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" preserveAspectRatio="xMidYMid meet"><path d="M21.7788,8a11.4843,11.4843,0,0,0-3.0547.5342,14.5665,14.5665,0,0,1-1.8225.4126A6.0054,6.0054,0,0,0,11,4V6a3.9933,3.9933,0,0,1,3.8267,2.9c-.5181-.0879-1.0569-.2207-1.6172-.3648A12.89,12.89,0,0,0,10.0317,8C6.311,8,4,11.0652,4,16c0,7.51,4.0679,11.999,8.0005,12h.0015a7.1576,7.1576,0,0,0,2.49-.59A5.0128,5.0128,0,0,1,16,27a5.0266,5.0266,0,0,1,1.5112.41A7.15,7.15,0,0,0,20,28h0c3.9307-.001,8-4.49,8-12C28,12.3094,26.3706,8,21.7788,8ZM20,26h0a5.5038,5.5038,0,0,1-1.7905-.4639A6.4168,6.4168,0,0,0,16,25a6.4043,6.4043,0,0,0-2.2061.5361A5.5343,5.5343,0,0,1,12,26h.0015C9.0513,25.9988,6,22.2586,6,16c0-1.8037.3931-6,4.0317-6a11.2363,11.2363,0,0,1,2.6807.4726A12.9819,12.9819,0,0,0,15.7612,11h.4922a11.7955,11.7955,0,0,0,2.9956-.5361A9.8483,9.8483,0,0,1,21.7788,10C25.7964,10,26,14.9978,26,16,26,22.2586,22.9482,25.9988,20,26Z"></path><path d="M18,7H17V6a2.0021,2.0021,0,0,1,2-2h1V5A2.0021,2.0021,0,0,1,18,7Z"></path></svg>`,
    6
  ),
  _tmpl$2 = template(`<title></title>`, 2);

const Apple24 = (props) =>
  (() => {
    const _el$ = _tmpl$.cloneNode(true),
      _el$2 = _el$.firstChild;
    _el$2.nextSibling;

    addEventListener(_el$, "keydown", props.onKeyDown, true);

    addEventListener(_el$, "keyup", props.onKeyUp, true);

    addEventListener(_el$, "mouseleave", props.onMouseLeave);

    addEventListener(_el$, "mouseenter", props.onMouseEnter);

    addEventListener(_el$, "mouseover", props.onMouseOver, true);

    addEventListener(_el$, "click", props.onClick, true);

    insert(
      _el$,
      (() => {
        const _c$ = memo(() => props.children, true);

        return () =>
          _c$() ||
          (props.title &&
            (() => {
              const _el$4 = _tmpl$2.cloneNode(true);

              insert(_el$4, () => props.title);

              return _el$4;
            })());
      })(),
      null
    );

    effect(
      (_p$) => {
        const _v$ = props.fill || "currentColor",
          _v$2 = props.width || "24",
          _v$3 = props.height || "24",
          _v$4 = props["aria-label"],
          _v$5 = props["aria-labelledby"],
          _v$6 = props["aria-label"] || props["aria-labelledby"] || props.title,
          _v$7 =
            props["aria-label"] || props["aria-labelledby"] || props.title
              ? "img"
              : undefined,
          _v$8 = props.tabindex === "0" ? true : props.focusable,
          _v$9 = props.tabindex,
          _v$10 = props.id,
          _v$11 = props.class,
          _v$12 = props.title,
          _v$13 = props.style,
          _v$14 = props.stroke;

        _v$ !== _p$._v$ && setAttribute(_el$, "fill", (_p$._v$ = _v$));
        _v$2 !== _p$._v$2 && setAttribute(_el$, "width", (_p$._v$2 = _v$2));
        _v$3 !== _p$._v$3 && setAttribute(_el$, "height", (_p$._v$3 = _v$3));
        _v$4 !== _p$._v$4 &&
          setAttribute(_el$, "aria-label", (_p$._v$4 = _v$4));
        _v$5 !== _p$._v$5 &&
          setAttribute(_el$, "aria-labelledby", (_p$._v$5 = _v$5));
        _v$6 !== _p$._v$6 &&
          setAttribute(_el$, "aria-hidden", (_p$._v$6 = _v$6));
        _v$7 !== _p$._v$7 && setAttribute(_el$, "role", (_p$._v$7 = _v$7));
        _v$8 !== _p$._v$8 && setAttribute(_el$, "focusable", (_p$._v$8 = _v$8));
        _v$9 !== _p$._v$9 && setAttribute(_el$, "tabindex", (_p$._v$9 = _v$9));
        _v$10 !== _p$._v$10 && setAttribute(_el$, "id", (_p$._v$10 = _v$10));
        _v$11 !== _p$._v$11 && setAttribute(_el$, "class", (_p$._v$11 = _v$11));
        _v$12 !== _p$._v$12 && setAttribute(_el$, "title", (_p$._v$12 = _v$12));
        _p$._v$13 = style(_el$, _v$13, _p$._v$13);
        _v$14 !== _p$._v$14 &&
          setAttribute(_el$, "stroke", (_p$._v$14 = _v$14));
        return _p$;
      },
      {
        _v$: undefined,
        _v$2: undefined,
        _v$3: undefined,
        _v$4: undefined,
        _v$5: undefined,
        _v$6: undefined,
        _v$7: undefined,
        _v$8: undefined,
        _v$9: undefined,
        _v$10: undefined,
        _v$11: undefined,
        _v$12: undefined,
        _v$13: undefined,
        _v$14: undefined,
      }
    );

    return _el$;
  })();

delegateEvents(["click", "mouseover", "keyup", "keydown"]);

export default Apple24;
export { Apple24 };
