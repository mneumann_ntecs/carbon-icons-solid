import { CarbonIconComponent } from "../types";
export const CheckmarkOutlineWarning16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="CheckmarkOutlineWarning16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M14,24A10,10,0,1,1,24,14h2A12,12,0,1,0,14,26Z"></path>
    <path d="M12 15.59L9.41 13 8 14.41 12 18.41 19 11.41 17.59 10 12 15.59zM27.38 28H20.6178L24 21.2358zM24 18a1 1 0 00-.8947.5527l-5 10A1.0005 1.0005 0 0019 30H29a1 1 0 00.9214-1.3892L24.8946 18.5527A1 1 0 0024 18z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default CheckmarkOutlineWarning16;
