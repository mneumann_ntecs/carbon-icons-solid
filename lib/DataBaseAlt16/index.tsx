import { CarbonIconComponent } from "../types";
export const DataBaseAlt16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="DataBaseAlt16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M19 24H26V26H19zM19 20H26V22H19zM19 16H26V18H19zM6 24H13V26H6zM6 20H13V22H6z"></path>
    <path d="M28,4H17a2.0023,2.0023,0,0,0-2,2v6H4a2.0023,2.0023,0,0,0-2,2V28a2.0023,2.0023,0,0,0,2,2H28a2.0023,2.0023,0,0,0,2-2V6A2.0023,2.0023,0,0,0,28,4ZM15,28H4V14H15Zm2,0V6H28V28Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default DataBaseAlt16;
