import { CarbonIconComponent } from "../types";
export const ResearchHintonPlot32: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="ResearchHintonPlot32"
    fill={props.fill || "currentColor"}
    width={props.width || "32"}
    height={props.height || "32"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M2 2H6V6H2zM10 2H14V6H10zM18 2H22V6H18zM26 2H30V6H26zM2 10H6V14H2zM10 10H14V14H10zM18 10H22V14H18zM26 10H30V14H26zM2 18H6V22H2zM10 18H14V22H10zM18 18H22V22H18zM26 18H30V22H26zM2 26H6V30H2zM10 26H14V30H10zM18 26H22V30H18zM26 26H30V30H26z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default ResearchHintonPlot32;
