import { CarbonIconComponent } from "../types";
export const Diagram24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="Diagram24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M13,30A11,11,0,0,1,13,8a1,1,0,0,1,1,1v9h9a1,1,0,0,1,1,1A11,11,0,0,1,13,30ZM12,10.06A9,9,0,1,0,21.94,20H14a2,2,0,0,1-2-2Z"></path>
    <path d="M28,14H19a2,2,0,0,1-2-2V3a1,1,0,0,1,1-1A11,11,0,0,1,29,13,1,1,0,0,1,28,14Zm-9-2h7.94A9,9,0,0,0,19,4.06Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default Diagram24;
