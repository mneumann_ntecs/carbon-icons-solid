import { CarbonIconComponent } from "../types";
export const NoTicket32: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="NoTicket32"
    fill={props.fill || "currentColor"}
    width={props.width || "32"}
    height={props.height || "32"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M28 6h-.5859L30 3.4141 28.5859 2 2 28.5859 3.4141 30l4-4H28a2.0027 2.0027 0 002-2V19a1 1 0 00-1-1 2 2 0 010-4 1 1 0 001-1V8A2.0023 2.0023 0 0028 6zm0 6.1265a4 4 0 000 7.7465V24H21V21H19v3H9.4141L19 14.4141V19h2V12.4141L25.4141 8H28zM4 12.1265V8H19V6H4A2.0023 2.0023 0 002 8v5a1 1 0 001 1 2 2 0 010 4 1 1 0 00-1 1v5H4V19.873a4 4 0 000-7.7465z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default NoTicket32;
