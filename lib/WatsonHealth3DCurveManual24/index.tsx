import { CarbonIconComponent } from "../types";
export const WatsonHealth3DCurveManual24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="WatsonHealth3DCurveManual24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M15 21a3 3 0 01-3 3h-.1a5 5 0 100 2H12a5 5 0 005-5zM7 28a3 3 0 113-3A3 3 0 017 28zM15 13H17V19H15zM25 2a5 5 0 00-4.9 4H20a5 5 0 00-5 5h2a3 3 0 013-3h.1A5 5 0 1025 2zm0 8a3 3 0 113-3A3 3 0 0125 10z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default WatsonHealth3DCurveManual24;
