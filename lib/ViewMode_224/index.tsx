import { CarbonIconComponent } from "../types";
export const ViewMode_224: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="ViewMode_224"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M4 26H28V28H4zM4 19H28V21H4zM12 6v6H6V6h6m0-2H6A2 2 0 004 6v6a2 2 0 002 2h6a2 2 0 002-2V6a2 2 0 00-2-2zM26 6v6H20V6h6m0-2H20a2 2 0 00-2 2v6a2 2 0 002 2h6a2 2 0 002-2V6a2 2 0 00-2-2z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default ViewMode_224;
