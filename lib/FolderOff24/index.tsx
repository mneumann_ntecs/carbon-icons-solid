import { CarbonIconComponent } from "../types";
export const FolderOff24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="FolderOff24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M28 8H25.4141L30 3.4141 28.5859 2 2 28.5859 3.4141 30l2-2H28a2 2 0 002-2V10A2 2 0 0028 8zm0 18H7.4141l16-16H28zM4 6h7.1716l3.4141 3.4141L15.1716 10H18V8H16L12.5859 4.5859A2.0007 2.0007 0 0011.1716 4H4A2 2 0 002 6V24H4z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default FolderOff24;
