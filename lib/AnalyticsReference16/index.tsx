import { CarbonIconComponent } from "../types";
export const AnalyticsReference16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="AnalyticsReference16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M4 20L4 22 8.586 22 2 28.586 3.414 30 10 23.414 10 28 12 28 12 20 4 20zM30 4H23V6h3.5859L19 13.5859 14.707 9.293a1 1 0 00-1.414 0L8 14.5858 9.4142 16 14 11.4141l4.293 4.2929a1 1 0 001.414 0L28 7.4141V11h2zM16 28H30V30H16zM2 2H4V16H2z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default AnalyticsReference16;
