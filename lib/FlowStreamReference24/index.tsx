import { CarbonIconComponent } from "../types";
export const FlowStreamReference24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="FlowStreamReference24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M20 24H16v2h4v3h8V21H20zm2-1h4v4H22zM4 20L4 22 8.586 22 2 28.586 3.414 30 10 23.414 10 28 12 28 12 20 4 20zM24 5a3.9962 3.9962 0 00-3.8579 3H14v2h6.1421a3.94 3.94 0 00.4248 1.019L14 17.5862 15.4138 19l6.5672-6.5669A3.9521 3.9521 0 0024 13a4 4 0 000-8zm0 6a2 2 0 112-2A2.0023 2.0023 0 0124 11zM9.6929 12.7505a5 5 0 01-.0005-7.5L11.0166 6.75a3 3 0 00-.0005 4.501z"></path>
    <path d="M7.0474,15.751a9,9,0,0,1,0-13.501l1.3232,1.5a7,7,0,0,0,0,10.501Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default FlowStreamReference24;
