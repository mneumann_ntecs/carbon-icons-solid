import { CarbonIconComponent } from "../types";
export const Misuse20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="Misuse20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 20 20"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M10,1c-4.9,0-9,4.1-9,9s4.1,9,9,9s9-4,9-9S15,1,10,1z M13.5,14.5L10,11l-3.5,3.5l-1-1L9,10L5.5,6.5l1-1L10,9l3.5-3.5l1,1	L11,10l3.5,3.5L13.5,14.5z"></path>
    <path
      fill="none"
      d="M13.5,14.5L10,11l-3.5,3.5l-1-1L9,10L5.5,6.5l1-1L10,9l3.5-3.5l1,1L11,10l3.5,3.5L13.5,14.5z"
      data-icon-path="inner-path"
      opacity="0"
    ></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default Misuse20;
