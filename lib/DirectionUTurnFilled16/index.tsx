import { CarbonIconComponent } from "../types";
export const DirectionUTurnFilled16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="DirectionUTurnFilled16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M28,2H4A2,2,0,0,0,2,4V28a2,2,0,0,0,2,2H28a2,2,0,0,0,2-2V4A2,2,0,0,0,28,2ZM21,26l-7-7,1.4141-1.4141L20,22.1719V8H8V26H6V8A2.0023,2.0023,0,0,1,8,6H20a2.0023,2.0023,0,0,1,2,2V22.1719l4.5859-4.586L28,19Z"></path>
    <path
      fill="none"
      d="M21,26l-7-7,1.4141-1.4141L20,22.1719V8H8V26H6V8A2.0023,2.0023,0,0,1,8,6H20a2.0023,2.0023,0,0,1,2,2V22.1719l4.5859-4.586L28,19Z"
      data-icon-path="inner-path"
    ></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default DirectionUTurnFilled16;
