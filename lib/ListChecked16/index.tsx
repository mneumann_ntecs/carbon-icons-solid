import { CarbonIconComponent } from "../types";
export const ListChecked16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="ListChecked16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M16 8H30V10H16zM6 10.59L3.41 8 2 9.41 6 13.41 14 5.41 12.59 4 6 10.59zM16 22H30V24H16zM6 24.59L3.41 22 2 23.41 6 27.41 14 19.41 12.59 18 6 24.59z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default ListChecked16;
