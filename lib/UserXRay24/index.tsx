import { CarbonIconComponent } from "../types";
export const UserXRay24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="UserXRay24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path
      fill="none"
      d="M28 9L28 7 25 7 25 5 23 5 23 7 20 7 20 9 23 9 23 12 20 12 20 14 23 14 23 16 25 16 25 14 28 14 28 12 25 12 25 9 28 9z"
    ></path>
    <path d="M31 3H17a1 1 0 00-1 1V17a1 1 0 001 1H31a1 1 0 001-1V4A1 1 0 0031 3zM28 9H25v3h3v2H25v2H23V14H20V12h3V9H20V7h3V5h2V7h3zM15 30H13V26a2.9465 2.9465 0 00-3-3H6a2.9465 2.9465 0 00-3 3v4H1V26a4.9514 4.9514 0 015-5h4a4.9514 4.9514 0 015 5zM8 11a3 3 0 010 6 3 3 0 010-6M8 9A5 5 0 008 19 5 5 0 008 9z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default UserXRay24;
