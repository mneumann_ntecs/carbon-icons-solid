import { CarbonIconComponent } from "../types";
export const AlarmAdd24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="AlarmAdd24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M16,28A11,11,0,1,1,27,17,11,11,0,0,1,16,28ZM16,8a9,9,0,1,0,9,9A9,9,0,0,0,16,8Z"></path>
    <path
      d="M3.96 5.5H9.030000000000001V7.5H3.96z"
      transform="rotate(-45.06 6.502 6.497)"
    ></path>
    <path
      d="M24.5 3.96H26.5V9.030000000000001H24.5z"
      transform="rotate(-44.94 25.5 6.498)"
    ></path>
    <path d="M21 16L17 16 17 12 15 12 15 16 11 16 11 18 15 18 15 22 17 22 17 18 21 18 21 16z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default AlarmAdd24;
