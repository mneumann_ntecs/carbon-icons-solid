import { CarbonIconComponent } from "../types";
export const DistributeVerticalCenter20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="DistributeVerticalCenter20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M30 21H26V20a2.0023 2.0023 0 00-2-2H8a2.0023 2.0023 0 00-2 2v1H2v2H6v1a2.0023 2.0023 0 002 2H24a2.0023 2.0023 0 002-2V23h4zm-6 3H8V20l16-.001zM30 9H22V8a2.0023 2.0023 0 00-2-2H12a2.0023 2.0023 0 00-2 2V9H2v2h8v1a2.0023 2.0023 0 002 2h8a2.0023 2.0023 0 002-2V11h8zM20 12H12V8l8-.001z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default DistributeVerticalCenter20;
