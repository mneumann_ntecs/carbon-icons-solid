import { CarbonIconComponent } from "../types";
export const Harbor20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="Harbor20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M26,16a10.0132,10.0132,0,0,1-9,9.9492V16h3V14H17V9.8579a4,4,0,1,0-2,0V14H12v2h3v9.9492A10.0132,10.0132,0,0,1,6,16H4a12,12,0,0,0,24,0ZM14,6a2,2,0,1,1,2,2A2.0023,2.0023,0,0,1,14,6Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default Harbor20;
