import { CarbonIconComponent } from "../types";
export const ChartComboStacked20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="ChartComboStacked20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M27,28V16H19V28H15V12H7V28H4V2H2V28a2,2,0,0,0,2,2H30V28ZM13,14v6H9V14Zm12,4v6H21V18Z"></path>
    <path d="M22.7856,14a1.9877,1.9877,0,0,1-1.1787-.3865L11.2041,5.987,8.2419,10,6.6374,8.8065l2.9812-4a1.9978,1.9978,0,0,1,2.749-.446l10.4214,7.64,3.6045-4.8613L28,8.3306l-3.6045,4.8611A2.0014,2.0014,0,0,1,22.7856,14Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default ChartComboStacked20;
