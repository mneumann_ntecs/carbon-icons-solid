import { CarbonIconComponent } from "../types";
export const Restaurant32: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="Restaurant32"
    fill={props.fill || "currentColor"}
    width={props.width || "32"}
    height={props.height || "32"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M9 2H11V12H9z"></path>
    <path d="M14 11a4 4 0 01-8 0V2H4v9a6 6 0 005 5.91V30h2V16.91A6 6 0 0016 11V2H14zM22 2H21V30h2V20h3a2 2 0 002-2V8A5.78 5.78 0 0022 2zm4 16H23V4.09c2.88.56 3 3.54 3 3.91z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default Restaurant32;
