import { CarbonIconComponent } from "../types";
export const Opacity24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="Opacity24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M6 6H10V10H6zM10 10H14V14H10zM14 6H18V10H14zM22 6H26V10H22zM6 14H10V18H6zM14 14H18V18H14zM22 14H26V18H22zM6 22H10V26H6zM14 22H18V26H14zM22 22H26V26H22zM18 10H22V14H18zM10 18H14V22H10zM18 18H22V22H18z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default Opacity24;
