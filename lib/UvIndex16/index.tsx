import { CarbonIconComponent } from "../types";
export const UvIndex16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="UvIndex16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M16,5.9121l1.7444,2.9326.7822,1.315,1.4739-.4107,3.1206-.87L22.2512,12,21.84,13.4731l1.315.7823L26.0876,16l-2.9323,1.7441-1.315.7818L22.2512,20l.87,3.1211-3.1208-.87L18.5266,21.84l-.7822,1.315L16,26.0879l-1.7444-2.9326-.7822-1.315L12,22.251l-3.1208.87L9.7488,20l.4109-1.4741-1.315-.7818L5.9124,16l2.9323-1.7446,1.315-.7823L9.7488,12l-.87-3.1206L12,9.749l1.4739.4107.7822-1.315L16,5.9121M16,2,12.5366,7.8223,6,6l1.8223,6.5366L2,16l5.8223,3.4629L6,26l6.5366-1.8223L16,30l3.4634-5.8223L26,26l-1.8223-6.5371L30,16l-5.8223-3.4634L26,6,19.4634,7.8223,16,2Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default UvIndex16;
