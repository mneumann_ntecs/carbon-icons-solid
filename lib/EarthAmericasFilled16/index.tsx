import { CarbonIconComponent } from "../types";
export const EarthAmericasFilled16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="EarthAmericasFilled16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M16,2A14,14,0,1,0,30,16,14.0158,14.0158,0,0,0,16,2ZM4,16a11.915,11.915,0,0,1,.7-4H10l5,4-2.8,3.6929A1,1,0,0,0,12.293,21L15,24v3.9492A12.01,12.01,0,0,1,4,16ZM21.4348,26.6846l2.5457-7.6992A1,1,0,0,0,23.614,18L15,11l2-2h5.2793a1,1,0,0,0,.9487-.6836L23.7234,6.83a11.9742,11.9742,0,0,1-2.2886,19.855Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default EarthAmericasFilled16;
