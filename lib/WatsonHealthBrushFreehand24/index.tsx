import { CarbonIconComponent } from "../types";
export const WatsonHealthBrushFreehand24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="WatsonHealthBrushFreehand24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M28.8281 3.1719a4.0941 4.0941 0 00-5.6562 0L4.05 22.292A6.9537 6.9537 0 002 27.2412V30H4.7559a6.9523 6.9523 0 004.95-2.05L28.8281 8.8286a3.999 3.999 0 000-5.6567zM10.91 18.26l2.8286 2.8286L11.6172 23.21 8.7886 20.3818zM8.2915 26.5356A4.9665 4.9665 0 014.7559 28H4v-.7588a4.9669 4.9669 0 011.4644-3.5351l1.91-1.91 2.8286 2.8281zM27.4141 7.4141L15.1528 19.6748l-2.8286-2.8286 12.2617-12.26a2.0473 2.0473 0 012.8282 0 1.9995 1.9995 0 010 2.8282zM6.5 15A3.4994 3.4994 0 014.0249 9.026l3.5005-3.5a1.5019 1.5019 0 000-2.121 1.537 1.537 0 00-2.1216 0L3.415 5.3936 2 3.98 3.99 1.9915a3.5849 3.5849 0 014.95 0 3.5039 3.5039 0 010 4.949L5.439 10.44a1.5019 1.5019 0 000 2.121 1.5369 1.5369 0 002.1215 0l4.0249-4.0243L13 9.9507 8.9746 13.975A3.4754 3.4754 0 016.5 15z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default WatsonHealthBrushFreehand24;
