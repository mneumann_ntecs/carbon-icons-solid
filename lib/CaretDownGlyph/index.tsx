import { CarbonIconComponent } from "../types";
export const CaretDownGlyph: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="CaretDownGlyph"
    fill={props.fill || "currentColor"}
    width={props.width || "8"}
    height={props.height || "4"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 8 4"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M8 0L4 4 0 0z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default CaretDownGlyph;
