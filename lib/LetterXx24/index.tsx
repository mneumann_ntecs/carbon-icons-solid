import { CarbonIconComponent } from "../types";
export const LetterXx24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="LetterXx24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M15 9L13 9 11 15 9 9 7 9 9.75 16 7 23 9 23 11 17 13 23 15 23 12.24 16 15 9zM25 13L23 13 21 16.9 19 13 17 13 19.91 18 17 23 19 23 21 19.2 23 23 25 23 22.1 18 25 13z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default LetterXx24;
