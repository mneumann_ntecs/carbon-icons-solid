import { CarbonIconComponent } from "../types";
export const DataClass24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="DataClass24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M26,16a3.9612,3.9612,0,0,0-2.02.5657l-2.8586-2.8585,2.293-2.293a2,2,0,0,0,0-2.8284l-6-6a2,2,0,0,0-2.8284,0l-6,6a2,2,0,0,0,0,2.8284l2.293,2.293L8.02,16.5657A4.043,4.043,0,1,0,9.4343,17.98l2.8585-2.8586,2.293,2.293A1.9773,1.9773,0,0,0,15,17.7233V22H12v8h8V22H17V17.7233a1.9773,1.9773,0,0,0,.4142-.3091l2.293-2.293L22.5657,17.98A3.9885,3.9885,0,1,0,26,16ZM8,20a2,2,0,1,1-2-2A2.0023,2.0023,0,0,1,8,20Zm10,4v4H14V24h4Zm-2-8-6-6,6-6,6,6Zm10,6a2,2,0,1,1,2-2A2.0023,2.0023,0,0,1,26,22Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default DataClass24;
