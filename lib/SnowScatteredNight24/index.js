import {
  template,
  addEventListener,
  insert,
  memo,
  effect,
  setAttribute,
  style,
  delegateEvents,
} from "solid-js/web";

const _tmpl$ = template(
    `<svg data-carbon-icon="SnowScatteredNight24" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" preserveAspectRatio="xMidYMid meet"><path d="M2 26H4V28H2zM4 28H6V30H4zM6 30H8V32H6zM6 26H8V28H6zM2 30H4V32H2zM20 26H22V28H20zM22 28H24V30H22zM24 30H26V32H24zM24 26H26V28H24zM20 30H22V32H20zM11 26H13V28H11zM13 28H15V30H13zM15 30H17V32H15zM15 26H17V28H15zM11 30H13V32H11zM29.8439 13.0347a1.517 1.517 0 00-1.23-.8658 5.3552 5.3552 0 01-3.4095-1.7158 6.4655 6.4655 0 01-1.286-6.3926 1.6025 1.6025 0 00-.2989-1.5459 1.4543 1.4543 0 00-1.36-.4931l-.0191.0039a7.7685 7.7685 0 00-5.8847 5.5737A7.3706 7.3706 0 0013.5 7a7.5511 7.5511 0 00-7.1494 5.2441A5.9926 5.9926 0 008 24H19a6.0066 6.0066 0 006-6c0-.0571-.0123-.1113-.0139-.1685a7.5076 7.5076 0 004.7611-3.2768A1.5369 1.5369 0 0029.8439 13.0347zM19 22H8a3.9926 3.9926 0 01-.6733-7.9292l.663-.1128.1456-.6562a5.496 5.496 0 0110.7294 0l.1456.6562.6626.1128A3.9925 3.9925 0 0119 22zm5.5974-6.1289a5.9661 5.9661 0 00-3.948-3.627 7.49 7.49 0 00-2.489-3.58 5.9018 5.9018 0 013.6381-4.4473 8.4577 8.4577 0 001.94 7.5967A7.4007 7.4007 0 0027.64 14.041 5.4487 5.4487 0 0124.5974 15.8711z"></path></svg>`,
    4
  ),
  _tmpl$2 = template(`<title></title>`, 2);

const SnowScatteredNight24 = (props) =>
  (() => {
    const _el$ = _tmpl$.cloneNode(true);
    _el$.firstChild;

    addEventListener(_el$, "keydown", props.onKeyDown, true);

    addEventListener(_el$, "keyup", props.onKeyUp, true);

    addEventListener(_el$, "mouseleave", props.onMouseLeave);

    addEventListener(_el$, "mouseenter", props.onMouseEnter);

    addEventListener(_el$, "mouseover", props.onMouseOver, true);

    addEventListener(_el$, "click", props.onClick, true);

    insert(
      _el$,
      (() => {
        const _c$ = memo(() => props.children, true);

        return () =>
          _c$() ||
          (props.title &&
            (() => {
              const _el$3 = _tmpl$2.cloneNode(true);

              insert(_el$3, () => props.title);

              return _el$3;
            })());
      })(),
      null
    );

    effect(
      (_p$) => {
        const _v$ = props.fill || "currentColor",
          _v$2 = props.width || "24",
          _v$3 = props.height || "24",
          _v$4 = props["aria-label"],
          _v$5 = props["aria-labelledby"],
          _v$6 = props["aria-label"] || props["aria-labelledby"] || props.title,
          _v$7 =
            props["aria-label"] || props["aria-labelledby"] || props.title
              ? "img"
              : undefined,
          _v$8 = props.tabindex === "0" ? true : props.focusable,
          _v$9 = props.tabindex,
          _v$10 = props.id,
          _v$11 = props.class,
          _v$12 = props.title,
          _v$13 = props.style,
          _v$14 = props.stroke;

        _v$ !== _p$._v$ && setAttribute(_el$, "fill", (_p$._v$ = _v$));
        _v$2 !== _p$._v$2 && setAttribute(_el$, "width", (_p$._v$2 = _v$2));
        _v$3 !== _p$._v$3 && setAttribute(_el$, "height", (_p$._v$3 = _v$3));
        _v$4 !== _p$._v$4 &&
          setAttribute(_el$, "aria-label", (_p$._v$4 = _v$4));
        _v$5 !== _p$._v$5 &&
          setAttribute(_el$, "aria-labelledby", (_p$._v$5 = _v$5));
        _v$6 !== _p$._v$6 &&
          setAttribute(_el$, "aria-hidden", (_p$._v$6 = _v$6));
        _v$7 !== _p$._v$7 && setAttribute(_el$, "role", (_p$._v$7 = _v$7));
        _v$8 !== _p$._v$8 && setAttribute(_el$, "focusable", (_p$._v$8 = _v$8));
        _v$9 !== _p$._v$9 && setAttribute(_el$, "tabindex", (_p$._v$9 = _v$9));
        _v$10 !== _p$._v$10 && setAttribute(_el$, "id", (_p$._v$10 = _v$10));
        _v$11 !== _p$._v$11 && setAttribute(_el$, "class", (_p$._v$11 = _v$11));
        _v$12 !== _p$._v$12 && setAttribute(_el$, "title", (_p$._v$12 = _v$12));
        _p$._v$13 = style(_el$, _v$13, _p$._v$13);
        _v$14 !== _p$._v$14 &&
          setAttribute(_el$, "stroke", (_p$._v$14 = _v$14));
        return _p$;
      },
      {
        _v$: undefined,
        _v$2: undefined,
        _v$3: undefined,
        _v$4: undefined,
        _v$5: undefined,
        _v$6: undefined,
        _v$7: undefined,
        _v$8: undefined,
        _v$9: undefined,
        _v$10: undefined,
        _v$11: undefined,
        _v$12: undefined,
        _v$13: undefined,
        _v$14: undefined,
      }
    );

    return _el$;
  })();

delegateEvents(["click", "mouseover", "keyup", "keydown"]);

export default SnowScatteredNight24;
export { SnowScatteredNight24 };
