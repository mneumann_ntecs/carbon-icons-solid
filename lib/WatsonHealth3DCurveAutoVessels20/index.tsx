import { CarbonIconComponent } from "../types";
export const WatsonHealth3DCurveAutoVessels20: CarbonIconComponent = (
  props
) => (
  <svg
    data-carbon-icon="WatsonHealth3DCurveAutoVessels20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M17,11a3,3,0,0,1,3-3h.1a5,5,0,1,0,0-2H20a5,5,0,0,0-5,5v1H13a3,3,0,0,1-3-3V7A5,5,0,0,0,5,2H4V4H5A3,3,0,0,1,8,7V9a5,5,0,0,0,5,5h2v7a3,3,0,0,1-3,3h-.1a5,5,0,1,0,0,2H12a5,5,0,0,0,5-5h2.5A2.5,2.5,0,0,1,22,23.5,4.51,4.51,0,0,0,26.5,28H28V26H26.5A2.5,2.5,0,0,1,24,23.5,4.51,4.51,0,0,0,19.5,19H17Zm8-7a3,3,0,1,1-3,3A3,3,0,0,1,25,4ZM7,28a3,3,0,1,1,3-3A3,3,0,0,1,7,28Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default WatsonHealth3DCurveAutoVessels20;
