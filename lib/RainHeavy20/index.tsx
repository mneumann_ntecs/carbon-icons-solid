import { CarbonIconComponent } from "../types";
export const RainHeavy20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="RainHeavy20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M17 30a1 1 0 01-.8944-1.4474l2-4.0005a1 1 0 111.7888.8947l-2 4A.9981.9981 0 0117 30zM8 30a1 1 0 01-.8944-1.4474l2-4.0005a1 1 0 111.7888.8947l-2 4A.9981.9981 0 018 30z"></path>
    <path d="M30,15.5a6.5323,6.5323,0,0,0-5.1992-6.3638,8.9943,8.9943,0,0,0-17.6006,0A6.4905,6.4905,0,0,0,5.7026,21.3584L4.106,24.5527a1,1,0,1,0,1.7885.8946L7.6174,22l6.7644,0L13.106,24.5527a1,1,0,1,0,1.7885.8946L16.6177,22h6.7641L22.106,24.5527a1,1,0,1,0,1.7885.8946l1.9444-3.89A6.5058,6.5058,0,0,0,30,15.5ZM23.5,20H8.5a4.4975,4.4975,0,0,1-.356-8.981l.8155-.0639.0991-.812a6.9938,6.9938,0,0,1,13.8838,0l.0986.812.8154.0639A4.4975,4.4975,0,0,1,23.5,20Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default RainHeavy20;
