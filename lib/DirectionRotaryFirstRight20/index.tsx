import { CarbonIconComponent } from "../types";
export const DirectionRotaryFirstRight20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="DirectionRotaryFirstRight20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M26,18v6.5859L15.8712,14.457A5.9242,5.9242,0,0,0,17,11a6,6,0,1,0-7,5.91V28h2V16.91a5.9566,5.9566,0,0,0,2.4554-1.04L24.5859,26H18v2H28V18ZM11,15a4,4,0,1,1,4-4A4.0045,4.0045,0,0,1,11,15Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default DirectionRotaryFirstRight20;
