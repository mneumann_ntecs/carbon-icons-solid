import {
  template,
  addEventListener,
  insert,
  memo,
  effect,
  setAttribute,
  style,
  delegateEvents,
} from "solid-js/web";

const _tmpl$ = template(
    `<svg data-carbon-icon="Cloudy32" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" preserveAspectRatio="xMidYMid meet"><path d="M30.0005,15.5005a6.5324,6.5324,0,0,0-5.1992-6.3638,8.9943,8.9943,0,0,0-17.6006,0,6.5321,6.5321,0,0,0-5.2,6.3638,6.4543,6.4543,0,0,0,1.6887,4.35A5.9829,5.9829,0,0,0,8,30H19a5.9764,5.9764,0,0,0,5.6094-8.1016A6.5051,6.5051,0,0,0,30.0005,15.5005ZM19,28H8a3.9925,3.9925,0,0,1-.6731-7.9292L7.99,19.958l.1458-.6562a5.496,5.496,0,0,1,10.7294,0l.1458.6562.6626.1128A3.9925,3.9925,0,0,1,19,28Zm4.5-8h-.0554a5.9562,5.9562,0,0,0-2.7959-1.7564,7.4952,7.4952,0,0,0-14.2984,0,5.9877,5.9877,0,0,0-1.0315.4073A4.4446,4.4446,0,0,1,4,15.5005a4.5171,4.5171,0,0,1,4.144-4.481l.8155-.0639.0991-.812a6.9938,6.9938,0,0,1,13.8838,0l.0986.812.8154.0639a4.4975,4.4975,0,0,1-.3564,8.981Z"></path></svg>`,
    4
  ),
  _tmpl$2 = template(`<title></title>`, 2);

const Cloudy32 = (props) =>
  (() => {
    const _el$ = _tmpl$.cloneNode(true);
    _el$.firstChild;

    addEventListener(_el$, "keydown", props.onKeyDown, true);

    addEventListener(_el$, "keyup", props.onKeyUp, true);

    addEventListener(_el$, "mouseleave", props.onMouseLeave);

    addEventListener(_el$, "mouseenter", props.onMouseEnter);

    addEventListener(_el$, "mouseover", props.onMouseOver, true);

    addEventListener(_el$, "click", props.onClick, true);

    insert(
      _el$,
      (() => {
        const _c$ = memo(() => props.children, true);

        return () =>
          _c$() ||
          (props.title &&
            (() => {
              const _el$3 = _tmpl$2.cloneNode(true);

              insert(_el$3, () => props.title);

              return _el$3;
            })());
      })(),
      null
    );

    effect(
      (_p$) => {
        const _v$ = props.fill || "currentColor",
          _v$2 = props.width || "32",
          _v$3 = props.height || "32",
          _v$4 = props["aria-label"],
          _v$5 = props["aria-labelledby"],
          _v$6 = props["aria-label"] || props["aria-labelledby"] || props.title,
          _v$7 =
            props["aria-label"] || props["aria-labelledby"] || props.title
              ? "img"
              : undefined,
          _v$8 = props.tabindex === "0" ? true : props.focusable,
          _v$9 = props.tabindex,
          _v$10 = props.id,
          _v$11 = props.class,
          _v$12 = props.title,
          _v$13 = props.style,
          _v$14 = props.stroke;

        _v$ !== _p$._v$ && setAttribute(_el$, "fill", (_p$._v$ = _v$));
        _v$2 !== _p$._v$2 && setAttribute(_el$, "width", (_p$._v$2 = _v$2));
        _v$3 !== _p$._v$3 && setAttribute(_el$, "height", (_p$._v$3 = _v$3));
        _v$4 !== _p$._v$4 &&
          setAttribute(_el$, "aria-label", (_p$._v$4 = _v$4));
        _v$5 !== _p$._v$5 &&
          setAttribute(_el$, "aria-labelledby", (_p$._v$5 = _v$5));
        _v$6 !== _p$._v$6 &&
          setAttribute(_el$, "aria-hidden", (_p$._v$6 = _v$6));
        _v$7 !== _p$._v$7 && setAttribute(_el$, "role", (_p$._v$7 = _v$7));
        _v$8 !== _p$._v$8 && setAttribute(_el$, "focusable", (_p$._v$8 = _v$8));
        _v$9 !== _p$._v$9 && setAttribute(_el$, "tabindex", (_p$._v$9 = _v$9));
        _v$10 !== _p$._v$10 && setAttribute(_el$, "id", (_p$._v$10 = _v$10));
        _v$11 !== _p$._v$11 && setAttribute(_el$, "class", (_p$._v$11 = _v$11));
        _v$12 !== _p$._v$12 && setAttribute(_el$, "title", (_p$._v$12 = _v$12));
        _p$._v$13 = style(_el$, _v$13, _p$._v$13);
        _v$14 !== _p$._v$14 &&
          setAttribute(_el$, "stroke", (_p$._v$14 = _v$14));
        return _p$;
      },
      {
        _v$: undefined,
        _v$2: undefined,
        _v$3: undefined,
        _v$4: undefined,
        _v$5: undefined,
        _v$6: undefined,
        _v$7: undefined,
        _v$8: undefined,
        _v$9: undefined,
        _v$10: undefined,
        _v$11: undefined,
        _v$12: undefined,
        _v$13: undefined,
        _v$14: undefined,
      }
    );

    return _el$;
  })();

delegateEvents(["click", "mouseover", "keyup", "keydown"]);

export default Cloudy32;
export { Cloudy32 };
