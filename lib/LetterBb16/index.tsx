import { CarbonIconComponent } from "../types";
export const LetterBb16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="LetterBb16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M23 13H19V9H17V23h6a2 2 0 002-2V15A2 2 0 0023 13zm-4 8V15h4v6zM15 12a3 3 0 00-3-3H7V23h5a3 3 0 003-3V18a3 3 0 00-.78-2A3 3 0 0015 14zM9 11h3a1 1 0 011 1v2a1 1 0 01-1 1H9zm4 9a1 1 0 01-1 1H9V17h3a1 1 0 011 1z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default LetterBb16;
