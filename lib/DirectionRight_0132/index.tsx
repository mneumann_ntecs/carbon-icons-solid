import { CarbonIconComponent } from "../types";
export const DirectionRight_0132: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="DirectionRight_0132"
    fill={props.fill || "currentColor"}
    width={props.width || "32"}
    height={props.height || "32"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M19,4,17.5859,5.4141,22.1719,10H10a2,2,0,0,0-2,2V28h2V12H22.1719l-4.586,4.5859L19,18l7-7Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default DirectionRight_0132;
