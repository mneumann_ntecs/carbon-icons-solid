import { CarbonIconComponent } from "../types";
export const HeatMapStocks24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="HeatMapStocks24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M27,3H5A2,2,0,0,0,3,5V27a2,2,0,0,0,2,2H27a2,2,0,0,0,2-2V5A2,2,0,0,0,27,3ZM9,21H5V17H9Zm2,2h4v4H11Zm6,0h4v4H17Zm0-8V11h4v4Zm4-6H17V5h4Zm2,2h4v4H23ZM15,5V15H5V5Zm8,22V17h4V27Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default HeatMapStocks24;
