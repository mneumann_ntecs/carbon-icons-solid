import {
  template,
  addEventListener,
  insert,
  memo,
  effect,
  setAttribute,
  style,
  delegateEvents,
} from "solid-js/web";

const _tmpl$ = template(
    `<svg data-carbon-icon="LogoSkype24" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" preserveAspectRatio="xMidYMid meet"><path d="M27.91,17.88A11.63,11.63,0,0,0,28.06,16,12.06,12.06,0,0,0,16,3.94a11.63,11.63,0,0,0-1.88.15,7.29,7.29,0,0,0-10,10A11.63,11.63,0,0,0,3.94,16,12.06,12.06,0,0,0,16,28.06a11.63,11.63,0,0,0,1.88-.15,7.29,7.29,0,0,0,10-10Zm-6.18,3.6a5,5,0,0,1-2.24,1.73,9,9,0,0,1-3.48.62A8.41,8.41,0,0,1,12,23a5.2,5.2,0,0,1-1.82-1.63,3.59,3.59,0,0,1-.7-2,1.36,1.36,0,0,1,.43-1A1.56,1.56,0,0,1,11,18a1.38,1.38,0,0,1,.91.32,2.5,2.5,0,0,1,.63.94,5.83,5.83,0,0,0,.66,1.18,2.61,2.61,0,0,0,1,.78,3.87,3.87,0,0,0,1.69.31,4,4,0,0,0,2.34-.62,1.8,1.8,0,0,0,.89-1.53,1.54,1.54,0,0,0-.47-1.17,3.09,3.09,0,0,0-1.25-.7c-.52-.17-1.21-.34-2.08-.53A16,16,0,0,1,12.41,16a4.78,4.78,0,0,1-1.89-1.4,3.5,3.5,0,0,1-.7-2.21,3.65,3.65,0,0,1,.74-2.24,4.7,4.7,0,0,1,2.14-1.5A9.51,9.51,0,0,1,16,8.17a8.63,8.63,0,0,1,2.59.35,5.81,5.81,0,0,1,1.83.92,3.91,3.91,0,0,1,1.06,1.21,2.62,2.62,0,0,1,.34,1.25,1.4,1.4,0,0,1-.43,1,1.43,1.43,0,0,1-1.07.46,1.25,1.25,0,0,1-.89-.28,3.36,3.36,0,0,1-.65-.87,3.79,3.79,0,0,0-1.05-1.31,3.13,3.13,0,0,0-1.95-.46,3.64,3.64,0,0,0-2,.5,1.44,1.44,0,0,0-.76,1.2,1.15,1.15,0,0,0,.25.75,2.21,2.21,0,0,0,.72.55,4.74,4.74,0,0,0,.92.36c.32.09.84.22,1.57.39.92.2,1.74.42,2.49.66a7.61,7.61,0,0,1,1.9.88,3.59,3.59,0,0,1,1.23,1.31A4,4,0,0,1,22.5,19,4.24,4.24,0,0,1,21.73,21.48Z"></path></svg>`,
    4
  ),
  _tmpl$2 = template(`<title></title>`, 2);

const LogoSkype24 = (props) =>
  (() => {
    const _el$ = _tmpl$.cloneNode(true);
    _el$.firstChild;

    addEventListener(_el$, "keydown", props.onKeyDown, true);

    addEventListener(_el$, "keyup", props.onKeyUp, true);

    addEventListener(_el$, "mouseleave", props.onMouseLeave);

    addEventListener(_el$, "mouseenter", props.onMouseEnter);

    addEventListener(_el$, "mouseover", props.onMouseOver, true);

    addEventListener(_el$, "click", props.onClick, true);

    insert(
      _el$,
      (() => {
        const _c$ = memo(() => props.children, true);

        return () =>
          _c$() ||
          (props.title &&
            (() => {
              const _el$3 = _tmpl$2.cloneNode(true);

              insert(_el$3, () => props.title);

              return _el$3;
            })());
      })(),
      null
    );

    effect(
      (_p$) => {
        const _v$ = props.fill || "currentColor",
          _v$2 = props.width || "24",
          _v$3 = props.height || "24",
          _v$4 = props["aria-label"],
          _v$5 = props["aria-labelledby"],
          _v$6 = props["aria-label"] || props["aria-labelledby"] || props.title,
          _v$7 =
            props["aria-label"] || props["aria-labelledby"] || props.title
              ? "img"
              : undefined,
          _v$8 = props.tabindex === "0" ? true : props.focusable,
          _v$9 = props.tabindex,
          _v$10 = props.id,
          _v$11 = props.class,
          _v$12 = props.title,
          _v$13 = props.style,
          _v$14 = props.stroke;

        _v$ !== _p$._v$ && setAttribute(_el$, "fill", (_p$._v$ = _v$));
        _v$2 !== _p$._v$2 && setAttribute(_el$, "width", (_p$._v$2 = _v$2));
        _v$3 !== _p$._v$3 && setAttribute(_el$, "height", (_p$._v$3 = _v$3));
        _v$4 !== _p$._v$4 &&
          setAttribute(_el$, "aria-label", (_p$._v$4 = _v$4));
        _v$5 !== _p$._v$5 &&
          setAttribute(_el$, "aria-labelledby", (_p$._v$5 = _v$5));
        _v$6 !== _p$._v$6 &&
          setAttribute(_el$, "aria-hidden", (_p$._v$6 = _v$6));
        _v$7 !== _p$._v$7 && setAttribute(_el$, "role", (_p$._v$7 = _v$7));
        _v$8 !== _p$._v$8 && setAttribute(_el$, "focusable", (_p$._v$8 = _v$8));
        _v$9 !== _p$._v$9 && setAttribute(_el$, "tabindex", (_p$._v$9 = _v$9));
        _v$10 !== _p$._v$10 && setAttribute(_el$, "id", (_p$._v$10 = _v$10));
        _v$11 !== _p$._v$11 && setAttribute(_el$, "class", (_p$._v$11 = _v$11));
        _v$12 !== _p$._v$12 && setAttribute(_el$, "title", (_p$._v$12 = _v$12));
        _p$._v$13 = style(_el$, _v$13, _p$._v$13);
        _v$14 !== _p$._v$14 &&
          setAttribute(_el$, "stroke", (_p$._v$14 = _v$14));
        return _p$;
      },
      {
        _v$: undefined,
        _v$2: undefined,
        _v$3: undefined,
        _v$4: undefined,
        _v$5: undefined,
        _v$6: undefined,
        _v$7: undefined,
        _v$8: undefined,
        _v$9: undefined,
        _v$10: undefined,
        _v$11: undefined,
        _v$12: undefined,
        _v$13: undefined,
        _v$14: undefined,
      }
    );

    return _el$;
  })();

delegateEvents(["click", "mouseover", "keyup", "keydown"]);

export default LogoSkype24;
export { LogoSkype24 };
