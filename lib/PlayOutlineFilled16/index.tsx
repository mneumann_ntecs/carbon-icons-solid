import { CarbonIconComponent } from "../types";
export const PlayOutlineFilled16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="PlayOutlineFilled16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 16 16"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M8,1C4.1,1,1,4.1,1,8s3.1,7,7,7s7-3.1,7-7S11.9,1,8,1z M11.7,8.4l-5.5,3c-0.2,0.1-0.3,0.1-0.5,0c-0.2-0.1-0.2-0.3-0.2-0.4V5	c0-0.2,0.1-0.3,0.2-0.4c0.2-0.1,0.3-0.1,0.5,0l5.5,3C12,7.7,12.1,8,11.9,8.2C11.9,8.3,11.8,8.4,11.7,8.4L11.7,8.4z"></path>
    <path d="M6.5 10.2L10.5 8 6.5 5.8z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default PlayOutlineFilled16;
