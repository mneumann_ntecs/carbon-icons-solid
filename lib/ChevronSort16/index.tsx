import { CarbonIconComponent } from "../types";
export const ChevronSort16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="ChevronSort16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 16 16"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M8 14L4.5 10.5 5.2 9.8 8 12.6 10.8 9.8 11.5 10.5zM8 2L11.5 5.5 10.8 6.2 8 3.4 5.2 6.2 4.5 5.5z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default ChevronSort16;
