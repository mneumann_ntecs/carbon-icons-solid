import { CarbonIconComponent } from "../types";
export const PdfReference24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="PdfReference24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M4 20L4 22 8.586 22 2 28.586 3.414 30 10 23.414 10 28 12 28 12 20 4 20zM22 16L24 16 24 10 29 10 29 8 24 8 24 4 30 4 30 2 22 2 22 16zM16 2H12V16h4a4 4 0 004-4V6A4 4 0 0016 2zm2 10a2 2 0 01-2 2H14V4h2a2 2 0 012 2zM8 2H2V16H4V11H8a2 2 0 002-2V4A2 2 0 008 2zM8 9H4V4H8z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default PdfReference24;
