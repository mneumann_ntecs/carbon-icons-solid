import { CarbonIconComponent } from "../types";
export const ScriptReference16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="ScriptReference16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M4 20L4 22 8.586 22 2 28.586 3.414 30 10 23.414 10 28 12 28 12 20 4 20zM27.17 26L24.59 28.58 26 30 30 26 26 22 24.58 23.41 27.17 26zM18.83 26L21.41 23.42 20 22 16 26 20 30 21.42 28.59 18.83 26zM25.7 9.3l-7-7A.9087.9087 0 0018 2H8A2.0058 2.0058 0 006 4V16H8V4h8v6a2.0058 2.0058 0 002 2h6v6h2V10A.9092.9092 0 0025.7 9.3zM18 10V4.4L23.6 10z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default ScriptReference16;
