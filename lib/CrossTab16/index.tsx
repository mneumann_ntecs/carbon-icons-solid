import { CarbonIconComponent } from "../types";
export const CrossTab16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="CrossTab16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M28,2H10A2.0023,2.0023,0,0,0,8,4V7H4A2.0023,2.0023,0,0,0,2,9V28a2.0023,2.0023,0,0,0,2,2H28a2.0023,2.0023,0,0,0,2-2V4A2.0023,2.0023,0,0,0,28,2ZM10,4H28V7H10ZM28,14H20V9h8ZM10,21V16h8v5Zm8,2v5H10V23ZM8,21H4V16H8ZM18,9v5H10V9Zm2,7h8v5H20ZM8,9v5H4V9ZM4,23H8v5H4Zm16,5V23h8v5Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default CrossTab16;
