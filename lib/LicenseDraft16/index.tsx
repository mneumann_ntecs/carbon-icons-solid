import { CarbonIconComponent } from "../types";
export const LicenseDraft16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="LicenseDraft16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M8 6H20V8H8zM8 10H20V12H8zM8 14H14V16H8zM8 24H12V26H8z"></path>
    <path d="M29.707 19.293l-3-3a.9994.9994 0 00-1.414 0L16 25.5859V30h4.4141l9.2929-9.293A.9994.9994 0 0029.707 19.293zM19.5859 28H18V26.4141l5-5L24.5859 23zM26 21.5859L24.4141 20 26 18.4141 27.5859 20zM12 30H6a2.0021 2.0021 0 01-2-2V4A2.0021 2.0021 0 016 2H22a2.0021 2.0021 0 012 2V14H22V4H6V28h6z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default LicenseDraft16;
