import { CarbonIconComponent } from "../types";
export const RotateCounterclockwiseAltFilled20: CarbonIconComponent = (
  props
) => (
  <svg
    data-carbon-icon="RotateCounterclockwiseAltFilled20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M14 28V16a2.0023 2.0023 0 012-2H28a2.0023 2.0023 0 012 2V28a2.0023 2.0023 0 01-2 2H16A2.0023 2.0023 0 0114 28zM2 15l1.41-1.41L6 16.17V11a7.0078 7.0078 0 017-7h5V6H13a5.0057 5.0057 0 00-5 5v5.17l2.59-2.58L12 15 7 20z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default RotateCounterclockwiseAltFilled20;
