import { CarbonIconComponent } from "../types";
export const DirectionRotaryFirstRightFilled32: CarbonIconComponent = (
  props
) => (
  <svg
    data-carbon-icon="DirectionRotaryFirstRightFilled32"
    fill={props.fill || "currentColor"}
    width={props.width || "32"}
    height={props.height || "32"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <circle cx="11" cy="11" r="3"></circle>
    <path d="M28,2H4A2,2,0,0,0,2,4V28a2,2,0,0,0,2,2H28a2,2,0,0,0,2-2V4A2,2,0,0,0,28,2ZM26,26H16V24h6.5859l-8.833-8.833A4.9678,4.9678,0,0,1,12,15.8989V26H10V15.8989a5.0113,5.0113,0,1,1,5.167-2.146L24,22.5859V16h2Z"></path>
    <path
      fill="none"
      d="M11,14a3,3,0,1,1,3-3A3.0033,3.0033,0,0,1,11,14Zm13,2v6.5859l-8.833-8.833A4.9959,4.9959,0,1,0,10,15.8989V26h2V15.8989a4.9678,4.9678,0,0,0,1.7529-.7319L22.5859,24H16v2H26V16Z"
      data-icon-path="inner-path"
    ></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default DirectionRotaryFirstRightFilled32;
