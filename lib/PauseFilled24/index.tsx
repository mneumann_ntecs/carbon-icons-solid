import { CarbonIconComponent } from "../types";
export const PauseFilled24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="PauseFilled24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M12 6H10A2 2 0 008 8V24a2 2 0 002 2h2a2 2 0 002-2V8a2 2 0 00-2-2zM22 6H20a2 2 0 00-2 2V24a2 2 0 002 2h2a2 2 0 002-2V8a2 2 0 00-2-2z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default PauseFilled24;
