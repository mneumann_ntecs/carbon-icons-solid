import { CarbonIconComponent } from "../types";
export const CarFront16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="CarFront16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M13 19H19V21H13z"></path>
    <path d="M25.4409,14,24.1687,9.45A2.009,2.009,0,0,0,22.2456,8H9.7544A2.0089,2.0089,0,0,0,7.8313,9.4507L6.5315,14H4v2H6v7a2.0025,2.0025,0,0,0,2,2v3h2V25H22v3h2V25a2.0025,2.0025,0,0,0,2-2V16h2V14ZM9.7544,10H22.2458l1.4285,5H8.3257ZM24,21v2H8V21h2V19H8V17H24v2H22v2Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default CarFront16;
