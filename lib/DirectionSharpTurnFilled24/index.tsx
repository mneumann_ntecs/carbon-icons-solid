import { CarbonIconComponent } from "../types";
export const DirectionSharpTurnFilled24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="DirectionSharpTurnFilled24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M28,2H4A2,2,0,0,0,2,4V28a2,2,0,0,0,2,2H28a2,2,0,0,0,2-2V4A2,2,0,0,0,28,2ZM26,26H16V24h6.5859L8,9.4141V26H6V7a1,1,0,0,1,1.707-.707L24,22.5859V16h2Z"></path>
    <path
      fill="none"
      d="M26,26H16V24h6.5859L8,9.4141V26H6V7a1,1,0,0,1,1.707-.707L24,22.5859V16h2Z"
      data-icon-path="inner-path"
    ></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default DirectionSharpTurnFilled24;
