import { CarbonIconComponent } from "../types";
export const TrafficWeatherIncident16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="TrafficWeatherIncident16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M2 24H4V30H2zM28 2H30V30H28zM15 2H17V6H15zM15 10H17V14H15zM15 18H17V22H15zM15 26H17V30H15zM6 12A3.8978 3.8978 0 012 8.223a3.9017 3.9017 0 01.6533-2.0639L5.17 2.4141a1.0381 1.0381 0 011.6592 0L9.3154 6.11A3.9693 3.9693 0 0110 8.223 3.8978 3.8978 0 016 12zm0-7.2368L4.3438 7.2257A1.89 1.89 0 004 8.223a1.9007 1.9007 0 002 1.7775A1.9007 1.9007 0 008 8.223a1.98 1.98 0 00-.375-1.0466zM11 11.7627L9.3438 14.2253A1.89 1.89 0 009 15.2226 1.9007 1.9007 0 0011 17a1.9007 1.9007 0 002-1.7774 1.98 1.98 0 00-.375-1.0467zM6 15.7627L4.3438 18.2253A1.89 1.89 0 004 19.2226 1.9007 1.9007 0 006 21a1.9007 1.9007 0 002-1.7774 1.98 1.98 0 00-.375-1.0467z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default TrafficWeatherIncident16;
