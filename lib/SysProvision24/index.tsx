import { CarbonIconComponent } from "../types";
export const SysProvision24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="SysProvision24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M21 20L21 30 30 25 21 20z"></path>
    <circle cx="14" cy="14" r="2"></circle>
    <path d="M14,20a6,6,0,1,1,6-6A6.0066,6.0066,0,0,1,14,20Zm0-10a4,4,0,1,0,4,4A4.0045,4.0045,0,0,0,14,10Z"></path>
    <path d="M25.9512,12.91l-.0064,0A12.05,12.05,0,1,0,17,25.605V23.5386a9.9813,9.9813,0,1,1,6.6235-6.81l1.9248.544A12.0341,12.0341,0,0,0,26,14Q26,13.4492,25.9512,12.91Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default SysProvision24;
