import { CarbonIconComponent } from "../types";
export const DistributeHorizontalCenter20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="DistributeHorizontalCenter20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M24 10H23V2H21v8H20a2.0023 2.0023 0 00-2 2v8a2.0023 2.0023 0 002 2h1v8h2V22h1a2.0023 2.0023 0 002-2V12A2.0023 2.0023 0 0024 10zm0 10H20V12h4zM12 6H11V2H9V6H8A2.0023 2.0023 0 006 8V24a2.0023 2.0023 0 002 2H9v4h2V26h1a2.0023 2.0023 0 002-2V8A2.0023 2.0023 0 0012 6zm0 18H8V8h4z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default DistributeHorizontalCenter20;
