import { CarbonIconComponent } from "../types";
export const EventsAlt32: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="EventsAlt32"
    fill={props.fill || "currentColor"}
    width={props.width || "32"}
    height={props.height || "32"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M18 31h2V29a1.0006 1.0006 0 011-1h6a1.0006 1.0006 0 011 1v2h2V29a3.0033 3.0033 0 00-3-3H21a3.0033 3.0033 0 00-3 3zM24 25a4 4 0 114-4A4.0039 4.0039 0 0124 25zm0-6a2 2 0 102 2A2.0027 2.0027 0 0024 19zM2 31H4V29a1.0009 1.0009 0 011-1h6a1.0009 1.0009 0 011 1v2h2V29a3.0033 3.0033 0 00-3-3H5a3.0033 3.0033 0 00-3 3zM8 25a4 4 0 114-4A4.0042 4.0042 0 018 25zm0-6a2 2 0 102 2A2.0023 2.0023 0 008 19zM18 16h2V14a1.0009 1.0009 0 011-1h6a1.0009 1.0009 0 011 1v2h2V14a3.0033 3.0033 0 00-3-3H21a3.0033 3.0033 0 00-3 3zM24 10a4 4 0 114-4A4.0042 4.0042 0 0124 10zm0-6a2 2 0 102 2A2.0023 2.0023 0 0024 4zM2 16H4V14a1.0013 1.0013 0 011-1h6a1.0013 1.0013 0 011 1v2h2V14a3.0033 3.0033 0 00-3-3H5a3.0033 3.0033 0 00-3 3zM8 10a4 4 0 114-4A4.0045 4.0045 0 018 10zM8 4a2 2 0 102 2A2.002 2.002 0 008 4z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default EventsAlt32;
