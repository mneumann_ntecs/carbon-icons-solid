import {
  template,
  addEventListener,
  insert,
  memo,
  effect,
  setAttribute,
  style,
  delegateEvents,
} from "solid-js/web";

const _tmpl$ = template(
    `<svg data-carbon-icon="LogoVmware20" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" preserveAspectRatio="xMidYMid meet"><path d="M30,16.67V14.1036c0-2.1716-2.1715-3.356-3.9482-3.356a5.9018,5.9018,0,0,0-3.356,1.3818,4.4329,4.4329,0,0,0-3.1586-1.3818,5.1967,5.1967,0,0,0-3.356,1.3818A3.34,3.34,0,0,0,13.22,10.7476a3.055,3.055,0,0,0-2.9233,1.5937L7.6925,18.2492,4.801,11.622a1.4616,1.4616,0,0,0-1.9148-.7574h0a1.4617,1.4617,0,0,0-.7518,1.9548l3.584,7.7987S6.3107,22,7.4951,22a1.67,1.67,0,0,0,1.7767-.9871c.2735-.41,2.3459-4.769,3.5944-7.41a.4969.4969,0,0,1,.9461.2132v6.551a1.6153,1.6153,0,0,0,1.4741,1.63,1.5794,1.5794,0,0,0,1.6845-1.5759V14.9919a1.678,1.678,0,1,1,3.356,0v5.4288a1.5793,1.5793,0,1,0,3.1585,0V14.9919a1.678,1.678,0,0,1,3.356,0v5.4288a1.5793,1.5793,0,0,0,3.1586,0Z"></path></svg>`,
    4
  ),
  _tmpl$2 = template(`<title></title>`, 2);

const LogoVmware20 = (props) =>
  (() => {
    const _el$ = _tmpl$.cloneNode(true);
    _el$.firstChild;

    addEventListener(_el$, "keydown", props.onKeyDown, true);

    addEventListener(_el$, "keyup", props.onKeyUp, true);

    addEventListener(_el$, "mouseleave", props.onMouseLeave);

    addEventListener(_el$, "mouseenter", props.onMouseEnter);

    addEventListener(_el$, "mouseover", props.onMouseOver, true);

    addEventListener(_el$, "click", props.onClick, true);

    insert(
      _el$,
      (() => {
        const _c$ = memo(() => props.children, true);

        return () =>
          _c$() ||
          (props.title &&
            (() => {
              const _el$3 = _tmpl$2.cloneNode(true);

              insert(_el$3, () => props.title);

              return _el$3;
            })());
      })(),
      null
    );

    effect(
      (_p$) => {
        const _v$ = props.fill || "currentColor",
          _v$2 = props.width || "20",
          _v$3 = props.height || "20",
          _v$4 = props["aria-label"],
          _v$5 = props["aria-labelledby"],
          _v$6 = props["aria-label"] || props["aria-labelledby"] || props.title,
          _v$7 =
            props["aria-label"] || props["aria-labelledby"] || props.title
              ? "img"
              : undefined,
          _v$8 = props.tabindex === "0" ? true : props.focusable,
          _v$9 = props.tabindex,
          _v$10 = props.id,
          _v$11 = props.class,
          _v$12 = props.title,
          _v$13 = props.style,
          _v$14 = props.stroke;

        _v$ !== _p$._v$ && setAttribute(_el$, "fill", (_p$._v$ = _v$));
        _v$2 !== _p$._v$2 && setAttribute(_el$, "width", (_p$._v$2 = _v$2));
        _v$3 !== _p$._v$3 && setAttribute(_el$, "height", (_p$._v$3 = _v$3));
        _v$4 !== _p$._v$4 &&
          setAttribute(_el$, "aria-label", (_p$._v$4 = _v$4));
        _v$5 !== _p$._v$5 &&
          setAttribute(_el$, "aria-labelledby", (_p$._v$5 = _v$5));
        _v$6 !== _p$._v$6 &&
          setAttribute(_el$, "aria-hidden", (_p$._v$6 = _v$6));
        _v$7 !== _p$._v$7 && setAttribute(_el$, "role", (_p$._v$7 = _v$7));
        _v$8 !== _p$._v$8 && setAttribute(_el$, "focusable", (_p$._v$8 = _v$8));
        _v$9 !== _p$._v$9 && setAttribute(_el$, "tabindex", (_p$._v$9 = _v$9));
        _v$10 !== _p$._v$10 && setAttribute(_el$, "id", (_p$._v$10 = _v$10));
        _v$11 !== _p$._v$11 && setAttribute(_el$, "class", (_p$._v$11 = _v$11));
        _v$12 !== _p$._v$12 && setAttribute(_el$, "title", (_p$._v$12 = _v$12));
        _p$._v$13 = style(_el$, _v$13, _p$._v$13);
        _v$14 !== _p$._v$14 &&
          setAttribute(_el$, "stroke", (_p$._v$14 = _v$14));
        return _p$;
      },
      {
        _v$: undefined,
        _v$2: undefined,
        _v$3: undefined,
        _v$4: undefined,
        _v$5: undefined,
        _v$6: undefined,
        _v$7: undefined,
        _v$8: undefined,
        _v$9: undefined,
        _v$10: undefined,
        _v$11: undefined,
        _v$12: undefined,
        _v$13: undefined,
        _v$14: undefined,
      }
    );

    return _el$;
  })();

delegateEvents(["click", "mouseover", "keyup", "keydown"]);

export default LogoVmware20;
export { LogoVmware20 };
