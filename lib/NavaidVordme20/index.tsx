import { CarbonIconComponent } from "../types";
export const NavaidVordme20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="NavaidVordme20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <circle cx="16" cy="16" r="2"></circle>
    <path d="M30,6a2.0023,2.0023,0,0,0-2-2H4A2.0023,2.0023,0,0,0,2,6V26a2.0023,2.0023,0,0,0,2,2H28a2.0023,2.0023,0,0,0,2-2Zm-2,6.9258L22.9636,6H28ZM27.7637,16,20.491,26H11.509L4.2363,16,11.509,6h8.982ZM9.0364,6,4,12.9248V6ZM4,19.0752,9.0364,26H4ZM22.9636,26l5.0374-6.9263L28,26Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default NavaidVordme20;
