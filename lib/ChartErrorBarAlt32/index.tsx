import { CarbonIconComponent } from "../types";
export const ChartErrorBarAlt32: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="ChartErrorBarAlt32"
    fill={props.fill || "currentColor"}
    width={props.width || "32"}
    height={props.height || "32"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M14 10V8H8v2h2v4.1836a2.983 2.983 0 000 5.6328V24H8v2h6V24H12V19.8164a2.983 2.983 0 000-5.6328V10zM26 6V4H20V6h2V8.1836a2.983 2.983 0 000 5.6328V18H20v2h6V18H24V13.8164a2.983 2.983 0 000-5.6328V6z"></path>
    <path d="M30,30H4a2,2,0,0,1-2-2V2H4V28H30Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default ChartErrorBarAlt32;
