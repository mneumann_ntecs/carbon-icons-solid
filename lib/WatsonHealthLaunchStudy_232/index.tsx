import { CarbonIconComponent } from "../types";
export const WatsonHealthLaunchStudy_232: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="WatsonHealthLaunchStudy_232"
    fill={props.fill || "currentColor"}
    width={props.width || "32"}
    height={props.height || "32"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <circle cx="23" cy="7" r="1"></circle>
    <path d="M26 4v6H6V4H26m0-2H6A2 2 0 004 4v6a2 2 0 002 2H26a2 2 0 002-2V4a2 2 0 00-2-2zM20.59 21.41L17 17.83 17 30 15 30 15 17.83 11.41 21.41 10 20 16 14 22 20 20.59 21.41z"></path>
    <path d="M8 6H20V8H8z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default WatsonHealthLaunchStudy_232;
