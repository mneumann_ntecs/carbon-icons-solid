import { CarbonIconComponent } from "../types";
export const LetterPp16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="LetterPp16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M23 13H17V27h2V23h4a2 2 0 002-2V15A2 2 0 0023 13zm-4 8V15h4v6zM9 23H7V9h6a2 2 0 012 2v5a2 2 0 01-2 2H9zm0-7h4V11H9z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default LetterPp16;
