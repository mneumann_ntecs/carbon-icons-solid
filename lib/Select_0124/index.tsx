import { CarbonIconComponent } from "../types";
export const Select_0124: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="Select_0124"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M12 6L8 6 8 2 6 2 6 6 2 6 2 8 6 8 6 12 8 12 8 8 12 8 12 6zM16 6H20V8H16zM24 6V8h4v4h2V8a2 2 0 00-2-2zM6 16H8V20H6zM8 28V24H6v4a2 2 0 002 2h4V28zM28 16H30V20H28zM16 28H20V30H16zM28 24v4H24v2h4a2 2 0 002-2V24z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default Select_0124;
