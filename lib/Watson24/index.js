import {
  template,
  addEventListener,
  insert,
  memo,
  effect,
  setAttribute,
  style,
  delegateEvents,
} from "solid-js/web";

const _tmpl$ = template(
    `<svg data-carbon-icon="Watson24" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" preserveAspectRatio="xMidYMid meet"><path d="M21.74 9.49h0A11.41 11.41 0 0016 8h0a.76.76 0 100 1.51 10.15 10.15 0 011.91.21c-2.26 1.08-4.76 3.58-6.73 7a22.48 22.48 0 00-2 4.44A9.58 9.58 0 017 17.22a3.43 3.43 0 01.28-2.66v0h0c.79-1.37 2.44-2.15 4.63-2.2a.76.76 0 00.74-.78.75.75 0 00-.78-.74C9.19 10.88 7.1 11.92 6 13.74H6v0s0 0 0 0a4.84 4.84 0 00-.44 3.79 12 12 0 003.2 5.22A11.36 11.36 0 008.52 26a10 10 0 01-2-3.48A.75.75 0 005.57 22a.76.76 0 00-.49 1 11.45 11.45 0 005.18 6.38h0A11.42 11.42 0 0016 30.92a11.74 11.74 0 003-.39 11.48 11.48 0 002.77-21zM18.58 29.06a9.9 9.9 0 01-7.56-1h0c-.86-.49-1.21-2-.94-4a18.85 18.85 0 002.48 1.72 13.92 13.92 0 006.93 2 11 11 0 002.42-.28A9.78 9.78 0 0118.58 29.06zm6.06-4.66c-2 2-6.66 2.74-11.32.05a17.36 17.36 0 01-2.89-2.12 21.08 21.08 0 012.08-4.91c2.94-5.08 6.83-7.57 8.47-6.62h0A10 10 0 0124.64 24.4zM4.16 11.72L1.14 10a.76.76 0 10-.76 1.31L3.4 13a.86.86 0 00.38.1.77.77 0 00.66-.38A.76.76 0 004.16 11.72zM8.29 7.59A.74.74 0 008.94 8a.75.75 0 00.38-.1.76.76 0 00.28-1l-1.74-3a.76.76 0 00-1-.27.75.75 0 00-.28 1zM16 6.08a.76.76 0 00.76-.76V1.83a.76.76 0 00-1.52 0V5.32A.76.76 0 0016 6.08zM22.68 7.87a.75.75 0 001-.28l1.75-3a.75.75 0 00-.28-1 .76.76 0 00-1 .27l-1.74 3A.76.76 0 0022.68 7.87zM31.9 10.25a.76.76 0 00-1-.27l-3 1.74a.76.76 0 00-.28 1 .77.77 0 00.66.38.86.86 0 00.38-.1l3-1.75A.76.76 0 0031.9 10.25z"></path></svg>`,
    4
  ),
  _tmpl$2 = template(`<title></title>`, 2);

const Watson24 = (props) =>
  (() => {
    const _el$ = _tmpl$.cloneNode(true);
    _el$.firstChild;

    addEventListener(_el$, "keydown", props.onKeyDown, true);

    addEventListener(_el$, "keyup", props.onKeyUp, true);

    addEventListener(_el$, "mouseleave", props.onMouseLeave);

    addEventListener(_el$, "mouseenter", props.onMouseEnter);

    addEventListener(_el$, "mouseover", props.onMouseOver, true);

    addEventListener(_el$, "click", props.onClick, true);

    insert(
      _el$,
      (() => {
        const _c$ = memo(() => props.children, true);

        return () =>
          _c$() ||
          (props.title &&
            (() => {
              const _el$3 = _tmpl$2.cloneNode(true);

              insert(_el$3, () => props.title);

              return _el$3;
            })());
      })(),
      null
    );

    effect(
      (_p$) => {
        const _v$ = props.fill || "currentColor",
          _v$2 = props.width || "24",
          _v$3 = props.height || "24",
          _v$4 = props["aria-label"],
          _v$5 = props["aria-labelledby"],
          _v$6 = props["aria-label"] || props["aria-labelledby"] || props.title,
          _v$7 =
            props["aria-label"] || props["aria-labelledby"] || props.title
              ? "img"
              : undefined,
          _v$8 = props.tabindex === "0" ? true : props.focusable,
          _v$9 = props.tabindex,
          _v$10 = props.id,
          _v$11 = props.class,
          _v$12 = props.title,
          _v$13 = props.style,
          _v$14 = props.stroke;

        _v$ !== _p$._v$ && setAttribute(_el$, "fill", (_p$._v$ = _v$));
        _v$2 !== _p$._v$2 && setAttribute(_el$, "width", (_p$._v$2 = _v$2));
        _v$3 !== _p$._v$3 && setAttribute(_el$, "height", (_p$._v$3 = _v$3));
        _v$4 !== _p$._v$4 &&
          setAttribute(_el$, "aria-label", (_p$._v$4 = _v$4));
        _v$5 !== _p$._v$5 &&
          setAttribute(_el$, "aria-labelledby", (_p$._v$5 = _v$5));
        _v$6 !== _p$._v$6 &&
          setAttribute(_el$, "aria-hidden", (_p$._v$6 = _v$6));
        _v$7 !== _p$._v$7 && setAttribute(_el$, "role", (_p$._v$7 = _v$7));
        _v$8 !== _p$._v$8 && setAttribute(_el$, "focusable", (_p$._v$8 = _v$8));
        _v$9 !== _p$._v$9 && setAttribute(_el$, "tabindex", (_p$._v$9 = _v$9));
        _v$10 !== _p$._v$10 && setAttribute(_el$, "id", (_p$._v$10 = _v$10));
        _v$11 !== _p$._v$11 && setAttribute(_el$, "class", (_p$._v$11 = _v$11));
        _v$12 !== _p$._v$12 && setAttribute(_el$, "title", (_p$._v$12 = _v$12));
        _p$._v$13 = style(_el$, _v$13, _p$._v$13);
        _v$14 !== _p$._v$14 &&
          setAttribute(_el$, "stroke", (_p$._v$14 = _v$14));
        return _p$;
      },
      {
        _v$: undefined,
        _v$2: undefined,
        _v$3: undefined,
        _v$4: undefined,
        _v$5: undefined,
        _v$6: undefined,
        _v$7: undefined,
        _v$8: undefined,
        _v$9: undefined,
        _v$10: undefined,
        _v$11: undefined,
        _v$12: undefined,
        _v$13: undefined,
        _v$14: undefined,
      }
    );

    return _el$;
  })();

delegateEvents(["click", "mouseover", "keyup", "keydown"]);

export default Watson24;
export { Watson24 };
