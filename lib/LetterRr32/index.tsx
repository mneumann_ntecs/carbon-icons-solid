import { CarbonIconComponent } from "../types";
export const LetterRr32: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="LetterRr32"
    fill={props.fill || "currentColor"}
    width={props.width || "32"}
    height={props.height || "32"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M16 15V11a2 2 0 00-2-2H8V23h2V17h1.48l2.34 6H16l-2.33-6H14A2 2 0 0016 15zm-6-4h4v4H10zM24 13L18 13 18 23 20 23 20 15 24 15 24 13z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default LetterRr32;
