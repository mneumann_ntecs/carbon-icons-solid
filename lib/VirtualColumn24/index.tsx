import { CarbonIconComponent } from "../types";
export const VirtualColumn24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="VirtualColumn24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M25 11L26.414 12.414 23.828 15 30 15 30 17 23.828 17 26.414 19.586 25 21 20 16 25 11z"></path>
    <path d="M30 28H20a2.0023 2.0023 0 01-2-2V6a2.0023 2.0023 0 012-2H30V6H20V26H30zM12 28H2V26H12V6H2V4H12a2.0023 2.0023 0 012 2V26A2.0023 2.0023 0 0112 28z"></path>
    <path d="M7 11L5.586 12.414 8.172 15 2 15 2 17 8.172 17 5.586 19.586 7 21 12 16 7 11z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default VirtualColumn24;
