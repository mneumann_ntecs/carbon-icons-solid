import { CarbonIconComponent } from "../types";
export const DragHorizontal24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="DragHorizontal24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M12 4L12 15 5.83 15 8.41 12.41 7 11 2 16 7 21 8.41 19.59 5.83 17 12 17 12 28 14 28 14 4 12 4zM25 11L23.59 12.41 26.17 15 20 15 20 4 18 4 18 28 20 28 20 17 26.17 17 23.59 19.59 25 21 30 16 25 11z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default DragHorizontal24;
