import { CarbonIconComponent } from "../types";
export const TextIndentLess16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="TextIndentLess16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M2 6H12V8H2zM5 12H12V14H5zM2 18H12V20H2zM5 24H12V26H5zM16 4H18V28H16zM28.15 23.5L29.56 22.12 23.27 16 29.56 9.88 28.15 8.5 20.44 16 28.15 23.5z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default TextIndentLess16;
