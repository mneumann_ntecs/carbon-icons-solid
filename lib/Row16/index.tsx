import { CarbonIconComponent } from "../types";
export const Row16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="Row16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M4 24H28V26H4zM26 18H6V14H26v4m2 0V14a2 2 0 00-2-2H6a2 2 0 00-2 2v4a2 2 0 002 2H26a2 2 0 002-2zM4 6H28V8H4z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default Row16;
