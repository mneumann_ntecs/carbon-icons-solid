import { CarbonIconComponent } from "../types";
export const VmdkDisk16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="VmdkDisk16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <circle cx="10.5" cy="24.5" r="1.5"></circle>
    <path
      d="M19.5 14.964H21.5V20.035H19.5z"
      transform="rotate(-45 20.5 17.5)"
    ></path>
    <circle cx="16" cy="13" r="2"></circle>
    <path d="M16,6a7,7,0,0,0,0,14V18a5,5,0,1,1,5-5h2A7,7,0,0,0,16,6Z"></path>
    <path d="M26,2H6A2,2,0,0,0,4,4V28a2,2,0,0,0,2,2H26a2,2,0,0,0,2-2V4A2,2,0,0,0,26,2Zm0,26H6V4H26Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default VmdkDisk16;
