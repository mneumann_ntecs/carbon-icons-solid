import { CarbonIconComponent } from "../types";
export const AirlineDigitalGate32: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="AirlineDigitalGate32"
    fill={props.fill || "currentColor"}
    width={props.width || "32"}
    height={props.height || "32"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M18 23H16V21a3.0033 3.0033 0 00-3-3H9a3.0033 3.0033 0 00-3 3v2H4V21a5.0058 5.0058 0 015-5h4a5.0059 5.0059 0 015 5zM11 6A3 3 0 118 9a3 3 0 013-3m0-2a5 5 0 105 5A5 5 0 0011 4zM2 26H30V28H2zM31.9658 11.7413a1.0007 1.0007 0 00-1.2246-.707l-3.0861.826L24 7l-1 .2676 1.4584 5.4483-2.7663.7405L20 11.2066l-1 .2676.9337 3.4883a.9993.9993 0 001.2246.707l10.1005-2.7036A1 1 0 0031.9658 11.7413z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default AirlineDigitalGate32;
