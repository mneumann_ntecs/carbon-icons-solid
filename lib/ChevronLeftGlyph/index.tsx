import { CarbonIconComponent } from "../types";
export const ChevronLeftGlyph: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="ChevronLeftGlyph"
    fill={props.fill || "currentColor"}
    width={props.width || "6"}
    height={props.height || "10"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 6 10"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M0 5L5 0 5.7 0.7 1.4 5 5.7 9.3 5 10z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default ChevronLeftGlyph;
