import { CarbonIconComponent } from "../types";
export const WatsonHealthStackedScrolling_232: CarbonIconComponent = (
  props
) => (
  <svg
    data-carbon-icon="WatsonHealthStackedScrolling_232"
    fill={props.fill || "currentColor"}
    width={props.width || "32"}
    height={props.height || "32"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M8 30H2a2 2 0 01-2-2V14a2 2 0 012-2H8a2 2 0 012 2V28A2 2 0 018 30zM2 14V28H8V14zM20 30H14a2 2 0 01-2-2V14a2 2 0 012-2h6a2 2 0 012 2V28A2 2 0 0120 30zM14 14V28h6V14z"></path>
    <path d="M27,21H25V9H17V7h8a2,2,0,0,1,2,2Z"></path>
    <path d="M32,16H30V4H22V2h8a2,2,0,0,1,2,2Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default WatsonHealthStackedScrolling_232;
