import { CarbonIconComponent } from "../types";
export const Zip16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="Zip16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M28 9H22V23h2V18h4a2 2 0 002-2V11A2 2 0 0028 9zm0 7H24V11h4zM12 9L12 11 15 11 15 21 12 21 12 23 20 23 20 21 17 21 17 11 20 11 20 9 12 9zM10 9L2 9 2 11 8 11 2 21 2 23 10 23 10 21 4 21 10 11 10 9z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default Zip16;
