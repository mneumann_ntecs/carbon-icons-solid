import {
  template,
  addEventListener,
  insert,
  memo,
  effect,
  setAttribute,
  style,
  delegateEvents,
} from "solid-js/web";

const _tmpl$ = template(
    `<svg data-carbon-icon="TropicalStormTracks16" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" preserveAspectRatio="xMidYMid meet"><path d="M16,21a5,5,0,1,1,5-5A5.0057,5.0057,0,0,1,16,21Zm0-8a3,3,0,1,0,3,3A3.0033,3.0033,0,0,0,16,13Z"></path><path d="M26.86,12.4805h0a12.9277,12.9277,0,0,0-4.8575-4.9991q.2044-.213.4424-.4589h0c.82-.8482,1.93-1.9825,3.2622-3.3155A1,1,0,0,0,25,2c-.354,0-8.7363.0488-14.269,4.3018h0A12.15,12.15,0,0,0,7.481,9.998c-.1416-.1367-.295-.2841-.4585-.4423C6.1743,8.7349,5.04,7.6255,3.707,6.293A1,1,0,0,0,2,7c0,.3594.05,8.874,4.4058,14.4023a12.1023,12.1023,0,0,0,3.5918,3.1163c-.21.2177-.4346.4516-.6563.68h0c-.7954.8208-1.8286,1.8745-3.0483,3.0943A1,1,0,0,0,7,30c.2856,0,7.061-.0352,12.459-3.1055a12.9618,12.9618,0,0,0,5.06-4.8925q.3062.2937.68.6567c.82.7954,1.8745,1.8286,3.0943,3.0483A1,1,0,0,0,30,25C30,24.7119,29.9644,17.8877,26.86,12.4805Zm-3.03,6.1074-.5469,1.3672A10.5415,10.5415,0,0,1,18.47,25.1562,24.3514,24.3514,0,0,1,9.584,27.8135c.4409-.4492,3.8281-3.9824,3.8281-3.9824l-1.3682-.5474a9.8021,9.8021,0,0,1-4.0668-3.1191c-2.5406-3.2242-3.4585-7.7623-3.79-10.58.5435.5337,3.9815,3.8266,3.9815,3.8266l.5468-1.3672A9.8569,9.8569,0,0,1,11.95,7.8877h0C15.1665,5.415,19.6309,4.5146,22.4155,4.187c-.5332.5435-3.8276,3.9819-3.8276,3.9819l1.3677.5469a10.52,10.52,0,0,1,5.17,4.7608v0a24.29,24.29,0,0,1,2.688,8.94C27.3643,21.9751,23.83,18.5879,23.83,18.5879Z"></path></svg>`,
    6
  ),
  _tmpl$2 = template(`<title></title>`, 2);

const TropicalStormTracks16 = (props) =>
  (() => {
    const _el$ = _tmpl$.cloneNode(true),
      _el$2 = _el$.firstChild;
    _el$2.nextSibling;

    addEventListener(_el$, "keydown", props.onKeyDown, true);

    addEventListener(_el$, "keyup", props.onKeyUp, true);

    addEventListener(_el$, "mouseleave", props.onMouseLeave);

    addEventListener(_el$, "mouseenter", props.onMouseEnter);

    addEventListener(_el$, "mouseover", props.onMouseOver, true);

    addEventListener(_el$, "click", props.onClick, true);

    insert(
      _el$,
      (() => {
        const _c$ = memo(() => props.children, true);

        return () =>
          _c$() ||
          (props.title &&
            (() => {
              const _el$4 = _tmpl$2.cloneNode(true);

              insert(_el$4, () => props.title);

              return _el$4;
            })());
      })(),
      null
    );

    effect(
      (_p$) => {
        const _v$ = props.fill || "currentColor",
          _v$2 = props.width || "16",
          _v$3 = props.height || "16",
          _v$4 = props["aria-label"],
          _v$5 = props["aria-labelledby"],
          _v$6 = props["aria-label"] || props["aria-labelledby"] || props.title,
          _v$7 =
            props["aria-label"] || props["aria-labelledby"] || props.title
              ? "img"
              : undefined,
          _v$8 = props.tabindex === "0" ? true : props.focusable,
          _v$9 = props.tabindex,
          _v$10 = props.id,
          _v$11 = props.class,
          _v$12 = props.title,
          _v$13 = props.style,
          _v$14 = props.stroke;

        _v$ !== _p$._v$ && setAttribute(_el$, "fill", (_p$._v$ = _v$));
        _v$2 !== _p$._v$2 && setAttribute(_el$, "width", (_p$._v$2 = _v$2));
        _v$3 !== _p$._v$3 && setAttribute(_el$, "height", (_p$._v$3 = _v$3));
        _v$4 !== _p$._v$4 &&
          setAttribute(_el$, "aria-label", (_p$._v$4 = _v$4));
        _v$5 !== _p$._v$5 &&
          setAttribute(_el$, "aria-labelledby", (_p$._v$5 = _v$5));
        _v$6 !== _p$._v$6 &&
          setAttribute(_el$, "aria-hidden", (_p$._v$6 = _v$6));
        _v$7 !== _p$._v$7 && setAttribute(_el$, "role", (_p$._v$7 = _v$7));
        _v$8 !== _p$._v$8 && setAttribute(_el$, "focusable", (_p$._v$8 = _v$8));
        _v$9 !== _p$._v$9 && setAttribute(_el$, "tabindex", (_p$._v$9 = _v$9));
        _v$10 !== _p$._v$10 && setAttribute(_el$, "id", (_p$._v$10 = _v$10));
        _v$11 !== _p$._v$11 && setAttribute(_el$, "class", (_p$._v$11 = _v$11));
        _v$12 !== _p$._v$12 && setAttribute(_el$, "title", (_p$._v$12 = _v$12));
        _p$._v$13 = style(_el$, _v$13, _p$._v$13);
        _v$14 !== _p$._v$14 &&
          setAttribute(_el$, "stroke", (_p$._v$14 = _v$14));
        return _p$;
      },
      {
        _v$: undefined,
        _v$2: undefined,
        _v$3: undefined,
        _v$4: undefined,
        _v$5: undefined,
        _v$6: undefined,
        _v$7: undefined,
        _v$8: undefined,
        _v$9: undefined,
        _v$10: undefined,
        _v$11: undefined,
        _v$12: undefined,
        _v$13: undefined,
        _v$14: undefined,
      }
    );

    return _el$;
  })();

delegateEvents(["click", "mouseover", "keyup", "keydown"]);

export default TropicalStormTracks16;
export { TropicalStormTracks16 };
