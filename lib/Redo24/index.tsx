import { CarbonIconComponent } from "../types";
export const Redo24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="Redo24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 24 24"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M6.8,11.2c-1.8,0-3.2,1.7-3.2,3.3c0,1.5,1.5,3.2,3.2,3.2H10v1.5H6.8C4.6,19.2,2,17,2,14.5s2-4.8,4.8-4.8	h12.4l-3-3l1.1-1.1l4.8,4.8l-4.8,4.8l-1.1-1.1l3-3H6.8z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default Redo24;
