import { CarbonIconComponent } from "../types";
export const Cad20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="Cad20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M18 9H14a2 2 0 00-2 2V23h2V18h4v5h2V11A2 2 0 0018 9zm-4 7V11h4v5zM26 23H22V9h4a4 4 0 014 4v6A4 4 0 0126 23zm-2-2h2a2 2 0 002-2V13a2 2 0 00-2-2H24zM10 23H4a2 2 0 01-2-2V11A2 2 0 014 9h6v2H4V21h6z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default Cad20;
