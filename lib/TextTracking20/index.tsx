import { CarbonIconComponent } from "../types";
export const TextTracking20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="TextTracking20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M25.29 19.29L23.88 20.71 26.17 23 5.83 23 8.12 20.71 6.71 19.29 2 24 6.71 28.71 8.12 27.29 5.83 25 26.17 25 23.88 27.29 25.29 28.71 30 24 25.29 19.29zM26 17h2L23 3H21L16 17h2l1-3h6zm-6.33-5L22 5l2.33 7zM14 3L10 15 6 3 4 3 9 17 11 17 16 3 14 3z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default TextTracking20;
