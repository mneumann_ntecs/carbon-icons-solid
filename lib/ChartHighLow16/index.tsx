import { CarbonIconComponent } from "../types";
export const ChartHighLow16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="ChartHighLow16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M15 8L12 8 12 6 10 6 10 20 7 20 7 22 10 22 10 24 12 24 12 10 15 10 15 8zM27 10L24 10 24 6 22 6 22 18 19 18 19 20 22 20 22 24 24 24 24 12 27 12 27 10z"></path>
    <path d="M30,30H4a2,2,0,0,1-2-2V2H4V28H30Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default ChartHighLow16;
