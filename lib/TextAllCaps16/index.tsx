import { CarbonIconComponent } from "../types";
export const TextAllCaps16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="TextAllCaps16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M1 8L1 10 7 10 7 24 9 24 9 10 15 10 15 8 1 8zM17 8L17 10 23 10 23 24 25 24 25 10 31 10 31 8 17 8z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default TextAllCaps16;
