import { CarbonIconComponent } from "../types";
export const Light16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="Light16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 16 16"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M7.5 1H8.5V3.5H7.5z"></path>
    <path
      d="M10.8 3.4H13.3V4.4H10.8z"
      transform="rotate(-45.001 12.041 3.923)"
    ></path>
    <path d="M12.5 7.5H15V8.5H12.5z"></path>
    <path
      d="M11.6 10.8H12.6V13.3H11.6z"
      transform="rotate(-45.001 12.075 12.041)"
    ></path>
    <path d="M7.5 12.5H8.5V15H7.5z"></path>
    <path
      d="M2.7 11.6H5.2V12.6H2.7z"
      transform="rotate(-45.001 3.96 12.079)"
    ></path>
    <path d="M1 7.5H3.5V8.5H1z"></path>
    <path
      d="M3.4 2.7H4.4V5.2H3.4z"
      transform="rotate(-45.001 3.925 3.961)"
    ></path>
    <path d="M8,6c1.1,0,2,0.9,2,2s-0.9,2-2,2S6,9.1,6,8S6.9,6,8,6 M8,5C6.3,5,5,6.3,5,8s1.3,3,3,3s3-1.3,3-3S9.7,5,8,5z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default Light16;
