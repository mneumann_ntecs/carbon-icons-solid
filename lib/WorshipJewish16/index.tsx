import { CarbonIconComponent } from "../types";
export const WorshipJewish16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="WorshipJewish16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M24.291,16l3.585-6.5181A1,1,0,0,0,27,8H19.8911L16.876,2.5181a1,1,0,0,0-1.752,0L12.1089,8H5a1,1,0,0,0-.876,1.4819L7.709,16,4.124,22.5181A1,1,0,0,0,5,24h7.1089l3.0151,5.4819a1,1,0,0,0,1.752,0L19.8911,24H27a1,1,0,0,0,.876-1.4819Zm-5.5823,6L16,26.9248,13.2913,22h-6.6l3.3-6-3.3-6h6.6L16,5.0752,18.7087,10h6.6l-3.3,6,3.3,6Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default WorshipJewish16;
