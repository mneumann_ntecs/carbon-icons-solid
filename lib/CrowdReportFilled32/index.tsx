import { CarbonIconComponent } from "../types";
export const CrowdReportFilled32: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="CrowdReportFilled32"
    fill={props.fill || "currentColor"}
    width={props.width || "32"}
    height={props.height || "32"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path
      fill="none"
      d="M16.832,19.5547l-1.664-1.1094L17.4648,15H20V10H12v5h2v2H12a2.0021,2.0021,0,0,1-2-2V10a2.0021,2.0021,0,0,1,2-2h8a2.0021,2.0021,0,0,1,2,2v5a2.0021,2.0021,0,0,1-2,2H18.5352Z"
    ></path>
    <path d="M16,2A11.0134,11.0134,0,0,0,5,13a10.8885,10.8885,0,0,0,2.2163,6.6s.3.3945.3482.4517L16,30l8.439-9.9526c.0444-.0533.3447-.4478.3447-.4478l.0015-.0024A10.8846,10.8846,0,0,0,27,13,11.0134,11.0134,0,0,0,16,2Zm6,13a2.0023,2.0023,0,0,1-2,2H18.5352L16.832,19.5547l-1.664-1.1094L17.4648,15H20V10H12v5h2v2H12a2.0023,2.0023,0,0,1-2-2V10a2.0023,2.0023,0,0,1,2-2h8a2.0023,2.0023,0,0,1,2,2Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default CrowdReportFilled32;
