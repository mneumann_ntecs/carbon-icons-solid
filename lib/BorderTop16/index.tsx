import { CarbonIconComponent } from "../types";
export const BorderTop16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="BorderTop16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M3 3H29V5H3zM3 7H5V9H3zM3 11H5V13H3zM3 15H5V17H3zM3 19H5V21H3zM3 23H5V25H3zM3 27H5V29H3zM7 27H9V29H7zM11 27H13V29H11zM15 27H17V29H15zM23 27H25V29H23zM19 27H21V29H19zM27 7H29V9H27zM27 11H29V13H27zM27 15H29V17H27zM27 19H29V21H27zM27 23H29V25H27zM27 27H29V29H27zM8 10H18V12H8zM8 15H14V17H8z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default BorderTop16;
