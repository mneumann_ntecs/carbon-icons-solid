import { CarbonIconComponent } from "../types";
export const Dvr16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="Dvr16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M30 15V11a2 2 0 00-2-2H22V23h2V17h1.48l2.34 6H30l-2.33-6H28A2 2 0 0030 15zm-6-4h4v4H24zM18 9L16 22 14 9 12 9 14.52 23 17.48 23 20 9 18 9zM6 23H2V9H6a4 4 0 014 4v6A4 4 0 016 23zM4 21H6a2 2 0 002-2V13a2 2 0 00-2-2H4z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default Dvr16;
