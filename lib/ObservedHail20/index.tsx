import { CarbonIconComponent } from "../types";
export const ObservedHail20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="ObservedHail20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M4,18A12,12,0,1,0,16,6H12V1L6,7l6,6V8h4A10,10,0,1,1,6,18Z"></path>
    <circle cx="13.5" cy="23.5" r="1.5"></circle>
    <circle cx="10.5" cy="19.5" r="1.5"></circle>
    <circle cx="16.5" cy="19.5" r="1.5"></circle>
    <path
      d="M11.964 14.5H17.036V16.499H11.964z"
      transform="rotate(-45 14.5 15.5)"
    ></path>
    <path
      d="M17.964 14.5H23.035999999999998V16.499H17.964z"
      transform="rotate(-45 20.5 15.5)"
    ></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default ObservedHail20;
