import { CarbonIconComponent } from "../types";
export const DataShare16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="DataShare16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M5 25V15.8281l-3.5859 3.586L0 18l6-6 6 6-1.4141 1.4141L7 15.8281V25H19v2H7A2.0024 2.0024 0 015 25zM24 22h4a2.002 2.002 0 012 2v4a2.002 2.002 0 01-2 2H24a2.002 2.002 0 01-2-2V24A2.002 2.002 0 0124 22zm4 6V24H23.9985L24 28zM27 6v9.1719l3.5859-3.586L32 13l-6 6-6-6 1.4141-1.4141L25 15.1719V6H13V4H25A2.0024 2.0024 0 0127 6zM2 6H8V8H2zM2 2H10V4H2z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default DataShare16;
