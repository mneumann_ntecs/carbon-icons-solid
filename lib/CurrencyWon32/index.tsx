import { CarbonIconComponent } from "../types";
export const CurrencyWon32: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="CurrencyWon32"
    fill={props.fill || "currentColor"}
    width={props.width || "32"}
    height={props.height || "32"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M28 14L28 12 23.045 12 24 5 22 5 20 23 17 8 15 8 12 23 10 5 8 5 8.955 12 4 12 4 14 9.227 14 9.636 17 4 17 4 19 9.909 19 11 27 13 27 16 12 19 27 21 27 22.091 19 28 19 28 17 22.364 17 22.773 14 28 14z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default CurrencyWon32;
