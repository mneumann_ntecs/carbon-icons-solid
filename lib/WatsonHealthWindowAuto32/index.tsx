import { CarbonIconComponent } from "../types";
export const WatsonHealthWindowAuto32: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="WatsonHealthWindowAuto32"
    fill={props.fill || "currentColor"}
    width={props.width || "32"}
    height={props.height || "32"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M15 4H17V7H15zM25 15H28V17H25zM15 25H17V28H15zM4 15H7V17H4z"></path>
    <path
      d="M7.55 7.04H9.55V10.04H7.55z"
      transform="rotate(-45 8.55 8.548)"
    ></path>
    <path
      d="M21.96 7.55H24.96V9.55H21.96z"
      transform="rotate(-45 23.454 8.555)"
    ></path>
    <path
      d="M22.45 21.95H24.45V24.95H22.45z"
      transform="rotate(-45 23.452 23.446)"
    ></path>
    <path
      d="M7.05 22.45H10.05V24.45H7.05z"
      transform="rotate(-45 8.544 23.451)"
    ></path>
    <path d="M4 30H28V32H4zM4 0H28V2H4zM16 10a6 6 0 106 6A6 6 0 0016 10zm-4 6a4 4 0 014-4v8A4 4 0 0112 16z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default WatsonHealthWindowAuto32;
