import {
  template,
  addEventListener,
  insert,
  memo,
  effect,
  setAttribute,
  style,
  delegateEvents,
} from "solid-js/web";

const _tmpl$ = template(
    `<svg data-carbon-icon="HeatMap_0220" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" preserveAspectRatio="xMidYMid meet"><circle cx="21" cy="20" r="2"></circle><circle cx="14" cy="12" r="2"></circle><circle cx="29" cy="19" r="1"></circle><path d="M26.5 30A3.5 3.5 0 1130 26.5 3.5041 3.5041 0 0126.5 30zm0-5A1.5 1.5 0 1028 26.5 1.5017 1.5017 0 0026.5 25zM14 30a3.958 3.958 0 01-2.126-.6211 6.9977 6.9977 0 114.1109-6.8384A3.9916 3.9916 0 0114 30zm-1.8843-3.0278l.5391.4946a1.9915 1.9915 0 102.0039-3.343l-.6909-.2432.03-.8467a5.0085 5.0085 0 10-2.5166 4.3023zM24 16a6.0067 6.0067 0 01-6-6 5.3246 5.3246 0 01.0269-.5327A3.9564 3.9564 0 0116 6a4.0045 4.0045 0 014-4 3.9564 3.9564 0 013.4673 2.0271C23.6484 4.009 23.8252 4 24 4a6 6 0 010 12zM20 4a2.0021 2.0021 0 00-2 2 1.9805 1.9805 0 001.43 1.9023l.9018.2706-.2153.9162A3.9938 3.9938 0 1024 6a4.0064 4.0064 0 00-.9121.1162l-.9155.2141-.27-.9006A1.9807 1.9807 0 0020 4zM6.5 11A4.5 4.5 0 1111 6.5 4.5051 4.5051 0 016.5 11zm0-7A2.5 2.5 0 109 6.5 2.503 2.503 0 006.5 4z"></path></svg>`,
    10
  ),
  _tmpl$2 = template(`<title></title>`, 2);

const HeatMap_0220 = (props) =>
  (() => {
    const _el$ = _tmpl$.cloneNode(true),
      _el$2 = _el$.firstChild,
      _el$3 = _el$2.nextSibling,
      _el$4 = _el$3.nextSibling;
    _el$4.nextSibling;

    addEventListener(_el$, "keydown", props.onKeyDown, true);

    addEventListener(_el$, "keyup", props.onKeyUp, true);

    addEventListener(_el$, "mouseleave", props.onMouseLeave);

    addEventListener(_el$, "mouseenter", props.onMouseEnter);

    addEventListener(_el$, "mouseover", props.onMouseOver, true);

    addEventListener(_el$, "click", props.onClick, true);

    insert(
      _el$,
      (() => {
        const _c$ = memo(() => props.children, true);

        return () =>
          _c$() ||
          (props.title &&
            (() => {
              const _el$6 = _tmpl$2.cloneNode(true);

              insert(_el$6, () => props.title);

              return _el$6;
            })());
      })(),
      null
    );

    effect(
      (_p$) => {
        const _v$ = props.fill || "currentColor",
          _v$2 = props.width || "20",
          _v$3 = props.height || "20",
          _v$4 = props["aria-label"],
          _v$5 = props["aria-labelledby"],
          _v$6 = props["aria-label"] || props["aria-labelledby"] || props.title,
          _v$7 =
            props["aria-label"] || props["aria-labelledby"] || props.title
              ? "img"
              : undefined,
          _v$8 = props.tabindex === "0" ? true : props.focusable,
          _v$9 = props.tabindex,
          _v$10 = props.id,
          _v$11 = props.class,
          _v$12 = props.title,
          _v$13 = props.style,
          _v$14 = props.stroke;

        _v$ !== _p$._v$ && setAttribute(_el$, "fill", (_p$._v$ = _v$));
        _v$2 !== _p$._v$2 && setAttribute(_el$, "width", (_p$._v$2 = _v$2));
        _v$3 !== _p$._v$3 && setAttribute(_el$, "height", (_p$._v$3 = _v$3));
        _v$4 !== _p$._v$4 &&
          setAttribute(_el$, "aria-label", (_p$._v$4 = _v$4));
        _v$5 !== _p$._v$5 &&
          setAttribute(_el$, "aria-labelledby", (_p$._v$5 = _v$5));
        _v$6 !== _p$._v$6 &&
          setAttribute(_el$, "aria-hidden", (_p$._v$6 = _v$6));
        _v$7 !== _p$._v$7 && setAttribute(_el$, "role", (_p$._v$7 = _v$7));
        _v$8 !== _p$._v$8 && setAttribute(_el$, "focusable", (_p$._v$8 = _v$8));
        _v$9 !== _p$._v$9 && setAttribute(_el$, "tabindex", (_p$._v$9 = _v$9));
        _v$10 !== _p$._v$10 && setAttribute(_el$, "id", (_p$._v$10 = _v$10));
        _v$11 !== _p$._v$11 && setAttribute(_el$, "class", (_p$._v$11 = _v$11));
        _v$12 !== _p$._v$12 && setAttribute(_el$, "title", (_p$._v$12 = _v$12));
        _p$._v$13 = style(_el$, _v$13, _p$._v$13);
        _v$14 !== _p$._v$14 &&
          setAttribute(_el$, "stroke", (_p$._v$14 = _v$14));
        return _p$;
      },
      {
        _v$: undefined,
        _v$2: undefined,
        _v$3: undefined,
        _v$4: undefined,
        _v$5: undefined,
        _v$6: undefined,
        _v$7: undefined,
        _v$8: undefined,
        _v$9: undefined,
        _v$10: undefined,
        _v$11: undefined,
        _v$12: undefined,
        _v$13: undefined,
        _v$14: undefined,
      }
    );

    return _el$;
  })();

delegateEvents(["click", "mouseover", "keyup", "keydown"]);

export default HeatMap_0220;
export { HeatMap_0220 };
