import { CarbonIconComponent } from "../types";
export const HybridNetworking20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="HybridNetworking20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M17,24.1836V20H15v4.1836a3,3,0,1,0,2,0Z"></path>
    <path d="M26,12a3.9962,3.9962,0,0,0-3.8579,3H9.8579a4,4,0,1,0,0,2H22.1421A3.9934,3.9934,0,1,0,26,12ZM6,18a2,2,0,1,1,2-2A2.0025,2.0025,0,0,1,6,18Zm20,0a2,2,0,1,1,2-2A2.0025,2.0025,0,0,1,26,18Z"></path>
    <path d="M19,5a3,3,0,1,0-4,2.8164V12h2V7.8164A2.9917,2.9917,0,0,0,19,5Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default HybridNetworking20;
