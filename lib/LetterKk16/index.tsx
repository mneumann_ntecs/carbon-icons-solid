import { CarbonIconComponent } from "../types";
export const LetterKk16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="LetterKk16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M15 9L12.89 9 9 15.55 9 9 7 9 7 23 9 23 9 18.71 9.93 17.22 12.89 23 15 23 11.11 15.43 15 9zM22.78 23L25 23 21.22 17 25 13 22.76 13 19 17.17 19 9 17 9 17 23 19 23 19 19.25 19.96 18.21 22.78 23z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default LetterKk16;
