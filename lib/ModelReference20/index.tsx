import { CarbonIconComponent } from "../types";
export const ModelReference20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="ModelReference20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M4 20L4 22 8.586 22 2 28.586 3.414 30 10 23.414 10 28 12 28 12 20 4 20z"></path>
    <path d="M23.5,4H8.5L1.7158,13.0454,6.01,18l1.5114-1.31L5.19,14h5.2554L16,22.8867,21.5544,14H26.81L15.8125,26.6919,17.3242,28l12.96-14.9546ZM5,12,9.5,6h4.6958l-3.75,6ZM16,6.8867,19.1958,12H12.8042Zm0,12.2266L12.8044,14h6.3912ZM21.5542,12l-3.75-6H22.5L27,12Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default ModelReference20;
