import { CarbonIconComponent } from "../types";
export const SplitScreen20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="SplitScreen20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M15 4H17V28H15zM10 7V25H4V7h6m0-2H4A2 2 0 002 7V25a2 2 0 002 2h6a2 2 0 002-2V7a2 2 0 00-2-2zM28 7V25H22V7h6m0-2H22a2 2 0 00-2 2V25a2 2 0 002 2h6a2 2 0 002-2V7a2 2 0 00-2-2z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default SplitScreen20;
