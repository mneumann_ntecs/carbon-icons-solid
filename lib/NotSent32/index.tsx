import { CarbonIconComponent } from "../types";
export const NotSent32: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="NotSent32"
    fill={props.fill || "currentColor"}
    width={props.width || "32"}
    height={props.height || "32"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M30 28.59L3.41 2 2 3.41l8 8L2.66 14.06a1 1 0 000 1.87l9.6 3.84 3.84 9.6A1 1 0 0017 30h0a1 1 0 00.92-.66L20.6 22l8 8zM5.8 15L11.54 13l3.05 3-1.83 1.83zM17 26.2l-2.79-7L16 17.41l3 3.05zM15.69 11.45l7.64-2.78-2.78 7.64 1.56 1.56L25.94 7.34a1 1 0 00-1.28-1.28L14.13 9.89z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default NotSent32;
