import { CarbonIconComponent } from "../types";
export const CategoryAdd32: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="CategoryAdd32"
    fill={props.fill || "currentColor"}
    width={props.width || "32"}
    height={props.height || "32"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M15 6L13.59 7.41 17.17 11 13.59 14.59 15 16 20 11 15 6z"></path>
    <path d="M28,24H17V20h6a2.0027,2.0027,0,0,0,2-2V4a2.0023,2.0023,0,0,0-2-2H9A2.002,2.002,0,0,0,7,4V18a2.0023,2.0023,0,0,0,2,2h6v4H4a2,2,0,0,0-2,2v4H4V26H15v4h2V26H28v4h2V26A2,2,0,0,0,28,24ZM9,4H23l.002,14H9Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default CategoryAdd32;
