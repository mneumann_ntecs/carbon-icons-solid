import {
  template,
  addEventListener,
  insert,
  memo,
  effect,
  setAttribute,
  style,
  delegateEvents,
} from "solid-js/web";

const _tmpl$ = template(
    `<svg data-carbon-icon="LogoKeybase24" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" preserveAspectRatio="xMidYMid meet"><path d="M14.1233 26.9328a1.1122 1.1122 0 11-1.1121-1.1122 1.112 1.112 0 011.1121 1.1122M21.0257 26.9328a1.1122 1.1122 0 11-1.1122-1.1122 1.1121 1.1121 0 011.1122 1.1122"></path><path d="M9.999 5.797H10.973V6.771H9.999z" transform="rotate(-86.462 10.486 6.284)"></path><path d="M26.4275,16.3825l-.0013.001a13.0868,13.0868,0,0,0-5.1431-4.1825c-.3754-.1667-.7543-.3142-1.1354-.4454.0128-.0314.0277-.0615.04-.0933a4.83,4.83,0,0,0-2.9861-6.319,5.9368,5.9368,0,0,0-1.9962-.3558c-.0366-.1253-.1165-.6743.58-2.1571L14.3742,2q-.17.236-.3368.4661c-.384.5309-.7549,1.0444-1.1029,1.55a2.1657,2.1657,0,0,0-1.4486-.6771L9.8543,3.2377H9.852l-.0025,0c-.0429-.0025-.0864-.0039-.1293-.0039A2.1877,2.1877,0,0,0,7.54,5.2825L7.4388,6.9169l0,.003,0,.0028A2.1882,2.1882,0,0,0,9.4854,9.23l1.1673.0723a5.1711,5.1711,0,0,0,.47,2.688,13.1954,13.1954,0,0,0-4.2637,2.9C3,18.7883,3,23.06,3,26.4916v2.0706l1.524-1.6163A12.3293,12.3293,0,0,0,5.8548,30H7.8367A10.7091,10.7091,0,0,1,5.878,25.51l2.2353-2.3708L6.963,26.745l2.03-1.4774c3.5858-2.61,7.8678-3.2093,12.7263-1.7826a4.6276,4.6276,0,0,0,5.2189-1.468c.0441-.0628.0828-.128.1228-.1926a9.6141,9.6141,0,0,1,.1638,1.7454,12.4214,12.4214,0,0,1-1.61,6.43h1.877A14.4022,14.4022,0,0,0,28.87,23.57,12.2666,12.2666,0,0,0,26.4275,16.3825ZM13.8455,5.9257a1.5872,1.5872,0,0,0,1.3983.7068,4.2111,4.2111,0,0,1,1.44.2719,3.17,3.17,0,0,1,1.0279,5.4638l-.4752-.5849-.0032-.004a1.7048,1.7048,0,0,0-3.0235.9865,3.2143,3.2143,0,0,1-1.899-3.5412,8.4815,8.4815,0,0,1,1.4374-3.466A1.642,1.642,0,0,0,13.8455,5.9257Zm3.49,9.2589-.6072.4962a.2675.2675,0,0,1-.376-.038l-.1264-.1549a.2791.2791,0,0,1,.0394-.3927l.6-.49-1.235-1.5158a.362.362,0,1,1,.5611-.4575l3.4546,4.2517a.3619.3619,0,0,1-.1988.5815.3566.3566,0,0,1-.0816.0094.3609.3609,0,0,1-.2806-.1332l-.3441-.4239-1.2215.9971a.2815.2815,0,0,1-.3965-.04l-.5532-.6775a.2786.2786,0,0,1,.04-.3919l1.2275-1.002ZM9.08,7.0184l.1014-1.6342A.5394.5394,0,0,1,9.72,4.8781l.0326.001,1.6335.1013a.5387.5387,0,0,1,.5046.5716l-.0073.1182a10.6739,10.6739,0,0,0-.9028,2.0046L9.5859,7.5886A.5407.5407,0,0,1,9.08,7.0184ZM25.5944,21.07a2.9883,2.9883,0,0,1-3.4112.8373c-4.5313-1.3305-8.6289-1.0534-12.2124.82l1.9069-5.978-7.203,7.6393a11.67,11.67,0,0,1,3.3532-8.3419,11.4953,11.4953,0,0,1,4.1073-2.6908,5.42,5.42,0,0,0,2.2411,1.2214,4.714,4.714,0,0,0,.5627.12,1.6258,1.6258,0,0,0,.2449,1.6416l.0847.1038a1.6247,1.6247,0,0,0,.2594,1.605l.5532.6777a1.6276,1.6276,0,0,0,2.29.2314l.3314-.2705a1.7079,1.7079,0,0,0,1.9867-2.6535l-1.9427-2.3911a4.88,4.88,0,0,0,.4863-.456,12.449,12.449,0,0,1,1.3829.5181,11.419,11.419,0,0,1,4.5085,3.6838C26.0987,18.7278,26.2745,20.1043,25.5944,21.07Z"></path></svg>`,
    8
  ),
  _tmpl$2 = template(`<title></title>`, 2);

const LogoKeybase24 = (props) =>
  (() => {
    const _el$ = _tmpl$.cloneNode(true),
      _el$2 = _el$.firstChild,
      _el$3 = _el$2.nextSibling;
    _el$3.nextSibling;

    addEventListener(_el$, "keydown", props.onKeyDown, true);

    addEventListener(_el$, "keyup", props.onKeyUp, true);

    addEventListener(_el$, "mouseleave", props.onMouseLeave);

    addEventListener(_el$, "mouseenter", props.onMouseEnter);

    addEventListener(_el$, "mouseover", props.onMouseOver, true);

    addEventListener(_el$, "click", props.onClick, true);

    insert(
      _el$,
      (() => {
        const _c$ = memo(() => props.children, true);

        return () =>
          _c$() ||
          (props.title &&
            (() => {
              const _el$5 = _tmpl$2.cloneNode(true);

              insert(_el$5, () => props.title);

              return _el$5;
            })());
      })(),
      null
    );

    effect(
      (_p$) => {
        const _v$ = props.fill || "currentColor",
          _v$2 = props.width || "24",
          _v$3 = props.height || "24",
          _v$4 = props["aria-label"],
          _v$5 = props["aria-labelledby"],
          _v$6 = props["aria-label"] || props["aria-labelledby"] || props.title,
          _v$7 =
            props["aria-label"] || props["aria-labelledby"] || props.title
              ? "img"
              : undefined,
          _v$8 = props.tabindex === "0" ? true : props.focusable,
          _v$9 = props.tabindex,
          _v$10 = props.id,
          _v$11 = props.class,
          _v$12 = props.title,
          _v$13 = props.style,
          _v$14 = props.stroke;

        _v$ !== _p$._v$ && setAttribute(_el$, "fill", (_p$._v$ = _v$));
        _v$2 !== _p$._v$2 && setAttribute(_el$, "width", (_p$._v$2 = _v$2));
        _v$3 !== _p$._v$3 && setAttribute(_el$, "height", (_p$._v$3 = _v$3));
        _v$4 !== _p$._v$4 &&
          setAttribute(_el$, "aria-label", (_p$._v$4 = _v$4));
        _v$5 !== _p$._v$5 &&
          setAttribute(_el$, "aria-labelledby", (_p$._v$5 = _v$5));
        _v$6 !== _p$._v$6 &&
          setAttribute(_el$, "aria-hidden", (_p$._v$6 = _v$6));
        _v$7 !== _p$._v$7 && setAttribute(_el$, "role", (_p$._v$7 = _v$7));
        _v$8 !== _p$._v$8 && setAttribute(_el$, "focusable", (_p$._v$8 = _v$8));
        _v$9 !== _p$._v$9 && setAttribute(_el$, "tabindex", (_p$._v$9 = _v$9));
        _v$10 !== _p$._v$10 && setAttribute(_el$, "id", (_p$._v$10 = _v$10));
        _v$11 !== _p$._v$11 && setAttribute(_el$, "class", (_p$._v$11 = _v$11));
        _v$12 !== _p$._v$12 && setAttribute(_el$, "title", (_p$._v$12 = _v$12));
        _p$._v$13 = style(_el$, _v$13, _p$._v$13);
        _v$14 !== _p$._v$14 &&
          setAttribute(_el$, "stroke", (_p$._v$14 = _v$14));
        return _p$;
      },
      {
        _v$: undefined,
        _v$2: undefined,
        _v$3: undefined,
        _v$4: undefined,
        _v$5: undefined,
        _v$6: undefined,
        _v$7: undefined,
        _v$8: undefined,
        _v$9: undefined,
        _v$10: undefined,
        _v$11: undefined,
        _v$12: undefined,
        _v$13: undefined,
        _v$14: undefined,
      }
    );

    return _el$;
  })();

delegateEvents(["click", "mouseover", "keyup", "keydown"]);

export default LogoKeybase24;
export { LogoKeybase24 };
