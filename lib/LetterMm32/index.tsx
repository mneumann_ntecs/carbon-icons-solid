import { CarbonIconComponent } from "../types";
export const LetterMm32: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="LetterMm32"
    fill={props.fill || "currentColor"}
    width={props.width || "32"}
    height={props.height || "32"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M24 13H16V23h2V15h2v8h2V15h2v8h2V15A2 2 0 0024 13zM12 9L10.48 14 10 15.98 9.54 14 8 9 6 9 6 23 8 23 8 15 7.84 13 8.42 15 10 19.63 11.58 15 12.16 13 12 15 12 23 14 23 14 9 12 9z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default LetterMm32;
