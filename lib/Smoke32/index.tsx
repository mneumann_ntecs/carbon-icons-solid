import { CarbonIconComponent } from "../types";
export const Smoke32: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="Smoke32"
    fill={props.fill || "currentColor"}
    width={props.width || "32"}
    height={props.height || "32"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M27.001 28a.998.998 0 01-.8008-.4l-.8145-1.086a8.9976 8.9976 0 01-1.6005-7.2856l1.4736-6.8762a6.9956 6.9956 0 00-1.2446-5.6668L23.2 5.6a1 1 0 111.6-1.2l.8145 1.0857a8.9976 8.9976 0 011.6005 7.2856l-1.4736 6.8762a6.9956 6.9956 0 001.2446 5.6668L27.8 26.4A1 1 0 0127.001 28zM22.001 28a.998.998 0 01-.8008-.4l-.8145-1.086a8.9976 8.9976 0 01-1.6005-7.2856l1.4736-6.8762a6.9956 6.9956 0 00-1.2446-5.6668L18.2 5.6a1 1 0 111.6-1.2l.8145 1.0857a8.9976 8.9976 0 011.6005 7.2856l-1.4736 6.8762a6.9956 6.9956 0 001.2446 5.6668L22.8 26.4A1 1 0 0122.001 28zM17.001 28a.998.998 0 01-.8008-.4l-.8145-1.086a8.9976 8.9976 0 01-1.6005-7.2856l1.4736-6.8762a6.9956 6.9956 0 00-1.2446-5.6668L13.2 5.6a1 1 0 111.6-1.2l.8145 1.0857a8.9976 8.9976 0 011.6005 7.2856l-1.4736 6.8762a6.9956 6.9956 0 001.2446 5.6668L17.8 26.4A1 1 0 0117.001 28zM12.001 28a.998.998 0 01-.8008-.4l-.8145-1.086a8.9976 8.9976 0 01-1.6005-7.2856l1.4736-6.8762A6.9956 6.9956 0 009.0142 6.6855L8.2 5.6A1 1 0 119.8 4.4l.8145 1.0857a8.9976 8.9976 0 011.6005 7.2856l-1.4736 6.8762a6.9956 6.9956 0 001.2446 5.6668L12.8 26.4A1 1 0 0112.001 28zM7.001 28A.998.998 0 016.2 27.6l-.8145-1.086a8.9976 8.9976 0 01-1.6-7.2856l1.4736-6.8762A6.9956 6.9956 0 004.0142 6.6855L3.2 5.6A1 1 0 114.8 4.4l.8145 1.0857a8.9976 8.9976 0 011.6005 7.2856L5.7412 19.6474a6.9956 6.9956 0 001.2446 5.6668L7.8 26.4A1 1 0 017.001 28z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default Smoke32;
