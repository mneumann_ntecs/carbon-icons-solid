import { CarbonIconComponent } from "../types";
export const Http20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="Http20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M30 11H25V21h2V18h3a2.0027 2.0027 0 002-2V13A2.0023 2.0023 0 0030 11zm-3 5V13h3l.001 3zM10 13L12 13 12 21 14 21 14 13 16 13 16 11 10 11 10 13zM23 11L17 11 17 13 19 13 19 21 21 21 21 13 23 13 23 11zM6 11L6 15 3 15 3 11 1 11 1 21 3 21 3 17 6 17 6 21 8 21 8 11 6 11z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default Http20;
