import { CarbonIconComponent } from "../types";
export const Pedestrian20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="Pedestrian20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M21.6772,14l-1.2456-3.1143A2.9861,2.9861,0,0,0,17.646,9H13.5542a3.0018,3.0018,0,0,0-1.5439.4277L7,12.4336V18H9V13.5664l3-1.8V23.6973L8.5383,28.8906,10.2024,30,14,24.3027V11h3.646a.9949.9949,0,0,1,.9282.6289L20.3228,16H26V14Z"></path>
    <path d="M17.051 18.316L19 24.162 19 30 21 30 21 23.838 18.949 17.684 17.051 18.316zM16.5 8A3.5 3.5 0 1120 4.5 3.5042 3.5042 0 0116.5 8zm0-5A1.5 1.5 0 1018 4.5 1.5017 1.5017 0 0016.5 3z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default Pedestrian20;
