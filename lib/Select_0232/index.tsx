import { CarbonIconComponent } from "../types";
export const Select_0232: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="Select_0232"
    fill={props.fill || "currentColor"}
    width={props.width || "32"}
    height={props.height || "32"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M12 6L8 6 8 2 6 2 6 6 2 6 2 8 6 8 6 12 8 12 8 8 12 8 12 6zM30 10V4H24V6H16V8h8v2h2V24H24v2H10V24H8V16H6v8H4v6h6V28H24v2h6V24H28V10zM8 28H6V26H8zm20 0H26V26h2zM26 6h2V8H26z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default Select_0232;
