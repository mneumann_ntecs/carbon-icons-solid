import { CarbonIconComponent } from "../types";
export const ZipReference32: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="ZipReference32"
    fill={props.fill || "currentColor"}
    width={props.width || "32"}
    height={props.height || "32"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M4 20L4 22 8.586 22 2 28.586 3.414 30 10 23.414 10 28 12 28 12 20 4 20zM28 2H22V16h2V11h4a2 2 0 002-2V4A2 2 0 0028 2zm0 7H24V4h4zM12 4L15 4 15 14 12 14 12 16 20 16 20 14 17 14 17 4 20 4 20 2 12 2 12 4zM2 4L8 4 2 14 2 16 10 16 10 14 4 14 10 4 10 2 2 2 2 4z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default ZipReference32;
