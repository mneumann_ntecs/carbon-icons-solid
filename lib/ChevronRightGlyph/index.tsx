import { CarbonIconComponent } from "../types";
export const ChevronRightGlyph: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="ChevronRightGlyph"
    fill={props.fill || "currentColor"}
    width={props.width || "6"}
    height={props.height || "10"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 6 10"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M6 5L1 10 0.3 9.3 4.6 5 0.3 0.7 1 0z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default ChevronRightGlyph;
