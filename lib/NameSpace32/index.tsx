import { CarbonIconComponent } from "../types";
export const NameSpace32: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="NameSpace32"
    fill={props.fill || "currentColor"}
    width={props.width || "32"}
    height={props.height || "32"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M7 12H4a2.0023 2.0023 0 01-2-2V6A2.0023 2.0023 0 014 4H7V6H4v4H7zM9 10H15V12H9zM17 10H23V12H17zM28 12H25V10h3V6H25V4h3a2.0023 2.0023 0 012 2v4A2.0023 2.0023 0 0128 12zM17 4H23V6H17zM9 4H15V6H9zM28 28H4a2.0023 2.0023 0 01-2-2V22a2.0023 2.0023 0 012-2H28a2.0023 2.0023 0 012 2v4A2.0023 2.0023 0 0128 28zM4 22v4H28V22zM2 15H30V17H2z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default NameSpace32;
