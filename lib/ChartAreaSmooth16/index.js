import {
  template,
  addEventListener,
  insert,
  memo,
  effect,
  setAttribute,
  style,
  delegateEvents,
} from "solid-js/web";

const _tmpl$ = template(
    `<svg data-carbon-icon="ChartAreaSmooth16" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" preserveAspectRatio="xMidYMid meet"><path d="M30,6.6143l-1.3152.4365A20.9218,20.9218,0,0,1,23,8c-1.7344,0-3.3225-.9072-5.0039-1.8682C16.1621,5.084,14.2656,4,12,4,9.123,4,5.9146,6.2061,4,7.772V2H2V28a2.0025,2.0025,0,0,0,2,2H30ZM4,20.2944C5.874,17.3765,9.56,13,12,13c1.6379,0,2.96,1.1016,4.36,2.2686C17.8965,16.5488,19.6379,18,22,18a9.4686,9.4686,0,0,0,6-2.5288v4.9556A13.0026,13.0026,0,0,1,22,22a13.8619,13.8619,0,0,1-4.6838-.9487A15.682,15.682,0,0,0,12,20c-2.927,0-6.0676,2.959-8,5.1577ZM12,6c1.7344,0,3.3225.9072,5.0039,1.8682C18.8379,8.916,20.7344,10,23,10a21.432,21.432,0,0,0,5-.6782v3.3213C27.3167,13.4463,24.9155,16,22,16c-1.6379,0-2.96-1.1016-4.36-2.2686C16.1035,12.4512,14.3621,11,12,11c-2.9358,0-6.0632,3.3394-8,5.8447V10.4238C5.5461,8.9658,9.2588,6,12,6ZM4.249,28C5.8623,25.9087,9.41,22,12,22a13.8619,13.8619,0,0,1,4.6838.9487A15.682,15.682,0,0,0,22,24a14.7362,14.7362,0,0,0,6-1.34V28Z"></path></svg>`,
    4
  ),
  _tmpl$2 = template(`<title></title>`, 2);

const ChartAreaSmooth16 = (props) =>
  (() => {
    const _el$ = _tmpl$.cloneNode(true);
    _el$.firstChild;

    addEventListener(_el$, "keydown", props.onKeyDown, true);

    addEventListener(_el$, "keyup", props.onKeyUp, true);

    addEventListener(_el$, "mouseleave", props.onMouseLeave);

    addEventListener(_el$, "mouseenter", props.onMouseEnter);

    addEventListener(_el$, "mouseover", props.onMouseOver, true);

    addEventListener(_el$, "click", props.onClick, true);

    insert(
      _el$,
      (() => {
        const _c$ = memo(() => props.children, true);

        return () =>
          _c$() ||
          (props.title &&
            (() => {
              const _el$3 = _tmpl$2.cloneNode(true);

              insert(_el$3, () => props.title);

              return _el$3;
            })());
      })(),
      null
    );

    effect(
      (_p$) => {
        const _v$ = props.fill || "currentColor",
          _v$2 = props.width || "16",
          _v$3 = props.height || "16",
          _v$4 = props["aria-label"],
          _v$5 = props["aria-labelledby"],
          _v$6 = props["aria-label"] || props["aria-labelledby"] || props.title,
          _v$7 =
            props["aria-label"] || props["aria-labelledby"] || props.title
              ? "img"
              : undefined,
          _v$8 = props.tabindex === "0" ? true : props.focusable,
          _v$9 = props.tabindex,
          _v$10 = props.id,
          _v$11 = props.class,
          _v$12 = props.title,
          _v$13 = props.style,
          _v$14 = props.stroke;

        _v$ !== _p$._v$ && setAttribute(_el$, "fill", (_p$._v$ = _v$));
        _v$2 !== _p$._v$2 && setAttribute(_el$, "width", (_p$._v$2 = _v$2));
        _v$3 !== _p$._v$3 && setAttribute(_el$, "height", (_p$._v$3 = _v$3));
        _v$4 !== _p$._v$4 &&
          setAttribute(_el$, "aria-label", (_p$._v$4 = _v$4));
        _v$5 !== _p$._v$5 &&
          setAttribute(_el$, "aria-labelledby", (_p$._v$5 = _v$5));
        _v$6 !== _p$._v$6 &&
          setAttribute(_el$, "aria-hidden", (_p$._v$6 = _v$6));
        _v$7 !== _p$._v$7 && setAttribute(_el$, "role", (_p$._v$7 = _v$7));
        _v$8 !== _p$._v$8 && setAttribute(_el$, "focusable", (_p$._v$8 = _v$8));
        _v$9 !== _p$._v$9 && setAttribute(_el$, "tabindex", (_p$._v$9 = _v$9));
        _v$10 !== _p$._v$10 && setAttribute(_el$, "id", (_p$._v$10 = _v$10));
        _v$11 !== _p$._v$11 && setAttribute(_el$, "class", (_p$._v$11 = _v$11));
        _v$12 !== _p$._v$12 && setAttribute(_el$, "title", (_p$._v$12 = _v$12));
        _p$._v$13 = style(_el$, _v$13, _p$._v$13);
        _v$14 !== _p$._v$14 &&
          setAttribute(_el$, "stroke", (_p$._v$14 = _v$14));
        return _p$;
      },
      {
        _v$: undefined,
        _v$2: undefined,
        _v$3: undefined,
        _v$4: undefined,
        _v$5: undefined,
        _v$6: undefined,
        _v$7: undefined,
        _v$8: undefined,
        _v$9: undefined,
        _v$10: undefined,
        _v$11: undefined,
        _v$12: undefined,
        _v$13: undefined,
        _v$14: undefined,
      }
    );

    return _el$;
  })();

delegateEvents(["click", "mouseover", "keyup", "keydown"]);

export default ChartAreaSmooth16;
export { ChartAreaSmooth16 };
