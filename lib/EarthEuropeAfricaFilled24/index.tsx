import { CarbonIconComponent } from "../types";
export const EarthEuropeAfricaFilled24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="EarthEuropeAfricaFilled24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M16,2A14,14,0,1,0,30,16,14.0158,14.0158,0,0,0,16,2ZM4,16A11.9814,11.9814,0,0,1,9,6.2734V9a1,1,0,0,0,1,1h3.3821a1,1,0,0,0,.8943-.5527L15,8h4v6H13.2361a1.0006,1.0006,0,0,0-.4473.1055L9.5527,15.7236A1,1,0,0,0,9,16.6182v3.7046a1,1,0,0,0,.6287.9287L14,23l.8235,4.94A12.0152,12.0152,0,0,1,4,16ZM22,26.376V23l1.7546-2.6318a1.0012,1.0012,0,0,0,.1382-.7974L23,16V13h4.605A11.901,11.901,0,0,1,22,26.376Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default EarthEuropeAfricaFilled24;
