import { CarbonIconComponent } from "../types";
export const NumberSmall_024: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="NumberSmall_024"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M17,21H15a2,2,0,0,1-2-2V13a2,2,0,0,1,2-2h2a2,2,0,0,1,2,2v6A2,2,0,0,1,17,21Zm-2-8v6h2V13Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default NumberSmall_024;
