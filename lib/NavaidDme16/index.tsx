import { CarbonIconComponent } from "../types";
export const NavaidDme16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="NavaidDme16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M16,20a4,4,0,1,1,4-4A4.0045,4.0045,0,0,1,16,20Zm0-6a2,2,0,1,0,2,2A2.0021,2.0021,0,0,0,16,14Z"></path>
    <path d="M24,24H8a2.0021,2.0021,0,0,1-2-2V10A2.0021,2.0021,0,0,1,8,8H24a2.0021,2.0021,0,0,1,2,2V22A2.0021,2.0021,0,0,1,24,24ZM8,10V22H24V10Z"></path>
    <path d="M28,28H4a2.0021,2.0021,0,0,1-2-2V6A2.0021,2.0021,0,0,1,4,4H28a2.0021,2.0021,0,0,1,2,2V26A2.0021,2.0021,0,0,1,28,28ZM4,6V26H28V6Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default NavaidDme16;
