import { CarbonIconComponent } from "../types";
export const DeploymentPolicy20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="DeploymentPolicy20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M19 16L19 14 26.171 14 23.878 11.707 25.292 10.293 30 15 25.292 19.707 23.878 18.293 26.171 16 19 16zM17 12L15 12 15 5.828 12.707 8.121 11.293 6.707 16 2 20.707 6.707 19.293 8.121 17 5.828 17 12zM17 20.1011V18a4.0045 4.0045 0 00-4-4H5.8281l2.293-2.293L6.707 10.293 2 15l4.707 4.707 1.4141-1.414L5.8281 16H13a2.0025 2.0025 0 012 2v2.1011a5 5 0 102 0zM16 28a3 3 0 113-3A3.0033 3.0033 0 0116 28z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default DeploymentPolicy20;
