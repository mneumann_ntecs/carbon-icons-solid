import { CarbonIconComponent } from "../types";
export const CloudSatellite20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="CloudSatellite20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <circle cx="9" cy="20" r="2"></circle>
    <path d="M16,20a4,4,0,1,1,4-4A4.0118,4.0118,0,0,1,16,20Zm0-6a2,2,0,1,0,2,2A2.0059,2.0059,0,0,0,16,14Z"></path>
    <circle cx="23" cy="12" r="2"></circle>
    <path d="M23,24.7588l-7,4.0835L5,22.4258V9.5742L16,3.1577,27,9.5742V22h2V9a1,1,0,0,0-.4961-.8638l-12-7a1,1,0,0,0-1.0078,0l-12,7A1,1,0,0,0,3,9V23a1,1,0,0,0,.4961.8638l12,7a1,1,0,0,0,1.0078,0L24,26.4907Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default CloudSatellite20;
