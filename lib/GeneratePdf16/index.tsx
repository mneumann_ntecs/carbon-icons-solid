import { CarbonIconComponent } from "../types";
export const GeneratePdf16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="GeneratePdf16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M24,24v4H8V24H6v4H6a2,2,0,0,0,2,2H24a2,2,0,0,0,2-2h0V24Z"></path>
    <path d="M21 21L19.586 19.586 17 22.172 17 14 15 14 15 22.172 12.414 19.586 11 21 16 26 21 21zM28 4L28 2 22 2 22 12 24 12 24 8 27 8 27 6 24 6 24 4 28 4zM17 12H13V2h4a3.0033 3.0033 0 013 3V9A3.0033 3.0033 0 0117 12zm-2-2h2a1.0011 1.0011 0 001-1V5a1.0011 1.0011 0 00-1-1H15zM9 2H4V12H6V9H9a2.0027 2.0027 0 002-2V4A2.0023 2.0023 0 009 2zM6 7V4H9l.001 3z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default GeneratePdf16;
