import { CarbonIconComponent } from "../types";
export const TextLeading20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="TextLeading20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M14 13H30V15H14zM15 28H30V30H15zM25.85 27H28L23.64 17H21.36L17 27h2.15l.8-2h5.1zm-5.1-4l1.75-4.37L24.25 23zM25.85 12H28L23.64 2H21.36L17 12h2.15l.8-2h5.1zm-5.1-4L22.5 3.63 24.25 8zM6 15.83L8.58 18.41 10 17 6 13 2 17 3.41 18.42 6 15.83zM6 27.17L3.42 24.59 2 26 6 30 10 26 8.59 24.58 6 27.17z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default TextLeading20;
