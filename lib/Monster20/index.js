import {
  template,
  addEventListener,
  insert,
  memo,
  effect,
  setAttribute,
  style,
  delegateEvents,
} from "solid-js/web";

const _tmpl$ = template(
    `<svg data-carbon-icon="Monster20" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" preserveAspectRatio="xMidYMid meet"><path d="M23 22L21 22 21 20 19 20 19 22 17 22 17 20 15 20 15 22 13 22 13 20 11 20 11 22 9 22 9 24 11 24 11 26 13 26 13 24 15 24 15 26 17 26 17 24 19 24 19 26 21 26 21 24 23 24 23 22z"></path><path d="M28,11H27V4a2.0025,2.0025,0,0,0-2-2H7A2.0025,2.0025,0,0,0,5,4v7H4a2.0025,2.0025,0,0,0-2,2v4a2.0025,2.0025,0,0,0,2,2H5v4a7.0078,7.0078,0,0,0,7,7h8a7.0078,7.0078,0,0,0,7-7V19h1a2.0025,2.0025,0,0,0,2-2V13A2.0025,2.0025,0,0,0,28,11ZM25,4V7.3818L22.4473,6.1055a1.0008,1.0008,0,0,0-.8946,0L18,7.8818,14.4473,6.1055a1.0008,1.0008,0,0,0-.8946,0L10,7.8818l-3-1.5V4Zm3,13H25v6a5.0057,5.0057,0,0,1-5,5H12a5.0057,5.0057,0,0,1-5-5V17H4V13H7V8.6182L9.5527,9.8945a1.0008,1.0008,0,0,0,.8946,0L14,8.1182l3.5527,1.7763a1.001,1.001,0,0,0,.8946,0L22,8.1182l3,1.5V13h3Z"></path><path d="M9 14H14V16H9zM18 14H23V16H18z"></path></svg>`,
    8
  ),
  _tmpl$2 = template(`<title></title>`, 2);

const Monster20 = (props) =>
  (() => {
    const _el$ = _tmpl$.cloneNode(true),
      _el$2 = _el$.firstChild,
      _el$3 = _el$2.nextSibling;
    _el$3.nextSibling;

    addEventListener(_el$, "keydown", props.onKeyDown, true);

    addEventListener(_el$, "keyup", props.onKeyUp, true);

    addEventListener(_el$, "mouseleave", props.onMouseLeave);

    addEventListener(_el$, "mouseenter", props.onMouseEnter);

    addEventListener(_el$, "mouseover", props.onMouseOver, true);

    addEventListener(_el$, "click", props.onClick, true);

    insert(
      _el$,
      (() => {
        const _c$ = memo(() => props.children, true);

        return () =>
          _c$() ||
          (props.title &&
            (() => {
              const _el$5 = _tmpl$2.cloneNode(true);

              insert(_el$5, () => props.title);

              return _el$5;
            })());
      })(),
      null
    );

    effect(
      (_p$) => {
        const _v$ = props.fill || "currentColor",
          _v$2 = props.width || "20",
          _v$3 = props.height || "20",
          _v$4 = props["aria-label"],
          _v$5 = props["aria-labelledby"],
          _v$6 = props["aria-label"] || props["aria-labelledby"] || props.title,
          _v$7 =
            props["aria-label"] || props["aria-labelledby"] || props.title
              ? "img"
              : undefined,
          _v$8 = props.tabindex === "0" ? true : props.focusable,
          _v$9 = props.tabindex,
          _v$10 = props.id,
          _v$11 = props.class,
          _v$12 = props.title,
          _v$13 = props.style,
          _v$14 = props.stroke;

        _v$ !== _p$._v$ && setAttribute(_el$, "fill", (_p$._v$ = _v$));
        _v$2 !== _p$._v$2 && setAttribute(_el$, "width", (_p$._v$2 = _v$2));
        _v$3 !== _p$._v$3 && setAttribute(_el$, "height", (_p$._v$3 = _v$3));
        _v$4 !== _p$._v$4 &&
          setAttribute(_el$, "aria-label", (_p$._v$4 = _v$4));
        _v$5 !== _p$._v$5 &&
          setAttribute(_el$, "aria-labelledby", (_p$._v$5 = _v$5));
        _v$6 !== _p$._v$6 &&
          setAttribute(_el$, "aria-hidden", (_p$._v$6 = _v$6));
        _v$7 !== _p$._v$7 && setAttribute(_el$, "role", (_p$._v$7 = _v$7));
        _v$8 !== _p$._v$8 && setAttribute(_el$, "focusable", (_p$._v$8 = _v$8));
        _v$9 !== _p$._v$9 && setAttribute(_el$, "tabindex", (_p$._v$9 = _v$9));
        _v$10 !== _p$._v$10 && setAttribute(_el$, "id", (_p$._v$10 = _v$10));
        _v$11 !== _p$._v$11 && setAttribute(_el$, "class", (_p$._v$11 = _v$11));
        _v$12 !== _p$._v$12 && setAttribute(_el$, "title", (_p$._v$12 = _v$12));
        _p$._v$13 = style(_el$, _v$13, _p$._v$13);
        _v$14 !== _p$._v$14 &&
          setAttribute(_el$, "stroke", (_p$._v$14 = _v$14));
        return _p$;
      },
      {
        _v$: undefined,
        _v$2: undefined,
        _v$3: undefined,
        _v$4: undefined,
        _v$5: undefined,
        _v$6: undefined,
        _v$7: undefined,
        _v$8: undefined,
        _v$9: undefined,
        _v$10: undefined,
        _v$11: undefined,
        _v$12: undefined,
        _v$13: undefined,
        _v$14: undefined,
      }
    );

    return _el$;
  })();

delegateEvents(["click", "mouseover", "keyup", "keydown"]);

export default Monster20;
export { Monster20 };
