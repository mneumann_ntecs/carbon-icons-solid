import { CarbonIconComponent } from "../types";
export const Choices20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="Choices20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M21 4L21 6 24.586 6 16 14.586 7.414 6 11 6 11 4 4 4 4 11 6 11 6 7.414 15 16.414 15 28 17 28 17 16.414 26 7.414 26 11 28 11 28 4 21 4z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default Choices20;
