import { CarbonIconComponent } from "../types";
export const NoImage16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="NoImage16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M30 3.4141L28.5859 2 2 28.5859 3.4141 30l2-2H26a2.0027 2.0027 0 002-2V5.4141zM26 26H7.4141l7.7929-7.793 2.3788 2.3787a2 2 0 002.8284 0L22 19l4 3.9973zm0-5.8318l-2.5858-2.5859a2 2 0 00-2.8284 0L19 19.1682l-2.377-2.3771L26 7.4141zM6 22V19l5-4.9966 1.3733 1.3733 1.4159-1.416-1.375-1.375a2 2 0 00-2.8284 0L6 16.1716V6H22V4H6A2.002 2.002 0 004 6V22z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default NoImage16;
