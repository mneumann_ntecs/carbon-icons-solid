import { CarbonIconComponent } from "../types";
export const DocumentWordProcessorReference20: CarbonIconComponent = (
  props
) => (
  <svg
    data-carbon-icon="DocumentWordProcessorReference20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M28.3 20L27.391 28.611 26 20 24 20 22.609 28.611 21.7 20 20 20 21.36 30 23.64 30 25 21.626 26.36 30 28.64 30 30 20 28.3 20zM4 20L4 22 8.586 22 2 28.586 3.414 30 10 23.414 10 28 12 28 12 20 4 20zM25.707 9.293l-7-7A1 1 0 0018 2H8A2.002 2.002 0 006 4V16H8V4h8v6a2.002 2.002 0 002 2h6v4h2V10A1 1 0 0025.707 9.293zM18 10V4.4141L23.5859 10z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default DocumentWordProcessorReference20;
