import { CarbonIconComponent } from "../types";
export const DistributeVerticalBottom16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="DistributeVerticalBottom16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M2 28H30V30H2zM24 26H8a2.0021 2.0021 0 01-2-2V20a2.0021 2.0021 0 012-2H24a2.0021 2.0021 0 012 2v4A2.0021 2.0021 0 0124 26zm0-6.0012L8 20v4H24zM2 12H30V14H2zM20 10H12a2.0021 2.0021 0 01-2-2V4a2.0021 2.0021 0 012-2h8a2.0021 2.0021 0 012 2V8A2.0021 2.0021 0 0120 10zm0-6.0012L12 4V8h8z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default DistributeVerticalBottom16;
