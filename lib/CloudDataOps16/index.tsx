import { CarbonIconComponent } from "../types";
export const CloudDataOps16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="CloudDataOps16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M28 18H30V28H28zM24 14H26V28H24zM20 22H22V28H20zM22.175 10L23.76 8.7339A9.9522 9.9522 0 0017.89 5.1816 10.0025 10.0025 0 006.17 13.1152 7.5054 7.5054 0 00.0544 21.4087 7.6843 7.6843 0 007.7692 28H16V26H7.6945a5.632 5.632 0 01-5.6023-4.4858 5.5064 5.5064 0 014.4339-6.4307l1.3486-.2441.2139-1.11a8.206 8.206 0 016.7426-6.6426 7.9666 7.9666 0 013.0137.13A7.8037 7.8037 0 0122.175 10z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default CloudDataOps16;
