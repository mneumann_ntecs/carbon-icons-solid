import { CarbonIconComponent } from "../types";
export const LetterQq24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="LetterQq24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M19 13a2 2 0 00-2 2v6a2 2 0 002 2h4v4h2V13zm4 8H19V15h4zM13 9H9a2 2 0 00-2 2V21a2 2 0 002 2h1v2a2 2 0 002 2h2V25H12V23h1a2 2 0 002-2V11A2 2 0 0013 9zM9 21V11h4V21z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default LetterQq24;
