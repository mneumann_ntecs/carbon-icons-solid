import { CarbonIconComponent } from "../types";
export const ForecastHail20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="ForecastHail20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M26,18A10,10,0,1,1,16,8h4v5l6-6L20,1V6H16A12,12,0,1,0,28,18Z"></path>
    <circle cx="14.5" cy="23.5" r="1.5"></circle>
    <circle cx="11.5" cy="19.5" r="1.5"></circle>
    <circle cx="17.5" cy="19.5" r="1.5"></circle>
    <path
      d="M12.964 14.5H18.036V16.499H12.964z"
      transform="rotate(-45 15.5 15.5)"
    ></path>
    <path
      d="M18.964 14.5H24.035999999999998V16.499H18.964z"
      transform="rotate(-45 21.5 15.5)"
    ></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default ForecastHail20;
