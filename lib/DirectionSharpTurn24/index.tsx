import { CarbonIconComponent } from "../types";
export const DirectionSharpTurn24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="DirectionSharpTurn24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M26,18v6.5859L7.707,6.293A1,1,0,0,0,6,7V28H8V9.4141L24.5859,26H18v2H28V18Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default DirectionSharpTurn24;
