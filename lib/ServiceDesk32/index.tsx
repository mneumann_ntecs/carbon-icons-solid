import { CarbonIconComponent } from "../types";
export const ServiceDesk32: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="ServiceDesk32"
    fill={props.fill || "currentColor"}
    width={props.width || "32"}
    height={props.height || "32"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path
      d="M28,24V22A12.01,12.01,0,0,0,17,10.0507V8h3V6H12V8h3v2.0507A12.01,12.01,0,0,0,4,22v2H2v2H30V24ZM16,12a10.0167,10.0167,0,0,1,9.7984,8H6.2015A10.0165,10.0165,0,0,1,16,12ZM6,22H26v2H6Z"
      transform="translate(0 .005)"
    ></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default ServiceDesk32;
