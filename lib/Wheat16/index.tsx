import { CarbonIconComponent } from "../types";
export const Wheat16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="Wheat16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M21,3V5.5857l-4,4V1H15V9.5857l-4-4V3H9V19a7.004,7.004,0,0,0,6,6.92V30h2V25.92A7.004,7.004,0,0,0,23,19V3ZM15,23.8987A5.008,5.008,0,0,1,11,19v-2.586l4,4Zm0-6.313-4-4V8.4138l4,4Zm2-5.1719,4-4v5.1719l-4,4Zm0,11.4849V20.4138l4-4V19A5.008,5.008,0,0,1,17,23.8987Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default Wheat16;
