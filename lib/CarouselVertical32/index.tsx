import { CarbonIconComponent } from "../types";
export const CarouselVertical32: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="CarouselVertical32"
    fill={props.fill || "currentColor"}
    width={props.width || "32"}
    height={props.height || "32"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M26 10V22a2 2 0 01-2 2H8a2 2 0 01-2-2V10A2 2 0 018 8H24A2 2 0 0126 10zM8 22H24V10H8zM24 28v4H22V28H10v4H8V28a2 2 0 012-2H22A2 2 0 0124 28zM24 0V4a2 2 0 01-2 2H10A2 2 0 018 4V0h2V4H22V0z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default CarouselVertical32;
