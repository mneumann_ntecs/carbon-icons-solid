import { CarbonIconComponent } from "../types";
export const QOperation32: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="QOperation32"
    fill={props.fill || "currentColor"}
    width={props.width || "32"}
    height={props.height || "32"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M23 26L21 26 26 16 21 6 23 6 28 16 23 26zM4 6H6V26H4zM16 9H12a2 2 0 00-2 2V21a2 2 0 002 2h4a2 2 0 002-2V11A2 2 0 0016 9zm0 12H12V11h4z"></path>
    <path d="M13 15H15V17H13z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default QOperation32;
