import { CarbonIconComponent } from "../types";
export const Road24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="Road24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M13 13H19V15H13z"></path>
    <path d="M25.4409 8L24.1687 3.45A2.009 2.009 0 0022.2456 2H9.7544A2.0089 2.0089 0 007.8313 3.4507L6.5315 8H4v2H6v7a2.0025 2.0025 0 002 2v3h2V19H22v3h2V19a2.0025 2.0025 0 002-2V10h2V8zM9.7544 4H22.2458l1.4285 5H8.3257zM24 13H22v2h2v2H8V15h2V13H8V11H24zM2 16H4V30H2zM28 16H30V30H28z"></path>
    <path d="M15 21H17V24H15zM15 26H17V30H15z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default Road24;
