import { CarbonIconComponent } from "../types";
export const LetterTt20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="LetterTt20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M8 11L11 11 11 23 13 23 13 11 16 11 16 9 8 9 8 11zM23 15V13H20V11H18v2H16v2h2v6a2 2 0 002 2h3V21H20V15z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default LetterTt20;
