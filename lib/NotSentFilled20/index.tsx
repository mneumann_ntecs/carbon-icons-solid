import { CarbonIconComponent } from "../types";
export const NotSentFilled20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="NotSentFilled20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M30 28.59L3.41 2 2 3.41l8 8L2.66 14.06a1 1 0 000 1.87l8.59 3.43L14.59 16 16 17.41l-3.37 3.37 3.44 8.59A1 1 0 0017 30h0a1 1 0 00.92-.66L20.6 22l8 8zM22.49 16.83l3.45-9.49a1 1 0 00-1.28-1.28L15.17 9.51z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default NotSentFilled20;
