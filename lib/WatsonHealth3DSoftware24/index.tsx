import { CarbonIconComponent } from "../types";
export const WatsonHealth3DSoftware24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="WatsonHealth3DSoftware24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M21.49 13.1151l-9-5a1 1 0 00-1 0l-9 5A1.0078 1.0078 0 002 14v9.9951a1 1 0 00.52.87l9 5A1.0045 1.0045 0 0012 30a1.0559 1.0559 0 00.49-.1349l9-5A.9923.9923 0 0022 24V14A1.0079 1.0079 0 0021.49 13.1151zM11 27.2951l-7-3.89v-7.72l7 3.89zm1-9.45L5.06 14 12 10.1351l6.94 3.86zm8 5.56l-7 3.89v-7.72l7-3.89zM30 6L26 6 26 2 24 2 24 6 20 6 20 8 24 8 24 12 26 12 26 8 30 8 30 6z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default WatsonHealth3DSoftware24;
