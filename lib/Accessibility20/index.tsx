import { CarbonIconComponent } from "../types";
export const Accessibility20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="Accessibility20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M29.55 26.11L26.5 27.63 23.66 21H15a2 2 0 01-2-2V13a2 2 0 014 0v4h7V15H19V13a4 4 0 00-8 0v1a9 9 0 108.77 11H17.71A7 7 0 1111 16v3a4 4 0 004 4h7.34l3.16 7.37 4.95-2.48zM15.5 8A3.5 3.5 0 1119 4.5 3.5 3.5 0 0115.5 8zm0-5A1.5 1.5 0 1017 4.5 1.5 1.5 0 0015.5 3z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default Accessibility20;
