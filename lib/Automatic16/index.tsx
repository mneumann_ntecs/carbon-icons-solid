import { CarbonIconComponent } from "../types";
export const Automatic16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="Automatic16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M26 16H22a2.002 2.002 0 00-2 2V30h2V25h4v5h2V18A2.002 2.002 0 0026 16zm-4 7V18h4v5zM16 27a10.9862 10.9862 0 01-9.2156-5H12V20H4v8H6V24.3149A13.0239 13.0239 0 0016 29zM20 10h5.2155A10.9973 10.9973 0 005 16H3A13.0048 13.0048 0 0126 7.6849V4h2v8H20z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default Automatic16;
