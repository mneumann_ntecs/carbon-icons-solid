import { CarbonIconComponent } from "../types";
export const DistributeHorizontalRight16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="DistributeHorizontalRight16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M28 2H30V30H28zM24 22H20a2.0021 2.0021 0 01-2-2V12a2.0021 2.0021 0 012-2h4a2.0021 2.0021 0 012 2v8A2.0021 2.0021 0 0124 22zM20 12h-.0015L20 20h4V12zM12 2H14V30H12zM8 26H4a2.0021 2.0021 0 01-2-2V8A2.0021 2.0021 0 014 6H8a2.0021 2.0021 0 012 2V24A2.0021 2.0021 0 018 26zM4 8H3.9985L4 24H8V8z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default DistributeHorizontalRight16;
