import { CarbonIconComponent } from "../types";
export const LocationHazardFilled24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="LocationHazardFilled24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path
      fill="none"
      d="M16,22a1.5,1.5,0,1,1,1.5-1.5A1.5,1.5,0,0,1,16,22Zm-1.125-6h2.25V7h-2.25Z"
    ></path>
    <path d="M16,2A11.0134,11.0134,0,0,0,5,13a10.8885,10.8885,0,0,0,2.2163,6.6s.3.3945.3482.4517L16,30l8.439-9.9526c.0444-.0533.3447-.4478.3447-.4478l.0015-.0024A10.8846,10.8846,0,0,0,27,13,11.0134,11.0134,0,0,0,16,2ZM14.875,7h2.25v9h-2.25ZM16,22a1.5,1.5,0,1,1,1.5-1.5A1.5,1.5,0,0,1,16,22Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default LocationHazardFilled24;
