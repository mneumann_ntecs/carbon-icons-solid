import { CarbonIconComponent } from "../types";
export const Redo20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="Redo20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 20 20"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M5,9.3c-1.5,0-2.8,1.3-2.8,2.7c0,1.5,1.3,2.7,2.8,2.7h4V16H5c-2.3,0-4-1.7-4-4c0-2.2,1.8-4,4-4h11.6	l-2.5-2.5L15,4.6l4,4l-4,4l-0.9-0.9l2.5-2.5H5z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default Redo20;
