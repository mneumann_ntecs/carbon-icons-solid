import { CarbonIconComponent } from "../types";
export const SkipForwardOutline24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="SkipForwardOutline24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M21 10H23V22H21zM8.4927 21.8618A1 1 0 018 21V11a1 1 0 011.4858-.8742l9 5a1 1 0 010 1.7485l-9 5a1.0009 1.0009 0 01-.9931-.0125zM10 12.7v6.6011L15.9409 16z"></path>
    <path d="M16,30A14,14,0,1,1,30,16,14.0158,14.0158,0,0,1,16,30ZM16,4A12,12,0,1,0,28,16,12.0137,12.0137,0,0,0,16,4Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default SkipForwardOutline24;
