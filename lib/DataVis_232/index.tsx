import { CarbonIconComponent } from "../types";
export const DataVis_232: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="DataVis_232"
    fill={props.fill || "currentColor"}
    width={props.width || "32"}
    height={props.height || "32"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M28,2H17a2.002,2.002,0,0,0-2,2v6H4a2.002,2.002,0,0,0-2,2V28a2.0023,2.0023,0,0,0,2,2H15a2.0023,2.0023,0,0,0,2-2V22H28a2.0027,2.0027,0,0,0,2-2V4A2.0023,2.0023,0,0,0,28,2Zm0,2,0,4H17V4ZM15.0009,22H4V18H15ZM17,10H28.0007l.0005,4H17Zm-2,2v4H4V12ZM4,28V24H15.0011l0,4Zm13-8V16H28.0015l0,4Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default DataVis_232;
