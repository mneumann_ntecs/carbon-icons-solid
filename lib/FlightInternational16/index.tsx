import { CarbonIconComponent } from "../types";
export const FlightInternational16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="FlightInternational16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M16,28a5.3257,5.3257,0,0,1-.6714-.044A21.7855,21.7855,0,0,1,12.0332,17H29.95c.0236-.3315.05-.6626.05-1A14,14,0,1,0,16,30ZM27.9492,15H21.9631A24.3247,24.3247,0,0,0,19.21,4.4492,12.0123,12.0123,0,0,1,27.9492,15ZM16.6714,4.0439A21.7855,21.7855,0,0,1,19.9668,15H12.0332A21.7855,21.7855,0,0,1,15.3286,4.0439,5.159,5.159,0,0,1,16.6714,4.0439ZM12.79,4.4492A24.3275,24.3275,0,0,0,10.0369,15H4.0508A12.0126,12.0126,0,0,1,12.79,4.4492ZM4.0508,17h5.9861A24.3275,24.3275,0,0,0,12.79,27.5508,12.0126,12.0126,0,0,1,4.0508,17Z"></path>
    <path
      fill-rule="evenodd"
      d="M25,25l5,2V25l-5-2.5V20a1,1,0,0,0-2,0v2.5L18,25v2l5-2v3.5L21,30v1l3-1,3,1V30l-2-1.5Z"
    ></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default FlightInternational16;
