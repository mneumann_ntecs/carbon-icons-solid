import { CarbonIconComponent } from "../types";
export const LetterVv24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="LetterVv24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M25 13L23.25 13 21 22.03 18.79 13 17 13 19.5 23 22.5 23 25 13zM13 9L11 22 9 9 7 9 9.52 23 12.48 23 15 9 13 9z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default LetterVv24;
