import { CarbonIconComponent } from "../types";
export const Minimize16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="Minimize16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 16 16"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M2 9L2 10 5.3 10 1 14.3 1.7 15 6 10.7 6 14 7 14 7 9zM14 7L14 6 10.7 6 15 1.7 14.3 1 10 5.3 10 2 9 2 9 7z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default Minimize16;
