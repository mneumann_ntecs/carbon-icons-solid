import { CarbonIconComponent } from "../types";
export const MedicationReminder24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="MedicationReminder24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M20,2H4A2,2,0,0,0,2,4V8a2,2,0,0,0,2,2V28a2,2,0,0,0,2,2H18a2,2,0,0,0,2-2V10a2,2,0,0,0,2-2V4A2,2,0,0,0,20,2ZM6,14H9V24H6ZM18,28H6V26h5V12H6V10H18ZM4,8V4H20V8Z"></path>
    <circle cx="26" cy="16" r="4"></circle>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default MedicationReminder24;
