import { CarbonIconComponent } from "../types";
export const CurrencyEuro24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="CurrencyEuro24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M17,26c-3.6162,0-6.3328-2.2974-7.4456-6H19V18H9.1321A15.2782,15.2782,0,0,1,9,16c0-.33.01-.6641.0259-1H19V13H9.2371C9.8447,9.3525,11.81,6,17,6c3.8533,0,5.5315,1.6465,7.1282,4.4893l1.7436-.9786C24.2649,6.6484,22.0779,4,17,4,10.6448,4,7,8.374,7,16c0,7.0654,4.1121,12,10,12,5.0779,0,7.2649-2.6484,8.8718-5.5107l-1.7436-.9786C22.5315,24.3535,20.8533,26,17,26Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default CurrencyEuro24;
