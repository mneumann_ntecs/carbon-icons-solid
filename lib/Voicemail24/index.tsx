import { CarbonIconComponent } from "../types";
export const Voicemail24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="Voicemail24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M24,10a6,6,0,0,0-4.46,10H12.46A6,6,0,1,0,8,22H24a6,6,0,0,0,0-12ZM4,16a4,4,0,1,1,4,4A4,4,0,0,1,4,16Zm20,4a4,4,0,1,1,4-4A4,4,0,0,1,24,20Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default Voicemail24;
