import { CarbonIconComponent } from "../types";
export const MagicWand20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="MagicWand20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M29.4141,24,12,6.5859a2.0476,2.0476,0,0,0-2.8281,0l-2.586,2.586a2.0021,2.0021,0,0,0,0,2.8281L23.999,29.4141a2.0024,2.0024,0,0,0,2.8281,0l2.587-2.5865a1.9993,1.9993,0,0,0,0-2.8281ZM8,10.5859,10.5859,8l5,5-2.5866,2.5869-5-5ZM25.4131,28l-11-10.999L17,14.4141l11,11Z"></path>
    <path
      d="M2.586 14.586H5.414V17.414H2.586z"
      transform="rotate(-45 4 16)"
    ></path>
    <path
      d="M14.586 2.586H17.414V5.414H14.586z"
      transform="rotate(-45 16 4)"
    ></path>
    <path
      d="M2.586 2.586H5.414V5.414H2.586z"
      transform="rotate(-45 4 4)"
    ></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default MagicWand20;
