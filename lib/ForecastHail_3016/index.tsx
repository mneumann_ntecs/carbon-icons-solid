import { CarbonIconComponent } from "../types";
export const ForecastHail_3016: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="ForecastHail_3016"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path
      d="M8.964 13.5H14.036000000000001V15.499H8.964z"
      transform="rotate(-45 11.5 14.5)"
    ></path>
    <path
      d="M15.379 14.5H17.622V16.499H15.379z"
      transform="rotate(-44.995 16.5 15.5)"
    ></path>
    <circle cx="10.5" cy="22.5" r="1.5"></circle>
    <circle cx="7.5" cy="18.5" r="1.5"></circle>
    <circle cx="13.5" cy="18.5" r="1.5"></circle>
    <path d="M14,28A10,10,0,0,1,14,8h4v5l6-6L18,1V6H14a12,12,0,0,0,0,24Z"></path>
    <path d="M20 20H16v2h4v2H17v2h3v2H16v2h4a2.0027 2.0027 0 002-2V22A2.0023 2.0023 0 0020 20zM28 30H26a2.0021 2.0021 0 01-2-2V22a2.0021 2.0021 0 012-2h2a2.0021 2.0021 0 012 2v6A2.0021 2.0021 0 0128 30zm-2-8v6h2V22z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default ForecastHail_3016;
