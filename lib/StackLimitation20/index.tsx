import { CarbonIconComponent } from "../types";
export const StackLimitation20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="StackLimitation20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M8 22H16V30H8zM24 12H20v2h4v2H21v2h3v2H20v2h4a2.0027 2.0027 0 002-2V14A2.0023 2.0023 0 0024 12zM16 20H8V12h8zm-6-2h4V14H10zM16 3.41L14.59 2 12 4.59 9.41 2 8 3.41 10.59 6 8 8.59 9.41 10 12 7.41 14.59 10 16 8.59 13.41 6 16 3.41z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default StackLimitation20;
