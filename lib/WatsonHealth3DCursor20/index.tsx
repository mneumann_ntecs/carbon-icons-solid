import { CarbonIconComponent } from "../types";
export const WatsonHealth3DCursor20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="WatsonHealth3DCursor20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M13 4L4 4 4 13.01 6 13.01 6 6 13 6 13 4zM29.49 13.12l-9-5a1 1 0 00-1 0l-9 5A1 1 0 0010 14V24a1 1 0 00.52.87l9 5A1 1 0 0020 30a1.05 1.05 0 00.49-.13l9-5A1 1 0 0030 24V14A1 1 0 0029.49 13.12zM19 27.3l-7-3.89V15.69l7 3.89zm1-9.45L13.06 14 20 10.14 26.94 14zm8 5.56L21 27.3V19.58l7-3.89z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default WatsonHealth3DCursor20;
