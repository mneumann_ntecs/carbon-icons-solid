import { CarbonIconComponent } from "../types";
export const Transgender16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="Transgender16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M22,2V4h4.5859l-6.4016,6.4014a6.9474,6.9474,0,0,0-8.3686,0L10.4143,9,13,6.4141,11.5857,5,9,7.5859,5.4141,4H10V2H2v8H4V5.4141L7.5859,9,5,11.5854,6.4143,13,9,10.4141l1.4014,1.4013A6.9785,6.9785,0,0,0,15,22.92V25H11v2h4v3h2V27h4V25H17V22.92a6.9785,6.9785,0,0,0,4.5984-11.1045L28,5.4141V10h2V2ZM16,21a5,5,0,1,1,5-5A5.0059,5.0059,0,0,1,16,21Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default Transgender16;
