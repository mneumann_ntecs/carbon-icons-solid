import { CarbonIconComponent } from "../types";
export const CalculatorCheck24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="CalculatorCheck24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M24 26.59L21.41 24 20 25.41 24 29.41 31 22.41 29.59 21 24 26.59zM15 23H17V25H15zM9 23H11V25H9zM21 18H23V20H21zM15 18H17V20H15zM9 18H11V20H9zM21 13H23V15H21zM15 13H17V15H15zM9 13H11V15H9zM9 7H23V10H9z"></path>
    <path d="M17,30H6.0046A2.007,2.007,0,0,1,4,27.9951V3.9961A1.9984,1.9984,0,0,1,5.9961,2H26.0037A1.9985,1.9985,0,0,1,28,3.9961V18H26V4H6V28H17Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default CalculatorCheck24;
