import { CarbonIconComponent } from "../types";
export const WatsonHealth3DICa24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="WatsonHealth3DICa24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M15 14H10a2 2 0 01-2-2V4a2 2 0 01.59-1.42A3.57 3.57 0 0110 2h5V4H10v8h5zM4 6H6V14H4zM4 2H6V4H4zM27.45 19.11l-6-3a1 1 0 00-.9 0l-6 3A1 1 0 0014 20v7a1 1 0 00.55.89l6 3a1 1 0 00.9 0l6-3A1 1 0 0028 27V20A1 1 0 0027.45 19.11zm-6.45-1L24.76 20 21 21.88 17.24 20zm-5 3.5l4 2v4.76l-4-2zm6 6.76V23.62l4-2v4.76zM23 2H19a2 2 0 00-2 2V14h2V10h4v4h2V4A2 2 0 0023 2zM19 8V4h4V8z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default WatsonHealth3DICa24;
