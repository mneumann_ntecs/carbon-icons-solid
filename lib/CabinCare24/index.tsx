import { CarbonIconComponent } from "../types";
export const CabinCare24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="CabinCare24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M27.3022 2a2.6617 2.6617 0 00-1.9079.8059l-.3931.4053-.397-.4053a2.6613 2.6613 0 00-3.8158 0 2.7992 2.7992 0 000 3.8963L25.0012 11 29.21 6.7022a2.7992 2.7992 0 000-3.8963A2.6613 2.6613 0 0027.3022 2zM23.8218 18H15.083L11.8643 5.9653a4 4 0 00-7.7276 2.07L8.5454 24.5168A2 2 0 0010.4775 26H19v2H4v2H19a2 2 0 002-2V26h3a4.0046 4.0046 0 003.98-4.4A4.1214 4.1214 0 0023.8218 18zM24 24H10.4776L6.0686 7.5181A2 2 0 119.9324 6.4829L13.5466 20H24a2 2 0 010 4z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default CabinCare24;
