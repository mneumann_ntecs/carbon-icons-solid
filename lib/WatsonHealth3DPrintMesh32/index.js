import {
  template,
  addEventListener,
  insert,
  memo,
  effect,
  setAttribute,
  style,
  delegateEvents,
} from "solid-js/web";

const _tmpl$ = template(
    `<svg data-carbon-icon="WatsonHealth3DPrintMesh32" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" preserveAspectRatio="xMidYMid meet"><path d="M29.9761,15.7832l-2-9a.9992.9992,0,0,0-.4214-.6152l-6-4A1.0008,1.0008,0,0,0,21,2H11a1.0008,1.0008,0,0,0-.5547.168l-6,4a.9992.9992,0,0,0-.4214.6152l-2,9a1.0019,1.0019,0,0,0,.0181.5039l3,10a1,1,0,0,0,.6709.6709l10,3,.0051.0005a.9789.9789,0,0,0,.564,0l.0051-.0005,10-3a1,1,0,0,0,.6709-.6709l3-10A1.0019,1.0019,0,0,0,29.9761,15.7832Zm-19.05.833L7.0168,8.7974l7.2815,2.6479ZM16,12.4971,19.5889,18H12.4111ZM19.3818,20,16,26.7637,12.6182,20Zm-1.68-8.5547,7.2815-2.6479-3.91,7.8188ZM18.19,9.14l3.0961-4.747,3.5152,2.3432ZM16,8.8364,12.8459,4h6.3082Zm-2.19.3032L7.1992,6.7358l3.5152-2.3432Zm-4.8439,8.03-4.802-1.8007L5.3652,9.9668ZM14.07,27.377,7.5679,25.4263l3.1284-4.7969Zm7.2334-6.7476,3.1284,4.7969L17.93,27.377ZM26.6348,9.9668l1.2006,5.4019-4.802,1.8007ZM4.5374,17.6445l4.5944,1.7227L6.3391,23.65ZM25.6609,23.65l-2.7927-4.2827,4.5944-1.7227Z"></path></svg>`,
    4
  ),
  _tmpl$2 = template(`<title></title>`, 2);

const WatsonHealth3DPrintMesh32 = (props) =>
  (() => {
    const _el$ = _tmpl$.cloneNode(true);
    _el$.firstChild;

    addEventListener(_el$, "keydown", props.onKeyDown, true);

    addEventListener(_el$, "keyup", props.onKeyUp, true);

    addEventListener(_el$, "mouseleave", props.onMouseLeave);

    addEventListener(_el$, "mouseenter", props.onMouseEnter);

    addEventListener(_el$, "mouseover", props.onMouseOver, true);

    addEventListener(_el$, "click", props.onClick, true);

    insert(
      _el$,
      (() => {
        const _c$ = memo(() => props.children, true);

        return () =>
          _c$() ||
          (props.title &&
            (() => {
              const _el$3 = _tmpl$2.cloneNode(true);

              insert(_el$3, () => props.title);

              return _el$3;
            })());
      })(),
      null
    );

    effect(
      (_p$) => {
        const _v$ = props.fill || "currentColor",
          _v$2 = props.width || "32",
          _v$3 = props.height || "32",
          _v$4 = props["aria-label"],
          _v$5 = props["aria-labelledby"],
          _v$6 = props["aria-label"] || props["aria-labelledby"] || props.title,
          _v$7 =
            props["aria-label"] || props["aria-labelledby"] || props.title
              ? "img"
              : undefined,
          _v$8 = props.tabindex === "0" ? true : props.focusable,
          _v$9 = props.tabindex,
          _v$10 = props.id,
          _v$11 = props.class,
          _v$12 = props.title,
          _v$13 = props.style,
          _v$14 = props.stroke;

        _v$ !== _p$._v$ && setAttribute(_el$, "fill", (_p$._v$ = _v$));
        _v$2 !== _p$._v$2 && setAttribute(_el$, "width", (_p$._v$2 = _v$2));
        _v$3 !== _p$._v$3 && setAttribute(_el$, "height", (_p$._v$3 = _v$3));
        _v$4 !== _p$._v$4 &&
          setAttribute(_el$, "aria-label", (_p$._v$4 = _v$4));
        _v$5 !== _p$._v$5 &&
          setAttribute(_el$, "aria-labelledby", (_p$._v$5 = _v$5));
        _v$6 !== _p$._v$6 &&
          setAttribute(_el$, "aria-hidden", (_p$._v$6 = _v$6));
        _v$7 !== _p$._v$7 && setAttribute(_el$, "role", (_p$._v$7 = _v$7));
        _v$8 !== _p$._v$8 && setAttribute(_el$, "focusable", (_p$._v$8 = _v$8));
        _v$9 !== _p$._v$9 && setAttribute(_el$, "tabindex", (_p$._v$9 = _v$9));
        _v$10 !== _p$._v$10 && setAttribute(_el$, "id", (_p$._v$10 = _v$10));
        _v$11 !== _p$._v$11 && setAttribute(_el$, "class", (_p$._v$11 = _v$11));
        _v$12 !== _p$._v$12 && setAttribute(_el$, "title", (_p$._v$12 = _v$12));
        _p$._v$13 = style(_el$, _v$13, _p$._v$13);
        _v$14 !== _p$._v$14 &&
          setAttribute(_el$, "stroke", (_p$._v$14 = _v$14));
        return _p$;
      },
      {
        _v$: undefined,
        _v$2: undefined,
        _v$3: undefined,
        _v$4: undefined,
        _v$5: undefined,
        _v$6: undefined,
        _v$7: undefined,
        _v$8: undefined,
        _v$9: undefined,
        _v$10: undefined,
        _v$11: undefined,
        _v$12: undefined,
        _v$13: undefined,
        _v$14: undefined,
      }
    );

    return _el$;
  })();

delegateEvents(["click", "mouseover", "keyup", "keydown"]);

export default WatsonHealth3DPrintMesh32;
export { WatsonHealth3DPrintMesh32 };
