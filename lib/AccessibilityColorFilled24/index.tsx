import { CarbonIconComponent } from "../types";
export const AccessibilityColorFilled24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="AccessibilityColorFilled24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M16,10a8.5975,8.5975,0,0,0-7.9648,6A8.5975,8.5975,0,0,0,16,22a8.5975,8.5975,0,0,0,7.9648-6A8.5975,8.5975,0,0,0,16,10Zm0,10a4,4,0,1,1,4-4A4.0045,4.0045,0,0,1,16,20Z"></path>
    <path d="M16,2A14,14,0,1,0,30,16,14,14,0,0,0,16,2Zm0,22a10.6543,10.6543,0,0,1-9.97-7.7578L5.9692,16l.0606-.2422A10.6543,10.6543,0,0,1,16,8a10.6543,10.6543,0,0,1,9.97,7.7578L26.0308,16l-.0606.2422A10.6543,10.6543,0,0,1,16,24Z"></path>
    <circle cx="16" cy="16" r="2"></circle>
    <path
      fill="none"
      d="M16,20a4,4,0,1,1,4-4A4.0045,4.0045,0,0,1,16,20Zm0-6a2,2,0,1,0,2,2A2.0021,2.0021,0,0,0,16,14Z"
    ></path>
    <path
      fill="none"
      d="M16,24a10.6547,10.6547,0,0,1-9.97-7.7576L5.9692,16l.0606-.2424A10.6547,10.6547,0,0,1,16,8a10.6547,10.6547,0,0,1,9.97,7.7576L26.0308,16l-.0606.2424A10.6547,10.6547,0,0,1,16,24ZM8.0352,16A8.5975,8.5975,0,0,0,16,22a8.5975,8.5975,0,0,0,7.9648-6A8.5975,8.5975,0,0,0,16,10,8.5975,8.5975,0,0,0,8.0352,16Z"
    ></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default AccessibilityColorFilled24;
