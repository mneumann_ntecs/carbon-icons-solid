import { CarbonIconComponent } from "../types";
export const ChevronUpGlyph: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="ChevronUpGlyph"
    fill={props.fill || "currentColor"}
    width={props.width || "10"}
    height={props.height || "6"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 10 6"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M5 0L10 5 9.3 5.7 5 1.4 0.7 5.7 0 5z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default ChevronUpGlyph;
