import {
  template,
  addEventListener,
  insert,
  memo,
  effect,
  setAttribute,
  style,
  delegateEvents,
} from "solid-js/web";

const _tmpl$ = template(
    `<svg data-carbon-icon="BorderLeft20" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" preserveAspectRatio="xMidYMid meet"><path d="M-9 15H17V17H-9z" transform="rotate(-90 4 16)"></path><path d="M7 27H9V29H7z" transform="rotate(-90 8 28)"></path><path d="M11 27H13V29H11z" transform="rotate(-90 12 28)"></path><path d="M15 27H17V29H15z" transform="rotate(-90 16 28)"></path><path d="M19 27H21V29H19z" transform="rotate(-90 20 28)"></path><path d="M23 27H25V29H23z" transform="rotate(-90 24 28)"></path><path d="M27 27H29V29H27z" transform="rotate(-90 28 28)"></path><path d="M27 23H29V25H27z" transform="rotate(-90 28 24)"></path><path d="M27 19H29V21H27z" transform="rotate(-90 28 20)"></path><path d="M27 15H29V17H27z" transform="rotate(-90 28 16)"></path><path d="M27 7H29V9H27z" transform="rotate(-90 28 8)"></path><path d="M27 11H29V13H27z" transform="rotate(-90 28 12)"></path><path d="M7 3H9V5H7z" transform="rotate(-90 8 4)"></path><path d="M11 3H13V5H11z" transform="rotate(-90 12 4)"></path><path d="M15 3H17V5H15z" transform="rotate(-90 16 4)"></path><path d="M19 3H21V5H19z" transform="rotate(-90 20 4)"></path><path d="M23 3H25V5H23z" transform="rotate(-90 24 4)"></path><path d="M27 3H29V5H27z" transform="rotate(-90 28 4)"></path><path d="M8 10H18V12H8zM8 15H14V17H8z"></path></svg>`,
    40
  ),
  _tmpl$2 = template(`<title></title>`, 2);

const BorderLeft20 = (props) =>
  (() => {
    const _el$ = _tmpl$.cloneNode(true),
      _el$2 = _el$.firstChild,
      _el$3 = _el$2.nextSibling,
      _el$4 = _el$3.nextSibling,
      _el$5 = _el$4.nextSibling,
      _el$6 = _el$5.nextSibling,
      _el$7 = _el$6.nextSibling,
      _el$8 = _el$7.nextSibling,
      _el$9 = _el$8.nextSibling,
      _el$10 = _el$9.nextSibling,
      _el$11 = _el$10.nextSibling,
      _el$12 = _el$11.nextSibling,
      _el$13 = _el$12.nextSibling,
      _el$14 = _el$13.nextSibling,
      _el$15 = _el$14.nextSibling,
      _el$16 = _el$15.nextSibling,
      _el$17 = _el$16.nextSibling,
      _el$18 = _el$17.nextSibling,
      _el$19 = _el$18.nextSibling;
    _el$19.nextSibling;

    addEventListener(_el$, "keydown", props.onKeyDown, true);

    addEventListener(_el$, "keyup", props.onKeyUp, true);

    addEventListener(_el$, "mouseleave", props.onMouseLeave);

    addEventListener(_el$, "mouseenter", props.onMouseEnter);

    addEventListener(_el$, "mouseover", props.onMouseOver, true);

    addEventListener(_el$, "click", props.onClick, true);

    insert(
      _el$,
      (() => {
        const _c$ = memo(() => props.children, true);

        return () =>
          _c$() ||
          (props.title &&
            (() => {
              const _el$21 = _tmpl$2.cloneNode(true);

              insert(_el$21, () => props.title);

              return _el$21;
            })());
      })(),
      null
    );

    effect(
      (_p$) => {
        const _v$ = props.fill || "currentColor",
          _v$2 = props.width || "20",
          _v$3 = props.height || "20",
          _v$4 = props["aria-label"],
          _v$5 = props["aria-labelledby"],
          _v$6 = props["aria-label"] || props["aria-labelledby"] || props.title,
          _v$7 =
            props["aria-label"] || props["aria-labelledby"] || props.title
              ? "img"
              : undefined,
          _v$8 = props.tabindex === "0" ? true : props.focusable,
          _v$9 = props.tabindex,
          _v$10 = props.id,
          _v$11 = props.class,
          _v$12 = props.title,
          _v$13 = props.style,
          _v$14 = props.stroke;

        _v$ !== _p$._v$ && setAttribute(_el$, "fill", (_p$._v$ = _v$));
        _v$2 !== _p$._v$2 && setAttribute(_el$, "width", (_p$._v$2 = _v$2));
        _v$3 !== _p$._v$3 && setAttribute(_el$, "height", (_p$._v$3 = _v$3));
        _v$4 !== _p$._v$4 &&
          setAttribute(_el$, "aria-label", (_p$._v$4 = _v$4));
        _v$5 !== _p$._v$5 &&
          setAttribute(_el$, "aria-labelledby", (_p$._v$5 = _v$5));
        _v$6 !== _p$._v$6 &&
          setAttribute(_el$, "aria-hidden", (_p$._v$6 = _v$6));
        _v$7 !== _p$._v$7 && setAttribute(_el$, "role", (_p$._v$7 = _v$7));
        _v$8 !== _p$._v$8 && setAttribute(_el$, "focusable", (_p$._v$8 = _v$8));
        _v$9 !== _p$._v$9 && setAttribute(_el$, "tabindex", (_p$._v$9 = _v$9));
        _v$10 !== _p$._v$10 && setAttribute(_el$, "id", (_p$._v$10 = _v$10));
        _v$11 !== _p$._v$11 && setAttribute(_el$, "class", (_p$._v$11 = _v$11));
        _v$12 !== _p$._v$12 && setAttribute(_el$, "title", (_p$._v$12 = _v$12));
        _p$._v$13 = style(_el$, _v$13, _p$._v$13);
        _v$14 !== _p$._v$14 &&
          setAttribute(_el$, "stroke", (_p$._v$14 = _v$14));
        return _p$;
      },
      {
        _v$: undefined,
        _v$2: undefined,
        _v$3: undefined,
        _v$4: undefined,
        _v$5: undefined,
        _v$6: undefined,
        _v$7: undefined,
        _v$8: undefined,
        _v$9: undefined,
        _v$10: undefined,
        _v$11: undefined,
        _v$12: undefined,
        _v$13: undefined,
        _v$14: undefined,
      }
    );

    return _el$;
  })();

delegateEvents(["click", "mouseover", "keyup", "keydown"]);

export default BorderLeft20;
export { BorderLeft20 };
