import { CarbonIconComponent } from "../types";
export const ClassifierLanguage20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="ClassifierLanguage20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M30 10V2H22v8h3v4H23v2h7V14H27V10zM24 4h4V8H24zM20 23.41L18.59 22 16 24.59 13.41 22 12 23.41 14.59 26 12 28.59 13.41 30 16 27.41 18.59 30 20 28.59 17.41 26 20 23.41zM20 14L12 14 12 16 15 16 15 21 17 21 17 16 20 16 20 14zM7 9.86a4 4 0 10-2 0V14H2v2H9V14H7zM4 6A2 2 0 116 8 2 2 0 014 6z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default ClassifierLanguage20;
