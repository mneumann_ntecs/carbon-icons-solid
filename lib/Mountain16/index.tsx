import { CarbonIconComponent } from "../types";
export const Mountain16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="Mountain16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M27.6343,26,17.7888,5.1055a2,2,0,0,0-3.5879.021L4.3657,26H2v2H30V26ZM15.99,5.979,20.9473,16.5,19,17.7979l-3-2-3,2-1.9551-1.3033ZM10.1846,18.3247,13,20.2021l3-2,3,2,2.8091-1.873L25.4233,26H6.5752Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default Mountain16;
