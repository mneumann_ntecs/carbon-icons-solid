import { CarbonIconComponent } from "../types";
export const NonCertified20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="NonCertified20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M6 8H16V10H6zM6 12H14V14H6zM6 16H10V18H6z"></path>
    <path d="M28,26H7.4141L30,3.4141,28.5859,2l-2,2H4A2.0023,2.0023,0,0,0,2,6V22H4V6H24.5859L2,28.5859,3.4141,30l2-2H28a2.0023,2.0023,0,0,0,2-2V10H28Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default NonCertified20;
