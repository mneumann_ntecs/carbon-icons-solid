import { CarbonIconComponent } from "../types";
export const CurrencyShekel20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="CurrencyShekel20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M9,27H7V5h8a5.0057,5.0057,0,0,1,5,5V22H18V10a3.0033,3.0033,0,0,0-3-3H9Z"></path>
    <path d="M20,27H12V10h2V25h6a3.0033,3.0033,0,0,0,3-3V5h2V22A5.0057,5.0057,0,0,1,20,27Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default CurrencyShekel20;
