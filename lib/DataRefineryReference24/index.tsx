import { CarbonIconComponent } from "../types";
export const DataRefineryReference24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="DataRefineryReference24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M4 20L4 22 8.586 22 2 28.586 3.414 30 10 23.414 10 28 12 28 12 20 4 20zM28 12H22a2.0023 2.0023 0 01-2-2V4a2.0023 2.0023 0 012-2h6a2.0023 2.0023 0 012 2v6A2.0023 2.0023 0 0128 12zM22 4v6h6V4z"></path>
    <path d="M24,26H16V24h8V17H8a2.0021,2.0021,0,0,1-2-2V8A2.0021,2.0021,0,0,1,8,6H18V8H8v7H24a2.0021,2.0021,0,0,1,2,2v7A2.0021,2.0021,0,0,1,24,26Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default DataRefineryReference24;
