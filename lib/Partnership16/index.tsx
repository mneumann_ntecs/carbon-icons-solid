import { CarbonIconComponent } from "../types";
export const Partnership16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="Partnership16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M8 9a4 4 0 114-4A4 4 0 018 9zM8 3a2 2 0 102 2A2 2 0 008 3zM24 9a4 4 0 114-4A4 4 0 0124 9zm0-6a2 2 0 102 2A2 2 0 0024 3zM26 30H22a2 2 0 01-2-2V21h2v7h4V19h2V13a1 1 0 00-1-1H20.58L16 20l-4.58-8H5a1 1 0 00-1 1v6H6v9h4V21h2v7a2 2 0 01-2 2H6a2 2 0 01-2-2V21a2 2 0 01-2-2V13a3 3 0 013-3h7.58L16 16l3.42-6H27a3 3 0 013 3v6a2 2 0 01-2 2v7A2 2 0 0126 30z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default Partnership16;
