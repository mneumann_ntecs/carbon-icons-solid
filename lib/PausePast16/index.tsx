import { CarbonIconComponent } from "../types";
export const PausePast16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="PausePast16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M14 11L12 11 12 22 14 22 14 11 14 11zM20 11L18 11 18 22 20 22 20 11 20 11z"></path>
    <path d="M16,2A13.9158,13.9158,0,0,0,6,6.2343V2H4v8h8V8H7.0783A11.9818,11.9818,0,1,1,4,16H2A14,14,0,1,0,16,2Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default PausePast16;
