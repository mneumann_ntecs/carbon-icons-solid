import { CarbonIconComponent } from "../types";
export const Soccer20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="Soccer20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <circle cx="17" cy="28" r="2"></circle>
    <path
      d="M7.55 17.5H15.45V19.5H7.55z"
      transform="rotate(-45 11.5 18.5)"
    ></path>
    <path d="M28 16.584L19.4143 8H6v2H18.5857l3 3L6 28.5859 7.4141 30 23 14.415 26.584 18 23 21.5859 24.4141 23 28 19.416A2.0044 2.0044 0 0028 16.584zM24.5 9A3.5 3.5 0 1128 5.5 3.504 3.504 0 0124.5 9zm0-5A1.5 1.5 0 1026 5.5 1.5017 1.5017 0 0024.5 4z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default Soccer20;
