import { CarbonIconComponent } from "../types";
export const Scooter24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="Scooter24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M26 28a4 4 0 114-4A4.0045 4.0045 0 0126 28zm0-6a2 2 0 102 2A2.0023 2.0023 0 0026 22zM24 8H18v2h6a1 1 0 010 2H21a1 1 0 00-.98 1.1963l.9241 4.6211L18.4338 22h-2.69l-2.5723-8.5752A1.988 1.988 0 0011.2559 12H6v2h5.2561l.6 2H7a5.0057 5.0057 0 00-5 5v2a1 1 0 001 1H4a4 4 0 008 0h7a1 1 0 00.8574-.4854l3-5a1 1 0 00.1231-.7109L22.22 14H24a3 3 0 000-6zM8 26a2.0025 2.0025 0 01-2-2h4A2.0025 2.0025 0 018 26zM4 22V21a3.0033 3.0033 0 013-3h5.4561l1.2 4z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default Scooter24;
