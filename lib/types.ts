import { Component } from "solid-js";
export declare interface CarbonIconProps {
  id?: string;
  class?: string;
  tabindex?: string;
  focusable?: boolean;
  title?: string;
  "aria-label"?: string;
  "aria-labelledby"?: string;
  style?: string;
  fill?: string;
  stroke?: string;
  width?: string;
  height?: string;
  onClick?: MouseEvent;
  onMouseOver?: MouseEvent;
  onMouseEnter?: MouseEvent;
  onMouseLeave?: MouseEvent;
  onKeyUp?: KeyboardEvent;
  onKeyDown?: KeyboardEvent;
}
export declare type CarbonIconComponent = Component<CarbonIconProps>;
