import { CarbonIconComponent } from "../types";
export const LetterDd32: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="LetterDd32"
    fill={props.fill || "currentColor"}
    width={props.width || "32"}
    height={props.height || "32"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M23 9v4H19a2 2 0 00-2 2v6a2 2 0 002 2h6V9zm-4 6h4v6H19zM11 23H7V9h4a4 4 0 014 4v6A4 4 0 0111 23zM9 21h2a2 2 0 002-2V13a2 2 0 00-2-2H9z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default LetterDd32;
