import { CarbonIconComponent } from "../types";
export const WatsonHealthZoomPan32: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="WatsonHealthZoomPan32"
    fill={props.fill || "currentColor"}
    width={props.width || "32"}
    height={props.height || "32"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M27.01 12L25.6 13.41 28.18 16 25.59 18.59 27.01 20 31.01 16 27.01 12zM6.41 13.42L5 12 1 16 5 20 6.42 18.59 3.83 16 6.41 13.42zM16 28.17L13.41 25.58 12 27 16 31 20 27 18.59 25.59 16 28.17zM16 3.83L18.58 6.41 20 5 16 1 12 5 13.41 6.42 16 3.83zM22 16a6 6 0 10-2.53 4.89l3.82 3.82 1.42-1.42-3.82-3.82A6 6 0 0022 16zm-6 4a4 4 0 114-4A4 4 0 0116 20z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default WatsonHealthZoomPan32;
