import { CarbonIconComponent } from "../types";
export const SoilTemperatureGlobal24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="SoilTemperatureGlobal24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M25 30a4.9863 4.9863 0 01-3-8.98V15a3 3 0 016 0v6.02A4.9863 4.9863 0 0125 30zm0-16a1.0011 1.0011 0 00-1 1v7.13l-.4971.2893A2.9676 2.9676 0 0022 25a3 3 0 006 0 2.9676 2.9676 0 00-1.5029-2.5811L26 22.13V15A1.0011 1.0011 0 0025 14zM16 24H14a11.89 11.89 0 013.7642-8.7271l1.373 1.4542A9.909 9.909 0 0016 24z"></path>
    <path d="M11,24H9A17.0192,17.0192,0,0,1,26,7h4V9H26A15.017,15.017,0,0,0,11,24Z"></path>
    <path d="M26,2A21.8625,21.8625,0,0,0,13.9993,5.5757,21.856,21.856,0,0,0,2,2V4A19.8735,19.8735,0,0,1,12.2622,6.835,22.1514,22.1514,0,0,0,9.9475,8.979,17.001,17.001,0,0,0,2,7V9a15.0142,15.0142,0,0,1,6.6191,1.5366,22.0768,22.0768,0,0,0-1.7016,2.5371A12.0319,12.0319,0,0,0,2,12v2a9.8838,9.8838,0,0,1,4.0007.8506A21.861,21.861,0,0,0,4,24H6A20.0226,20.0226,0,0,1,26,4h4V2Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default SoilTemperatureGlobal24;
