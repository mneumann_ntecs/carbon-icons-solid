import { CarbonIconComponent } from "../types";
export const Fragile24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="Fragile24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M23,12V6H21v6a5,5,0,0,1-10,0V4h5.5859L14.293,6.293a.9994.9994,0,0,0,0,1.414L15.5859,9,13.293,11.293l1.414,1.414,3-3a.9994.9994,0,0,0,0-1.414L16.4141,7,19.707,3.707A1,1,0,0,0,19,2H10A1,1,0,0,0,9,3v9a7.0053,7.0053,0,0,0,6,6.92V28H10v2H22V28H17V18.92A7.0053,7.0053,0,0,0,23,12Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default Fragile24;
