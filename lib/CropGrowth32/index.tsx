import { CarbonIconComponent } from "../types";
export const CropGrowth32: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="CropGrowth32"
    fill={props.fill || "currentColor"}
    width={props.width || "32"}
    height={props.height || "32"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M26,13a4.0045,4.0045,0,0,0,4-4V6H27a3.979,3.979,0,0,0-2.7468,1.1064A6.0041,6.0041,0,0,0,19,4H16V7a6.0066,6.0066,0,0,0,6,6h1V26H11V21h1a4.0045,4.0045,0,0,0,4-4V14H13a3.979,3.979,0,0,0-2.7468,1.1064A6.0041,6.0041,0,0,0,5,12H2v3a6.0066,6.0066,0,0,0,6,6H9v5H2v2H30V26H25V13Zm-1-3a2.002,2.002,0,0,1,2-2h1V9a2.002,2.002,0,0,1-2,2H25ZM11,18a2.002,2.002,0,0,1,2-2h1v1a2.002,2.002,0,0,1-2,2H11ZM9,19H8a4.0045,4.0045,0,0,1-4-4V14H5a4.0045,4.0045,0,0,1,4,4Zm14-8H22a4.0045,4.0045,0,0,1-4-4V6h1a4.0045,4.0045,0,0,1,4,4Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default CropGrowth32;
