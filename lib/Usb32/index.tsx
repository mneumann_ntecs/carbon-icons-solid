import { CarbonIconComponent } from "../types";
export const Usb32: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="Usb32"
    fill={props.fill || "currentColor"}
    width={props.width || "32"}
    height={props.height || "32"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M24,15V6a2,2,0,0,0-2-2H10A2,2,0,0,0,8,6v9a2,2,0,0,0-2,2V28H8V17H24V28h2V17A2,2,0,0,0,24,15ZM10,6H22v9H10Z"></path>
    <path d="M12 10H15V12H12zM17 10H20V12H17z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default Usb32;
