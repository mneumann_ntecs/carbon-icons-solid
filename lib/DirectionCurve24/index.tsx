import { CarbonIconComponent } from "../types";
export const DirectionCurve24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="DirectionCurve24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M17,4l-7,7,1.4141,1.4141L16,7.8281v6.7686a3.01,3.01,0,0,1-.6572,1.874l-2.2471,2.8086A5.0206,5.0206,0,0,0,12,22.4033V28h2V22.4033a3.01,3.01,0,0,1,.6572-1.874l2.2471-2.8086A5.0206,5.0206,0,0,0,18,14.5967V7.8281l4.5859,4.586L24,11Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default DirectionCurve24;
