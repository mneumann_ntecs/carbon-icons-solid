import { CarbonIconComponent } from "../types";
export const QComposerEdit20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="QComposerEdit20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M25.82,10H30V8H25.82a3,3,0,0,0-5.64,0H13V5H5V8H2v2H5v3h8V10h7.18A3,3,0,0,0,22,11.82v7.32A4,4,0,0,0,19.14,22H2v2H19.14a4,4,0,0,0,7.72,0H30V22H26.86A4,4,0,0,0,24,19.14V11.82A3,3,0,0,0,25.82,10ZM11,11H7V7h4ZM25,23a2,2,0,1,1-2-2A2,2,0,0,1,25,23Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default QComposerEdit20;
