import {
  template,
  addEventListener,
  insert,
  memo,
  effect,
  setAttribute,
  style,
  delegateEvents,
} from "solid-js/web";

const _tmpl$ = template(
    `<svg data-carbon-icon="Hearing24" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" preserveAspectRatio="xMidYMid meet"><path d="M18,30V28A10.0114,10.0114,0,0,0,28,18h2A12.0134,12.0134,0,0,1,18,30Z"></path><path d="M18,26V24a6.0066,6.0066,0,0,0,6-6h2A8.0092,8.0092,0,0,1,18,26Z"></path><path d="M18 22V20a2.0023 2.0023 0 002-2h2A4.0042 4.0042 0 0118 22zM10 2a9.01 9.01 0 00-9 9H3a7 7 0 0114 0 7.09 7.09 0 01-3.501 6.1348L13 17.4229v3.0732a2.9354 2.9354 0 01-.9009 2.1514 4.1824 4.1824 0 01-4.6318 1.03A4.0918 4.0918 0 015 20H3a6.1156 6.1156 0 003.6694 5.5117 5.7822 5.7822 0 002.3145.4863A6.5854 6.5854 0 0013.4624 24.11 4.94 4.94 0 0015 20.4961V18.5537A9.1077 9.1077 0 0019 11 9.01 9.01 0 0010 2z"></path><path d="M9.28,8.0825A3.0061,3.0061,0,0,1,13,11h2a4.9786,4.9786,0,0,0-1.8843-3.9111A5.0414,5.0414,0,0,0,8.835,6.1323,4.95,4.95,0,0,0,5.1323,9.835,5.0318,5.0318,0,0,0,7.436,15.2935,3.0777,3.0777,0,0,1,9,17.9229V20h2V17.9229a5.0608,5.0608,0,0,0-2.5371-4.3458A3.0016,3.0016,0,0,1,9.28,8.0825Z"></path></svg>`,
    10
  ),
  _tmpl$2 = template(`<title></title>`, 2);

const Hearing24 = (props) =>
  (() => {
    const _el$ = _tmpl$.cloneNode(true),
      _el$2 = _el$.firstChild,
      _el$3 = _el$2.nextSibling,
      _el$4 = _el$3.nextSibling;
    _el$4.nextSibling;

    addEventListener(_el$, "keydown", props.onKeyDown, true);

    addEventListener(_el$, "keyup", props.onKeyUp, true);

    addEventListener(_el$, "mouseleave", props.onMouseLeave);

    addEventListener(_el$, "mouseenter", props.onMouseEnter);

    addEventListener(_el$, "mouseover", props.onMouseOver, true);

    addEventListener(_el$, "click", props.onClick, true);

    insert(
      _el$,
      (() => {
        const _c$ = memo(() => props.children, true);

        return () =>
          _c$() ||
          (props.title &&
            (() => {
              const _el$6 = _tmpl$2.cloneNode(true);

              insert(_el$6, () => props.title);

              return _el$6;
            })());
      })(),
      null
    );

    effect(
      (_p$) => {
        const _v$ = props.fill || "currentColor",
          _v$2 = props.width || "24",
          _v$3 = props.height || "24",
          _v$4 = props["aria-label"],
          _v$5 = props["aria-labelledby"],
          _v$6 = props["aria-label"] || props["aria-labelledby"] || props.title,
          _v$7 =
            props["aria-label"] || props["aria-labelledby"] || props.title
              ? "img"
              : undefined,
          _v$8 = props.tabindex === "0" ? true : props.focusable,
          _v$9 = props.tabindex,
          _v$10 = props.id,
          _v$11 = props.class,
          _v$12 = props.title,
          _v$13 = props.style,
          _v$14 = props.stroke;

        _v$ !== _p$._v$ && setAttribute(_el$, "fill", (_p$._v$ = _v$));
        _v$2 !== _p$._v$2 && setAttribute(_el$, "width", (_p$._v$2 = _v$2));
        _v$3 !== _p$._v$3 && setAttribute(_el$, "height", (_p$._v$3 = _v$3));
        _v$4 !== _p$._v$4 &&
          setAttribute(_el$, "aria-label", (_p$._v$4 = _v$4));
        _v$5 !== _p$._v$5 &&
          setAttribute(_el$, "aria-labelledby", (_p$._v$5 = _v$5));
        _v$6 !== _p$._v$6 &&
          setAttribute(_el$, "aria-hidden", (_p$._v$6 = _v$6));
        _v$7 !== _p$._v$7 && setAttribute(_el$, "role", (_p$._v$7 = _v$7));
        _v$8 !== _p$._v$8 && setAttribute(_el$, "focusable", (_p$._v$8 = _v$8));
        _v$9 !== _p$._v$9 && setAttribute(_el$, "tabindex", (_p$._v$9 = _v$9));
        _v$10 !== _p$._v$10 && setAttribute(_el$, "id", (_p$._v$10 = _v$10));
        _v$11 !== _p$._v$11 && setAttribute(_el$, "class", (_p$._v$11 = _v$11));
        _v$12 !== _p$._v$12 && setAttribute(_el$, "title", (_p$._v$12 = _v$12));
        _p$._v$13 = style(_el$, _v$13, _p$._v$13);
        _v$14 !== _p$._v$14 &&
          setAttribute(_el$, "stroke", (_p$._v$14 = _v$14));
        return _p$;
      },
      {
        _v$: undefined,
        _v$2: undefined,
        _v$3: undefined,
        _v$4: undefined,
        _v$5: undefined,
        _v$6: undefined,
        _v$7: undefined,
        _v$8: undefined,
        _v$9: undefined,
        _v$10: undefined,
        _v$11: undefined,
        _v$12: undefined,
        _v$13: undefined,
        _v$14: undefined,
      }
    );

    return _el$;
  })();

delegateEvents(["click", "mouseover", "keyup", "keydown"]);

export default Hearing24;
export { Hearing24 };
