import { CarbonIconComponent } from "../types";
export const Insert16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="Insert16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M2 9h9V2H2zM4 4H9V7H4zM2 19h9V12H2zm2-5H9v3H4zM2 29h9V22H2zm2-5H9v3H4zM27 9H18l3.41-3.59L20 4l-6 6 6 6 1.41-1.41L18 11h9a1 1 0 011 1V24a1 1 0 01-1 1H15v2H27a3 3 0 003-3V12A3 3 0 0027 9z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default Insert16;
