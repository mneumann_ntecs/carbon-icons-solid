import { CarbonIconComponent } from "../types";
export const Doc20: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="Doc20"
    fill={props.fill || "currentColor"}
    width={props.width || "20"}
    height={props.height || "20"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M30 23H24a2.0023 2.0023 0 01-2-2V11a2.002 2.002 0 012-2h6v2H24V21h6zM18 23H14a2.0023 2.0023 0 01-2-2V11a2.002 2.002 0 012-2h4a2.002 2.002 0 012 2V21A2.0023 2.0023 0 0118 23zM14 11V21h4V11zM6 23H2V9H6a4.0045 4.0045 0 014 4v6A4.0045 4.0045 0 016 23zM4 21H6a2.002 2.002 0 002-2V13a2.002 2.002 0 00-2-2H4z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default Doc20;
