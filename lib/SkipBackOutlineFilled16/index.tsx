import { CarbonIconComponent } from "../types";
export const SkipBackOutlineFilled16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="SkipBackOutlineFilled16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M16,2A14,14,0,1,0,30,16,14,14,0,0,0,16,2ZM10,22H8V10h2Zm14-1a1,1,0,0,1-1.4858.874l-9-5a1,1,0,0,1,0-1.748l9-5A1,1,0,0,1,24,11Z"></path>
    <path d="M22 19.301L22 12.699 16.059 16 22 19.301z"></path>
    <path
      fill="none"
      d="M23,22a1.0037,1.0037,0,0,1-.4858-.1257l-9-5a1,1,0,0,1,0-1.7486l9-5A1,1,0,0,1,24,11V21a1,1,0,0,1-1,1Zm-6.9409-6L22,19.3005V12.7Z"
    ></path>
    <path fill="none" d="M8 10H10V22H8z" transform="rotate(-180 9 16)"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default SkipBackOutlineFilled16;
