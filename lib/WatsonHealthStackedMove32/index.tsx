import { CarbonIconComponent } from "../types";
export const WatsonHealthStackedMove32: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="WatsonHealthStackedMove32"
    fill={props.fill || "currentColor"}
    width={props.width || "32"}
    height={props.height || "32"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M15,28H4a2,2,0,0,1-2-2V11A2,2,0,0,1,4,9H15a2,2,0,0,1,2,2V26A2,2,0,0,1,15,28ZM4,11V26H15V11Z"></path>
    <path d="M26,6,24.59,7.41,27,10H22V6a2,2,0,0,0-2-2H10V6H20V20h2V12h5l-2.41,2.59L26,16l5-5Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default WatsonHealthStackedMove32;
