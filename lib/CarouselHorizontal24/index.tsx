import { CarbonIconComponent } from "../types";
export const CarouselHorizontal24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="CarouselHorizontal24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M22 26H10a2 2 0 01-2-2V8a2 2 0 012-2H22a2 2 0 012 2V24A2 2 0 0122 26zM10 8V24H22V8zM4 24H0V22H4V10H0V8H4a2 2 0 012 2V22A2 2 0 014 24zM32 24H28a2 2 0 01-2-2V10a2 2 0 012-2h4v2H28V22h4z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default CarouselHorizontal24;
