import { CarbonIconComponent } from "../types";
export const WatsonHealthWindowBase32: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="WatsonHealthWindowBase32"
    fill={props.fill || "currentColor"}
    width={props.width || "32"}
    height={props.height || "32"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M15 2H17V5H15zM25 13H28V15H25zM15 23H17V26H15zM4 13H7V15H4z"></path>
    <path
      d="M7.55 5.03H9.55V8.030000000000001H7.55z"
      transform="rotate(-45 8.56 6.544)"
    ></path>
    <path
      d="M21.96 5.54H24.96V7.54H21.96z"
      transform="rotate(-45 23.469 6.539)"
    ></path>
    <path
      d="M22.46 19.94H24.46V22.94H22.46z"
      transform="rotate(-45 23.462 21.442)"
    ></path>
    <path
      d="M7.04 20.45H10.04V22.45H7.04z"
      transform="rotate(-45 8.554 21.447)"
    ></path>
    <path d="M4 28H28V30H4zM16 20a6 6 0 10-6-6A6 6 0 0016 20zm0-10v8a4 4 0 010-8z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default WatsonHealthWindowBase32;
