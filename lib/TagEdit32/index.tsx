import { CarbonIconComponent } from "../types";
export const TagEdit32: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="TagEdit32"
    fill={props.fill || "currentColor"}
    width={props.width || "32"}
    height={props.height || "32"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M29.707 7.293l-3-3a.9994.9994 0 00-1.414 0L16 13.5859V18h4.4141L29.707 8.707A.9994.9994 0 0029.707 7.293zM19.5859 16H18V14.4141l5-5L24.5859 11zM26 9.5859L24.4141 8 26 6.4141 27.5859 8zM10 14a4 4 0 114-4A4.0045 4.0045 0 0110 14zm0-6a2 2 0 102 2A2.002 2.002 0 0010 8z"></path>
    <path d="M29.4155,16.5859,27.0364,14.207l-1.4141,1.4141L28,18,18,28,4,14V4H14l4.3774,4.376,1.4141-1.4141L15.4138,2.584A2.0023,2.0023,0,0,0,14,2H4A2.0034,2.0034,0,0,0,2,4V14a2.0025,2.0025,0,0,0,.5842,1.4136l14,14a2.0019,2.0019,0,0,0,2.831,0l10-9.9995A2,2,0,0,0,29.4155,16.5859Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default TagEdit32;
