import { CarbonIconComponent } from "../types";
export const InsertPage16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="InsertPage16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M26 30H24V20H12V30H10V20a2.0021 2.0021 0 012-2H24a2.0021 2.0021 0 012 2zM5.17 16L2 19.17 3.411 20.589 8 16 3.42 11.42 2 12.83 5.17 16zM24 14H12a2.0021 2.0021 0 01-2-2V2h2V12H24V2h2V12A2.0021 2.0021 0 0124 14z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default InsertPage16;
