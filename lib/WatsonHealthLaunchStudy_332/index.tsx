import { CarbonIconComponent } from "../types";
export const WatsonHealthLaunchStudy_332: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="WatsonHealthLaunchStudy_332"
    fill={props.fill || "currentColor"}
    width={props.width || "32"}
    height={props.height || "32"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <circle cx="23" cy="25" r="1"></circle>
    <path d="M26 22v6H6V22H26m0-2H6a2 2 0 00-2 2v6a2 2 0 002 2H26a2 2 0 002-2V22a2 2 0 00-2-2zM20.59 4.59L17 8.17 17 0 15 0 15 8.17 11.41 4.59 10 6 16 12 22 6 20.59 4.59z"></path>
    <path d="M20.59 10.59L16 15.16 11.41 10.59 10 12 16 18 22 12 20.59 10.59zM8 24H20V26H8z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default WatsonHealthLaunchStudy_332;
