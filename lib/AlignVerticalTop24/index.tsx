import { CarbonIconComponent } from "../types";
export const AlignVerticalTop24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="AlignVerticalTop24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M24 20H20a2.0023 2.0023 0 01-2-2V11a2.0023 2.0023 0 012-2h4a2.0023 2.0023 0 012 2v7A2.0023 2.0023 0 0124 20zm-4-9v7h4.0012L24 11zM12 28H8a2.0023 2.0023 0 01-2-2V11A2.0023 2.0023 0 018 9h4a2.0023 2.0023 0 012 2V26A2.0023 2.0023 0 0112 28zM8 11V26h4.0012L12 11zM2 4H30V6H2z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default AlignVerticalTop24;
