import { CarbonIconComponent } from "../types";
export const CubeView24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="CubeView24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <circle cx="16" cy="16" r="2"></circle>
    <path d="M23.7769,15.4785A8.64,8.64,0,0,0,16,10a8.64,8.64,0,0,0-7.7769,5.4785L8,16l.2231.5215A8.64,8.64,0,0,0,16,22a8.64,8.64,0,0,0,7.7769-5.4785L24,16ZM16,20a4,4,0,1,1,4-4A4.0045,4.0045,0,0,1,16,20Z"></path>
    <path d="M28.5039,8.1362l-12-7a1,1,0,0,0-1.0078,0l-12,7A1,1,0,0,0,3,9V23a1,1,0,0,0,.4961.8638l12,7a1,1,0,0,0,1.0078,0l12-7A1,1,0,0,0,29,23V9A1,1,0,0,0,28.5039,8.1362ZM27,22.4258,16,28.8423,5,22.4258V9.5742L16,3.1577,27,9.5742Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default CubeView24;
