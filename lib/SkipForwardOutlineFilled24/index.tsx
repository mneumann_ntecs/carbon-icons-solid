import { CarbonIconComponent } from "../types";
export const SkipForwardOutlineFilled24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="SkipForwardOutlineFilled24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M10 19.301L15.941 16 10 12.699 10 19.301z"></path>
    <path d="M16,2A14,14,0,1,0,30,16,14,14,0,0,0,16,2Zm2.4858,14.874-9,5A1,1,0,0,1,8,21V11a1,1,0,0,1,1.4858-.874l9,5a1,1,0,0,1,0,1.748ZM24,22H22V10h2Z"></path>
    <path
      fill="none"
      d="M22 10H24V22H22zM8.4927 21.8618A1 1 0 018 21V11a1 1 0 011.4858-.8743l9 5a1 1 0 010 1.7486l-9 5a1.0009 1.0009 0 01-.9931-.0125zM10 12.7v6.601L15.9409 16z"
    ></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default SkipForwardOutlineFilled24;
