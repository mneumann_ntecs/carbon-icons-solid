import { CarbonIconComponent } from "../types";
export const Undo24: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="Undo24"
    fill={props.fill || "currentColor"}
    width={props.width || "24"}
    height={props.height || "24"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 24 24"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M17.2,11.2c1.8,0,3.2,1.7,3.2,3.3c0,1.5-1.5,3.2-3.2,3.2H14v1.5h3.2c2.2,0,4.8-2.3,4.8-4.8s-2-4.8-4.8-4.8	H4.8l3-3L6.8,5.7L2,10.5l4.8,4.8l1.1-1.1l-3-3H17.2z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default Undo24;
