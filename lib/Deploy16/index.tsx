import { CarbonIconComponent } from "../types";
export const Deploy16: CarbonIconComponent = (props) => (
  <svg
    data-carbon-icon="Deploy16"
    fill={props.fill || "currentColor"}
    width={props.width || "16"}
    height={props.height || "16"}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
    aria-label={props["aria-label"]}
    aria-labelledby={props["aria-labelledby"]}
    aria-hidden={props["aria-label"] || props["aria-labelledby"] || props.title}
    role={
      props["aria-label"] || props["aria-labelledby"] || props.title
        ? "img"
        : undefined
    }
    focusable={props.tabindex === "0" ? true : props.focusable}
    tabindex={props.tabindex}
    id={props.id}
    class={props.class}
    title={props.title}
    style={props.style}
    stroke={props.stroke}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onKeyUp={props.onKeyUp}
    onKeyDown={props.onKeyDown}
  >
    <path d="M23,2,17,8l1.4146,1.4023L22,5.8184V28H6V12H4V28a2.0025,2.0025,0,0,0,2,2H22a2.0025,2.0025,0,0,0,2-2V5.8154l3.5859,3.5869L29,8Z"></path>
    <path d="M16,24H12a2.0023,2.0023,0,0,1-2-2V18a2.0023,2.0023,0,0,1,2-2h4a2.0023,2.0023,0,0,1,2,2v4A2.0023,2.0023,0,0,1,16,24Zm-4-6v4h4V18Z"></path>
    {props.children || (props.title && <title>{props.title}</title>)}
  </svg>
);
export default Deploy16;
