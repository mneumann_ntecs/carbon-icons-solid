# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

<!-- ## Unreleased -->

## [0.0.2](https://gitlab.com/mneumann_ntecs/carbon-icons-solid/-/tree/v0.0.2) - 2020-02-08

**Features**

* Updated to `solid-js` version 0.24.0.

## [0.0.1](https://gitlab.com/mneumann_ntecs/carbon-icons-solid/-/tree/v0.0.1) - 2020-12-30

**Features**

- Initial publication
